import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gkt/domain/repository/main_repository.dart';
import 'package:gkt/domain/repository/maps_repository.dart';
import 'package:gkt/injector.dart';
import 'package:gkt/presentation/blocs/auth/auth_cubit.dart';
import 'package:gkt/presentation/config/app_route.dart';
import 'package:gkt/presentation/config/theme_colors.dart';
import 'package:gkt/presentation/pages/dashboard/dashboard_page.dart';
import 'package:gkt/presentation/pages/home/home_guest_page.dart';
import 'package:gkt/presentation/pages/login/login_page.dart';

import 'domain/repository/auth_repository.dart';
import 'domain/repository/user_repository.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  NavigatorState get _navigator =>
      injector<GlobalKey<NavigatorState>>().currentState!;

  @override
  Widget build(BuildContext context) {
    final themeData = Theme.of(context);
    final textTheme = themeData.textTheme;

    final theme = ThemeData(
      scaffoldBackgroundColor: Colors.white,
      primarySwatch: ThemeColors.gold,
      textTheme: textTheme.apply(
        fontFamily: 'Poppins',
        bodyColor: ThemeColors.blackPrimary,
        displayColor: ThemeColors.blackPrimary,
      ),
      appBarTheme: themeData.appBarTheme.copyWith(
        elevation: 0.0,
        color: Colors.white,
        actionsIconTheme: themeData.primaryIconTheme.copyWith(
          color: ThemeColors.greyPrimary,
        ),
        iconTheme: themeData.primaryIconTheme.copyWith(
          color: ThemeColors.blackPrimary,
        ),
        titleTextStyle: themeData.textTheme.headline6?.copyWith(
          color: ThemeColors.blackPrimary,
          fontFamily: 'Poppins',
          fontWeight: FontWeight.bold,
          fontSize: 16,
        ),
        systemOverlayStyle: SystemUiOverlayStyle.dark,
      ),
    );

    final app = MaterialApp(
      title: 'GKT',
      debugShowCheckedModeBanner: false,
      navigatorKey: injector(),
      theme: theme,
      onGenerateRoute: AppRoute.onGenerateRoute,
      builder: _builder,
    );

    return MultiRepositoryProvider(
        providers: [
          RepositoryProvider<AuthRepository>.value(value: injector()),
          RepositoryProvider<UserRepository>.value(value: injector()),
          RepositoryProvider<MainRepository>.value(value: injector()),
          RepositoryProvider<MapsRepository>.value(value: injector()),
        ],
        child: MultiBlocProvider(
          providers: [
            BlocProvider(
              create: (context) {
                return AuthCubit(
                  authRepository: injector(),
                  userRepository: injector(),
                );
              },
            ),
          ],
          child: app,
        ));
  }

  Widget _builder(context, child) {
    return MediaQuery(
        data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
        child: MultiBlocListener(
          listeners: [
            BlocListener<AuthCubit, AuthState>(
              listener: (context, state) {
                switch (state.status) {
                  case AuthenticationStatus.authenticated:
                    _navigator.pushNamedAndRemoveUntil(
                        DashboardPage.routeName, (route) => false);
                    break;
                  case AuthenticationStatus.guest:
                    _navigator.pushNamedAndRemoveUntil(
                        HomeGuestPage.routeName, (route) => false);
                    break;
                  case AuthenticationStatus.unauthenticated:
                    _navigator.pushNamedAndRemoveUntil(
                        LoginPage.routeName, (route) => false);
                    break;
                  default:
                }
              },
            ),
          ],
          child: child,
        ));
  }
}

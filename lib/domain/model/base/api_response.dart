import 'package:freezed_annotation/freezed_annotation.dart';

part 'api_response.freezed.dart';
part 'api_response.g.dart';

@Freezed(genericArgumentFactories: true)
class ApiResponse<T> with _$ApiResponse<T> {
  const factory ApiResponse({
    required int statusCode,
    String? message,
    @Default(false) bool error,
    T? data,
  }) = _ApiResponse<T>;

  factory ApiResponse.fromJson(
          Map<String, dynamic> json, T Function(Object?) fromJsonT) =>
      _$ApiResponseFromJson(json, fromJsonT);
}

@Freezed(genericArgumentFactories: true)
class ApiResponseList<T> with _$ApiResponseList<T> {
  const factory ApiResponseList({
    required int statusCode,
    String? message,
    @Default(false) bool error,
    List<T>? data,
  }) = _ApiResponseList<T>;

  factory ApiResponseList.fromJson(
          Map<String, dynamic> json, T Function(Object?) fromJsonT) =>
      _$ApiResponseListFromJson(json, fromJsonT);
}

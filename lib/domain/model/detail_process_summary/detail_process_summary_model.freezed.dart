// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'detail_process_summary_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

DetailProcessSummaryModel _$DetailProcessSummaryModelFromJson(
    Map<String, dynamic> json) {
  return _DetailProcessSummaryModel.fromJson(json);
}

/// @nodoc
mixin _$DetailProcessSummaryModel {
  int? get id => throw _privateConstructorUsedError;
  @JsonKey(name: 'nama_proses')
  String? get namaProses => throw _privateConstructorUsedError;
  String? get fase => throw _privateConstructorUsedError;
  int? get persentase => throw _privateConstructorUsedError;
  @JsonKey(name: 'created_at')
  String? get createdAt => throw _privateConstructorUsedError;
  @JsonKey(name: 'updated_at')
  String? get updatedAt => throw _privateConstructorUsedError;
  List<DetailProcessSummaryItemModel>? get detail =>
      throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $DetailProcessSummaryModelCopyWith<DetailProcessSummaryModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DetailProcessSummaryModelCopyWith<$Res> {
  factory $DetailProcessSummaryModelCopyWith(DetailProcessSummaryModel value,
          $Res Function(DetailProcessSummaryModel) then) =
      _$DetailProcessSummaryModelCopyWithImpl<$Res, DetailProcessSummaryModel>;
  @useResult
  $Res call(
      {int? id,
      @JsonKey(name: 'nama_proses') String? namaProses,
      String? fase,
      int? persentase,
      @JsonKey(name: 'created_at') String? createdAt,
      @JsonKey(name: 'updated_at') String? updatedAt,
      List<DetailProcessSummaryItemModel>? detail});
}

/// @nodoc
class _$DetailProcessSummaryModelCopyWithImpl<$Res,
        $Val extends DetailProcessSummaryModel>
    implements $DetailProcessSummaryModelCopyWith<$Res> {
  _$DetailProcessSummaryModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? namaProses = freezed,
    Object? fase = freezed,
    Object? persentase = freezed,
    Object? createdAt = freezed,
    Object? updatedAt = freezed,
    Object? detail = freezed,
  }) {
    return _then(_value.copyWith(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      namaProses: freezed == namaProses
          ? _value.namaProses
          : namaProses // ignore: cast_nullable_to_non_nullable
              as String?,
      fase: freezed == fase
          ? _value.fase
          : fase // ignore: cast_nullable_to_non_nullable
              as String?,
      persentase: freezed == persentase
          ? _value.persentase
          : persentase // ignore: cast_nullable_to_non_nullable
              as int?,
      createdAt: freezed == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String?,
      updatedAt: freezed == updatedAt
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as String?,
      detail: freezed == detail
          ? _value.detail
          : detail // ignore: cast_nullable_to_non_nullable
              as List<DetailProcessSummaryItemModel>?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_DetailProcessSummaryModelCopyWith<$Res>
    implements $DetailProcessSummaryModelCopyWith<$Res> {
  factory _$$_DetailProcessSummaryModelCopyWith(
          _$_DetailProcessSummaryModel value,
          $Res Function(_$_DetailProcessSummaryModel) then) =
      __$$_DetailProcessSummaryModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int? id,
      @JsonKey(name: 'nama_proses') String? namaProses,
      String? fase,
      int? persentase,
      @JsonKey(name: 'created_at') String? createdAt,
      @JsonKey(name: 'updated_at') String? updatedAt,
      List<DetailProcessSummaryItemModel>? detail});
}

/// @nodoc
class __$$_DetailProcessSummaryModelCopyWithImpl<$Res>
    extends _$DetailProcessSummaryModelCopyWithImpl<$Res,
        _$_DetailProcessSummaryModel>
    implements _$$_DetailProcessSummaryModelCopyWith<$Res> {
  __$$_DetailProcessSummaryModelCopyWithImpl(
      _$_DetailProcessSummaryModel _value,
      $Res Function(_$_DetailProcessSummaryModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? namaProses = freezed,
    Object? fase = freezed,
    Object? persentase = freezed,
    Object? createdAt = freezed,
    Object? updatedAt = freezed,
    Object? detail = freezed,
  }) {
    return _then(_$_DetailProcessSummaryModel(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      namaProses: freezed == namaProses
          ? _value.namaProses
          : namaProses // ignore: cast_nullable_to_non_nullable
              as String?,
      fase: freezed == fase
          ? _value.fase
          : fase // ignore: cast_nullable_to_non_nullable
              as String?,
      persentase: freezed == persentase
          ? _value.persentase
          : persentase // ignore: cast_nullable_to_non_nullable
              as int?,
      createdAt: freezed == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String?,
      updatedAt: freezed == updatedAt
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as String?,
      detail: freezed == detail
          ? _value._detail
          : detail // ignore: cast_nullable_to_non_nullable
              as List<DetailProcessSummaryItemModel>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_DetailProcessSummaryModel implements _DetailProcessSummaryModel {
  _$_DetailProcessSummaryModel(
      {this.id,
      @JsonKey(name: 'nama_proses') this.namaProses,
      this.fase,
      this.persentase,
      @JsonKey(name: 'created_at') this.createdAt,
      @JsonKey(name: 'updated_at') this.updatedAt,
      final List<DetailProcessSummaryItemModel>? detail})
      : _detail = detail;

  factory _$_DetailProcessSummaryModel.fromJson(Map<String, dynamic> json) =>
      _$$_DetailProcessSummaryModelFromJson(json);

  @override
  final int? id;
  @override
  @JsonKey(name: 'nama_proses')
  final String? namaProses;
  @override
  final String? fase;
  @override
  final int? persentase;
  @override
  @JsonKey(name: 'created_at')
  final String? createdAt;
  @override
  @JsonKey(name: 'updated_at')
  final String? updatedAt;
  final List<DetailProcessSummaryItemModel>? _detail;
  @override
  List<DetailProcessSummaryItemModel>? get detail {
    final value = _detail;
    if (value == null) return null;
    if (_detail is EqualUnmodifiableListView) return _detail;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'DetailProcessSummaryModel(id: $id, namaProses: $namaProses, fase: $fase, persentase: $persentase, createdAt: $createdAt, updatedAt: $updatedAt, detail: $detail)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_DetailProcessSummaryModel &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.namaProses, namaProses) ||
                other.namaProses == namaProses) &&
            (identical(other.fase, fase) || other.fase == fase) &&
            (identical(other.persentase, persentase) ||
                other.persentase == persentase) &&
            (identical(other.createdAt, createdAt) ||
                other.createdAt == createdAt) &&
            (identical(other.updatedAt, updatedAt) ||
                other.updatedAt == updatedAt) &&
            const DeepCollectionEquality().equals(other._detail, _detail));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, namaProses, fase, persentase,
      createdAt, updatedAt, const DeepCollectionEquality().hash(_detail));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_DetailProcessSummaryModelCopyWith<_$_DetailProcessSummaryModel>
      get copyWith => __$$_DetailProcessSummaryModelCopyWithImpl<
          _$_DetailProcessSummaryModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_DetailProcessSummaryModelToJson(
      this,
    );
  }
}

abstract class _DetailProcessSummaryModel implements DetailProcessSummaryModel {
  factory _DetailProcessSummaryModel(
          {final int? id,
          @JsonKey(name: 'nama_proses') final String? namaProses,
          final String? fase,
          final int? persentase,
          @JsonKey(name: 'created_at') final String? createdAt,
          @JsonKey(name: 'updated_at') final String? updatedAt,
          final List<DetailProcessSummaryItemModel>? detail}) =
      _$_DetailProcessSummaryModel;

  factory _DetailProcessSummaryModel.fromJson(Map<String, dynamic> json) =
      _$_DetailProcessSummaryModel.fromJson;

  @override
  int? get id;
  @override
  @JsonKey(name: 'nama_proses')
  String? get namaProses;
  @override
  String? get fase;
  @override
  int? get persentase;
  @override
  @JsonKey(name: 'created_at')
  String? get createdAt;
  @override
  @JsonKey(name: 'updated_at')
  String? get updatedAt;
  @override
  List<DetailProcessSummaryItemModel>? get detail;
  @override
  @JsonKey(ignore: true)
  _$$_DetailProcessSummaryModelCopyWith<_$_DetailProcessSummaryModel>
      get copyWith => throw _privateConstructorUsedError;
}

DetailProcessSummaryItemModel _$DetailProcessSummaryItemModelFromJson(
    Map<String, dynamic> json) {
  return _DetailProcessSummaryItemModel.fromJson(json);
}

/// @nodoc
mixin _$DetailProcessSummaryItemModel {
  int? get id => throw _privateConstructorUsedError;
  String? get nama => throw _privateConstructorUsedError;
  String? get fase => throw _privateConstructorUsedError;
  int? get persentase => throw _privateConstructorUsedError;
  List<PhotoProcessSummaryModel>? get photos =>
      throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $DetailProcessSummaryItemModelCopyWith<DetailProcessSummaryItemModel>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DetailProcessSummaryItemModelCopyWith<$Res> {
  factory $DetailProcessSummaryItemModelCopyWith(
          DetailProcessSummaryItemModel value,
          $Res Function(DetailProcessSummaryItemModel) then) =
      _$DetailProcessSummaryItemModelCopyWithImpl<$Res,
          DetailProcessSummaryItemModel>;
  @useResult
  $Res call(
      {int? id,
      String? nama,
      String? fase,
      int? persentase,
      List<PhotoProcessSummaryModel>? photos});
}

/// @nodoc
class _$DetailProcessSummaryItemModelCopyWithImpl<$Res,
        $Val extends DetailProcessSummaryItemModel>
    implements $DetailProcessSummaryItemModelCopyWith<$Res> {
  _$DetailProcessSummaryItemModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? nama = freezed,
    Object? fase = freezed,
    Object? persentase = freezed,
    Object? photos = freezed,
  }) {
    return _then(_value.copyWith(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      nama: freezed == nama
          ? _value.nama
          : nama // ignore: cast_nullable_to_non_nullable
              as String?,
      fase: freezed == fase
          ? _value.fase
          : fase // ignore: cast_nullable_to_non_nullable
              as String?,
      persentase: freezed == persentase
          ? _value.persentase
          : persentase // ignore: cast_nullable_to_non_nullable
              as int?,
      photos: freezed == photos
          ? _value.photos
          : photos // ignore: cast_nullable_to_non_nullable
              as List<PhotoProcessSummaryModel>?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_DetailProcessSummaryItemModelCopyWith<$Res>
    implements $DetailProcessSummaryItemModelCopyWith<$Res> {
  factory _$$_DetailProcessSummaryItemModelCopyWith(
          _$_DetailProcessSummaryItemModel value,
          $Res Function(_$_DetailProcessSummaryItemModel) then) =
      __$$_DetailProcessSummaryItemModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int? id,
      String? nama,
      String? fase,
      int? persentase,
      List<PhotoProcessSummaryModel>? photos});
}

/// @nodoc
class __$$_DetailProcessSummaryItemModelCopyWithImpl<$Res>
    extends _$DetailProcessSummaryItemModelCopyWithImpl<$Res,
        _$_DetailProcessSummaryItemModel>
    implements _$$_DetailProcessSummaryItemModelCopyWith<$Res> {
  __$$_DetailProcessSummaryItemModelCopyWithImpl(
      _$_DetailProcessSummaryItemModel _value,
      $Res Function(_$_DetailProcessSummaryItemModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? nama = freezed,
    Object? fase = freezed,
    Object? persentase = freezed,
    Object? photos = freezed,
  }) {
    return _then(_$_DetailProcessSummaryItemModel(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      nama: freezed == nama
          ? _value.nama
          : nama // ignore: cast_nullable_to_non_nullable
              as String?,
      fase: freezed == fase
          ? _value.fase
          : fase // ignore: cast_nullable_to_non_nullable
              as String?,
      persentase: freezed == persentase
          ? _value.persentase
          : persentase // ignore: cast_nullable_to_non_nullable
              as int?,
      photos: freezed == photos
          ? _value._photos
          : photos // ignore: cast_nullable_to_non_nullable
              as List<PhotoProcessSummaryModel>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_DetailProcessSummaryItemModel
    implements _DetailProcessSummaryItemModel {
  _$_DetailProcessSummaryItemModel(
      {this.id,
      this.nama,
      this.fase,
      this.persentase,
      final List<PhotoProcessSummaryModel>? photos})
      : _photos = photos;

  factory _$_DetailProcessSummaryItemModel.fromJson(
          Map<String, dynamic> json) =>
      _$$_DetailProcessSummaryItemModelFromJson(json);

  @override
  final int? id;
  @override
  final String? nama;
  @override
  final String? fase;
  @override
  final int? persentase;
  final List<PhotoProcessSummaryModel>? _photos;
  @override
  List<PhotoProcessSummaryModel>? get photos {
    final value = _photos;
    if (value == null) return null;
    if (_photos is EqualUnmodifiableListView) return _photos;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'DetailProcessSummaryItemModel(id: $id, nama: $nama, fase: $fase, persentase: $persentase, photos: $photos)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_DetailProcessSummaryItemModel &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.nama, nama) || other.nama == nama) &&
            (identical(other.fase, fase) || other.fase == fase) &&
            (identical(other.persentase, persentase) ||
                other.persentase == persentase) &&
            const DeepCollectionEquality().equals(other._photos, _photos));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, nama, fase, persentase,
      const DeepCollectionEquality().hash(_photos));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_DetailProcessSummaryItemModelCopyWith<_$_DetailProcessSummaryItemModel>
      get copyWith => __$$_DetailProcessSummaryItemModelCopyWithImpl<
          _$_DetailProcessSummaryItemModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_DetailProcessSummaryItemModelToJson(
      this,
    );
  }
}

abstract class _DetailProcessSummaryItemModel
    implements DetailProcessSummaryItemModel {
  factory _DetailProcessSummaryItemModel(
          {final int? id,
          final String? nama,
          final String? fase,
          final int? persentase,
          final List<PhotoProcessSummaryModel>? photos}) =
      _$_DetailProcessSummaryItemModel;

  factory _DetailProcessSummaryItemModel.fromJson(Map<String, dynamic> json) =
      _$_DetailProcessSummaryItemModel.fromJson;

  @override
  int? get id;
  @override
  String? get nama;
  @override
  String? get fase;
  @override
  int? get persentase;
  @override
  List<PhotoProcessSummaryModel>? get photos;
  @override
  @JsonKey(ignore: true)
  _$$_DetailProcessSummaryItemModelCopyWith<_$_DetailProcessSummaryItemModel>
      get copyWith => throw _privateConstructorUsedError;
}

PhotoProcessSummaryModel _$PhotoProcessSummaryModelFromJson(
    Map<String, dynamic> json) {
  return _PhotoProcessSummaryModel.fromJson(json);
}

/// @nodoc
mixin _$PhotoProcessSummaryModel {
  int? get id => throw _privateConstructorUsedError;
  @JsonKey(name: 'image_url')
  String? get imageUrl => throw _privateConstructorUsedError;
  @JsonKey(name: 'created_at')
  String? get createdAt => throw _privateConstructorUsedError;
  @JsonKey(name: 'updated_at')
  String? get updatedAt => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PhotoProcessSummaryModelCopyWith<PhotoProcessSummaryModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PhotoProcessSummaryModelCopyWith<$Res> {
  factory $PhotoProcessSummaryModelCopyWith(PhotoProcessSummaryModel value,
          $Res Function(PhotoProcessSummaryModel) then) =
      _$PhotoProcessSummaryModelCopyWithImpl<$Res, PhotoProcessSummaryModel>;
  @useResult
  $Res call(
      {int? id,
      @JsonKey(name: 'image_url') String? imageUrl,
      @JsonKey(name: 'created_at') String? createdAt,
      @JsonKey(name: 'updated_at') String? updatedAt});
}

/// @nodoc
class _$PhotoProcessSummaryModelCopyWithImpl<$Res,
        $Val extends PhotoProcessSummaryModel>
    implements $PhotoProcessSummaryModelCopyWith<$Res> {
  _$PhotoProcessSummaryModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? imageUrl = freezed,
    Object? createdAt = freezed,
    Object? updatedAt = freezed,
  }) {
    return _then(_value.copyWith(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      imageUrl: freezed == imageUrl
          ? _value.imageUrl
          : imageUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      createdAt: freezed == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String?,
      updatedAt: freezed == updatedAt
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_PhotoProcessSummaryModelCopyWith<$Res>
    implements $PhotoProcessSummaryModelCopyWith<$Res> {
  factory _$$_PhotoProcessSummaryModelCopyWith(
          _$_PhotoProcessSummaryModel value,
          $Res Function(_$_PhotoProcessSummaryModel) then) =
      __$$_PhotoProcessSummaryModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int? id,
      @JsonKey(name: 'image_url') String? imageUrl,
      @JsonKey(name: 'created_at') String? createdAt,
      @JsonKey(name: 'updated_at') String? updatedAt});
}

/// @nodoc
class __$$_PhotoProcessSummaryModelCopyWithImpl<$Res>
    extends _$PhotoProcessSummaryModelCopyWithImpl<$Res,
        _$_PhotoProcessSummaryModel>
    implements _$$_PhotoProcessSummaryModelCopyWith<$Res> {
  __$$_PhotoProcessSummaryModelCopyWithImpl(_$_PhotoProcessSummaryModel _value,
      $Res Function(_$_PhotoProcessSummaryModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? imageUrl = freezed,
    Object? createdAt = freezed,
    Object? updatedAt = freezed,
  }) {
    return _then(_$_PhotoProcessSummaryModel(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      imageUrl: freezed == imageUrl
          ? _value.imageUrl
          : imageUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      createdAt: freezed == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String?,
      updatedAt: freezed == updatedAt
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_PhotoProcessSummaryModel implements _PhotoProcessSummaryModel {
  _$_PhotoProcessSummaryModel(
      {this.id,
      @JsonKey(name: 'image_url') this.imageUrl,
      @JsonKey(name: 'created_at') this.createdAt,
      @JsonKey(name: 'updated_at') this.updatedAt});

  factory _$_PhotoProcessSummaryModel.fromJson(Map<String, dynamic> json) =>
      _$$_PhotoProcessSummaryModelFromJson(json);

  @override
  final int? id;
  @override
  @JsonKey(name: 'image_url')
  final String? imageUrl;
  @override
  @JsonKey(name: 'created_at')
  final String? createdAt;
  @override
  @JsonKey(name: 'updated_at')
  final String? updatedAt;

  @override
  String toString() {
    return 'PhotoProcessSummaryModel(id: $id, imageUrl: $imageUrl, createdAt: $createdAt, updatedAt: $updatedAt)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PhotoProcessSummaryModel &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.imageUrl, imageUrl) ||
                other.imageUrl == imageUrl) &&
            (identical(other.createdAt, createdAt) ||
                other.createdAt == createdAt) &&
            (identical(other.updatedAt, updatedAt) ||
                other.updatedAt == updatedAt));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, id, imageUrl, createdAt, updatedAt);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_PhotoProcessSummaryModelCopyWith<_$_PhotoProcessSummaryModel>
      get copyWith => __$$_PhotoProcessSummaryModelCopyWithImpl<
          _$_PhotoProcessSummaryModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_PhotoProcessSummaryModelToJson(
      this,
    );
  }
}

abstract class _PhotoProcessSummaryModel implements PhotoProcessSummaryModel {
  factory _PhotoProcessSummaryModel(
          {final int? id,
          @JsonKey(name: 'image_url') final String? imageUrl,
          @JsonKey(name: 'created_at') final String? createdAt,
          @JsonKey(name: 'updated_at') final String? updatedAt}) =
      _$_PhotoProcessSummaryModel;

  factory _PhotoProcessSummaryModel.fromJson(Map<String, dynamic> json) =
      _$_PhotoProcessSummaryModel.fromJson;

  @override
  int? get id;
  @override
  @JsonKey(name: 'image_url')
  String? get imageUrl;
  @override
  @JsonKey(name: 'created_at')
  String? get createdAt;
  @override
  @JsonKey(name: 'updated_at')
  String? get updatedAt;
  @override
  @JsonKey(ignore: true)
  _$$_PhotoProcessSummaryModelCopyWith<_$_PhotoProcessSummaryModel>
      get copyWith => throw _privateConstructorUsedError;
}

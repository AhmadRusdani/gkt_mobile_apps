// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'detail_process_summary_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_DetailProcessSummaryModel _$$_DetailProcessSummaryModelFromJson(
        Map<String, dynamic> json) =>
    _$_DetailProcessSummaryModel(
      id: json['id'] as int?,
      namaProses: json['nama_proses'] as String?,
      fase: json['fase'] as String?,
      persentase: json['persentase'] as int?,
      createdAt: json['created_at'] as String?,
      updatedAt: json['updated_at'] as String?,
      detail: (json['detail'] as List<dynamic>?)
          ?.map((e) =>
              DetailProcessSummaryItemModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_DetailProcessSummaryModelToJson(
        _$_DetailProcessSummaryModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'nama_proses': instance.namaProses,
      'fase': instance.fase,
      'persentase': instance.persentase,
      'created_at': instance.createdAt,
      'updated_at': instance.updatedAt,
      'detail': instance.detail,
    };

_$_DetailProcessSummaryItemModel _$$_DetailProcessSummaryItemModelFromJson(
        Map<String, dynamic> json) =>
    _$_DetailProcessSummaryItemModel(
      id: json['id'] as int?,
      nama: json['nama'] as String?,
      fase: json['fase'] as String?,
      persentase: json['persentase'] as int?,
      photos: (json['photos'] as List<dynamic>?)
          ?.map((e) =>
              PhotoProcessSummaryModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_DetailProcessSummaryItemModelToJson(
        _$_DetailProcessSummaryItemModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'nama': instance.nama,
      'fase': instance.fase,
      'persentase': instance.persentase,
      'photos': instance.photos,
    };

_$_PhotoProcessSummaryModel _$$_PhotoProcessSummaryModelFromJson(
        Map<String, dynamic> json) =>
    _$_PhotoProcessSummaryModel(
      id: json['id'] as int?,
      imageUrl: json['image_url'] as String?,
      createdAt: json['created_at'] as String?,
      updatedAt: json['updated_at'] as String?,
    );

Map<String, dynamic> _$$_PhotoProcessSummaryModelToJson(
        _$_PhotoProcessSummaryModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'image_url': instance.imageUrl,
      'created_at': instance.createdAt,
      'updated_at': instance.updatedAt,
    };

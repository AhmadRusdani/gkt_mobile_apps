import 'package:freezed_annotation/freezed_annotation.dart';

part 'detail_process_summary_model.freezed.dart';
part 'detail_process_summary_model.g.dart';

@freezed
class DetailProcessSummaryModel with _$DetailProcessSummaryModel {
  factory DetailProcessSummaryModel({
    int? id,
    @JsonKey(name: 'nama_proses') String? namaProses,
    String? fase,
    int? persentase,
    @JsonKey(name: 'created_at') String? createdAt,
    @JsonKey(name: 'updated_at') String? updatedAt,
    List<DetailProcessSummaryItemModel>? detail,
  }) = _DetailProcessSummaryModel;

  factory DetailProcessSummaryModel.fromJson(Map<String, dynamic> json) =>
      _$DetailProcessSummaryModelFromJson(json);
}

@freezed
class DetailProcessSummaryItemModel with _$DetailProcessSummaryItemModel {
  factory DetailProcessSummaryItemModel({
    int? id,
    String? nama,
    String? fase,
    int? persentase,
    List<PhotoProcessSummaryModel>? photos,
  }) = _DetailProcessSummaryItemModel;

  factory DetailProcessSummaryItemModel.fromJson(Map<String, dynamic> json) =>
      _$DetailProcessSummaryItemModelFromJson(json);
}

@freezed
class PhotoProcessSummaryModel with _$PhotoProcessSummaryModel {
  factory PhotoProcessSummaryModel({
    int? id,
    @JsonKey(name: 'image_url') String? imageUrl,
    @JsonKey(name: 'created_at') String? createdAt,
    @JsonKey(name: 'updated_at') String? updatedAt,
  }) = _PhotoProcessSummaryModel;

  factory PhotoProcessSummaryModel.fromJson(Map<String, dynamic> json) =>
      _$PhotoProcessSummaryModelFromJson(json);
}

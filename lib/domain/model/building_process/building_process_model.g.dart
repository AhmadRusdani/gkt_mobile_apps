// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'building_process_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_BuildingProcessModel _$$_BuildingProcessModelFromJson(
        Map<String, dynamic> json) =>
    _$_BuildingProcessModel(
      namaPembangunan: json['nama_pembangunan'] as String?,
      totalPersentase: json['total_persentase'] as int?,
    );

Map<String, dynamic> _$$_BuildingProcessModelToJson(
        _$_BuildingProcessModel instance) =>
    <String, dynamic>{
      'nama_pembangunan': instance.namaPembangunan,
      'total_persentase': instance.totalPersentase,
    };

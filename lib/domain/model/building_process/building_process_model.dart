import 'package:freezed_annotation/freezed_annotation.dart';

part 'building_process_model.freezed.dart';
part 'building_process_model.g.dart';

@freezed
class BuildingProcessModel with _$BuildingProcessModel {
  const factory BuildingProcessModel({
    @JsonKey(name: 'nama_pembangunan') String? namaPembangunan,
    @JsonKey(name: 'total_persentase') int? totalPersentase,
  }) = _BuildingProcessModel;

  factory BuildingProcessModel.fromJson(Map<String, dynamic> json) =>
      _$BuildingProcessModelFromJson(json);
}

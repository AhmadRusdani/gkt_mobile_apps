// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'building_process_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

BuildingProcessModel _$BuildingProcessModelFromJson(Map<String, dynamic> json) {
  return _BuildingProcessModel.fromJson(json);
}

/// @nodoc
mixin _$BuildingProcessModel {
  @JsonKey(name: 'nama_pembangunan')
  String? get namaPembangunan => throw _privateConstructorUsedError;
  @JsonKey(name: 'total_persentase')
  int? get totalPersentase => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $BuildingProcessModelCopyWith<BuildingProcessModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BuildingProcessModelCopyWith<$Res> {
  factory $BuildingProcessModelCopyWith(BuildingProcessModel value,
          $Res Function(BuildingProcessModel) then) =
      _$BuildingProcessModelCopyWithImpl<$Res, BuildingProcessModel>;
  @useResult
  $Res call(
      {@JsonKey(name: 'nama_pembangunan') String? namaPembangunan,
      @JsonKey(name: 'total_persentase') int? totalPersentase});
}

/// @nodoc
class _$BuildingProcessModelCopyWithImpl<$Res,
        $Val extends BuildingProcessModel>
    implements $BuildingProcessModelCopyWith<$Res> {
  _$BuildingProcessModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? namaPembangunan = freezed,
    Object? totalPersentase = freezed,
  }) {
    return _then(_value.copyWith(
      namaPembangunan: freezed == namaPembangunan
          ? _value.namaPembangunan
          : namaPembangunan // ignore: cast_nullable_to_non_nullable
              as String?,
      totalPersentase: freezed == totalPersentase
          ? _value.totalPersentase
          : totalPersentase // ignore: cast_nullable_to_non_nullable
              as int?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_BuildingProcessModelCopyWith<$Res>
    implements $BuildingProcessModelCopyWith<$Res> {
  factory _$$_BuildingProcessModelCopyWith(_$_BuildingProcessModel value,
          $Res Function(_$_BuildingProcessModel) then) =
      __$$_BuildingProcessModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: 'nama_pembangunan') String? namaPembangunan,
      @JsonKey(name: 'total_persentase') int? totalPersentase});
}

/// @nodoc
class __$$_BuildingProcessModelCopyWithImpl<$Res>
    extends _$BuildingProcessModelCopyWithImpl<$Res, _$_BuildingProcessModel>
    implements _$$_BuildingProcessModelCopyWith<$Res> {
  __$$_BuildingProcessModelCopyWithImpl(_$_BuildingProcessModel _value,
      $Res Function(_$_BuildingProcessModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? namaPembangunan = freezed,
    Object? totalPersentase = freezed,
  }) {
    return _then(_$_BuildingProcessModel(
      namaPembangunan: freezed == namaPembangunan
          ? _value.namaPembangunan
          : namaPembangunan // ignore: cast_nullable_to_non_nullable
              as String?,
      totalPersentase: freezed == totalPersentase
          ? _value.totalPersentase
          : totalPersentase // ignore: cast_nullable_to_non_nullable
              as int?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_BuildingProcessModel implements _BuildingProcessModel {
  const _$_BuildingProcessModel(
      {@JsonKey(name: 'nama_pembangunan') this.namaPembangunan,
      @JsonKey(name: 'total_persentase') this.totalPersentase});

  factory _$_BuildingProcessModel.fromJson(Map<String, dynamic> json) =>
      _$$_BuildingProcessModelFromJson(json);

  @override
  @JsonKey(name: 'nama_pembangunan')
  final String? namaPembangunan;
  @override
  @JsonKey(name: 'total_persentase')
  final int? totalPersentase;

  @override
  String toString() {
    return 'BuildingProcessModel(namaPembangunan: $namaPembangunan, totalPersentase: $totalPersentase)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_BuildingProcessModel &&
            (identical(other.namaPembangunan, namaPembangunan) ||
                other.namaPembangunan == namaPembangunan) &&
            (identical(other.totalPersentase, totalPersentase) ||
                other.totalPersentase == totalPersentase));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, namaPembangunan, totalPersentase);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_BuildingProcessModelCopyWith<_$_BuildingProcessModel> get copyWith =>
      __$$_BuildingProcessModelCopyWithImpl<_$_BuildingProcessModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_BuildingProcessModelToJson(
      this,
    );
  }
}

abstract class _BuildingProcessModel implements BuildingProcessModel {
  const factory _BuildingProcessModel(
          {@JsonKey(name: 'nama_pembangunan') final String? namaPembangunan,
          @JsonKey(name: 'total_persentase') final int? totalPersentase}) =
      _$_BuildingProcessModel;

  factory _BuildingProcessModel.fromJson(Map<String, dynamic> json) =
      _$_BuildingProcessModel.fromJson;

  @override
  @JsonKey(name: 'nama_pembangunan')
  String? get namaPembangunan;
  @override
  @JsonKey(name: 'total_persentase')
  int? get totalPersentase;
  @override
  @JsonKey(ignore: true)
  _$$_BuildingProcessModelCopyWith<_$_BuildingProcessModel> get copyWith =>
      throw _privateConstructorUsedError;
}

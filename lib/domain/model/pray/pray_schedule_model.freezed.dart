// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'pray_schedule_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

PrayScheduleModel _$PrayScheduleModelFromJson(Map<String, dynamic> json) {
  return _PrayScheduleModel.fromJson(json);
}

/// @nodoc
mixin _$PrayScheduleModel {
  String? get hari => throw _privateConstructorUsedError;
  List<PrayDetail>? get details => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PrayScheduleModelCopyWith<PrayScheduleModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PrayScheduleModelCopyWith<$Res> {
  factory $PrayScheduleModelCopyWith(
          PrayScheduleModel value, $Res Function(PrayScheduleModel) then) =
      _$PrayScheduleModelCopyWithImpl<$Res, PrayScheduleModel>;
  @useResult
  $Res call({String? hari, List<PrayDetail>? details});
}

/// @nodoc
class _$PrayScheduleModelCopyWithImpl<$Res, $Val extends PrayScheduleModel>
    implements $PrayScheduleModelCopyWith<$Res> {
  _$PrayScheduleModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? hari = freezed,
    Object? details = freezed,
  }) {
    return _then(_value.copyWith(
      hari: freezed == hari
          ? _value.hari
          : hari // ignore: cast_nullable_to_non_nullable
              as String?,
      details: freezed == details
          ? _value.details
          : details // ignore: cast_nullable_to_non_nullable
              as List<PrayDetail>?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_PrayScheduleModelCopyWith<$Res>
    implements $PrayScheduleModelCopyWith<$Res> {
  factory _$$_PrayScheduleModelCopyWith(_$_PrayScheduleModel value,
          $Res Function(_$_PrayScheduleModel) then) =
      __$$_PrayScheduleModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String? hari, List<PrayDetail>? details});
}

/// @nodoc
class __$$_PrayScheduleModelCopyWithImpl<$Res>
    extends _$PrayScheduleModelCopyWithImpl<$Res, _$_PrayScheduleModel>
    implements _$$_PrayScheduleModelCopyWith<$Res> {
  __$$_PrayScheduleModelCopyWithImpl(
      _$_PrayScheduleModel _value, $Res Function(_$_PrayScheduleModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? hari = freezed,
    Object? details = freezed,
  }) {
    return _then(_$_PrayScheduleModel(
      hari: freezed == hari
          ? _value.hari
          : hari // ignore: cast_nullable_to_non_nullable
              as String?,
      details: freezed == details
          ? _value._details
          : details // ignore: cast_nullable_to_non_nullable
              as List<PrayDetail>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_PrayScheduleModel implements _PrayScheduleModel {
  const _$_PrayScheduleModel({this.hari, final List<PrayDetail>? details})
      : _details = details;

  factory _$_PrayScheduleModel.fromJson(Map<String, dynamic> json) =>
      _$$_PrayScheduleModelFromJson(json);

  @override
  final String? hari;
  final List<PrayDetail>? _details;
  @override
  List<PrayDetail>? get details {
    final value = _details;
    if (value == null) return null;
    if (_details is EqualUnmodifiableListView) return _details;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'PrayScheduleModel(hari: $hari, details: $details)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PrayScheduleModel &&
            (identical(other.hari, hari) || other.hari == hari) &&
            const DeepCollectionEquality().equals(other._details, _details));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType, hari, const DeepCollectionEquality().hash(_details));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_PrayScheduleModelCopyWith<_$_PrayScheduleModel> get copyWith =>
      __$$_PrayScheduleModelCopyWithImpl<_$_PrayScheduleModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_PrayScheduleModelToJson(
      this,
    );
  }
}

abstract class _PrayScheduleModel implements PrayScheduleModel {
  const factory _PrayScheduleModel(
      {final String? hari,
      final List<PrayDetail>? details}) = _$_PrayScheduleModel;

  factory _PrayScheduleModel.fromJson(Map<String, dynamic> json) =
      _$_PrayScheduleModel.fromJson;

  @override
  String? get hari;
  @override
  List<PrayDetail>? get details;
  @override
  @JsonKey(ignore: true)
  _$$_PrayScheduleModelCopyWith<_$_PrayScheduleModel> get copyWith =>
      throw _privateConstructorUsedError;
}

PrayDetail _$PrayDetailFromJson(Map<String, dynamic> json) {
  return _PrayDetail.fromJson(json);
}

/// @nodoc
mixin _$PrayDetail {
  int? get id => throw _privateConstructorUsedError;
  @JsonKey(name: 'nama_ibadah')
  String? get namaIbadah => throw _privateConstructorUsedError;
  @JsonKey(name: 'waktu_ibadah')
  String? get waktuIbadah => throw _privateConstructorUsedError;
  @JsonKey(name: 'nama_pendeta')
  String? get namaPendeta => throw _privateConstructorUsedError;
  @JsonKey(name: 'sesi_ibadah')
  String? get sesiIbadah => throw _privateConstructorUsedError;
  @JsonKey(name: 'created_at')
  String? get createdAt => throw _privateConstructorUsedError;
  @JsonKey(name: 'updated_at')
  String? get updatedAt => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PrayDetailCopyWith<PrayDetail> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PrayDetailCopyWith<$Res> {
  factory $PrayDetailCopyWith(
          PrayDetail value, $Res Function(PrayDetail) then) =
      _$PrayDetailCopyWithImpl<$Res, PrayDetail>;
  @useResult
  $Res call(
      {int? id,
      @JsonKey(name: 'nama_ibadah') String? namaIbadah,
      @JsonKey(name: 'waktu_ibadah') String? waktuIbadah,
      @JsonKey(name: 'nama_pendeta') String? namaPendeta,
      @JsonKey(name: 'sesi_ibadah') String? sesiIbadah,
      @JsonKey(name: 'created_at') String? createdAt,
      @JsonKey(name: 'updated_at') String? updatedAt});
}

/// @nodoc
class _$PrayDetailCopyWithImpl<$Res, $Val extends PrayDetail>
    implements $PrayDetailCopyWith<$Res> {
  _$PrayDetailCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? namaIbadah = freezed,
    Object? waktuIbadah = freezed,
    Object? namaPendeta = freezed,
    Object? sesiIbadah = freezed,
    Object? createdAt = freezed,
    Object? updatedAt = freezed,
  }) {
    return _then(_value.copyWith(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      namaIbadah: freezed == namaIbadah
          ? _value.namaIbadah
          : namaIbadah // ignore: cast_nullable_to_non_nullable
              as String?,
      waktuIbadah: freezed == waktuIbadah
          ? _value.waktuIbadah
          : waktuIbadah // ignore: cast_nullable_to_non_nullable
              as String?,
      namaPendeta: freezed == namaPendeta
          ? _value.namaPendeta
          : namaPendeta // ignore: cast_nullable_to_non_nullable
              as String?,
      sesiIbadah: freezed == sesiIbadah
          ? _value.sesiIbadah
          : sesiIbadah // ignore: cast_nullable_to_non_nullable
              as String?,
      createdAt: freezed == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String?,
      updatedAt: freezed == updatedAt
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_PrayDetailCopyWith<$Res>
    implements $PrayDetailCopyWith<$Res> {
  factory _$$_PrayDetailCopyWith(
          _$_PrayDetail value, $Res Function(_$_PrayDetail) then) =
      __$$_PrayDetailCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int? id,
      @JsonKey(name: 'nama_ibadah') String? namaIbadah,
      @JsonKey(name: 'waktu_ibadah') String? waktuIbadah,
      @JsonKey(name: 'nama_pendeta') String? namaPendeta,
      @JsonKey(name: 'sesi_ibadah') String? sesiIbadah,
      @JsonKey(name: 'created_at') String? createdAt,
      @JsonKey(name: 'updated_at') String? updatedAt});
}

/// @nodoc
class __$$_PrayDetailCopyWithImpl<$Res>
    extends _$PrayDetailCopyWithImpl<$Res, _$_PrayDetail>
    implements _$$_PrayDetailCopyWith<$Res> {
  __$$_PrayDetailCopyWithImpl(
      _$_PrayDetail _value, $Res Function(_$_PrayDetail) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? namaIbadah = freezed,
    Object? waktuIbadah = freezed,
    Object? namaPendeta = freezed,
    Object? sesiIbadah = freezed,
    Object? createdAt = freezed,
    Object? updatedAt = freezed,
  }) {
    return _then(_$_PrayDetail(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      namaIbadah: freezed == namaIbadah
          ? _value.namaIbadah
          : namaIbadah // ignore: cast_nullable_to_non_nullable
              as String?,
      waktuIbadah: freezed == waktuIbadah
          ? _value.waktuIbadah
          : waktuIbadah // ignore: cast_nullable_to_non_nullable
              as String?,
      namaPendeta: freezed == namaPendeta
          ? _value.namaPendeta
          : namaPendeta // ignore: cast_nullable_to_non_nullable
              as String?,
      sesiIbadah: freezed == sesiIbadah
          ? _value.sesiIbadah
          : sesiIbadah // ignore: cast_nullable_to_non_nullable
              as String?,
      createdAt: freezed == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String?,
      updatedAt: freezed == updatedAt
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_PrayDetail implements _PrayDetail {
  const _$_PrayDetail(
      {this.id,
      @JsonKey(name: 'nama_ibadah') this.namaIbadah,
      @JsonKey(name: 'waktu_ibadah') this.waktuIbadah,
      @JsonKey(name: 'nama_pendeta') this.namaPendeta,
      @JsonKey(name: 'sesi_ibadah') this.sesiIbadah,
      @JsonKey(name: 'created_at') this.createdAt,
      @JsonKey(name: 'updated_at') this.updatedAt});

  factory _$_PrayDetail.fromJson(Map<String, dynamic> json) =>
      _$$_PrayDetailFromJson(json);

  @override
  final int? id;
  @override
  @JsonKey(name: 'nama_ibadah')
  final String? namaIbadah;
  @override
  @JsonKey(name: 'waktu_ibadah')
  final String? waktuIbadah;
  @override
  @JsonKey(name: 'nama_pendeta')
  final String? namaPendeta;
  @override
  @JsonKey(name: 'sesi_ibadah')
  final String? sesiIbadah;
  @override
  @JsonKey(name: 'created_at')
  final String? createdAt;
  @override
  @JsonKey(name: 'updated_at')
  final String? updatedAt;

  @override
  String toString() {
    return 'PrayDetail(id: $id, namaIbadah: $namaIbadah, waktuIbadah: $waktuIbadah, namaPendeta: $namaPendeta, sesiIbadah: $sesiIbadah, createdAt: $createdAt, updatedAt: $updatedAt)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PrayDetail &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.namaIbadah, namaIbadah) ||
                other.namaIbadah == namaIbadah) &&
            (identical(other.waktuIbadah, waktuIbadah) ||
                other.waktuIbadah == waktuIbadah) &&
            (identical(other.namaPendeta, namaPendeta) ||
                other.namaPendeta == namaPendeta) &&
            (identical(other.sesiIbadah, sesiIbadah) ||
                other.sesiIbadah == sesiIbadah) &&
            (identical(other.createdAt, createdAt) ||
                other.createdAt == createdAt) &&
            (identical(other.updatedAt, updatedAt) ||
                other.updatedAt == updatedAt));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, namaIbadah, waktuIbadah,
      namaPendeta, sesiIbadah, createdAt, updatedAt);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_PrayDetailCopyWith<_$_PrayDetail> get copyWith =>
      __$$_PrayDetailCopyWithImpl<_$_PrayDetail>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_PrayDetailToJson(
      this,
    );
  }
}

abstract class _PrayDetail implements PrayDetail {
  const factory _PrayDetail(
      {final int? id,
      @JsonKey(name: 'nama_ibadah') final String? namaIbadah,
      @JsonKey(name: 'waktu_ibadah') final String? waktuIbadah,
      @JsonKey(name: 'nama_pendeta') final String? namaPendeta,
      @JsonKey(name: 'sesi_ibadah') final String? sesiIbadah,
      @JsonKey(name: 'created_at') final String? createdAt,
      @JsonKey(name: 'updated_at') final String? updatedAt}) = _$_PrayDetail;

  factory _PrayDetail.fromJson(Map<String, dynamic> json) =
      _$_PrayDetail.fromJson;

  @override
  int? get id;
  @override
  @JsonKey(name: 'nama_ibadah')
  String? get namaIbadah;
  @override
  @JsonKey(name: 'waktu_ibadah')
  String? get waktuIbadah;
  @override
  @JsonKey(name: 'nama_pendeta')
  String? get namaPendeta;
  @override
  @JsonKey(name: 'sesi_ibadah')
  String? get sesiIbadah;
  @override
  @JsonKey(name: 'created_at')
  String? get createdAt;
  @override
  @JsonKey(name: 'updated_at')
  String? get updatedAt;
  @override
  @JsonKey(ignore: true)
  _$$_PrayDetailCopyWith<_$_PrayDetail> get copyWith =>
      throw _privateConstructorUsedError;
}

import 'package:freezed_annotation/freezed_annotation.dart';

part 'pray_schedule_model.freezed.dart';
part 'pray_schedule_model.g.dart';

@freezed
class PrayScheduleModel with _$PrayScheduleModel {
  const factory PrayScheduleModel({
    String? hari,
    List<PrayDetail>? details,
  }) = _PrayScheduleModel;

  factory PrayScheduleModel.fromJson(Map<String, dynamic> json) =>
      _$PrayScheduleModelFromJson(json);
}

@freezed
class PrayDetail with _$PrayDetail {
  const factory PrayDetail({
    int? id,
    @JsonKey(name: 'nama_ibadah') String? namaIbadah,
    @JsonKey(name: 'waktu_ibadah') String? waktuIbadah,
    @JsonKey(name: 'nama_pendeta') String? namaPendeta,
    @JsonKey(name: 'sesi_ibadah') String? sesiIbadah,
    @JsonKey(name: 'created_at') String? createdAt,
    @JsonKey(name: 'updated_at') String? updatedAt,
  }) = _PrayDetail;

  factory PrayDetail.fromJson(Map<String, dynamic> json) =>
      _$PrayDetailFromJson(json);
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pray_schedule_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_PrayScheduleModel _$$_PrayScheduleModelFromJson(Map<String, dynamic> json) =>
    _$_PrayScheduleModel(
      hari: json['hari'] as String?,
      details: (json['details'] as List<dynamic>?)
          ?.map((e) => PrayDetail.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_PrayScheduleModelToJson(
        _$_PrayScheduleModel instance) =>
    <String, dynamic>{
      'hari': instance.hari,
      'details': instance.details,
    };

_$_PrayDetail _$$_PrayDetailFromJson(Map<String, dynamic> json) =>
    _$_PrayDetail(
      id: json['id'] as int?,
      namaIbadah: json['nama_ibadah'] as String?,
      waktuIbadah: json['waktu_ibadah'] as String?,
      namaPendeta: json['nama_pendeta'] as String?,
      sesiIbadah: json['sesi_ibadah'] as String?,
      createdAt: json['created_at'] as String?,
      updatedAt: json['updated_at'] as String?,
    );

Map<String, dynamic> _$$_PrayDetailToJson(_$_PrayDetail instance) =>
    <String, dynamic>{
      'id': instance.id,
      'nama_ibadah': instance.namaIbadah,
      'waktu_ibadah': instance.waktuIbadah,
      'nama_pendeta': instance.namaPendeta,
      'sesi_ibadah': instance.sesiIbadah,
      'created_at': instance.createdAt,
      'updated_at': instance.updatedAt,
    };

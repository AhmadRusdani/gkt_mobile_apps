// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'seremon_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_SeremonModel _$$_SeremonModelFromJson(Map<String, dynamic> json) =>
    _$_SeremonModel(
      id: json['id'] as int?,
      judulKhotbah: json['judul_khotbah'] as String?,
      ibadahId: json['ibadah_id'] as int?,
      type: json['type'] as String?,
      audioFileUri: json['audio_file_uri'] as String?,
      createdAt: json['created_at'] as String?,
      updatedAt: json['updated_at'] as String?,
      jadwalIbadah: json['jadwal_ibadah'] == null
          ? null
          : JadwalIbadah.fromJson(
              json['jadwal_ibadah'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_SeremonModelToJson(_$_SeremonModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'judul_khotbah': instance.judulKhotbah,
      'ibadah_id': instance.ibadahId,
      'type': instance.type,
      'audio_file_uri': instance.audioFileUri,
      'created_at': instance.createdAt,
      'updated_at': instance.updatedAt,
      'jadwal_ibadah': instance.jadwalIbadah,
    };

_$_JadwalIbadah _$$_JadwalIbadahFromJson(Map<String, dynamic> json) =>
    _$_JadwalIbadah(
      id: json['id'] as int?,
      namaIbadah: json['nama_ibadah'] as String?,
      waktuIbadah: json['waktu_ibadah'] as String?,
      namaPendeta: json['nama_pendeta'] as String?,
      sesiIbadah: json['sesi_ibadah'] as String?,
      createdAt: json['created_at'] as String?,
      updatedAt: json['updated_at'] as String?,
    );

Map<String, dynamic> _$$_JadwalIbadahToJson(_$_JadwalIbadah instance) =>
    <String, dynamic>{
      'id': instance.id,
      'nama_ibadah': instance.namaIbadah,
      'waktu_ibadah': instance.waktuIbadah,
      'nama_pendeta': instance.namaPendeta,
      'sesi_ibadah': instance.sesiIbadah,
      'created_at': instance.createdAt,
      'updated_at': instance.updatedAt,
    };

// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'seremon_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

SeremonModel _$SeremonModelFromJson(Map<String, dynamic> json) {
  return _SeremonModel.fromJson(json);
}

/// @nodoc
mixin _$SeremonModel {
  int? get id => throw _privateConstructorUsedError;
  @JsonKey(name: 'judul_khotbah')
  String? get judulKhotbah => throw _privateConstructorUsedError;
  @JsonKey(name: 'ibadah_id')
  int? get ibadahId => throw _privateConstructorUsedError;
  String? get type => throw _privateConstructorUsedError;
  @JsonKey(name: 'audio_file_uri')
  String? get audioFileUri => throw _privateConstructorUsedError;
  @JsonKey(name: 'created_at')
  String? get createdAt => throw _privateConstructorUsedError;
  @JsonKey(name: 'updated_at')
  String? get updatedAt => throw _privateConstructorUsedError;
  @JsonKey(name: 'jadwal_ibadah')
  JadwalIbadah? get jadwalIbadah => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $SeremonModelCopyWith<SeremonModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SeremonModelCopyWith<$Res> {
  factory $SeremonModelCopyWith(
          SeremonModel value, $Res Function(SeremonModel) then) =
      _$SeremonModelCopyWithImpl<$Res, SeremonModel>;
  @useResult
  $Res call(
      {int? id,
      @JsonKey(name: 'judul_khotbah') String? judulKhotbah,
      @JsonKey(name: 'ibadah_id') int? ibadahId,
      String? type,
      @JsonKey(name: 'audio_file_uri') String? audioFileUri,
      @JsonKey(name: 'created_at') String? createdAt,
      @JsonKey(name: 'updated_at') String? updatedAt,
      @JsonKey(name: 'jadwal_ibadah') JadwalIbadah? jadwalIbadah});

  $JadwalIbadahCopyWith<$Res>? get jadwalIbadah;
}

/// @nodoc
class _$SeremonModelCopyWithImpl<$Res, $Val extends SeremonModel>
    implements $SeremonModelCopyWith<$Res> {
  _$SeremonModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? judulKhotbah = freezed,
    Object? ibadahId = freezed,
    Object? type = freezed,
    Object? audioFileUri = freezed,
    Object? createdAt = freezed,
    Object? updatedAt = freezed,
    Object? jadwalIbadah = freezed,
  }) {
    return _then(_value.copyWith(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      judulKhotbah: freezed == judulKhotbah
          ? _value.judulKhotbah
          : judulKhotbah // ignore: cast_nullable_to_non_nullable
              as String?,
      ibadahId: freezed == ibadahId
          ? _value.ibadahId
          : ibadahId // ignore: cast_nullable_to_non_nullable
              as int?,
      type: freezed == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String?,
      audioFileUri: freezed == audioFileUri
          ? _value.audioFileUri
          : audioFileUri // ignore: cast_nullable_to_non_nullable
              as String?,
      createdAt: freezed == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String?,
      updatedAt: freezed == updatedAt
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as String?,
      jadwalIbadah: freezed == jadwalIbadah
          ? _value.jadwalIbadah
          : jadwalIbadah // ignore: cast_nullable_to_non_nullable
              as JadwalIbadah?,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $JadwalIbadahCopyWith<$Res>? get jadwalIbadah {
    if (_value.jadwalIbadah == null) {
      return null;
    }

    return $JadwalIbadahCopyWith<$Res>(_value.jadwalIbadah!, (value) {
      return _then(_value.copyWith(jadwalIbadah: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_SeremonModelCopyWith<$Res>
    implements $SeremonModelCopyWith<$Res> {
  factory _$$_SeremonModelCopyWith(
          _$_SeremonModel value, $Res Function(_$_SeremonModel) then) =
      __$$_SeremonModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int? id,
      @JsonKey(name: 'judul_khotbah') String? judulKhotbah,
      @JsonKey(name: 'ibadah_id') int? ibadahId,
      String? type,
      @JsonKey(name: 'audio_file_uri') String? audioFileUri,
      @JsonKey(name: 'created_at') String? createdAt,
      @JsonKey(name: 'updated_at') String? updatedAt,
      @JsonKey(name: 'jadwal_ibadah') JadwalIbadah? jadwalIbadah});

  @override
  $JadwalIbadahCopyWith<$Res>? get jadwalIbadah;
}

/// @nodoc
class __$$_SeremonModelCopyWithImpl<$Res>
    extends _$SeremonModelCopyWithImpl<$Res, _$_SeremonModel>
    implements _$$_SeremonModelCopyWith<$Res> {
  __$$_SeremonModelCopyWithImpl(
      _$_SeremonModel _value, $Res Function(_$_SeremonModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? judulKhotbah = freezed,
    Object? ibadahId = freezed,
    Object? type = freezed,
    Object? audioFileUri = freezed,
    Object? createdAt = freezed,
    Object? updatedAt = freezed,
    Object? jadwalIbadah = freezed,
  }) {
    return _then(_$_SeremonModel(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      judulKhotbah: freezed == judulKhotbah
          ? _value.judulKhotbah
          : judulKhotbah // ignore: cast_nullable_to_non_nullable
              as String?,
      ibadahId: freezed == ibadahId
          ? _value.ibadahId
          : ibadahId // ignore: cast_nullable_to_non_nullable
              as int?,
      type: freezed == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String?,
      audioFileUri: freezed == audioFileUri
          ? _value.audioFileUri
          : audioFileUri // ignore: cast_nullable_to_non_nullable
              as String?,
      createdAt: freezed == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String?,
      updatedAt: freezed == updatedAt
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as String?,
      jadwalIbadah: freezed == jadwalIbadah
          ? _value.jadwalIbadah
          : jadwalIbadah // ignore: cast_nullable_to_non_nullable
              as JadwalIbadah?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_SeremonModel implements _SeremonModel {
  const _$_SeremonModel(
      {this.id,
      @JsonKey(name: 'judul_khotbah') this.judulKhotbah,
      @JsonKey(name: 'ibadah_id') this.ibadahId,
      this.type,
      @JsonKey(name: 'audio_file_uri') this.audioFileUri,
      @JsonKey(name: 'created_at') this.createdAt,
      @JsonKey(name: 'updated_at') this.updatedAt,
      @JsonKey(name: 'jadwal_ibadah') this.jadwalIbadah});

  factory _$_SeremonModel.fromJson(Map<String, dynamic> json) =>
      _$$_SeremonModelFromJson(json);

  @override
  final int? id;
  @override
  @JsonKey(name: 'judul_khotbah')
  final String? judulKhotbah;
  @override
  @JsonKey(name: 'ibadah_id')
  final int? ibadahId;
  @override
  final String? type;
  @override
  @JsonKey(name: 'audio_file_uri')
  final String? audioFileUri;
  @override
  @JsonKey(name: 'created_at')
  final String? createdAt;
  @override
  @JsonKey(name: 'updated_at')
  final String? updatedAt;
  @override
  @JsonKey(name: 'jadwal_ibadah')
  final JadwalIbadah? jadwalIbadah;

  @override
  String toString() {
    return 'SeremonModel(id: $id, judulKhotbah: $judulKhotbah, ibadahId: $ibadahId, type: $type, audioFileUri: $audioFileUri, createdAt: $createdAt, updatedAt: $updatedAt, jadwalIbadah: $jadwalIbadah)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SeremonModel &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.judulKhotbah, judulKhotbah) ||
                other.judulKhotbah == judulKhotbah) &&
            (identical(other.ibadahId, ibadahId) ||
                other.ibadahId == ibadahId) &&
            (identical(other.type, type) || other.type == type) &&
            (identical(other.audioFileUri, audioFileUri) ||
                other.audioFileUri == audioFileUri) &&
            (identical(other.createdAt, createdAt) ||
                other.createdAt == createdAt) &&
            (identical(other.updatedAt, updatedAt) ||
                other.updatedAt == updatedAt) &&
            (identical(other.jadwalIbadah, jadwalIbadah) ||
                other.jadwalIbadah == jadwalIbadah));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, judulKhotbah, ibadahId, type,
      audioFileUri, createdAt, updatedAt, jadwalIbadah);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SeremonModelCopyWith<_$_SeremonModel> get copyWith =>
      __$$_SeremonModelCopyWithImpl<_$_SeremonModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_SeremonModelToJson(
      this,
    );
  }
}

abstract class _SeremonModel implements SeremonModel {
  const factory _SeremonModel(
          {final int? id,
          @JsonKey(name: 'judul_khotbah') final String? judulKhotbah,
          @JsonKey(name: 'ibadah_id') final int? ibadahId,
          final String? type,
          @JsonKey(name: 'audio_file_uri') final String? audioFileUri,
          @JsonKey(name: 'created_at') final String? createdAt,
          @JsonKey(name: 'updated_at') final String? updatedAt,
          @JsonKey(name: 'jadwal_ibadah') final JadwalIbadah? jadwalIbadah}) =
      _$_SeremonModel;

  factory _SeremonModel.fromJson(Map<String, dynamic> json) =
      _$_SeremonModel.fromJson;

  @override
  int? get id;
  @override
  @JsonKey(name: 'judul_khotbah')
  String? get judulKhotbah;
  @override
  @JsonKey(name: 'ibadah_id')
  int? get ibadahId;
  @override
  String? get type;
  @override
  @JsonKey(name: 'audio_file_uri')
  String? get audioFileUri;
  @override
  @JsonKey(name: 'created_at')
  String? get createdAt;
  @override
  @JsonKey(name: 'updated_at')
  String? get updatedAt;
  @override
  @JsonKey(name: 'jadwal_ibadah')
  JadwalIbadah? get jadwalIbadah;
  @override
  @JsonKey(ignore: true)
  _$$_SeremonModelCopyWith<_$_SeremonModel> get copyWith =>
      throw _privateConstructorUsedError;
}

JadwalIbadah _$JadwalIbadahFromJson(Map<String, dynamic> json) {
  return _JadwalIbadah.fromJson(json);
}

/// @nodoc
mixin _$JadwalIbadah {
  int? get id => throw _privateConstructorUsedError;
  @JsonKey(name: 'nama_ibadah')
  String? get namaIbadah => throw _privateConstructorUsedError;
  @JsonKey(name: 'waktu_ibadah')
  String? get waktuIbadah => throw _privateConstructorUsedError;
  @JsonKey(name: 'nama_pendeta')
  String? get namaPendeta => throw _privateConstructorUsedError;
  @JsonKey(name: 'sesi_ibadah')
  String? get sesiIbadah => throw _privateConstructorUsedError;
  @JsonKey(name: 'created_at')
  String? get createdAt => throw _privateConstructorUsedError;
  @JsonKey(name: 'updated_at')
  String? get updatedAt => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $JadwalIbadahCopyWith<JadwalIbadah> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $JadwalIbadahCopyWith<$Res> {
  factory $JadwalIbadahCopyWith(
          JadwalIbadah value, $Res Function(JadwalIbadah) then) =
      _$JadwalIbadahCopyWithImpl<$Res, JadwalIbadah>;
  @useResult
  $Res call(
      {int? id,
      @JsonKey(name: 'nama_ibadah') String? namaIbadah,
      @JsonKey(name: 'waktu_ibadah') String? waktuIbadah,
      @JsonKey(name: 'nama_pendeta') String? namaPendeta,
      @JsonKey(name: 'sesi_ibadah') String? sesiIbadah,
      @JsonKey(name: 'created_at') String? createdAt,
      @JsonKey(name: 'updated_at') String? updatedAt});
}

/// @nodoc
class _$JadwalIbadahCopyWithImpl<$Res, $Val extends JadwalIbadah>
    implements $JadwalIbadahCopyWith<$Res> {
  _$JadwalIbadahCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? namaIbadah = freezed,
    Object? waktuIbadah = freezed,
    Object? namaPendeta = freezed,
    Object? sesiIbadah = freezed,
    Object? createdAt = freezed,
    Object? updatedAt = freezed,
  }) {
    return _then(_value.copyWith(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      namaIbadah: freezed == namaIbadah
          ? _value.namaIbadah
          : namaIbadah // ignore: cast_nullable_to_non_nullable
              as String?,
      waktuIbadah: freezed == waktuIbadah
          ? _value.waktuIbadah
          : waktuIbadah // ignore: cast_nullable_to_non_nullable
              as String?,
      namaPendeta: freezed == namaPendeta
          ? _value.namaPendeta
          : namaPendeta // ignore: cast_nullable_to_non_nullable
              as String?,
      sesiIbadah: freezed == sesiIbadah
          ? _value.sesiIbadah
          : sesiIbadah // ignore: cast_nullable_to_non_nullable
              as String?,
      createdAt: freezed == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String?,
      updatedAt: freezed == updatedAt
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_JadwalIbadahCopyWith<$Res>
    implements $JadwalIbadahCopyWith<$Res> {
  factory _$$_JadwalIbadahCopyWith(
          _$_JadwalIbadah value, $Res Function(_$_JadwalIbadah) then) =
      __$$_JadwalIbadahCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int? id,
      @JsonKey(name: 'nama_ibadah') String? namaIbadah,
      @JsonKey(name: 'waktu_ibadah') String? waktuIbadah,
      @JsonKey(name: 'nama_pendeta') String? namaPendeta,
      @JsonKey(name: 'sesi_ibadah') String? sesiIbadah,
      @JsonKey(name: 'created_at') String? createdAt,
      @JsonKey(name: 'updated_at') String? updatedAt});
}

/// @nodoc
class __$$_JadwalIbadahCopyWithImpl<$Res>
    extends _$JadwalIbadahCopyWithImpl<$Res, _$_JadwalIbadah>
    implements _$$_JadwalIbadahCopyWith<$Res> {
  __$$_JadwalIbadahCopyWithImpl(
      _$_JadwalIbadah _value, $Res Function(_$_JadwalIbadah) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? namaIbadah = freezed,
    Object? waktuIbadah = freezed,
    Object? namaPendeta = freezed,
    Object? sesiIbadah = freezed,
    Object? createdAt = freezed,
    Object? updatedAt = freezed,
  }) {
    return _then(_$_JadwalIbadah(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      namaIbadah: freezed == namaIbadah
          ? _value.namaIbadah
          : namaIbadah // ignore: cast_nullable_to_non_nullable
              as String?,
      waktuIbadah: freezed == waktuIbadah
          ? _value.waktuIbadah
          : waktuIbadah // ignore: cast_nullable_to_non_nullable
              as String?,
      namaPendeta: freezed == namaPendeta
          ? _value.namaPendeta
          : namaPendeta // ignore: cast_nullable_to_non_nullable
              as String?,
      sesiIbadah: freezed == sesiIbadah
          ? _value.sesiIbadah
          : sesiIbadah // ignore: cast_nullable_to_non_nullable
              as String?,
      createdAt: freezed == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String?,
      updatedAt: freezed == updatedAt
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_JadwalIbadah implements _JadwalIbadah {
  const _$_JadwalIbadah(
      {this.id,
      @JsonKey(name: 'nama_ibadah') this.namaIbadah,
      @JsonKey(name: 'waktu_ibadah') this.waktuIbadah,
      @JsonKey(name: 'nama_pendeta') this.namaPendeta,
      @JsonKey(name: 'sesi_ibadah') this.sesiIbadah,
      @JsonKey(name: 'created_at') this.createdAt,
      @JsonKey(name: 'updated_at') this.updatedAt});

  factory _$_JadwalIbadah.fromJson(Map<String, dynamic> json) =>
      _$$_JadwalIbadahFromJson(json);

  @override
  final int? id;
  @override
  @JsonKey(name: 'nama_ibadah')
  final String? namaIbadah;
  @override
  @JsonKey(name: 'waktu_ibadah')
  final String? waktuIbadah;
  @override
  @JsonKey(name: 'nama_pendeta')
  final String? namaPendeta;
  @override
  @JsonKey(name: 'sesi_ibadah')
  final String? sesiIbadah;
  @override
  @JsonKey(name: 'created_at')
  final String? createdAt;
  @override
  @JsonKey(name: 'updated_at')
  final String? updatedAt;

  @override
  String toString() {
    return 'JadwalIbadah(id: $id, namaIbadah: $namaIbadah, waktuIbadah: $waktuIbadah, namaPendeta: $namaPendeta, sesiIbadah: $sesiIbadah, createdAt: $createdAt, updatedAt: $updatedAt)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_JadwalIbadah &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.namaIbadah, namaIbadah) ||
                other.namaIbadah == namaIbadah) &&
            (identical(other.waktuIbadah, waktuIbadah) ||
                other.waktuIbadah == waktuIbadah) &&
            (identical(other.namaPendeta, namaPendeta) ||
                other.namaPendeta == namaPendeta) &&
            (identical(other.sesiIbadah, sesiIbadah) ||
                other.sesiIbadah == sesiIbadah) &&
            (identical(other.createdAt, createdAt) ||
                other.createdAt == createdAt) &&
            (identical(other.updatedAt, updatedAt) ||
                other.updatedAt == updatedAt));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, namaIbadah, waktuIbadah,
      namaPendeta, sesiIbadah, createdAt, updatedAt);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_JadwalIbadahCopyWith<_$_JadwalIbadah> get copyWith =>
      __$$_JadwalIbadahCopyWithImpl<_$_JadwalIbadah>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_JadwalIbadahToJson(
      this,
    );
  }
}

abstract class _JadwalIbadah implements JadwalIbadah {
  const factory _JadwalIbadah(
      {final int? id,
      @JsonKey(name: 'nama_ibadah') final String? namaIbadah,
      @JsonKey(name: 'waktu_ibadah') final String? waktuIbadah,
      @JsonKey(name: 'nama_pendeta') final String? namaPendeta,
      @JsonKey(name: 'sesi_ibadah') final String? sesiIbadah,
      @JsonKey(name: 'created_at') final String? createdAt,
      @JsonKey(name: 'updated_at') final String? updatedAt}) = _$_JadwalIbadah;

  factory _JadwalIbadah.fromJson(Map<String, dynamic> json) =
      _$_JadwalIbadah.fromJson;

  @override
  int? get id;
  @override
  @JsonKey(name: 'nama_ibadah')
  String? get namaIbadah;
  @override
  @JsonKey(name: 'waktu_ibadah')
  String? get waktuIbadah;
  @override
  @JsonKey(name: 'nama_pendeta')
  String? get namaPendeta;
  @override
  @JsonKey(name: 'sesi_ibadah')
  String? get sesiIbadah;
  @override
  @JsonKey(name: 'created_at')
  String? get createdAt;
  @override
  @JsonKey(name: 'updated_at')
  String? get updatedAt;
  @override
  @JsonKey(ignore: true)
  _$$_JadwalIbadahCopyWith<_$_JadwalIbadah> get copyWith =>
      throw _privateConstructorUsedError;
}

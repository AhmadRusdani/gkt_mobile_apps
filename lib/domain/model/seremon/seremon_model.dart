import 'package:freezed_annotation/freezed_annotation.dart';

part 'seremon_model.freezed.dart';
part 'seremon_model.g.dart';

enum SeremonType {
  minggu,
  pa,
  mandarin;

  @override
  String toString() {
    switch (this) {
      case SeremonType.mandarin:
        return 'MANDARIN';
      case SeremonType.pa:
        return 'PA';
      default:
        return 'MINGGU';
    }
  }
}

@freezed
class SeremonModel with _$SeremonModel {
  const factory SeremonModel({
    int? id,
    @JsonKey(name: 'judul_khotbah') String? judulKhotbah,
    @JsonKey(name: 'ibadah_id') int? ibadahId,
    String? type,
    @JsonKey(name: 'audio_file_uri') String? audioFileUri,
    @JsonKey(name: 'created_at') String? createdAt,
    @JsonKey(name: 'updated_at') String? updatedAt,
    @JsonKey(name: 'jadwal_ibadah') JadwalIbadah? jadwalIbadah,
  }) = _SeremonModel;

  factory SeremonModel.fromJson(Map<String, dynamic> json) =>
      _$SeremonModelFromJson(json);
}

@freezed
class JadwalIbadah with _$JadwalIbadah {
  const factory JadwalIbadah({
    int? id,
    @JsonKey(name: 'nama_ibadah') String? namaIbadah,
    @JsonKey(name: 'waktu_ibadah') String? waktuIbadah,
    @JsonKey(name: 'nama_pendeta') String? namaPendeta,
    @JsonKey(name: 'sesi_ibadah') String? sesiIbadah,
    @JsonKey(name: 'created_at') String? createdAt,
    @JsonKey(name: 'updated_at') String? updatedAt,
  }) = _JadwalIbadah;

  factory JadwalIbadah.fromJson(Map<String, dynamic> json) =>
      _$JadwalIbadahFromJson(json);
}

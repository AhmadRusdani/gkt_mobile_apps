import 'package:json_annotation/json_annotation.dart';

@JsonEnum()
enum ResponseEnum {
  @JsonValue('yes')
  yes,
  @JsonValue('no')
  no,
}

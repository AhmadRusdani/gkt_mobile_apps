import 'package:freezed_annotation/freezed_annotation.dart';

part 'history_transaction_model.freezed.dart';
part 'history_transaction_model.g.dart';

enum HistoryTransactionType {
  monthly,
  all;

  @override
  String toString() {
    switch (this) {
      case HistoryTransactionType.all:
        return 'all';
      default:
        return 'monthly';
    }
  }
}

@freezed
class HistoryTransactionModel with _$HistoryTransactionModel {
  const factory HistoryTransactionModel({
    @JsonKey(name: 'id_transaksi') String? idTransaksi,
    String? tanggal,
    @JsonKey(name: 'is_paid') bool? isPaid,
    int? jumlah,
    @JsonKey(name: 'payment_link') String? paymentLink,
  }) = _HistoryTransactionModel;

  factory HistoryTransactionModel.fromJson(Map<String, dynamic> json) =>
      _$HistoryTransactionModelFromJson(json);
}

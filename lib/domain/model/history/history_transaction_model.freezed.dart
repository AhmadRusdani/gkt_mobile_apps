// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'history_transaction_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

HistoryTransactionModel _$HistoryTransactionModelFromJson(
    Map<String, dynamic> json) {
  return _HistoryTransactionModel.fromJson(json);
}

/// @nodoc
mixin _$HistoryTransactionModel {
  @JsonKey(name: 'id_transaksi')
  String? get idTransaksi => throw _privateConstructorUsedError;
  String? get tanggal => throw _privateConstructorUsedError;
  @JsonKey(name: 'is_paid')
  bool? get isPaid => throw _privateConstructorUsedError;
  int? get jumlah => throw _privateConstructorUsedError;
  @JsonKey(name: 'payment_link')
  String? get paymentLink => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $HistoryTransactionModelCopyWith<HistoryTransactionModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HistoryTransactionModelCopyWith<$Res> {
  factory $HistoryTransactionModelCopyWith(HistoryTransactionModel value,
          $Res Function(HistoryTransactionModel) then) =
      _$HistoryTransactionModelCopyWithImpl<$Res, HistoryTransactionModel>;
  @useResult
  $Res call(
      {@JsonKey(name: 'id_transaksi') String? idTransaksi,
      String? tanggal,
      @JsonKey(name: 'is_paid') bool? isPaid,
      int? jumlah,
      @JsonKey(name: 'payment_link') String? paymentLink});
}

/// @nodoc
class _$HistoryTransactionModelCopyWithImpl<$Res,
        $Val extends HistoryTransactionModel>
    implements $HistoryTransactionModelCopyWith<$Res> {
  _$HistoryTransactionModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? idTransaksi = freezed,
    Object? tanggal = freezed,
    Object? isPaid = freezed,
    Object? jumlah = freezed,
    Object? paymentLink = freezed,
  }) {
    return _then(_value.copyWith(
      idTransaksi: freezed == idTransaksi
          ? _value.idTransaksi
          : idTransaksi // ignore: cast_nullable_to_non_nullable
              as String?,
      tanggal: freezed == tanggal
          ? _value.tanggal
          : tanggal // ignore: cast_nullable_to_non_nullable
              as String?,
      isPaid: freezed == isPaid
          ? _value.isPaid
          : isPaid // ignore: cast_nullable_to_non_nullable
              as bool?,
      jumlah: freezed == jumlah
          ? _value.jumlah
          : jumlah // ignore: cast_nullable_to_non_nullable
              as int?,
      paymentLink: freezed == paymentLink
          ? _value.paymentLink
          : paymentLink // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_HistoryTransactionModelCopyWith<$Res>
    implements $HistoryTransactionModelCopyWith<$Res> {
  factory _$$_HistoryTransactionModelCopyWith(_$_HistoryTransactionModel value,
          $Res Function(_$_HistoryTransactionModel) then) =
      __$$_HistoryTransactionModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: 'id_transaksi') String? idTransaksi,
      String? tanggal,
      @JsonKey(name: 'is_paid') bool? isPaid,
      int? jumlah,
      @JsonKey(name: 'payment_link') String? paymentLink});
}

/// @nodoc
class __$$_HistoryTransactionModelCopyWithImpl<$Res>
    extends _$HistoryTransactionModelCopyWithImpl<$Res,
        _$_HistoryTransactionModel>
    implements _$$_HistoryTransactionModelCopyWith<$Res> {
  __$$_HistoryTransactionModelCopyWithImpl(_$_HistoryTransactionModel _value,
      $Res Function(_$_HistoryTransactionModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? idTransaksi = freezed,
    Object? tanggal = freezed,
    Object? isPaid = freezed,
    Object? jumlah = freezed,
    Object? paymentLink = freezed,
  }) {
    return _then(_$_HistoryTransactionModel(
      idTransaksi: freezed == idTransaksi
          ? _value.idTransaksi
          : idTransaksi // ignore: cast_nullable_to_non_nullable
              as String?,
      tanggal: freezed == tanggal
          ? _value.tanggal
          : tanggal // ignore: cast_nullable_to_non_nullable
              as String?,
      isPaid: freezed == isPaid
          ? _value.isPaid
          : isPaid // ignore: cast_nullable_to_non_nullable
              as bool?,
      jumlah: freezed == jumlah
          ? _value.jumlah
          : jumlah // ignore: cast_nullable_to_non_nullable
              as int?,
      paymentLink: freezed == paymentLink
          ? _value.paymentLink
          : paymentLink // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_HistoryTransactionModel implements _HistoryTransactionModel {
  const _$_HistoryTransactionModel(
      {@JsonKey(name: 'id_transaksi') this.idTransaksi,
      this.tanggal,
      @JsonKey(name: 'is_paid') this.isPaid,
      this.jumlah,
      @JsonKey(name: 'payment_link') this.paymentLink});

  factory _$_HistoryTransactionModel.fromJson(Map<String, dynamic> json) =>
      _$$_HistoryTransactionModelFromJson(json);

  @override
  @JsonKey(name: 'id_transaksi')
  final String? idTransaksi;
  @override
  final String? tanggal;
  @override
  @JsonKey(name: 'is_paid')
  final bool? isPaid;
  @override
  final int? jumlah;
  @override
  @JsonKey(name: 'payment_link')
  final String? paymentLink;

  @override
  String toString() {
    return 'HistoryTransactionModel(idTransaksi: $idTransaksi, tanggal: $tanggal, isPaid: $isPaid, jumlah: $jumlah, paymentLink: $paymentLink)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_HistoryTransactionModel &&
            (identical(other.idTransaksi, idTransaksi) ||
                other.idTransaksi == idTransaksi) &&
            (identical(other.tanggal, tanggal) || other.tanggal == tanggal) &&
            (identical(other.isPaid, isPaid) || other.isPaid == isPaid) &&
            (identical(other.jumlah, jumlah) || other.jumlah == jumlah) &&
            (identical(other.paymentLink, paymentLink) ||
                other.paymentLink == paymentLink));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType, idTransaksi, tanggal, isPaid, jumlah, paymentLink);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_HistoryTransactionModelCopyWith<_$_HistoryTransactionModel>
      get copyWith =>
          __$$_HistoryTransactionModelCopyWithImpl<_$_HistoryTransactionModel>(
              this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_HistoryTransactionModelToJson(
      this,
    );
  }
}

abstract class _HistoryTransactionModel implements HistoryTransactionModel {
  const factory _HistoryTransactionModel(
          {@JsonKey(name: 'id_transaksi') final String? idTransaksi,
          final String? tanggal,
          @JsonKey(name: 'is_paid') final bool? isPaid,
          final int? jumlah,
          @JsonKey(name: 'payment_link') final String? paymentLink}) =
      _$_HistoryTransactionModel;

  factory _HistoryTransactionModel.fromJson(Map<String, dynamic> json) =
      _$_HistoryTransactionModel.fromJson;

  @override
  @JsonKey(name: 'id_transaksi')
  String? get idTransaksi;
  @override
  String? get tanggal;
  @override
  @JsonKey(name: 'is_paid')
  bool? get isPaid;
  @override
  int? get jumlah;
  @override
  @JsonKey(name: 'payment_link')
  String? get paymentLink;
  @override
  @JsonKey(ignore: true)
  _$$_HistoryTransactionModelCopyWith<_$_HistoryTransactionModel>
      get copyWith => throw _privateConstructorUsedError;
}

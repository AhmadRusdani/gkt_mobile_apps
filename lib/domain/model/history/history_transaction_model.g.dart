// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'history_transaction_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_HistoryTransactionModel _$$_HistoryTransactionModelFromJson(
        Map<String, dynamic> json) =>
    _$_HistoryTransactionModel(
      idTransaksi: json['id_transaksi'] as String?,
      tanggal: json['tanggal'] as String?,
      isPaid: json['is_paid'] as bool?,
      jumlah: json['jumlah'] as int?,
      paymentLink: json['payment_link'] as String?,
    );

Map<String, dynamic> _$$_HistoryTransactionModelToJson(
        _$_HistoryTransactionModel instance) =>
    <String, dynamic>{
      'id_transaksi': instance.idTransaksi,
      'tanggal': instance.tanggal,
      'is_paid': instance.isPaid,
      'jumlah': instance.jumlah,
      'payment_link': instance.paymentLink,
    };

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'news_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_NewsModel _$$_NewsModelFromJson(Map<String, dynamic> json) => _$_NewsModel(
      id: json['id'] as int?,
      judulBerita: json['judul_berita'] as String?,
      deskripsiBerita: json['deskripsi_berita'] as String?,
      imageUri: json['image_uri'] as String?,
      createdAt: json['created_at'] as String?,
      updatedAt: json['updated_at'] as String?,
    );

Map<String, dynamic> _$$_NewsModelToJson(_$_NewsModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'judul_berita': instance.judulBerita,
      'deskripsi_berita': instance.deskripsiBerita,
      'image_uri': instance.imageUri,
      'created_at': instance.createdAt,
      'updated_at': instance.updatedAt,
    };

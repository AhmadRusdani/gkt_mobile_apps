import 'package:freezed_annotation/freezed_annotation.dart';
part 'process_summary_item_model.freezed.dart';

enum ProcessSummaryItemStatus {
  completed,
  process,
  iddle,
}

@freezed
class ProcessSummaryItemModel with _$ProcessSummaryItemModel {
  const factory ProcessSummaryItemModel({
    @Default('') String titleFloor,
    @Default(ProcessSummaryItemStatus.process) ProcessSummaryItemStatus status,
    @Default(0.0) double progressPercentage,
  }) = _ProcessSummaryItemModel;

  // factory ProcessSummaryItemModel.fromJson(Map<String, dynamic> json) =>
  //     _$ProcessSummaryItemModelFromJson(json);
}

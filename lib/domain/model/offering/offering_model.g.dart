// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'offering_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_OfferingModel _$$_OfferingModelFromJson(Map<String, dynamic> json) =>
    _$_OfferingModel(
      batasWaktu: json['batas_waktu'] as String?,
      jumlahPembayaran: json['jumlah_pembayaran'] as int?,
      paymentUrl: json['payment_url'] as String?,
    );

Map<String, dynamic> _$$_OfferingModelToJson(_$_OfferingModel instance) =>
    <String, dynamic>{
      'batas_waktu': instance.batasWaktu,
      'jumlah_pembayaran': instance.jumlahPembayaran,
      'payment_url': instance.paymentUrl,
    };

// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'offering_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

OfferingModel _$OfferingModelFromJson(Map<String, dynamic> json) {
  return _OfferingModel.fromJson(json);
}

/// @nodoc
mixin _$OfferingModel {
  @JsonKey(name: 'batas_waktu')
  String? get batasWaktu => throw _privateConstructorUsedError;
  @JsonKey(name: 'jumlah_pembayaran')
  int? get jumlahPembayaran => throw _privateConstructorUsedError;
  @JsonKey(name: 'payment_url')
  String? get paymentUrl => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $OfferingModelCopyWith<OfferingModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $OfferingModelCopyWith<$Res> {
  factory $OfferingModelCopyWith(
          OfferingModel value, $Res Function(OfferingModel) then) =
      _$OfferingModelCopyWithImpl<$Res, OfferingModel>;
  @useResult
  $Res call(
      {@JsonKey(name: 'batas_waktu') String? batasWaktu,
      @JsonKey(name: 'jumlah_pembayaran') int? jumlahPembayaran,
      @JsonKey(name: 'payment_url') String? paymentUrl});
}

/// @nodoc
class _$OfferingModelCopyWithImpl<$Res, $Val extends OfferingModel>
    implements $OfferingModelCopyWith<$Res> {
  _$OfferingModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? batasWaktu = freezed,
    Object? jumlahPembayaran = freezed,
    Object? paymentUrl = freezed,
  }) {
    return _then(_value.copyWith(
      batasWaktu: freezed == batasWaktu
          ? _value.batasWaktu
          : batasWaktu // ignore: cast_nullable_to_non_nullable
              as String?,
      jumlahPembayaran: freezed == jumlahPembayaran
          ? _value.jumlahPembayaran
          : jumlahPembayaran // ignore: cast_nullable_to_non_nullable
              as int?,
      paymentUrl: freezed == paymentUrl
          ? _value.paymentUrl
          : paymentUrl // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_OfferingModelCopyWith<$Res>
    implements $OfferingModelCopyWith<$Res> {
  factory _$$_OfferingModelCopyWith(
          _$_OfferingModel value, $Res Function(_$_OfferingModel) then) =
      __$$_OfferingModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: 'batas_waktu') String? batasWaktu,
      @JsonKey(name: 'jumlah_pembayaran') int? jumlahPembayaran,
      @JsonKey(name: 'payment_url') String? paymentUrl});
}

/// @nodoc
class __$$_OfferingModelCopyWithImpl<$Res>
    extends _$OfferingModelCopyWithImpl<$Res, _$_OfferingModel>
    implements _$$_OfferingModelCopyWith<$Res> {
  __$$_OfferingModelCopyWithImpl(
      _$_OfferingModel _value, $Res Function(_$_OfferingModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? batasWaktu = freezed,
    Object? jumlahPembayaran = freezed,
    Object? paymentUrl = freezed,
  }) {
    return _then(_$_OfferingModel(
      batasWaktu: freezed == batasWaktu
          ? _value.batasWaktu
          : batasWaktu // ignore: cast_nullable_to_non_nullable
              as String?,
      jumlahPembayaran: freezed == jumlahPembayaran
          ? _value.jumlahPembayaran
          : jumlahPembayaran // ignore: cast_nullable_to_non_nullable
              as int?,
      paymentUrl: freezed == paymentUrl
          ? _value.paymentUrl
          : paymentUrl // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_OfferingModel implements _OfferingModel {
  _$_OfferingModel(
      {@JsonKey(name: 'batas_waktu') this.batasWaktu,
      @JsonKey(name: 'jumlah_pembayaran') this.jumlahPembayaran,
      @JsonKey(name: 'payment_url') this.paymentUrl});

  factory _$_OfferingModel.fromJson(Map<String, dynamic> json) =>
      _$$_OfferingModelFromJson(json);

  @override
  @JsonKey(name: 'batas_waktu')
  final String? batasWaktu;
  @override
  @JsonKey(name: 'jumlah_pembayaran')
  final int? jumlahPembayaran;
  @override
  @JsonKey(name: 'payment_url')
  final String? paymentUrl;

  @override
  String toString() {
    return 'OfferingModel(batasWaktu: $batasWaktu, jumlahPembayaran: $jumlahPembayaran, paymentUrl: $paymentUrl)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_OfferingModel &&
            (identical(other.batasWaktu, batasWaktu) ||
                other.batasWaktu == batasWaktu) &&
            (identical(other.jumlahPembayaran, jumlahPembayaran) ||
                other.jumlahPembayaran == jumlahPembayaran) &&
            (identical(other.paymentUrl, paymentUrl) ||
                other.paymentUrl == paymentUrl));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, batasWaktu, jumlahPembayaran, paymentUrl);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_OfferingModelCopyWith<_$_OfferingModel> get copyWith =>
      __$$_OfferingModelCopyWithImpl<_$_OfferingModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_OfferingModelToJson(
      this,
    );
  }
}

abstract class _OfferingModel implements OfferingModel {
  factory _OfferingModel(
          {@JsonKey(name: 'batas_waktu') final String? batasWaktu,
          @JsonKey(name: 'jumlah_pembayaran') final int? jumlahPembayaran,
          @JsonKey(name: 'payment_url') final String? paymentUrl}) =
      _$_OfferingModel;

  factory _OfferingModel.fromJson(Map<String, dynamic> json) =
      _$_OfferingModel.fromJson;

  @override
  @JsonKey(name: 'batas_waktu')
  String? get batasWaktu;
  @override
  @JsonKey(name: 'jumlah_pembayaran')
  int? get jumlahPembayaran;
  @override
  @JsonKey(name: 'payment_url')
  String? get paymentUrl;
  @override
  @JsonKey(ignore: true)
  _$$_OfferingModelCopyWith<_$_OfferingModel> get copyWith =>
      throw _privateConstructorUsedError;
}

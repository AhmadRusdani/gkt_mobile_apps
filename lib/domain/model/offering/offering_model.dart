import 'package:freezed_annotation/freezed_annotation.dart';

part 'offering_model.freezed.dart';
part 'offering_model.g.dart';

@freezed
class OfferingModel with _$OfferingModel {
  factory OfferingModel({
    @JsonKey(name: 'batas_waktu') String? batasWaktu,
    @JsonKey(name: 'jumlah_pembayaran') int? jumlahPembayaran,
    @JsonKey(name: 'payment_url') String? paymentUrl,
  }) = _OfferingModel;

  factory OfferingModel.fromJson(Map<String, dynamic> json) =>
      _$OfferingModelFromJson(json);
}

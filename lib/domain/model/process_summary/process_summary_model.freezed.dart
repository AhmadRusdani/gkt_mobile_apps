// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'process_summary_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ProcessSummaryModel _$ProcessSummaryModelFromJson(Map<String, dynamic> json) {
  return _ProcessSummaryModel.fromJson(json);
}

/// @nodoc
mixin _$ProcessSummaryModel {
  int? get id => throw _privateConstructorUsedError;
  @JsonKey(name: 'nama_proses')
  String? get namaProses => throw _privateConstructorUsedError;
  String? get fase => throw _privateConstructorUsedError;
  int? get persentase => throw _privateConstructorUsedError;
  @JsonKey(name: 'created_at')
  String? get createdAt => throw _privateConstructorUsedError;
  @JsonKey(name: 'updated_at')
  String? get updatedAt => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ProcessSummaryModelCopyWith<ProcessSummaryModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProcessSummaryModelCopyWith<$Res> {
  factory $ProcessSummaryModelCopyWith(
          ProcessSummaryModel value, $Res Function(ProcessSummaryModel) then) =
      _$ProcessSummaryModelCopyWithImpl<$Res, ProcessSummaryModel>;
  @useResult
  $Res call(
      {int? id,
      @JsonKey(name: 'nama_proses') String? namaProses,
      String? fase,
      int? persentase,
      @JsonKey(name: 'created_at') String? createdAt,
      @JsonKey(name: 'updated_at') String? updatedAt});
}

/// @nodoc
class _$ProcessSummaryModelCopyWithImpl<$Res, $Val extends ProcessSummaryModel>
    implements $ProcessSummaryModelCopyWith<$Res> {
  _$ProcessSummaryModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? namaProses = freezed,
    Object? fase = freezed,
    Object? persentase = freezed,
    Object? createdAt = freezed,
    Object? updatedAt = freezed,
  }) {
    return _then(_value.copyWith(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      namaProses: freezed == namaProses
          ? _value.namaProses
          : namaProses // ignore: cast_nullable_to_non_nullable
              as String?,
      fase: freezed == fase
          ? _value.fase
          : fase // ignore: cast_nullable_to_non_nullable
              as String?,
      persentase: freezed == persentase
          ? _value.persentase
          : persentase // ignore: cast_nullable_to_non_nullable
              as int?,
      createdAt: freezed == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String?,
      updatedAt: freezed == updatedAt
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ProcessSummaryModelCopyWith<$Res>
    implements $ProcessSummaryModelCopyWith<$Res> {
  factory _$$_ProcessSummaryModelCopyWith(_$_ProcessSummaryModel value,
          $Res Function(_$_ProcessSummaryModel) then) =
      __$$_ProcessSummaryModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int? id,
      @JsonKey(name: 'nama_proses') String? namaProses,
      String? fase,
      int? persentase,
      @JsonKey(name: 'created_at') String? createdAt,
      @JsonKey(name: 'updated_at') String? updatedAt});
}

/// @nodoc
class __$$_ProcessSummaryModelCopyWithImpl<$Res>
    extends _$ProcessSummaryModelCopyWithImpl<$Res, _$_ProcessSummaryModel>
    implements _$$_ProcessSummaryModelCopyWith<$Res> {
  __$$_ProcessSummaryModelCopyWithImpl(_$_ProcessSummaryModel _value,
      $Res Function(_$_ProcessSummaryModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? namaProses = freezed,
    Object? fase = freezed,
    Object? persentase = freezed,
    Object? createdAt = freezed,
    Object? updatedAt = freezed,
  }) {
    return _then(_$_ProcessSummaryModel(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      namaProses: freezed == namaProses
          ? _value.namaProses
          : namaProses // ignore: cast_nullable_to_non_nullable
              as String?,
      fase: freezed == fase
          ? _value.fase
          : fase // ignore: cast_nullable_to_non_nullable
              as String?,
      persentase: freezed == persentase
          ? _value.persentase
          : persentase // ignore: cast_nullable_to_non_nullable
              as int?,
      createdAt: freezed == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String?,
      updatedAt: freezed == updatedAt
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ProcessSummaryModel implements _ProcessSummaryModel {
  _$_ProcessSummaryModel(
      {this.id,
      @JsonKey(name: 'nama_proses') this.namaProses,
      this.fase,
      this.persentase,
      @JsonKey(name: 'created_at') this.createdAt,
      @JsonKey(name: 'updated_at') this.updatedAt});

  factory _$_ProcessSummaryModel.fromJson(Map<String, dynamic> json) =>
      _$$_ProcessSummaryModelFromJson(json);

  @override
  final int? id;
  @override
  @JsonKey(name: 'nama_proses')
  final String? namaProses;
  @override
  final String? fase;
  @override
  final int? persentase;
  @override
  @JsonKey(name: 'created_at')
  final String? createdAt;
  @override
  @JsonKey(name: 'updated_at')
  final String? updatedAt;

  @override
  String toString() {
    return 'ProcessSummaryModel(id: $id, namaProses: $namaProses, fase: $fase, persentase: $persentase, createdAt: $createdAt, updatedAt: $updatedAt)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ProcessSummaryModel &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.namaProses, namaProses) ||
                other.namaProses == namaProses) &&
            (identical(other.fase, fase) || other.fase == fase) &&
            (identical(other.persentase, persentase) ||
                other.persentase == persentase) &&
            (identical(other.createdAt, createdAt) ||
                other.createdAt == createdAt) &&
            (identical(other.updatedAt, updatedAt) ||
                other.updatedAt == updatedAt));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType, id, namaProses, fase, persentase, createdAt, updatedAt);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ProcessSummaryModelCopyWith<_$_ProcessSummaryModel> get copyWith =>
      __$$_ProcessSummaryModelCopyWithImpl<_$_ProcessSummaryModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ProcessSummaryModelToJson(
      this,
    );
  }
}

abstract class _ProcessSummaryModel implements ProcessSummaryModel {
  factory _ProcessSummaryModel(
          {final int? id,
          @JsonKey(name: 'nama_proses') final String? namaProses,
          final String? fase,
          final int? persentase,
          @JsonKey(name: 'created_at') final String? createdAt,
          @JsonKey(name: 'updated_at') final String? updatedAt}) =
      _$_ProcessSummaryModel;

  factory _ProcessSummaryModel.fromJson(Map<String, dynamic> json) =
      _$_ProcessSummaryModel.fromJson;

  @override
  int? get id;
  @override
  @JsonKey(name: 'nama_proses')
  String? get namaProses;
  @override
  String? get fase;
  @override
  int? get persentase;
  @override
  @JsonKey(name: 'created_at')
  String? get createdAt;
  @override
  @JsonKey(name: 'updated_at')
  String? get updatedAt;
  @override
  @JsonKey(ignore: true)
  _$$_ProcessSummaryModelCopyWith<_$_ProcessSummaryModel> get copyWith =>
      throw _privateConstructorUsedError;
}

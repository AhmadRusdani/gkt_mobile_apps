import 'package:freezed_annotation/freezed_annotation.dart';

part 'process_summary_model.freezed.dart';
part 'process_summary_model.g.dart';

@freezed
class ProcessSummaryModel with _$ProcessSummaryModel {
  factory ProcessSummaryModel({
    int? id,
    @JsonKey(name: 'nama_proses') String? namaProses,
    String? fase,
    int? persentase,
    @JsonKey(name: 'created_at') String? createdAt,
    @JsonKey(name: 'updated_at') String? updatedAt,
  }) = _ProcessSummaryModel;

  factory ProcessSummaryModel.fromJson(Map<String, dynamic> json) =>
      _$ProcessSummaryModelFromJson(json);
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'process_summary_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ProcessSummaryModel _$$_ProcessSummaryModelFromJson(
        Map<String, dynamic> json) =>
    _$_ProcessSummaryModel(
      id: json['id'] as int?,
      namaProses: json['nama_proses'] as String?,
      fase: json['fase'] as String?,
      persentase: json['persentase'] as int?,
      createdAt: json['created_at'] as String?,
      updatedAt: json['updated_at'] as String?,
    );

Map<String, dynamic> _$$_ProcessSummaryModelToJson(
        _$_ProcessSummaryModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'nama_proses': instance.namaProses,
      'fase': instance.fase,
      'persentase': instance.persentase,
      'created_at': instance.createdAt,
      'updated_at': instance.updatedAt,
    };

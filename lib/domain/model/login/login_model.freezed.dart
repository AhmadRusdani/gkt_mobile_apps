// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'login_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

LoginModel _$LoginModelFromJson(Map<String, dynamic> json) {
  return _LoginModel.fromJson(json);
}

/// @nodoc
mixin _$LoginModel {
  int? get id => throw _privateConstructorUsedError;
  @JsonKey(name: 'no_anggota')
  String? get noAnggota => throw _privateConstructorUsedError;
  @JsonKey(name: 'nama_anggota')
  String? get namaAnggota => throw _privateConstructorUsedError;
  @JsonKey(name: 'last_login_date')
  String? get lastLoginDate => throw _privateConstructorUsedError;
  @JsonKey(name: 'is_login')
  int? get isLogin => throw _privateConstructorUsedError;
  @JsonKey(name: 'updated_at')
  String? get updatedAt => throw _privateConstructorUsedError;
  @JsonKey(name: 'access_token')
  String? get accessToken => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $LoginModelCopyWith<LoginModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LoginModelCopyWith<$Res> {
  factory $LoginModelCopyWith(
          LoginModel value, $Res Function(LoginModel) then) =
      _$LoginModelCopyWithImpl<$Res, LoginModel>;
  @useResult
  $Res call(
      {int? id,
      @JsonKey(name: 'no_anggota') String? noAnggota,
      @JsonKey(name: 'nama_anggota') String? namaAnggota,
      @JsonKey(name: 'last_login_date') String? lastLoginDate,
      @JsonKey(name: 'is_login') int? isLogin,
      @JsonKey(name: 'updated_at') String? updatedAt,
      @JsonKey(name: 'access_token') String? accessToken});
}

/// @nodoc
class _$LoginModelCopyWithImpl<$Res, $Val extends LoginModel>
    implements $LoginModelCopyWith<$Res> {
  _$LoginModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? noAnggota = freezed,
    Object? namaAnggota = freezed,
    Object? lastLoginDate = freezed,
    Object? isLogin = freezed,
    Object? updatedAt = freezed,
    Object? accessToken = freezed,
  }) {
    return _then(_value.copyWith(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      noAnggota: freezed == noAnggota
          ? _value.noAnggota
          : noAnggota // ignore: cast_nullable_to_non_nullable
              as String?,
      namaAnggota: freezed == namaAnggota
          ? _value.namaAnggota
          : namaAnggota // ignore: cast_nullable_to_non_nullable
              as String?,
      lastLoginDate: freezed == lastLoginDate
          ? _value.lastLoginDate
          : lastLoginDate // ignore: cast_nullable_to_non_nullable
              as String?,
      isLogin: freezed == isLogin
          ? _value.isLogin
          : isLogin // ignore: cast_nullable_to_non_nullable
              as int?,
      updatedAt: freezed == updatedAt
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as String?,
      accessToken: freezed == accessToken
          ? _value.accessToken
          : accessToken // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_LoginModelCopyWith<$Res>
    implements $LoginModelCopyWith<$Res> {
  factory _$$_LoginModelCopyWith(
          _$_LoginModel value, $Res Function(_$_LoginModel) then) =
      __$$_LoginModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int? id,
      @JsonKey(name: 'no_anggota') String? noAnggota,
      @JsonKey(name: 'nama_anggota') String? namaAnggota,
      @JsonKey(name: 'last_login_date') String? lastLoginDate,
      @JsonKey(name: 'is_login') int? isLogin,
      @JsonKey(name: 'updated_at') String? updatedAt,
      @JsonKey(name: 'access_token') String? accessToken});
}

/// @nodoc
class __$$_LoginModelCopyWithImpl<$Res>
    extends _$LoginModelCopyWithImpl<$Res, _$_LoginModel>
    implements _$$_LoginModelCopyWith<$Res> {
  __$$_LoginModelCopyWithImpl(
      _$_LoginModel _value, $Res Function(_$_LoginModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? noAnggota = freezed,
    Object? namaAnggota = freezed,
    Object? lastLoginDate = freezed,
    Object? isLogin = freezed,
    Object? updatedAt = freezed,
    Object? accessToken = freezed,
  }) {
    return _then(_$_LoginModel(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      noAnggota: freezed == noAnggota
          ? _value.noAnggota
          : noAnggota // ignore: cast_nullable_to_non_nullable
              as String?,
      namaAnggota: freezed == namaAnggota
          ? _value.namaAnggota
          : namaAnggota // ignore: cast_nullable_to_non_nullable
              as String?,
      lastLoginDate: freezed == lastLoginDate
          ? _value.lastLoginDate
          : lastLoginDate // ignore: cast_nullable_to_non_nullable
              as String?,
      isLogin: freezed == isLogin
          ? _value.isLogin
          : isLogin // ignore: cast_nullable_to_non_nullable
              as int?,
      updatedAt: freezed == updatedAt
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as String?,
      accessToken: freezed == accessToken
          ? _value.accessToken
          : accessToken // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_LoginModel implements _LoginModel {
  const _$_LoginModel(
      {this.id,
      @JsonKey(name: 'no_anggota') this.noAnggota,
      @JsonKey(name: 'nama_anggota') this.namaAnggota,
      @JsonKey(name: 'last_login_date') this.lastLoginDate,
      @JsonKey(name: 'is_login') this.isLogin,
      @JsonKey(name: 'updated_at') this.updatedAt,
      @JsonKey(name: 'access_token') this.accessToken});

  factory _$_LoginModel.fromJson(Map<String, dynamic> json) =>
      _$$_LoginModelFromJson(json);

  @override
  final int? id;
  @override
  @JsonKey(name: 'no_anggota')
  final String? noAnggota;
  @override
  @JsonKey(name: 'nama_anggota')
  final String? namaAnggota;
  @override
  @JsonKey(name: 'last_login_date')
  final String? lastLoginDate;
  @override
  @JsonKey(name: 'is_login')
  final int? isLogin;
  @override
  @JsonKey(name: 'updated_at')
  final String? updatedAt;
  @override
  @JsonKey(name: 'access_token')
  final String? accessToken;

  @override
  String toString() {
    return 'LoginModel(id: $id, noAnggota: $noAnggota, namaAnggota: $namaAnggota, lastLoginDate: $lastLoginDate, isLogin: $isLogin, updatedAt: $updatedAt, accessToken: $accessToken)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_LoginModel &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.noAnggota, noAnggota) ||
                other.noAnggota == noAnggota) &&
            (identical(other.namaAnggota, namaAnggota) ||
                other.namaAnggota == namaAnggota) &&
            (identical(other.lastLoginDate, lastLoginDate) ||
                other.lastLoginDate == lastLoginDate) &&
            (identical(other.isLogin, isLogin) || other.isLogin == isLogin) &&
            (identical(other.updatedAt, updatedAt) ||
                other.updatedAt == updatedAt) &&
            (identical(other.accessToken, accessToken) ||
                other.accessToken == accessToken));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, noAnggota, namaAnggota,
      lastLoginDate, isLogin, updatedAt, accessToken);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_LoginModelCopyWith<_$_LoginModel> get copyWith =>
      __$$_LoginModelCopyWithImpl<_$_LoginModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_LoginModelToJson(
      this,
    );
  }
}

abstract class _LoginModel implements LoginModel {
  const factory _LoginModel(
          {final int? id,
          @JsonKey(name: 'no_anggota') final String? noAnggota,
          @JsonKey(name: 'nama_anggota') final String? namaAnggota,
          @JsonKey(name: 'last_login_date') final String? lastLoginDate,
          @JsonKey(name: 'is_login') final int? isLogin,
          @JsonKey(name: 'updated_at') final String? updatedAt,
          @JsonKey(name: 'access_token') final String? accessToken}) =
      _$_LoginModel;

  factory _LoginModel.fromJson(Map<String, dynamic> json) =
      _$_LoginModel.fromJson;

  @override
  int? get id;
  @override
  @JsonKey(name: 'no_anggota')
  String? get noAnggota;
  @override
  @JsonKey(name: 'nama_anggota')
  String? get namaAnggota;
  @override
  @JsonKey(name: 'last_login_date')
  String? get lastLoginDate;
  @override
  @JsonKey(name: 'is_login')
  int? get isLogin;
  @override
  @JsonKey(name: 'updated_at')
  String? get updatedAt;
  @override
  @JsonKey(name: 'access_token')
  String? get accessToken;
  @override
  @JsonKey(ignore: true)
  _$$_LoginModelCopyWith<_$_LoginModel> get copyWith =>
      throw _privateConstructorUsedError;
}

import 'package:freezed_annotation/freezed_annotation.dart';

part 'login_model.freezed.dart';
part 'login_model.g.dart';

@freezed
class LoginModel with _$LoginModel {
  const factory LoginModel({
    int? id,
    @JsonKey(name: 'no_anggota') String? noAnggota,
    @JsonKey(name: 'nama_anggota') String? namaAnggota,
    @JsonKey(name: 'last_login_date') String? lastLoginDate,
    @JsonKey(name: 'is_login') int? isLogin,
    @JsonKey(name: 'updated_at') String? updatedAt,
    @JsonKey(name: 'access_token') String? accessToken,
  }) = _LoginModel;

  factory LoginModel.fromJson(Map<String, dynamic> json) =>
      _$LoginModelFromJson(json);
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_LoginModel _$$_LoginModelFromJson(Map<String, dynamic> json) =>
    _$_LoginModel(
      id: json['id'] as int?,
      noAnggota: json['no_anggota'] as String?,
      namaAnggota: json['nama_anggota'] as String?,
      lastLoginDate: json['last_login_date'] as String?,
      isLogin: json['is_login'] as int?,
      updatedAt: json['updated_at'] as String?,
      accessToken: json['access_token'] as String?,
    );

Map<String, dynamic> _$$_LoginModelToJson(_$_LoginModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'no_anggota': instance.noAnggota,
      'nama_anggota': instance.namaAnggota,
      'last_login_date': instance.lastLoginDate,
      'is_login': instance.isLogin,
      'updated_at': instance.updatedAt,
      'access_token': instance.accessToken,
    };

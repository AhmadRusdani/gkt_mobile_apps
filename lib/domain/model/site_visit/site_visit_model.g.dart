// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'site_visit_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_SiteVisitModel _$$_SiteVisitModelFromJson(Map<String, dynamic> json) =>
    _$_SiteVisitModel(
      id: json['id'] as int?,
      nama: json['nama'] as String?,
      tanggalBerlangsung: json['tanggal_berlangsung'] as String?,
      latitude: (json['latitude'] as num?)?.toDouble(),
      longitude: (json['longitude'] as num?)?.toDouble(),
      createdAt: json['created_at'] as String?,
      updatedAt: json['updated_at'] as String?,
    );

Map<String, dynamic> _$$_SiteVisitModelToJson(_$_SiteVisitModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'nama': instance.nama,
      'tanggal_berlangsung': instance.tanggalBerlangsung,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'created_at': instance.createdAt,
      'updated_at': instance.updatedAt,
    };

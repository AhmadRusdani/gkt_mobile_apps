import 'package:freezed_annotation/freezed_annotation.dart';

part 'site_visit_model.freezed.dart';
part 'site_visit_model.g.dart';

@freezed
class SiteVisitModel with _$SiteVisitModel {
  const factory SiteVisitModel({
    int? id,
    String? nama,
    @JsonKey(name: 'tanggal_berlangsung') String? tanggalBerlangsung,
    double? latitude,
    double? longitude,
    @JsonKey(name: 'created_at') String? createdAt,
    @JsonKey(name: 'updated_at') String? updatedAt,
  }) = _SiteVisitModel;

  factory SiteVisitModel.fromJson(Map<String, dynamic> json) =>
      _$SiteVisitModelFromJson(json);
}

// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'site_visit_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

SiteVisitModel _$SiteVisitModelFromJson(Map<String, dynamic> json) {
  return _SiteVisitModel.fromJson(json);
}

/// @nodoc
mixin _$SiteVisitModel {
  int? get id => throw _privateConstructorUsedError;
  String? get nama => throw _privateConstructorUsedError;
  @JsonKey(name: 'tanggal_berlangsung')
  String? get tanggalBerlangsung => throw _privateConstructorUsedError;
  double? get latitude => throw _privateConstructorUsedError;
  double? get longitude => throw _privateConstructorUsedError;
  @JsonKey(name: 'created_at')
  String? get createdAt => throw _privateConstructorUsedError;
  @JsonKey(name: 'updated_at')
  String? get updatedAt => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $SiteVisitModelCopyWith<SiteVisitModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SiteVisitModelCopyWith<$Res> {
  factory $SiteVisitModelCopyWith(
          SiteVisitModel value, $Res Function(SiteVisitModel) then) =
      _$SiteVisitModelCopyWithImpl<$Res, SiteVisitModel>;
  @useResult
  $Res call(
      {int? id,
      String? nama,
      @JsonKey(name: 'tanggal_berlangsung') String? tanggalBerlangsung,
      double? latitude,
      double? longitude,
      @JsonKey(name: 'created_at') String? createdAt,
      @JsonKey(name: 'updated_at') String? updatedAt});
}

/// @nodoc
class _$SiteVisitModelCopyWithImpl<$Res, $Val extends SiteVisitModel>
    implements $SiteVisitModelCopyWith<$Res> {
  _$SiteVisitModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? nama = freezed,
    Object? tanggalBerlangsung = freezed,
    Object? latitude = freezed,
    Object? longitude = freezed,
    Object? createdAt = freezed,
    Object? updatedAt = freezed,
  }) {
    return _then(_value.copyWith(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      nama: freezed == nama
          ? _value.nama
          : nama // ignore: cast_nullable_to_non_nullable
              as String?,
      tanggalBerlangsung: freezed == tanggalBerlangsung
          ? _value.tanggalBerlangsung
          : tanggalBerlangsung // ignore: cast_nullable_to_non_nullable
              as String?,
      latitude: freezed == latitude
          ? _value.latitude
          : latitude // ignore: cast_nullable_to_non_nullable
              as double?,
      longitude: freezed == longitude
          ? _value.longitude
          : longitude // ignore: cast_nullable_to_non_nullable
              as double?,
      createdAt: freezed == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String?,
      updatedAt: freezed == updatedAt
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_SiteVisitModelCopyWith<$Res>
    implements $SiteVisitModelCopyWith<$Res> {
  factory _$$_SiteVisitModelCopyWith(
          _$_SiteVisitModel value, $Res Function(_$_SiteVisitModel) then) =
      __$$_SiteVisitModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int? id,
      String? nama,
      @JsonKey(name: 'tanggal_berlangsung') String? tanggalBerlangsung,
      double? latitude,
      double? longitude,
      @JsonKey(name: 'created_at') String? createdAt,
      @JsonKey(name: 'updated_at') String? updatedAt});
}

/// @nodoc
class __$$_SiteVisitModelCopyWithImpl<$Res>
    extends _$SiteVisitModelCopyWithImpl<$Res, _$_SiteVisitModel>
    implements _$$_SiteVisitModelCopyWith<$Res> {
  __$$_SiteVisitModelCopyWithImpl(
      _$_SiteVisitModel _value, $Res Function(_$_SiteVisitModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? nama = freezed,
    Object? tanggalBerlangsung = freezed,
    Object? latitude = freezed,
    Object? longitude = freezed,
    Object? createdAt = freezed,
    Object? updatedAt = freezed,
  }) {
    return _then(_$_SiteVisitModel(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      nama: freezed == nama
          ? _value.nama
          : nama // ignore: cast_nullable_to_non_nullable
              as String?,
      tanggalBerlangsung: freezed == tanggalBerlangsung
          ? _value.tanggalBerlangsung
          : tanggalBerlangsung // ignore: cast_nullable_to_non_nullable
              as String?,
      latitude: freezed == latitude
          ? _value.latitude
          : latitude // ignore: cast_nullable_to_non_nullable
              as double?,
      longitude: freezed == longitude
          ? _value.longitude
          : longitude // ignore: cast_nullable_to_non_nullable
              as double?,
      createdAt: freezed == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String?,
      updatedAt: freezed == updatedAt
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_SiteVisitModel implements _SiteVisitModel {
  const _$_SiteVisitModel(
      {this.id,
      this.nama,
      @JsonKey(name: 'tanggal_berlangsung') this.tanggalBerlangsung,
      this.latitude,
      this.longitude,
      @JsonKey(name: 'created_at') this.createdAt,
      @JsonKey(name: 'updated_at') this.updatedAt});

  factory _$_SiteVisitModel.fromJson(Map<String, dynamic> json) =>
      _$$_SiteVisitModelFromJson(json);

  @override
  final int? id;
  @override
  final String? nama;
  @override
  @JsonKey(name: 'tanggal_berlangsung')
  final String? tanggalBerlangsung;
  @override
  final double? latitude;
  @override
  final double? longitude;
  @override
  @JsonKey(name: 'created_at')
  final String? createdAt;
  @override
  @JsonKey(name: 'updated_at')
  final String? updatedAt;

  @override
  String toString() {
    return 'SiteVisitModel(id: $id, nama: $nama, tanggalBerlangsung: $tanggalBerlangsung, latitude: $latitude, longitude: $longitude, createdAt: $createdAt, updatedAt: $updatedAt)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SiteVisitModel &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.nama, nama) || other.nama == nama) &&
            (identical(other.tanggalBerlangsung, tanggalBerlangsung) ||
                other.tanggalBerlangsung == tanggalBerlangsung) &&
            (identical(other.latitude, latitude) ||
                other.latitude == latitude) &&
            (identical(other.longitude, longitude) ||
                other.longitude == longitude) &&
            (identical(other.createdAt, createdAt) ||
                other.createdAt == createdAt) &&
            (identical(other.updatedAt, updatedAt) ||
                other.updatedAt == updatedAt));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, nama, tanggalBerlangsung,
      latitude, longitude, createdAt, updatedAt);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SiteVisitModelCopyWith<_$_SiteVisitModel> get copyWith =>
      __$$_SiteVisitModelCopyWithImpl<_$_SiteVisitModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_SiteVisitModelToJson(
      this,
    );
  }
}

abstract class _SiteVisitModel implements SiteVisitModel {
  const factory _SiteVisitModel(
      {final int? id,
      final String? nama,
      @JsonKey(name: 'tanggal_berlangsung') final String? tanggalBerlangsung,
      final double? latitude,
      final double? longitude,
      @JsonKey(name: 'created_at') final String? createdAt,
      @JsonKey(name: 'updated_at')
      final String? updatedAt}) = _$_SiteVisitModel;

  factory _SiteVisitModel.fromJson(Map<String, dynamic> json) =
      _$_SiteVisitModel.fromJson;

  @override
  int? get id;
  @override
  String? get nama;
  @override
  @JsonKey(name: 'tanggal_berlangsung')
  String? get tanggalBerlangsung;
  @override
  double? get latitude;
  @override
  double? get longitude;
  @override
  @JsonKey(name: 'created_at')
  String? get createdAt;
  @override
  @JsonKey(name: 'updated_at')
  String? get updatedAt;
  @override
  @JsonKey(ignore: true)
  _$$_SiteVisitModelCopyWith<_$_SiteVisitModel> get copyWith =>
      throw _privateConstructorUsedError;
}

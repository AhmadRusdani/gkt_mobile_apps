// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_UserModel _$$_UserModelFromJson(Map<String, dynamic> json) => _$_UserModel(
      id: json['id'] as int?,
      noAnggota: json['no_anggota'] as String?,
      namaAnggota: json['nama_anggota'] as String?,
      lastLoginDate: json['last_login_date'] as String?,
      isLogin: json['is_login'] as int?,
      isActive: json['is_active'] as int?,
      isDeleted: json['is_deleted'] as int?,
      createdAt: json['created_at'] as String?,
      updatedAt: json['updated_at'] as String?,
      detail: json['detail'] == null
          ? null
          : UserDetailModel.fromJson(json['detail'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_UserModelToJson(_$_UserModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'no_anggota': instance.noAnggota,
      'nama_anggota': instance.namaAnggota,
      'last_login_date': instance.lastLoginDate,
      'is_login': instance.isLogin,
      'is_active': instance.isActive,
      'is_deleted': instance.isDeleted,
      'created_at': instance.createdAt,
      'updated_at': instance.updatedAt,
      'detail': instance.detail,
    };

_$_UserDetailModel _$$_UserDetailModelFromJson(Map<String, dynamic> json) =>
    _$_UserDetailModel(
      tanggalLahir: json['tanggal_lahir'] as String?,
      tanggalBaptis: json['tanggal_baptis'] as String?,
      jenisKelamin: json['jenis_kelamin'] as String?,
      isMarried: json['is_married'] as String?,
      pekerjaan: json['pekerjaan'] as String?,
      phoneNumber: json['phone_number'] as String?,
      email: json['email'],
      alamat: json['alamat'] as String?,
      tanggalBergabung: json['tanggal_bergabung'] as String?,
      picId: json['pic_id'] as String?,
      noKtp: json['no_ktp'] as String?,
      tipeAnggota: json['tipe_anggota'] as String?,
      qrCode: json['qr_code'] as String?,
    );

Map<String, dynamic> _$$_UserDetailModelToJson(_$_UserDetailModel instance) =>
    <String, dynamic>{
      'tanggal_lahir': instance.tanggalLahir,
      'tanggal_baptis': instance.tanggalBaptis,
      'jenis_kelamin': instance.jenisKelamin,
      'is_married': instance.isMarried,
      'pekerjaan': instance.pekerjaan,
      'phone_number': instance.phoneNumber,
      'email': instance.email,
      'alamat': instance.alamat,
      'tanggal_bergabung': instance.tanggalBergabung,
      'pic_id': instance.picId,
      'no_ktp': instance.noKtp,
      'tipe_anggota': instance.tipeAnggota,
      'qr_code': instance.qrCode,
    };

_$_ChangeUserDetailModel _$$_ChangeUserDetailModelFromJson(
        Map<String, dynamic> json) =>
    _$_ChangeUserDetailModel(
      phoneNumber: json['phone_number'] as String?,
      alamat: json['alamat'] as String?,
    );

Map<String, dynamic> _$$_ChangeUserDetailModelToJson(
        _$_ChangeUserDetailModel instance) =>
    <String, dynamic>{
      'phone_number': instance.phoneNumber,
      'alamat': instance.alamat,
    };

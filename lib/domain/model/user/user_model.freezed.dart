// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'user_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

UserModel _$UserModelFromJson(Map<String, dynamic> json) {
  return _UserModel.fromJson(json);
}

/// @nodoc
mixin _$UserModel {
  int? get id => throw _privateConstructorUsedError;
  @JsonKey(name: 'no_anggota')
  String? get noAnggota => throw _privateConstructorUsedError;
  @JsonKey(name: 'nama_anggota')
  String? get namaAnggota => throw _privateConstructorUsedError;
  @JsonKey(name: 'last_login_date')
  String? get lastLoginDate => throw _privateConstructorUsedError;
  @JsonKey(name: 'is_login')
  int? get isLogin => throw _privateConstructorUsedError;
  @JsonKey(name: 'is_active')
  int? get isActive => throw _privateConstructorUsedError;
  @JsonKey(name: 'is_deleted')
  int? get isDeleted => throw _privateConstructorUsedError;
  @JsonKey(name: 'created_at')
  String? get createdAt => throw _privateConstructorUsedError;
  @JsonKey(name: 'updated_at')
  String? get updatedAt => throw _privateConstructorUsedError;
  UserDetailModel? get detail => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $UserModelCopyWith<UserModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserModelCopyWith<$Res> {
  factory $UserModelCopyWith(UserModel value, $Res Function(UserModel) then) =
      _$UserModelCopyWithImpl<$Res, UserModel>;
  @useResult
  $Res call(
      {int? id,
      @JsonKey(name: 'no_anggota') String? noAnggota,
      @JsonKey(name: 'nama_anggota') String? namaAnggota,
      @JsonKey(name: 'last_login_date') String? lastLoginDate,
      @JsonKey(name: 'is_login') int? isLogin,
      @JsonKey(name: 'is_active') int? isActive,
      @JsonKey(name: 'is_deleted') int? isDeleted,
      @JsonKey(name: 'created_at') String? createdAt,
      @JsonKey(name: 'updated_at') String? updatedAt,
      UserDetailModel? detail});

  $UserDetailModelCopyWith<$Res>? get detail;
}

/// @nodoc
class _$UserModelCopyWithImpl<$Res, $Val extends UserModel>
    implements $UserModelCopyWith<$Res> {
  _$UserModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? noAnggota = freezed,
    Object? namaAnggota = freezed,
    Object? lastLoginDate = freezed,
    Object? isLogin = freezed,
    Object? isActive = freezed,
    Object? isDeleted = freezed,
    Object? createdAt = freezed,
    Object? updatedAt = freezed,
    Object? detail = freezed,
  }) {
    return _then(_value.copyWith(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      noAnggota: freezed == noAnggota
          ? _value.noAnggota
          : noAnggota // ignore: cast_nullable_to_non_nullable
              as String?,
      namaAnggota: freezed == namaAnggota
          ? _value.namaAnggota
          : namaAnggota // ignore: cast_nullable_to_non_nullable
              as String?,
      lastLoginDate: freezed == lastLoginDate
          ? _value.lastLoginDate
          : lastLoginDate // ignore: cast_nullable_to_non_nullable
              as String?,
      isLogin: freezed == isLogin
          ? _value.isLogin
          : isLogin // ignore: cast_nullable_to_non_nullable
              as int?,
      isActive: freezed == isActive
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as int?,
      isDeleted: freezed == isDeleted
          ? _value.isDeleted
          : isDeleted // ignore: cast_nullable_to_non_nullable
              as int?,
      createdAt: freezed == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String?,
      updatedAt: freezed == updatedAt
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as String?,
      detail: freezed == detail
          ? _value.detail
          : detail // ignore: cast_nullable_to_non_nullable
              as UserDetailModel?,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $UserDetailModelCopyWith<$Res>? get detail {
    if (_value.detail == null) {
      return null;
    }

    return $UserDetailModelCopyWith<$Res>(_value.detail!, (value) {
      return _then(_value.copyWith(detail: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_UserModelCopyWith<$Res> implements $UserModelCopyWith<$Res> {
  factory _$$_UserModelCopyWith(
          _$_UserModel value, $Res Function(_$_UserModel) then) =
      __$$_UserModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int? id,
      @JsonKey(name: 'no_anggota') String? noAnggota,
      @JsonKey(name: 'nama_anggota') String? namaAnggota,
      @JsonKey(name: 'last_login_date') String? lastLoginDate,
      @JsonKey(name: 'is_login') int? isLogin,
      @JsonKey(name: 'is_active') int? isActive,
      @JsonKey(name: 'is_deleted') int? isDeleted,
      @JsonKey(name: 'created_at') String? createdAt,
      @JsonKey(name: 'updated_at') String? updatedAt,
      UserDetailModel? detail});

  @override
  $UserDetailModelCopyWith<$Res>? get detail;
}

/// @nodoc
class __$$_UserModelCopyWithImpl<$Res>
    extends _$UserModelCopyWithImpl<$Res, _$_UserModel>
    implements _$$_UserModelCopyWith<$Res> {
  __$$_UserModelCopyWithImpl(
      _$_UserModel _value, $Res Function(_$_UserModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? noAnggota = freezed,
    Object? namaAnggota = freezed,
    Object? lastLoginDate = freezed,
    Object? isLogin = freezed,
    Object? isActive = freezed,
    Object? isDeleted = freezed,
    Object? createdAt = freezed,
    Object? updatedAt = freezed,
    Object? detail = freezed,
  }) {
    return _then(_$_UserModel(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      noAnggota: freezed == noAnggota
          ? _value.noAnggota
          : noAnggota // ignore: cast_nullable_to_non_nullable
              as String?,
      namaAnggota: freezed == namaAnggota
          ? _value.namaAnggota
          : namaAnggota // ignore: cast_nullable_to_non_nullable
              as String?,
      lastLoginDate: freezed == lastLoginDate
          ? _value.lastLoginDate
          : lastLoginDate // ignore: cast_nullable_to_non_nullable
              as String?,
      isLogin: freezed == isLogin
          ? _value.isLogin
          : isLogin // ignore: cast_nullable_to_non_nullable
              as int?,
      isActive: freezed == isActive
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as int?,
      isDeleted: freezed == isDeleted
          ? _value.isDeleted
          : isDeleted // ignore: cast_nullable_to_non_nullable
              as int?,
      createdAt: freezed == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String?,
      updatedAt: freezed == updatedAt
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as String?,
      detail: freezed == detail
          ? _value.detail
          : detail // ignore: cast_nullable_to_non_nullable
              as UserDetailModel?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_UserModel implements _UserModel {
  _$_UserModel(
      {this.id,
      @JsonKey(name: 'no_anggota') this.noAnggota,
      @JsonKey(name: 'nama_anggota') this.namaAnggota,
      @JsonKey(name: 'last_login_date') this.lastLoginDate,
      @JsonKey(name: 'is_login') this.isLogin,
      @JsonKey(name: 'is_active') this.isActive,
      @JsonKey(name: 'is_deleted') this.isDeleted,
      @JsonKey(name: 'created_at') this.createdAt,
      @JsonKey(name: 'updated_at') this.updatedAt,
      this.detail});

  factory _$_UserModel.fromJson(Map<String, dynamic> json) =>
      _$$_UserModelFromJson(json);

  @override
  final int? id;
  @override
  @JsonKey(name: 'no_anggota')
  final String? noAnggota;
  @override
  @JsonKey(name: 'nama_anggota')
  final String? namaAnggota;
  @override
  @JsonKey(name: 'last_login_date')
  final String? lastLoginDate;
  @override
  @JsonKey(name: 'is_login')
  final int? isLogin;
  @override
  @JsonKey(name: 'is_active')
  final int? isActive;
  @override
  @JsonKey(name: 'is_deleted')
  final int? isDeleted;
  @override
  @JsonKey(name: 'created_at')
  final String? createdAt;
  @override
  @JsonKey(name: 'updated_at')
  final String? updatedAt;
  @override
  final UserDetailModel? detail;

  @override
  String toString() {
    return 'UserModel(id: $id, noAnggota: $noAnggota, namaAnggota: $namaAnggota, lastLoginDate: $lastLoginDate, isLogin: $isLogin, isActive: $isActive, isDeleted: $isDeleted, createdAt: $createdAt, updatedAt: $updatedAt, detail: $detail)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_UserModel &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.noAnggota, noAnggota) ||
                other.noAnggota == noAnggota) &&
            (identical(other.namaAnggota, namaAnggota) ||
                other.namaAnggota == namaAnggota) &&
            (identical(other.lastLoginDate, lastLoginDate) ||
                other.lastLoginDate == lastLoginDate) &&
            (identical(other.isLogin, isLogin) || other.isLogin == isLogin) &&
            (identical(other.isActive, isActive) ||
                other.isActive == isActive) &&
            (identical(other.isDeleted, isDeleted) ||
                other.isDeleted == isDeleted) &&
            (identical(other.createdAt, createdAt) ||
                other.createdAt == createdAt) &&
            (identical(other.updatedAt, updatedAt) ||
                other.updatedAt == updatedAt) &&
            (identical(other.detail, detail) || other.detail == detail));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      id,
      noAnggota,
      namaAnggota,
      lastLoginDate,
      isLogin,
      isActive,
      isDeleted,
      createdAt,
      updatedAt,
      detail);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_UserModelCopyWith<_$_UserModel> get copyWith =>
      __$$_UserModelCopyWithImpl<_$_UserModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_UserModelToJson(
      this,
    );
  }
}

abstract class _UserModel implements UserModel {
  factory _UserModel(
      {final int? id,
      @JsonKey(name: 'no_anggota') final String? noAnggota,
      @JsonKey(name: 'nama_anggota') final String? namaAnggota,
      @JsonKey(name: 'last_login_date') final String? lastLoginDate,
      @JsonKey(name: 'is_login') final int? isLogin,
      @JsonKey(name: 'is_active') final int? isActive,
      @JsonKey(name: 'is_deleted') final int? isDeleted,
      @JsonKey(name: 'created_at') final String? createdAt,
      @JsonKey(name: 'updated_at') final String? updatedAt,
      final UserDetailModel? detail}) = _$_UserModel;

  factory _UserModel.fromJson(Map<String, dynamic> json) =
      _$_UserModel.fromJson;

  @override
  int? get id;
  @override
  @JsonKey(name: 'no_anggota')
  String? get noAnggota;
  @override
  @JsonKey(name: 'nama_anggota')
  String? get namaAnggota;
  @override
  @JsonKey(name: 'last_login_date')
  String? get lastLoginDate;
  @override
  @JsonKey(name: 'is_login')
  int? get isLogin;
  @override
  @JsonKey(name: 'is_active')
  int? get isActive;
  @override
  @JsonKey(name: 'is_deleted')
  int? get isDeleted;
  @override
  @JsonKey(name: 'created_at')
  String? get createdAt;
  @override
  @JsonKey(name: 'updated_at')
  String? get updatedAt;
  @override
  UserDetailModel? get detail;
  @override
  @JsonKey(ignore: true)
  _$$_UserModelCopyWith<_$_UserModel> get copyWith =>
      throw _privateConstructorUsedError;
}

UserDetailModel _$UserDetailModelFromJson(Map<String, dynamic> json) {
  return _UserDetailModel.fromJson(json);
}

/// @nodoc
mixin _$UserDetailModel {
  @JsonKey(name: 'tanggal_lahir')
  String? get tanggalLahir => throw _privateConstructorUsedError;
  @JsonKey(name: 'tanggal_baptis')
  String? get tanggalBaptis => throw _privateConstructorUsedError;
  @JsonKey(name: 'jenis_kelamin')
  String? get jenisKelamin => throw _privateConstructorUsedError;
  @JsonKey(name: 'is_married')
  String? get isMarried => throw _privateConstructorUsedError;
  String? get pekerjaan => throw _privateConstructorUsedError;
  @JsonKey(name: 'phone_number')
  String? get phoneNumber => throw _privateConstructorUsedError;
  dynamic get email => throw _privateConstructorUsedError;
  String? get alamat => throw _privateConstructorUsedError;
  @JsonKey(name: 'tanggal_bergabung')
  String? get tanggalBergabung => throw _privateConstructorUsedError;
  @JsonKey(name: 'pic_id')
  String? get picId => throw _privateConstructorUsedError;
  @JsonKey(name: 'no_ktp')
  String? get noKtp => throw _privateConstructorUsedError;
  @JsonKey(name: 'tipe_anggota')
  String? get tipeAnggota => throw _privateConstructorUsedError;
  @JsonKey(name: 'qr_code')
  String? get qrCode => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $UserDetailModelCopyWith<UserDetailModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserDetailModelCopyWith<$Res> {
  factory $UserDetailModelCopyWith(
          UserDetailModel value, $Res Function(UserDetailModel) then) =
      _$UserDetailModelCopyWithImpl<$Res, UserDetailModel>;
  @useResult
  $Res call(
      {@JsonKey(name: 'tanggal_lahir') String? tanggalLahir,
      @JsonKey(name: 'tanggal_baptis') String? tanggalBaptis,
      @JsonKey(name: 'jenis_kelamin') String? jenisKelamin,
      @JsonKey(name: 'is_married') String? isMarried,
      String? pekerjaan,
      @JsonKey(name: 'phone_number') String? phoneNumber,
      dynamic email,
      String? alamat,
      @JsonKey(name: 'tanggal_bergabung') String? tanggalBergabung,
      @JsonKey(name: 'pic_id') String? picId,
      @JsonKey(name: 'no_ktp') String? noKtp,
      @JsonKey(name: 'tipe_anggota') String? tipeAnggota,
      @JsonKey(name: 'qr_code') String? qrCode});
}

/// @nodoc
class _$UserDetailModelCopyWithImpl<$Res, $Val extends UserDetailModel>
    implements $UserDetailModelCopyWith<$Res> {
  _$UserDetailModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? tanggalLahir = freezed,
    Object? tanggalBaptis = freezed,
    Object? jenisKelamin = freezed,
    Object? isMarried = freezed,
    Object? pekerjaan = freezed,
    Object? phoneNumber = freezed,
    Object? email = freezed,
    Object? alamat = freezed,
    Object? tanggalBergabung = freezed,
    Object? picId = freezed,
    Object? noKtp = freezed,
    Object? tipeAnggota = freezed,
    Object? qrCode = freezed,
  }) {
    return _then(_value.copyWith(
      tanggalLahir: freezed == tanggalLahir
          ? _value.tanggalLahir
          : tanggalLahir // ignore: cast_nullable_to_non_nullable
              as String?,
      tanggalBaptis: freezed == tanggalBaptis
          ? _value.tanggalBaptis
          : tanggalBaptis // ignore: cast_nullable_to_non_nullable
              as String?,
      jenisKelamin: freezed == jenisKelamin
          ? _value.jenisKelamin
          : jenisKelamin // ignore: cast_nullable_to_non_nullable
              as String?,
      isMarried: freezed == isMarried
          ? _value.isMarried
          : isMarried // ignore: cast_nullable_to_non_nullable
              as String?,
      pekerjaan: freezed == pekerjaan
          ? _value.pekerjaan
          : pekerjaan // ignore: cast_nullable_to_non_nullable
              as String?,
      phoneNumber: freezed == phoneNumber
          ? _value.phoneNumber
          : phoneNumber // ignore: cast_nullable_to_non_nullable
              as String?,
      email: freezed == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as dynamic,
      alamat: freezed == alamat
          ? _value.alamat
          : alamat // ignore: cast_nullable_to_non_nullable
              as String?,
      tanggalBergabung: freezed == tanggalBergabung
          ? _value.tanggalBergabung
          : tanggalBergabung // ignore: cast_nullable_to_non_nullable
              as String?,
      picId: freezed == picId
          ? _value.picId
          : picId // ignore: cast_nullable_to_non_nullable
              as String?,
      noKtp: freezed == noKtp
          ? _value.noKtp
          : noKtp // ignore: cast_nullable_to_non_nullable
              as String?,
      tipeAnggota: freezed == tipeAnggota
          ? _value.tipeAnggota
          : tipeAnggota // ignore: cast_nullable_to_non_nullable
              as String?,
      qrCode: freezed == qrCode
          ? _value.qrCode
          : qrCode // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_UserDetailModelCopyWith<$Res>
    implements $UserDetailModelCopyWith<$Res> {
  factory _$$_UserDetailModelCopyWith(
          _$_UserDetailModel value, $Res Function(_$_UserDetailModel) then) =
      __$$_UserDetailModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: 'tanggal_lahir') String? tanggalLahir,
      @JsonKey(name: 'tanggal_baptis') String? tanggalBaptis,
      @JsonKey(name: 'jenis_kelamin') String? jenisKelamin,
      @JsonKey(name: 'is_married') String? isMarried,
      String? pekerjaan,
      @JsonKey(name: 'phone_number') String? phoneNumber,
      dynamic email,
      String? alamat,
      @JsonKey(name: 'tanggal_bergabung') String? tanggalBergabung,
      @JsonKey(name: 'pic_id') String? picId,
      @JsonKey(name: 'no_ktp') String? noKtp,
      @JsonKey(name: 'tipe_anggota') String? tipeAnggota,
      @JsonKey(name: 'qr_code') String? qrCode});
}

/// @nodoc
class __$$_UserDetailModelCopyWithImpl<$Res>
    extends _$UserDetailModelCopyWithImpl<$Res, _$_UserDetailModel>
    implements _$$_UserDetailModelCopyWith<$Res> {
  __$$_UserDetailModelCopyWithImpl(
      _$_UserDetailModel _value, $Res Function(_$_UserDetailModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? tanggalLahir = freezed,
    Object? tanggalBaptis = freezed,
    Object? jenisKelamin = freezed,
    Object? isMarried = freezed,
    Object? pekerjaan = freezed,
    Object? phoneNumber = freezed,
    Object? email = freezed,
    Object? alamat = freezed,
    Object? tanggalBergabung = freezed,
    Object? picId = freezed,
    Object? noKtp = freezed,
    Object? tipeAnggota = freezed,
    Object? qrCode = freezed,
  }) {
    return _then(_$_UserDetailModel(
      tanggalLahir: freezed == tanggalLahir
          ? _value.tanggalLahir
          : tanggalLahir // ignore: cast_nullable_to_non_nullable
              as String?,
      tanggalBaptis: freezed == tanggalBaptis
          ? _value.tanggalBaptis
          : tanggalBaptis // ignore: cast_nullable_to_non_nullable
              as String?,
      jenisKelamin: freezed == jenisKelamin
          ? _value.jenisKelamin
          : jenisKelamin // ignore: cast_nullable_to_non_nullable
              as String?,
      isMarried: freezed == isMarried
          ? _value.isMarried
          : isMarried // ignore: cast_nullable_to_non_nullable
              as String?,
      pekerjaan: freezed == pekerjaan
          ? _value.pekerjaan
          : pekerjaan // ignore: cast_nullable_to_non_nullable
              as String?,
      phoneNumber: freezed == phoneNumber
          ? _value.phoneNumber
          : phoneNumber // ignore: cast_nullable_to_non_nullable
              as String?,
      email: freezed == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as dynamic,
      alamat: freezed == alamat
          ? _value.alamat
          : alamat // ignore: cast_nullable_to_non_nullable
              as String?,
      tanggalBergabung: freezed == tanggalBergabung
          ? _value.tanggalBergabung
          : tanggalBergabung // ignore: cast_nullable_to_non_nullable
              as String?,
      picId: freezed == picId
          ? _value.picId
          : picId // ignore: cast_nullable_to_non_nullable
              as String?,
      noKtp: freezed == noKtp
          ? _value.noKtp
          : noKtp // ignore: cast_nullable_to_non_nullable
              as String?,
      tipeAnggota: freezed == tipeAnggota
          ? _value.tipeAnggota
          : tipeAnggota // ignore: cast_nullable_to_non_nullable
              as String?,
      qrCode: freezed == qrCode
          ? _value.qrCode
          : qrCode // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_UserDetailModel implements _UserDetailModel {
  _$_UserDetailModel(
      {@JsonKey(name: 'tanggal_lahir') this.tanggalLahir,
      @JsonKey(name: 'tanggal_baptis') this.tanggalBaptis,
      @JsonKey(name: 'jenis_kelamin') this.jenisKelamin,
      @JsonKey(name: 'is_married') this.isMarried,
      this.pekerjaan,
      @JsonKey(name: 'phone_number') this.phoneNumber,
      this.email,
      this.alamat,
      @JsonKey(name: 'tanggal_bergabung') this.tanggalBergabung,
      @JsonKey(name: 'pic_id') this.picId,
      @JsonKey(name: 'no_ktp') this.noKtp,
      @JsonKey(name: 'tipe_anggota') this.tipeAnggota,
      @JsonKey(name: 'qr_code') this.qrCode});

  factory _$_UserDetailModel.fromJson(Map<String, dynamic> json) =>
      _$$_UserDetailModelFromJson(json);

  @override
  @JsonKey(name: 'tanggal_lahir')
  final String? tanggalLahir;
  @override
  @JsonKey(name: 'tanggal_baptis')
  final String? tanggalBaptis;
  @override
  @JsonKey(name: 'jenis_kelamin')
  final String? jenisKelamin;
  @override
  @JsonKey(name: 'is_married')
  final String? isMarried;
  @override
  final String? pekerjaan;
  @override
  @JsonKey(name: 'phone_number')
  final String? phoneNumber;
  @override
  final dynamic email;
  @override
  final String? alamat;
  @override
  @JsonKey(name: 'tanggal_bergabung')
  final String? tanggalBergabung;
  @override
  @JsonKey(name: 'pic_id')
  final String? picId;
  @override
  @JsonKey(name: 'no_ktp')
  final String? noKtp;
  @override
  @JsonKey(name: 'tipe_anggota')
  final String? tipeAnggota;
  @override
  @JsonKey(name: 'qr_code')
  final String? qrCode;

  @override
  String toString() {
    return 'UserDetailModel(tanggalLahir: $tanggalLahir, tanggalBaptis: $tanggalBaptis, jenisKelamin: $jenisKelamin, isMarried: $isMarried, pekerjaan: $pekerjaan, phoneNumber: $phoneNumber, email: $email, alamat: $alamat, tanggalBergabung: $tanggalBergabung, picId: $picId, noKtp: $noKtp, tipeAnggota: $tipeAnggota, qrCode: $qrCode)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_UserDetailModel &&
            (identical(other.tanggalLahir, tanggalLahir) ||
                other.tanggalLahir == tanggalLahir) &&
            (identical(other.tanggalBaptis, tanggalBaptis) ||
                other.tanggalBaptis == tanggalBaptis) &&
            (identical(other.jenisKelamin, jenisKelamin) ||
                other.jenisKelamin == jenisKelamin) &&
            (identical(other.isMarried, isMarried) ||
                other.isMarried == isMarried) &&
            (identical(other.pekerjaan, pekerjaan) ||
                other.pekerjaan == pekerjaan) &&
            (identical(other.phoneNumber, phoneNumber) ||
                other.phoneNumber == phoneNumber) &&
            const DeepCollectionEquality().equals(other.email, email) &&
            (identical(other.alamat, alamat) || other.alamat == alamat) &&
            (identical(other.tanggalBergabung, tanggalBergabung) ||
                other.tanggalBergabung == tanggalBergabung) &&
            (identical(other.picId, picId) || other.picId == picId) &&
            (identical(other.noKtp, noKtp) || other.noKtp == noKtp) &&
            (identical(other.tipeAnggota, tipeAnggota) ||
                other.tipeAnggota == tipeAnggota) &&
            (identical(other.qrCode, qrCode) || other.qrCode == qrCode));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      tanggalLahir,
      tanggalBaptis,
      jenisKelamin,
      isMarried,
      pekerjaan,
      phoneNumber,
      const DeepCollectionEquality().hash(email),
      alamat,
      tanggalBergabung,
      picId,
      noKtp,
      tipeAnggota,
      qrCode);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_UserDetailModelCopyWith<_$_UserDetailModel> get copyWith =>
      __$$_UserDetailModelCopyWithImpl<_$_UserDetailModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_UserDetailModelToJson(
      this,
    );
  }
}

abstract class _UserDetailModel implements UserDetailModel {
  factory _UserDetailModel(
      {@JsonKey(name: 'tanggal_lahir') final String? tanggalLahir,
      @JsonKey(name: 'tanggal_baptis') final String? tanggalBaptis,
      @JsonKey(name: 'jenis_kelamin') final String? jenisKelamin,
      @JsonKey(name: 'is_married') final String? isMarried,
      final String? pekerjaan,
      @JsonKey(name: 'phone_number') final String? phoneNumber,
      final dynamic email,
      final String? alamat,
      @JsonKey(name: 'tanggal_bergabung') final String? tanggalBergabung,
      @JsonKey(name: 'pic_id') final String? picId,
      @JsonKey(name: 'no_ktp') final String? noKtp,
      @JsonKey(name: 'tipe_anggota') final String? tipeAnggota,
      @JsonKey(name: 'qr_code') final String? qrCode}) = _$_UserDetailModel;

  factory _UserDetailModel.fromJson(Map<String, dynamic> json) =
      _$_UserDetailModel.fromJson;

  @override
  @JsonKey(name: 'tanggal_lahir')
  String? get tanggalLahir;
  @override
  @JsonKey(name: 'tanggal_baptis')
  String? get tanggalBaptis;
  @override
  @JsonKey(name: 'jenis_kelamin')
  String? get jenisKelamin;
  @override
  @JsonKey(name: 'is_married')
  String? get isMarried;
  @override
  String? get pekerjaan;
  @override
  @JsonKey(name: 'phone_number')
  String? get phoneNumber;
  @override
  dynamic get email;
  @override
  String? get alamat;
  @override
  @JsonKey(name: 'tanggal_bergabung')
  String? get tanggalBergabung;
  @override
  @JsonKey(name: 'pic_id')
  String? get picId;
  @override
  @JsonKey(name: 'no_ktp')
  String? get noKtp;
  @override
  @JsonKey(name: 'tipe_anggota')
  String? get tipeAnggota;
  @override
  @JsonKey(name: 'qr_code')
  String? get qrCode;
  @override
  @JsonKey(ignore: true)
  _$$_UserDetailModelCopyWith<_$_UserDetailModel> get copyWith =>
      throw _privateConstructorUsedError;
}

ChangeUserDetailModel _$ChangeUserDetailModelFromJson(
    Map<String, dynamic> json) {
  return _ChangeUserDetailModel.fromJson(json);
}

/// @nodoc
mixin _$ChangeUserDetailModel {
  @JsonKey(name: 'phone_number')
  String? get phoneNumber => throw _privateConstructorUsedError;
  String? get alamat => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ChangeUserDetailModelCopyWith<ChangeUserDetailModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ChangeUserDetailModelCopyWith<$Res> {
  factory $ChangeUserDetailModelCopyWith(ChangeUserDetailModel value,
          $Res Function(ChangeUserDetailModel) then) =
      _$ChangeUserDetailModelCopyWithImpl<$Res, ChangeUserDetailModel>;
  @useResult
  $Res call(
      {@JsonKey(name: 'phone_number') String? phoneNumber, String? alamat});
}

/// @nodoc
class _$ChangeUserDetailModelCopyWithImpl<$Res,
        $Val extends ChangeUserDetailModel>
    implements $ChangeUserDetailModelCopyWith<$Res> {
  _$ChangeUserDetailModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? phoneNumber = freezed,
    Object? alamat = freezed,
  }) {
    return _then(_value.copyWith(
      phoneNumber: freezed == phoneNumber
          ? _value.phoneNumber
          : phoneNumber // ignore: cast_nullable_to_non_nullable
              as String?,
      alamat: freezed == alamat
          ? _value.alamat
          : alamat // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ChangeUserDetailModelCopyWith<$Res>
    implements $ChangeUserDetailModelCopyWith<$Res> {
  factory _$$_ChangeUserDetailModelCopyWith(_$_ChangeUserDetailModel value,
          $Res Function(_$_ChangeUserDetailModel) then) =
      __$$_ChangeUserDetailModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: 'phone_number') String? phoneNumber, String? alamat});
}

/// @nodoc
class __$$_ChangeUserDetailModelCopyWithImpl<$Res>
    extends _$ChangeUserDetailModelCopyWithImpl<$Res, _$_ChangeUserDetailModel>
    implements _$$_ChangeUserDetailModelCopyWith<$Res> {
  __$$_ChangeUserDetailModelCopyWithImpl(_$_ChangeUserDetailModel _value,
      $Res Function(_$_ChangeUserDetailModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? phoneNumber = freezed,
    Object? alamat = freezed,
  }) {
    return _then(_$_ChangeUserDetailModel(
      phoneNumber: freezed == phoneNumber
          ? _value.phoneNumber
          : phoneNumber // ignore: cast_nullable_to_non_nullable
              as String?,
      alamat: freezed == alamat
          ? _value.alamat
          : alamat // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ChangeUserDetailModel implements _ChangeUserDetailModel {
  _$_ChangeUserDetailModel(
      {@JsonKey(name: 'phone_number') this.phoneNumber, this.alamat});

  factory _$_ChangeUserDetailModel.fromJson(Map<String, dynamic> json) =>
      _$$_ChangeUserDetailModelFromJson(json);

  @override
  @JsonKey(name: 'phone_number')
  final String? phoneNumber;
  @override
  final String? alamat;

  @override
  String toString() {
    return 'ChangeUserDetailModel(phoneNumber: $phoneNumber, alamat: $alamat)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ChangeUserDetailModel &&
            (identical(other.phoneNumber, phoneNumber) ||
                other.phoneNumber == phoneNumber) &&
            (identical(other.alamat, alamat) || other.alamat == alamat));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, phoneNumber, alamat);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ChangeUserDetailModelCopyWith<_$_ChangeUserDetailModel> get copyWith =>
      __$$_ChangeUserDetailModelCopyWithImpl<_$_ChangeUserDetailModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ChangeUserDetailModelToJson(
      this,
    );
  }
}

abstract class _ChangeUserDetailModel implements ChangeUserDetailModel {
  factory _ChangeUserDetailModel(
      {@JsonKey(name: 'phone_number') final String? phoneNumber,
      final String? alamat}) = _$_ChangeUserDetailModel;

  factory _ChangeUserDetailModel.fromJson(Map<String, dynamic> json) =
      _$_ChangeUserDetailModel.fromJson;

  @override
  @JsonKey(name: 'phone_number')
  String? get phoneNumber;
  @override
  String? get alamat;
  @override
  @JsonKey(ignore: true)
  _$$_ChangeUserDetailModelCopyWith<_$_ChangeUserDetailModel> get copyWith =>
      throw _privateConstructorUsedError;
}

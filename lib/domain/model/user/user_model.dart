import 'package:freezed_annotation/freezed_annotation.dart';

part 'user_model.freezed.dart';
part 'user_model.g.dart';

@freezed
class UserModel with _$UserModel {
  factory UserModel(
      {int? id,
      @JsonKey(name: 'no_anggota') String? noAnggota,
      @JsonKey(name: 'nama_anggota') String? namaAnggota,
      @JsonKey(name: 'last_login_date') String? lastLoginDate,
      @JsonKey(name: 'is_login') int? isLogin,
      @JsonKey(name: 'is_active') int? isActive,
      @JsonKey(name: 'is_deleted') int? isDeleted,
      @JsonKey(name: 'created_at') String? createdAt,
      @JsonKey(name: 'updated_at') String? updatedAt,
      UserDetailModel? detail}) = _UserModel;

  factory UserModel.fromJson(Map<String, dynamic> json) =>
      _$UserModelFromJson(json);
}

@freezed
class UserDetailModel with _$UserDetailModel {
  factory UserDetailModel({
    @JsonKey(name: 'tanggal_lahir') String? tanggalLahir,
    @JsonKey(name: 'tanggal_baptis') String? tanggalBaptis,
    @JsonKey(name: 'jenis_kelamin') String? jenisKelamin,
    @JsonKey(name: 'is_married') String? isMarried,
    String? pekerjaan,
    @JsonKey(name: 'phone_number') String? phoneNumber,
    dynamic email,
    String? alamat,
    @JsonKey(name: 'tanggal_bergabung') String? tanggalBergabung,
    @JsonKey(name: 'pic_id') String? picId,
    @JsonKey(name: 'no_ktp') String? noKtp,
    @JsonKey(name: 'tipe_anggota') String? tipeAnggota,
    @JsonKey(name: 'qr_code') String? qrCode,
  }) = _UserDetailModel;

  factory UserDetailModel.fromJson(Map<String, dynamic> json) =>
      _$UserDetailModelFromJson(json);
}

@freezed
class ChangeUserDetailModel with _$ChangeUserDetailModel {
  factory ChangeUserDetailModel({
    @JsonKey(name: 'phone_number') String? phoneNumber,
    String? alamat,
  }) = _ChangeUserDetailModel;

  factory ChangeUserDetailModel.fromJson(Map<String, dynamic> json) =>
      _$ChangeUserDetailModelFromJson(json);
}

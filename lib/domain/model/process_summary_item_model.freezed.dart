// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'process_summary_item_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ProcessSummaryItemModel {
  String get titleFloor => throw _privateConstructorUsedError;
  ProcessSummaryItemStatus get status => throw _privateConstructorUsedError;
  double get progressPercentage => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ProcessSummaryItemModelCopyWith<ProcessSummaryItemModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProcessSummaryItemModelCopyWith<$Res> {
  factory $ProcessSummaryItemModelCopyWith(ProcessSummaryItemModel value,
          $Res Function(ProcessSummaryItemModel) then) =
      _$ProcessSummaryItemModelCopyWithImpl<$Res, ProcessSummaryItemModel>;
  @useResult
  $Res call(
      {String titleFloor,
      ProcessSummaryItemStatus status,
      double progressPercentage});
}

/// @nodoc
class _$ProcessSummaryItemModelCopyWithImpl<$Res,
        $Val extends ProcessSummaryItemModel>
    implements $ProcessSummaryItemModelCopyWith<$Res> {
  _$ProcessSummaryItemModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? titleFloor = null,
    Object? status = null,
    Object? progressPercentage = null,
  }) {
    return _then(_value.copyWith(
      titleFloor: null == titleFloor
          ? _value.titleFloor
          : titleFloor // ignore: cast_nullable_to_non_nullable
              as String,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as ProcessSummaryItemStatus,
      progressPercentage: null == progressPercentage
          ? _value.progressPercentage
          : progressPercentage // ignore: cast_nullable_to_non_nullable
              as double,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ProcessSummaryItemModelCopyWith<$Res>
    implements $ProcessSummaryItemModelCopyWith<$Res> {
  factory _$$_ProcessSummaryItemModelCopyWith(_$_ProcessSummaryItemModel value,
          $Res Function(_$_ProcessSummaryItemModel) then) =
      __$$_ProcessSummaryItemModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String titleFloor,
      ProcessSummaryItemStatus status,
      double progressPercentage});
}

/// @nodoc
class __$$_ProcessSummaryItemModelCopyWithImpl<$Res>
    extends _$ProcessSummaryItemModelCopyWithImpl<$Res,
        _$_ProcessSummaryItemModel>
    implements _$$_ProcessSummaryItemModelCopyWith<$Res> {
  __$$_ProcessSummaryItemModelCopyWithImpl(_$_ProcessSummaryItemModel _value,
      $Res Function(_$_ProcessSummaryItemModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? titleFloor = null,
    Object? status = null,
    Object? progressPercentage = null,
  }) {
    return _then(_$_ProcessSummaryItemModel(
      titleFloor: null == titleFloor
          ? _value.titleFloor
          : titleFloor // ignore: cast_nullable_to_non_nullable
              as String,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as ProcessSummaryItemStatus,
      progressPercentage: null == progressPercentage
          ? _value.progressPercentage
          : progressPercentage // ignore: cast_nullable_to_non_nullable
              as double,
    ));
  }
}

/// @nodoc

class _$_ProcessSummaryItemModel implements _ProcessSummaryItemModel {
  const _$_ProcessSummaryItemModel(
      {this.titleFloor = '',
      this.status = ProcessSummaryItemStatus.process,
      this.progressPercentage = 0.0});

  @override
  @JsonKey()
  final String titleFloor;
  @override
  @JsonKey()
  final ProcessSummaryItemStatus status;
  @override
  @JsonKey()
  final double progressPercentage;

  @override
  String toString() {
    return 'ProcessSummaryItemModel(titleFloor: $titleFloor, status: $status, progressPercentage: $progressPercentage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ProcessSummaryItemModel &&
            (identical(other.titleFloor, titleFloor) ||
                other.titleFloor == titleFloor) &&
            (identical(other.status, status) || other.status == status) &&
            (identical(other.progressPercentage, progressPercentage) ||
                other.progressPercentage == progressPercentage));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, titleFloor, status, progressPercentage);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ProcessSummaryItemModelCopyWith<_$_ProcessSummaryItemModel>
      get copyWith =>
          __$$_ProcessSummaryItemModelCopyWithImpl<_$_ProcessSummaryItemModel>(
              this, _$identity);
}

abstract class _ProcessSummaryItemModel implements ProcessSummaryItemModel {
  const factory _ProcessSummaryItemModel(
      {final String titleFloor,
      final ProcessSummaryItemStatus status,
      final double progressPercentage}) = _$_ProcessSummaryItemModel;

  @override
  String get titleFloor;
  @override
  ProcessSummaryItemStatus get status;
  @override
  double get progressPercentage;
  @override
  @JsonKey(ignore: true)
  _$$_ProcessSummaryItemModelCopyWith<_$_ProcessSummaryItemModel>
      get copyWith => throw _privateConstructorUsedError;
}

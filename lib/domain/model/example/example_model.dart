import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'example_model.freezed.dart';

@freezed
class Example with _$Example {
  const Example._();
  const factory Example.person({String? name, int? age}) = Person;
  const factory Example.city({String? cityName}) = City;
}

class ExampleCubit extends Cubit<Example> {
  ExampleCubit() : super(const Example._());

  void asd() {
    emit(state);
  }
}

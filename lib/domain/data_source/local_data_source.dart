abstract class LocalDataSource {
  const LocalDataSource();

  Future<Map<String, dynamic>?> get(String key);

  Future<Object?> getValue(String key);

  Future<bool> set(String key, Map<String, dynamic>? json);

  Future<bool> setValue(String key, dynamic params);
}

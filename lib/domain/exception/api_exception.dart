class ApiException implements Exception {
  ApiException(this.message, {this.statusCode}) : super();

  final String message;
  final int? statusCode;

  @override
  String toString() {
    return message;
  }
}

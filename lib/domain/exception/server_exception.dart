class ServerException implements Exception {
  const ServerException();

  @override
  String toString() {
    return 'Server error';
  }
}

class InvalidTokenException implements Exception {
  const InvalidTokenException();

  @override
  String toString() {
    return 'Sesi anda telah berakhir!';
  }
}

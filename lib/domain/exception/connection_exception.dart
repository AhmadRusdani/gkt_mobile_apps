class ConnectionException implements Exception {
  const ConnectionException();

  @override
  String toString() {
    return 'Time out, silahkan periksa koneksi Anda!';
  }
}

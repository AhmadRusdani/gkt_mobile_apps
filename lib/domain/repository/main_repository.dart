import 'package:gkt/domain/model/base/api_response.dart';
import 'package:gkt/domain/model/building_process/building_process_model.dart';
import 'package:gkt/domain/model/detail_process_summary/detail_process_summary_model.dart';
import 'package:gkt/domain/model/history/history_transaction_model.dart';
import 'package:gkt/domain/model/news/news_model.dart';
import 'package:gkt/domain/model/offering/offering_model.dart';
import 'package:gkt/domain/model/pray/pray_schedule_model.dart';
import 'package:gkt/domain/model/process_summary/process_summary_model.dart';
import 'package:gkt/domain/model/seremon/seremon_model.dart';
import 'package:gkt/domain/model/site_visit/site_visit_model.dart';

abstract class MainRepository {
  Future<ApiResponse<List<PrayScheduleModel>>> getPraySchedule();

  Future<ApiResponse<List<NewsModel>>> getNews();

  Future<ApiResponse<List<SeremonModel>>> getSeremon({SeremonType? type});

  Future<ApiResponse<List<HistoryTransactionModel>>> getHistoryTransaction(
      {HistoryTransactionType? type});

  Future<ApiResponse<OfferingModel>> sendOffering({
    required int amount,
    required String notes,
  });

  Future<ApiResponse<OfferingModel>> sendOfferingGuess({required int amount});

  Future<ApiResponse<SiteVisitModel>> getSiteVisit();

  Future<ApiResponse<List<ProcessSummaryModel>>> getProcessSummary();

  Future<ApiResponse<DetailProcessSummaryModel>> getDetailProcessSummary(
      {required int id});

  Future<ApiResponse<BuildingProcessModel>> getBuildingProcess();
}

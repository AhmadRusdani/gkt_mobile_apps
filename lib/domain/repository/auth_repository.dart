import 'package:gkt/domain/model/base/api_response.dart';
import 'package:gkt/domain/model/login/login_model.dart';

enum AuthenticationStatus {
  unknown,
  authenticated,
  unauthenticated,
  update,
  guest,
}

abstract class AuthRepository {
  Stream<AuthenticationStatus> get status;
  Future<ApiResponse<LoginModel>> loginUser({
    required String noUser,
    required String password,
  });
  void logout();
  void dispose();
}

import 'dart:typed_data';

import 'package:geolocator/geolocator.dart';

abstract class MapsRepository {
  Future<Position?> getCurrentLocation();
}

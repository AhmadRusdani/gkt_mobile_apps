import 'package:gkt/domain/model/base/api_response.dart';
import 'package:gkt/domain/model/user/user_model.dart';

abstract class UserRepository {
  Future<UserModel?> get currentUser;

  Future<UserModel?> fetchUser();

  Future<String?> get sessionToken;

  Future<void> setCurrentUser(UserModel? user);

  Future<void> setSessionToken(String? token);

  Future<ApiResponse<UserModel?>> changePassword({
    required String oldPassword,
    required String newPassword,
  });

  Future<ApiResponse<ChangeUserDetailModel?>> changePhone({
    required String phone,
  });

  Future<ApiResponse<ChangeUserDetailModel?>> changeAddress({
    required String address,
  });
}

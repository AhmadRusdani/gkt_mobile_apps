import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gkt/injector.dart';
import 'package:gkt/presentation/blocs/cubit_observer.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';

import 'app.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  // inject dependencies
  initializeDependencies();

  // init localizations
  Intl.defaultLocale = 'id_ID';
  await initializeDateFormatting(Intl.defaultLocale);
  Bloc.observer = CubitObserver();
  disableStatusBar();
  runApp(const MyApp());
}

void disableStatusBar() {
  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,
  ));
}

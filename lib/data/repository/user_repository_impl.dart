import 'package:gkt/data/data_source/remote/api_service.dart';
import 'package:gkt/domain/data_source/local_data_source.dart';
import 'package:gkt/domain/exception/api_exception.dart';
import 'package:gkt/domain/model/base/api_response.dart';
import 'package:gkt/domain/model/user/user_model.dart';
import 'package:gkt/domain/repository/user_repository.dart';
import 'package:gkt/presentation/utils/extension.dart';

const _kKeyCurrentUser = 'CURRENT_USER';
const _kKeyStoreData = 'STORE_DATA';
const _kKeyToken = 'TOKEN_DATA';

class UserRepositoryImpl implements UserRepository {
  UserRepositoryImpl(this._service, this._storage);

  final ApiService _service;
  final LocalDataSource _storage;

  UserModel? _user;

  @override
  Future<UserModel?> get currentUser async {
    if (_user != null) return _user!;

    final map = await _storage.get(_kKeyCurrentUser);
    if (map == null) return null;

    return (_user ?? UserModel.fromJson(map));
  }

  @override
  Future<UserModel?> fetchUser({
    String? token,
  }) async {
    final tokenUser = await sessionToken;
    final response = await _service.fetchUserDetails(
      token: tokenUser ?? token ?? '',
    );

    if (response.error) {
      throw ApiException(response.message ?? 'Something wrong!');
    }

    _user = response.data;
    await setCurrentUser(response.data);
    return response.data;
  }

  @override
  Future<String?> get sessionToken async =>
      await _storage.getValue(_kKeyToken) as String?;

  @override
  Future<void> setCurrentUser(UserModel? user) async {
    if (user == null) {
      _user = null;
      await _storage.set(_kKeyCurrentUser, null);
      return;
    }
    _user = user;
    await _storage.set(_kKeyCurrentUser, _user!.toJson());
  }

  @override
  Future<void> setSessionToken(String? token) async {
    await _storage.setValue(
        _kKeyToken, token != null ? 'Bearer $token' : token);
  }

  @override
  Future<ApiResponse<UserModel?>> changePassword({
    required String oldPassword,
    required String newPassword,
  }) async {
    final token = await sessionToken;
    return _service.changePassword(token: token ?? '', body: {
      'old_password': oldPassword,
      'new_password': newPassword,
    }).handleResponse();
  }

  @override
  Future<ApiResponse<ChangeUserDetailModel?>> changeAddress({
    required String address,
  }) async {
    final token = await sessionToken;
    final user = await currentUser;
    final response =
        await _service.changeUserDetails(token: token ?? '', body: {
      'alamat': address,
      'phone_number': user?.detail?.phoneNumber ?? '',
    });

    if (response.error) {
      throw ApiException(response.message ?? "Something wrong!");
    }

    await setCurrentUser(
      user?.copyWith(
        detail: user.detail?.copyWith(
              alamat: address,
            ) ??
            UserDetailModel(alamat: address),
      ),
    );

    return response;
  }

  @override
  Future<ApiResponse<ChangeUserDetailModel?>> changePhone({
    required String phone,
  }) async {
    final token = await sessionToken;
    final user = await currentUser;
    final response =
        await _service.changeUserDetails(token: token ?? '', body: {
      'alamat': user?.detail?.alamat ?? '',
      'phone_number': '+62$phone',
    });

    if (response.error) {
      throw ApiException(response.message ?? "Something wrong!");
    }

    await setCurrentUser(
      user?.copyWith(
        detail: user.detail?.copyWith(
              phoneNumber: phone,
            ) ??
            UserDetailModel(phoneNumber: phone),
      ),
    );

    return response;
  }
}

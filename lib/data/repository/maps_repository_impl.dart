import 'package:geolocator/geolocator.dart';
import 'package:gkt/domain/repository/maps_repository.dart';

class MapsRepositoryImpl implements MapsRepository {
  @override
  Future<Position?> getCurrentLocation() async {
    try {
      final position = await Geolocator.getLastKnownPosition();
      if (position != null) {
        return position;
      }
      return await Geolocator.getCurrentPosition();
    } catch (e) {
      return null;
    }
  }
}

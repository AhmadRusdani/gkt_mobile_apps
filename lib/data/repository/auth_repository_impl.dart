import 'dart:async';

import 'package:gkt/data/data_source/remote/api_service.dart';
import 'package:gkt/domain/exception/api_exception.dart';
import 'package:gkt/domain/model/base/api_response.dart';
import 'package:gkt/domain/model/login/login_model.dart';
import 'package:gkt/domain/repository/auth_repository.dart';
import 'package:gkt/domain/repository/user_repository.dart';

class AuthRepositoryImpl implements AuthRepository {
  AuthRepositoryImpl(this._service, this._repo);

  final ApiService _service;
  final UserRepository _repo;
  final _controller = StreamController<AuthenticationStatus>();

  @override
  Stream<AuthenticationStatus> get status async* {
    final user = await _repo.currentUser;
    if (user == null) {
      yield AuthenticationStatus.unauthenticated;
    } else {
      yield AuthenticationStatus.authenticated;
    }
    yield* _controller.stream;
  }

  @override
  Future<ApiResponse<LoginModel>> loginUser({
    required String noUser,
    required String password,
  }) async {
    final response = await _service.loginUser(user: {
      'no_anggota': noUser,
      'password': password,
    });

    if (response.error) {
      throw ApiException(
          'Nomor anggota atau password yang Anda masukan salah!');
    }
    return response;
  }

  @override
  void dispose() {
    _controller.close();
  }

  @override
  void logout() {
    _repo.setCurrentUser(null);
    _controller.add(AuthenticationStatus.unauthenticated);
  }
}

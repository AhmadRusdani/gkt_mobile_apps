import 'package:gkt/data/data_source/remote/api_service.dart';
import 'package:gkt/domain/model/building_process/building_process_model.dart';
import 'package:gkt/domain/model/detail_process_summary/detail_process_summary_model.dart';
import 'package:gkt/domain/model/offering/offering_model.dart';
import 'package:gkt/domain/model/process_summary/process_summary_model.dart';
import 'package:gkt/domain/model/seremon/seremon_model.dart';

import 'package:gkt/domain/model/pray/pray_schedule_model.dart';

import 'package:gkt/domain/model/news/news_model.dart';

import 'package:gkt/domain/model/history/history_transaction_model.dart';

import 'package:gkt/domain/model/base/api_response.dart';
import 'package:gkt/domain/model/site_visit/site_visit_model.dart';
import 'package:gkt/presentation/utils/extension.dart';

import '../../domain/exception/api_exception.dart';
import '../../domain/repository/main_repository.dart';
import '../../domain/repository/user_repository.dart';

class MainRepositoryImpl implements MainRepository {
  const MainRepositoryImpl(this._user, this._service);

  final UserRepository _user;
  final ApiService _service;

  @override
  Future<ApiResponse<List<HistoryTransactionModel>>> getHistoryTransaction(
      {HistoryTransactionType? type}) async {
    final token = await _user.sessionToken;
    final response = await _service.getHistoryTransaction(
      token: token ?? '',
      type: type.toString(),
    );

    if (response.error) {
      throw ApiException(response.message ?? "Something wrong!");
    }
    return response;
  }

  @override
  Future<ApiResponse<List<NewsModel>>> getNews() async {
    final token = await _user.sessionToken;
    final response = await _service.getNews(token: token ?? '');
    if (response.error) {
      throw ApiException(response.message ?? "Something wrong!");
    }
    return response;
  }

  @override
  Future<ApiResponse<List<PrayScheduleModel>>> getPraySchedule() async {
    final token = await _user.sessionToken;
    final response = await _service.getPraySchedule(
      token: token ?? '',
    );

    if (response.error) {
      throw ApiException(response.message ?? "Something wrong!");
    }
    return response;
  }

  @override
  Future<ApiResponse<List<SeremonModel>>> getSeremon(
      {SeremonType? type}) async {
    final token = await _user.sessionToken;
    final response = await _service.getSeremon(
      token: token ?? '',
      type: type.toString(),
    );

    if (response.error) {
      throw ApiException(response.message ?? "Something wrong!");
    }
    return response;
  }

  @override
  Future<ApiResponse<OfferingModel>> sendOffering(
      {required int amount, required String notes,}) async {
    final token = await _user.sessionToken;
    return _service.sendOffering(
      token: token ?? '',
      amount: {
        'amount': amount,
        'notes': notes,
      },
    ).handleResponse();
  }

  @override
  Future<ApiResponse<SiteVisitModel>> getSiteVisit() async {
    final token = await _user.sessionToken;
    return _service.getSiteVisit(token: token ?? '').handleResponse();
  }

  @override
  Future<ApiResponse<List<ProcessSummaryModel>>> getProcessSummary() async {
    final token = await _user.sessionToken;
    return _service.getProcessSumarry(token: token ?? '').handleResponse();
  }

  @override
  Future<ApiResponse<DetailProcessSummaryModel>> getDetailProcessSummary(
      {required int id}) async {
    final token = await _user.sessionToken;
    return _service
        .getDetailProcessSumarry(token: token ?? '', id: id.toString())
        .handleResponse();
  }

  @override
  Future<ApiResponse<BuildingProcessModel>> getBuildingProcess() async {
    final token = await _user.sessionToken;
    return _service.getBuildingProcess(token: token ?? '').handleResponse();
  }

  @override
  Future<ApiResponse<OfferingModel>> sendOfferingGuess({required int amount}) {
    return _service.sendOfferingGuess(
      amount: {
        'amount': amount,
      },
    ).handleResponse();
  }
}

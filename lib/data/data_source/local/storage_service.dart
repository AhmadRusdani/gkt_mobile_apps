import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import '../../../domain/data_source/local_data_source.dart';

class StorageService extends LocalDataSource {
  const StorageService();

  @override
  Future<Map<String, dynamic>?> get(String key) async {
    final prefs = await SharedPreferences.getInstance();
    final source = prefs.getString(key);

    try {
      if (source != null) {
        return jsonDecode(source);
      }
    } on FormatException {
      return null;
    }
    return null;
  }

  @override
  Future<bool> set(String key, Map<String, dynamic>? json) async {
    final prefs = await SharedPreferences.getInstance();
    if (json == null) {
      return await prefs.remove(key);
    }

    return await prefs.setString(key, jsonEncode(json));
  }

  @override
  Future<Object?> getValue(String key) async {
    final prefs = await SharedPreferences.getInstance();
    final source = prefs.get(key);
    return source;
  }

  @override
  Future<bool> setValue(String key, dynamic params) async {
    final prefs = await SharedPreferences.getInstance();
    switch (params.runtimeType) {
      case int:
        return await prefs.setInt(key, params as int);
      case String:
        return await prefs.setString(key, params as String);
      case bool:
        return await prefs.setBool(key, params as bool);
      case double:
        return await prefs.setDouble(key, params as double);
      default:
    }
    if (params == null && prefs.containsKey(key)) {
      return await prefs.remove(key);
    }
    return false;
  }
}

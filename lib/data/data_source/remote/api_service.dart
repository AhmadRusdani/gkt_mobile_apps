import 'package:dio/dio.dart';
import 'package:gkt/domain/model/base/api_response.dart';
import 'package:gkt/domain/model/building_process/building_process_model.dart';
import 'package:gkt/domain/model/detail_process_summary/detail_process_summary_model.dart';
import 'package:gkt/domain/model/history/history_transaction_model.dart';
import 'package:gkt/domain/model/login/login_model.dart';
import 'package:gkt/domain/model/news/news_model.dart';
import 'package:gkt/domain/model/offering/offering_model.dart';
import 'package:gkt/domain/model/pray/pray_schedule_model.dart';
import 'package:gkt/domain/model/process_summary/process_summary_model.dart';
import 'package:gkt/domain/model/seremon/seremon_model.dart';
import 'package:gkt/domain/model/site_visit/site_visit_model.dart';
import 'package:gkt/domain/model/user/user_model.dart';
import 'package:gkt/presentation/utils/constant.dart';
import 'package:retrofit/retrofit.dart';

part 'api_service.g.dart';

@RestApi(baseUrl: 'http://45.77.241.243:8091')
abstract class ApiService {
  factory ApiService(Dio dio, {String baseUrl}) = _ApiService;

  @POST('/auth/login')
  Future<ApiResponse<LoginModel>> loginUser({
    @Body() required Map<String, String> user,
  });

  @GET('/user')
  Future<ApiResponse<UserModel>> fetchUser({
    @Header(kAuth) required String token,
  });

  @GET('/user/details')
  Future<ApiResponse<UserModel>> fetchUserDetails({
    @Header(kAuth) required String token,
  });

  @GET('/project/history_persembahan')
  Future<ApiResponse<List<HistoryTransactionModel>>> getHistoryTransaction({
    @Header(kAuth) required String token,
    @Query('type') String? type,
  });

  @GET('/home/news')
  Future<ApiResponse<List<NewsModel>>> getNews({
    @Header(kAuth) required String token,
  });

  @GET('/home/jadwal_ibadah')
  Future<ApiResponse<List<PrayScheduleModel>>> getPraySchedule({
    @Header(kAuth) required String token,
  });

  @GET('/home/khotbah')
  Future<ApiResponse<List<SeremonModel>>> getSeremon({
    @Header(kAuth) required String token,
    @Query('type') String? type,
  });

  @POST('/project/persembahan')
  Future<ApiResponse<OfferingModel>> sendOffering({
    @Header(kAuth) String? token,
    @Body() required Map<String, dynamic> amount,
  });

  @POST('/project/persembahan_guess')
  Future<ApiResponse<OfferingModel>> sendOfferingGuess({
    @Body() required Map<String, int> amount,
  });

  @GET('/project/site_visit')
  Future<ApiResponse<SiteVisitModel>> getSiteVisit({
    @Header(kAuth) required String token,
  });

  @GET('/project/proses_pembangunan')
  Future<ApiResponse<List<ProcessSummaryModel>>> getProcessSumarry({
    @Header(kAuth) required String token,
  });

  @GET('/project/proses_pembangunan/{id}')
  Future<ApiResponse<DetailProcessSummaryModel>> getDetailProcessSumarry({
    @Header(kAuth) required String token,
    @Path('id') required String id,
  });

  @GET('/project/ringkasan_pembangunan')
  Future<ApiResponse<BuildingProcessModel>> getBuildingProcess({
    @Header(kAuth) required String token,
  });

  @PUT('/user/change_password')
  Future<ApiResponse<UserModel>> changePassword({
    @Header(kAuth) required String token,
    @Body() required Map<String, String> body,
  });

  @PUT('/user/details')
  Future<ApiResponse<ChangeUserDetailModel>> changeUserDetails({
    @Header(kAuth) required String token,
    @Body() required Map<String, String> body,
  });

}

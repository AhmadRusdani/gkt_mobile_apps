import 'package:flimer/flimer.dart';
import 'package:flutter/material.dart';
import 'package:gkt/presentation/utils/constant.dart';
import 'package:image_cropper/image_cropper.dart';

import '../config/theme_colors.dart';
import '../config/theme_texts.dart';

class ImagePickers {
  static final _imageCropper = ImageCropper();

  static Future<XFile?> show({
    required BuildContext context,
    String title = 'Photo Profil',
  }) async {
    final imageSource = await showModalBottomSheet(
      context: context,
      shape: kModalBottomShape,
      builder: (BuildContext bc) {
        return LayoutBuilder(builder: (context, constraints) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                ListTile(
                  contentPadding: EdgeInsets.zero,
                  leading: const IconButton(
                    icon: Icon(Icons.close, color: Colors.transparent),
                    onPressed: null,
                  ),
                  title: Text(
                    title,
                    textAlign: TextAlign.center,
                    style: kTextStyleTitleBold18.copyWith(
                      fontSize: 14,
                    ),
                  ),
                  trailing: const IconButton(
                    icon: Icon(Icons.close, color: Colors.transparent),
                    onPressed: null,
                  ),
                ),
                const SizedBox(height: 8),
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 24),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.pop(context, ImageSource.gallery);
                        },
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: const [
                            Icon(
                              Icons.photo_size_select_actual_rounded,
                              color: ThemeColors.orangePrimary,
                              size: 34,
                            ),
                            Text(
                              'Galeri',
                              style: kTextStyleDescription12,
                            )
                          ],
                        ),
                      ),
                      const SizedBox(
                        width: 24,
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.pop(context, ImageSource.camera);
                        },
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: const [
                            Icon(
                              Icons.camera_alt_rounded,
                              color: ThemeColors.orangePrimary,
                              size: 34,
                            ),
                            Text(
                              'Kamera',
                              style: kTextStyleDescription12,
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 32),
              ],
            ),
          );
        });
      },
    );

    if (imageSource is ImageSource) {
      final photo = await flimer.pickImage(
        source: imageSource,
        maxWidth: 1024,
        maxHeight: 1024,
      );

      if (photo != null) {
        final croppedFile = await _imageCropper.cropImage(
          sourcePath: photo.path,
          aspectRatioPresets: [
            CropAspectRatioPreset.square,
          ],
          uiSettings: [
            AndroidUiSettings(
              toolbarTitle: 'Atur Posisi Foto',
              toolbarColor: ThemeColors.gold,
              toolbarWidgetColor: Colors.white,
              initAspectRatio: CropAspectRatioPreset.square,
              lockAspectRatio: true,
            ),
            IOSUiSettings(
              title: 'Atur Posisi Foto',
              aspectRatioLockEnabled: true,
            ),
          ],
        );

        if (croppedFile != null) {
          return XFile(croppedFile.path);
        }
      }
    }

    return null;
  }
}

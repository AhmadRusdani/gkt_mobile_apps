import 'package:flutter/material.dart';

//Copy this CustomPainter code to the Bottom of the File
class RPSCustomPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.1113801, size.height * 0.05390836);
    path_0.cubicTo(
        size.width * 0.08463511,
        size.height * 0.05390836,
        size.width * 0.06295400,
        size.height * 0.07804394,
        size.width * 0.06295400,
        size.height * 0.1078167);
    path_0.lineTo(size.width * 0.06295400, size.height * 0.8382749);
    path_0.cubicTo(
        size.width * 0.06295400,
        size.height * 0.8680485,
        size.width * 0.08463511,
        size.height * 0.8921833,
        size.width * 0.1113801,
        size.height * 0.8921833);
    path_0.lineTo(size.width * 0.4339879, size.height * 0.8921833);
    path_0.cubicTo(
        size.width * 0.4380654,
        size.height * 0.8580620,
        size.width * 0.4633559,
        size.height * 0.8317871,
        size.width * 0.4939467,
        size.height * 0.8317871);
    path_0.cubicTo(
        size.width * 0.5245375,
        size.height * 0.8317871,
        size.width * 0.5498281,
        size.height * 0.8580620,
        size.width * 0.5539056,
        size.height * 0.8921833);
    path_0.lineTo(size.width * 0.8789346, size.height * 0.8921833);
    path_0.cubicTo(
        size.width * 0.9056804,
        size.height * 0.8921833,
        size.width * 0.9273608,
        size.height * 0.8680485,
        size.width * 0.9273608,
        size.height * 0.8382749);
    path_0.lineTo(size.width * 0.9273608, size.height * 0.1078167);
    path_0.cubicTo(
        size.width * 0.9273608,
        size.height * 0.07804394,
        size.width * 0.9056804,
        size.height * 0.05390836,
        size.width * 0.8789346,
        size.height * 0.05390836);
    path_0.lineTo(size.width * 0.1113801, size.height * 0.05390836);
    path_0.close();

    Paint paint0Fill = Paint()..style = PaintingStyle.fill;
    paint0Fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_0, paint0Fill);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}

class DolDurmaClipper extends CustomClipper<Path> {
  DolDurmaClipper({required this.right, required this.holeRadius});

  final double holeRadius;
  final double right;

  @override
  Path getClip(Size size) {
    final path = Path()
      ..lineTo(size.width, 0.0)
      ..lineTo(size.width, size.height)
      ..lineTo(size.width / 1.7, size.height)
      ..arcToPoint(
        Offset(size.width / 2 - holeRadius / 1.4, size.height),
        clockwise: false,
        radius: const Radius.circular(10),
      );

    path.lineTo(0.0, size.height);

    path.close();
    return path;
  }

  @override
  bool shouldReclip(DolDurmaClipper oldClipper) => true;
}

class TicketClipper extends CustomClipper<Path> {
  TicketClipper({this.radius});
  final double? radius;
  @override
  Path getClip(Size size) {
    Path path = Path();

    path.lineTo(0.0, size.height);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0.0);

    path.addOval(Rect.fromCircle(
        center: Offset(0.0, size.height / 2), radius: radius ?? 20.0));
    path.addOval(Rect.fromCircle(
        center: Offset(size.width, size.height / 2), radius: radius ?? 20.0));

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}

class CropClipperPath extends CustomClipper<Path> {
  CropClipperPath({this.percentageCrop});

  final double? percentageCrop;

  @override
  Path getClip(Size size) {
    //size.height and size.width are the width and height of your child: Container() above
    final crop = (percentageCrop ?? 0) > 100 ? 100 : (percentageCrop ?? 0);
    final actualCrop = size.height * (crop / 100);
    Path path = Path();
    path.moveTo(0, 0);
    path.lineTo(size.width, 0);
    path.lineTo(size.width, size.height - actualCrop);
    path.lineTo(0, size.height - actualCrop);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) => true;
}

import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math' as math;

import 'package:crypto/crypto.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hud/flutter_hud.dart';
import 'package:formz/formz.dart';
import 'package:gkt/domain/exception/api_exception.dart';
import 'package:gkt/domain/exception/connection_exception.dart';
import 'package:gkt/domain/exception/invalid_token_exception.dart';
import 'package:gkt/domain/model/base/api_response.dart';
import 'package:gkt/presentation/utils/constant.dart';
import 'package:gkt/presentation/widgets/base/button_widget.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../domain/exception/server_exception.dart';
import '../blocs/auth/auth_cubit.dart';
import '../config/theme_colors.dart';
import '../config/theme_texts.dart';
import '../validation/password.dart';

extension ScaffoldMessengerStateExtension on ScaffoldMessengerState {
  void showSnackBarContent(
    Widget content, {
    SnackBarBehavior behavior = SnackBarBehavior.fixed,
    Color? backgroundColor,
  }) {
    hideCurrentSnackBar();
    showSnackBar(
      SnackBar(
        content: content,
        behavior: behavior,
        backgroundColor: backgroundColor,
      ),
    );
  }

  void showOkToast(String message) => showSnackBarContent(
        Row(
          children: [
            Expanded(child: Text(message)),
            const SizedBox(width: 8),
            const Icon(
              Icons.check_circle_rounded,
              color: ThemeColors.greenPrimary,
            ),
          ],
        ),
        behavior: SnackBarBehavior.floating,
      );

  void showCancelToast(String message) => showSnackBarContent(
        Row(
          children: [
            Expanded(child: Text(message)),
            const SizedBox(width: 8),
            const Icon(
              Icons.close,
              color: ThemeColors.redPrimary,
            ),
          ],
        ),
        behavior: SnackBarBehavior.floating,
      );
}

extension MyBuildContext on BuildContext {
  void showOkToast(String message) =>
      ScaffoldMessenger.of(this).showOkToast(message);

  void _showSnackBar(
    String message, {
    SnackBarBehavior behavior = SnackBarBehavior.fixed,
    Color? backgroundColor,
  }) =>
      ScaffoldMessenger.of(this)
        ..hideCurrentSnackBar()
        ..showSnackBarContent(
          Text(message),
          behavior: behavior,
          backgroundColor: backgroundColor,
        );

  void showSnackBar(String message) => _showSnackBar(message);

  void showToast(String message) => _showSnackBar(
        message,
        behavior: SnackBarBehavior.floating,
      );

  void showErrorSnackBar(String message) =>
      _showSnackBar(message, backgroundColor: Colors.red);

  void showErrorToast(String message) => _showSnackBar(message,
      behavior: SnackBarBehavior.floating, backgroundColor: Colors.red);

  Future<bool?> showConfirmDialog({
    required String title,
    required Object message,
    required String negativeButton,
    required String positiveButton,
  }) async {
    assert(
      message is String || message is Text,
      'Use message type either String or Text widget',
    );
    return await showDialog(
        context: this,
        builder: (context) {
          return AlertDialog(
            title: Text(title),
            content: message is String ? Text(message) : message as Text,
            actionsPadding: const EdgeInsets.only(right: 8),
            actions: [
              WhiteButton(
                onPressed: () {
                  Navigator.pop(context, false);
                },
                child: Text(negativeButton),
              ),
              PrimaryButton(
                onPressed: () {
                  Navigator.pop(context, true);
                },
                child: Text(positiveButton),
              ),
            ],
          );
        });
  }

  Future<bool?> showConfirmationBottomSheet({
    required String title,
    required Object message,
    String negativeButton = 'Batalkan',
    String positiveButton = 'Lanjutkan',
  }) async {
    assert(
      message is String || message is Text,
      'Use message type either String or Text widget',
    );
    return await showModalBottomSheet(
      context: this,
      shape: kModalBottomShape,
      isScrollControlled: true,
      builder: (BuildContext bc) {
        return LayoutBuilder(builder: (context, constraints) {
          return Container(
            padding: EdgeInsets.fromLTRB(
                16, 32, 16, context.responsivePaddingBottom()),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const SizedBox(height: 32),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 24),
                  child: Text(
                    title,
                    style: kTextStyleDescriptionBold16,
                    textAlign: TextAlign.center,
                  ),
                ),
                const SizedBox(height: 8),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 24),
                  child: message is String
                      ? Text(
                          message,
                          style: kTextStyleDescription,
                          textAlign: TextAlign.center,
                        )
                      : message as Text,
                ),
                const SizedBox(height: 32),
                Row(
                  children: [
                    Expanded(
                      child: WhiteButton(
                        child: Text(negativeButton),
                        onPressed: () {
                          Navigator.pop(this, false);
                        },
                      ),
                    ),
                    const SizedBox(width: 16),
                    Expanded(
                      child: PrimaryButton(
                        child: Text(positiveButton),
                        onPressed: () {
                          Navigator.pop(this, true);
                        },
                      ),
                    ),
                  ],
                ),
              ],
            ),
          );
        });
      },
    );
  }

  Future<bool?> showErrorBottomSheet({
    required String title,
    required Object message,
  }) async {
    assert(
      message is String || message is Text,
      'Use message type either String or Text widget',
    );
    return await showModalBottomSheet(
      context: this,
      shape: kModalBottomShape,
      isScrollControlled: true,
      builder: (BuildContext bc) {
        return LayoutBuilder(builder: (context, constraints) {
          return Container(
            padding: EdgeInsets.fromLTRB(
                16, 32, 16, context.responsivePaddingBottom()),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const SizedBox(height: 32),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 24),
                  child: Text(
                    title,
                    style: kTextStyleDescriptionBold16,
                    textAlign: TextAlign.center,
                  ),
                ),
                const SizedBox(height: 8),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 24),
                  child: message is String
                      ? Text(
                          message,
                          style: kTextStyleDescription,
                          textAlign: TextAlign.center,
                        )
                      : message as Text,
                ),
                const SizedBox(height: 32),
                Row(
                  children: [
                    Expanded(
                      child: PrimaryButton(
                        child: const Text('Mengerti'),
                        onPressed: () {
                          Navigator.pop(this, false);
                        },
                      ),
                    ),
                  ],
                ),
              ],
            ),
          );
        });
      },
    );
  }

  void handleError(Exception? err,
      {VoidCallback? retry, VoidCallback? action}) async {
    if (err == null) {
      return;
    }
    switch (err.runtimeType) {
      case ApiException:
        if (action != null) {
          action.call();
          return;
        }
        showErrorToast((err as ApiException).message);
        break;
      case ServerException:
      case ConnectionException:
        if (action != null) {
          action.call();
          return;
        }

        await showErrorBottomSheet(
          title: 'Ooppss!',
          message: err.toString(),
        ).then((value) {
          if (value == true) {
            retry?.call();
          }
        });
        break;
      case InvalidTokenException:
        final authBloc = read<AuthCubit>();
        authBloc.onLogout();
        break;
      case DioError:
        handleError((err as DioError).handleError(), retry: retry);
        break;
      default:
        showErrorToast(err.toString());
        break;
    }
  }

  void copyText(String text) {
    Clipboard.setData(ClipboardData(text: text)).then((_) {
      showToast('Text berhasil di salin!');
    });
  }

  Size screenSize() => MediaQuery.of(this).size;
  double screenWidth() => screenSize().width;
  double screenHeight() => screenSize().height;
  double responsivePaddingBottom() =>
      math.max(12, MediaQuery.of(this).viewPadding.bottom);

  void openMap({required LatLng latLng}) async {
    if (Platform.isAndroid) {
      final urlApp = Uri.parse(
          "google.navigation:q=${latLng.latitude},${latLng.longitude}&mode=d");
      final url =
          'https://www.google.com/maps/search/?api=1&query=${latLng.latitude},${latLng.longitude}';
      if (await canLaunchUrl(urlApp)) {
        await launchUrl(urlApp);
      } else if (await canLaunchUrl(Uri.parse(url))) {
        await launchUrl(Uri.parse(url));
      } else {
        throw 'Could not launch $url';
      }
    } else {
      final urlAppleMaps =
          'https://maps.apple.com/?q=${latLng.latitude},${latLng.longitude}';
      final url =
          'comgooglemaps://?saddr=&daddr=${latLng.latitude},${latLng.longitude}&directionsmode=driving';
      if (await canLaunchUrl(Uri.parse(url))) {
        await launchUrl(Uri.parse(url));
      } else if (await canLaunchUrl(Uri.parse(urlAppleMaps))) {
        await launchUrl(Uri.parse(urlAppleMaps));
      } else {
        throw 'Could not launch $url';
      }
    }
  }
}

extension NumExtension on num? {
  String toRp({bool addRp = true}) {
    final formatCurrency = NumberFormat.simpleCurrency(
        locale: 'id_ID', name: addRp ? 'Rp.' : '', decimalDigits: 0);
    return formatCurrency.format(this ?? 0);
  }
}

extension DoubleExtension on double {
  String toRp() {
    final formatCurrency = NumberFormat.simpleCurrency(
        locale: 'id_ID', name: 'Rp.', decimalDigits: 0);
    return formatCurrency.format(this);
  }
}

extension IntExtension on int {
  String toRp({bool addRp = true}) {
    final formatCurrency = NumberFormat.simpleCurrency(
        locale: 'id_ID', name: addRp ? 'Rp.' : '', decimalDigits: 0);
    return formatCurrency.format(this);
  }
}

extension ParseStringUtils on String? {
  bool validateEmail() {
    return RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(this ?? '');
  }

  String sanitizePhone({String defaultCountryCode = '62'}) {
    String val = this ?? '';

    // validating for non number
    final nonNumber = RegExp(r'[^\d]+');
    if (val.contains(nonNumber)) {
      val = val.replaceAll(RegExp(r'[^\d]+'), '');
    }

    // validating if prefixed with zero(s)
    final prefixedZero = RegExp(r'(0)+');
    if (val.startsWith(prefixedZero)) {
      val = val.replaceFirst(prefixedZero, defaultCountryCode);
    }

    return val;
  }

  String sanitizePhoneV2() {
    String val = this ?? '';

    // validating for non number
    final nonNumber = RegExp(r'[^\d]+');
    if (val.contains(nonNumber)) {
      val = val.replaceAll(RegExp(r'[^\d]+'), '');
    }

    // validating if prefixed with zero(s)
    final prefixedZero = RegExp(r'(62)+');
    if (val.startsWith(prefixedZero)) {
      val = val.replaceFirst(prefixedZero, '0');
    }

    return val;
  }

  String toCapitalized() => this != null
      ? this!.isNotEmpty
          ? '${this![0].toUpperCase()}${this!.substring(1).toLowerCase()}'
          : ''
      : '';

  String toTitleCase() => this != null
      ? this!
          .replaceAll(RegExp(' +'), ' ')
          .split(' ')
          .map((str) => str.toCapitalized())
          .join(' ')
      : '';

  String sanitizeDecimalInput() {
    String val = this ?? '0';

    // validating for non number
    final nonNumber = RegExp(r'[^\d]+');
    if (val.contains(nonNumber)) {
      val = val.replaceAll(RegExp(r'[^\d]+'), '');
    }

    final prefixedZero = RegExp(r'(0)+');
    if (val.startsWith(prefixedZero)) {
      val = val.replaceFirst(prefixedZero, '');
    }

    if (val.contains('.')) {
      val = val.replaceAll('.', '');
    }

    return val;
  }

  String sanitizeAccountBank() {
    String val = this ?? '';

    // validating for non number
    final nonNumber = RegExp(r'[^\d]+');
    if (val.contains(nonNumber)) {
      val = val.replaceAll(RegExp(r'[^\d]+'), '');
    }

    final prefixedZero = RegExp(r'(0)+');
    if (val.startsWith(prefixedZero)) {
      val = val.replaceFirst(prefixedZero, '');
    }

    if (val.contains('.')) {
      val = val.replaceAll('.', '');
    }

    return val;
  }

  String sanitizeUserNumber() {
    String val = this ?? '';

    // validating for non number
    final nonNumber = RegExp(r'[^\d]+');
    if (val.contains(nonNumber)) {
      val = val.replaceAll(RegExp(r'[^\d]+'), '');
    }

    if (val.contains('-')) {
      val = val.replaceAll('', '');
    }

    return val.trim();
  }

  String generateMD5() {
    final param = this ?? '';
    return md5.convert(utf8.encode(param)).toString();
  }

  String formatDate(
      {String format = 'dd MMM yyyy',
      String oldFormat = 'yyyy-MM-dd HH:mm:ss'}) {
    if (this != null) {
      final date = DateFormat(oldFormat).parse(this ?? '');
      return DateFormat(format).format(date);
    }
    return '';
  }

  DateTime toDateTime({String format = 'yyyy-MM-dd HH:mm:ss'}) {
    return DateFormat(format).parse(this ?? '');
  }

  String assamble() {
    return this != null ? '$this, ' : '';
  }

  bool isNullOrEmpty() {
    if (this != null) {
      return this!.isEmpty;
    }
    return this == null;
  }

  String? removeSuffix({int? range}) {
    if (!isNullOrEmpty()) {
      return this!.length >= (range ?? 1)
          ? this!.substring(0, this!.length - (range ?? 1))
          : null;
    }
    return null;
  }
}

extension FormzStatusExtension on FormzStatus {
  void handleLoading({PopupHUD? hud}) {
    try {
      switch (this) {
        case FormzStatus.submissionInProgress:
          hud?.show();
          break;
        case FormzStatus.submissionFailure:
        case FormzStatus.submissionSuccess:
          hud?.dismiss();
          break;
        default:
      }
    } catch (e) {
      debugPrint(e.toString());
    }
  }
}

extension ListUtils<T> on List<T>? {
  bool isNullOrEmpty() {
    if (this != null) {
      return this!.isEmpty;
    }
    return this == null;
  }

  String? assamble() {
    if (isNullOrEmpty()) {
      return null;
    }

    if (runtimeType != List<String?>) {
      return null;
    }
    String assambleText = '';
    for (var i = 0; i < this!.length; i++) {
      final text = this![i] as String?;
      assambleText =
          '$assambleText${!text.isNullOrEmpty() ? i != this!.length - 1 || i != 0 ? '$text, ' : text : ''}';
    }
    return assambleText.removeSuffix(range: 2);
  }
}

extension DioErrorExtension on DioError {
  Exception handleError() {
    if (response?.statusCode == 403) {
      return const InvalidTokenException();
    }

    if (response?.statusCode == 502) {
      return const ServerException();
    }

    switch (type) {
      case DioErrorType.connectTimeout:
      case DioErrorType.receiveTimeout:
      case DioErrorType.sendTimeout:
        return const ConnectionException();
      case DioErrorType.response:
        return const ServerException();
      default:
        return this;
    }
  }
}

extension ExceptionExtension on Exception {
  void handleError({required Function(Exception) errorResult}) {
    if (this is DioError) {
      errorResult.call((this as DioError).handleError());
      return;
    }

    if (this is ApiException) {
      errorResult.call(this as ApiException);
      return;
    }

    errorResult.call(this);
  }

  Exception handle() {
    if (this is DioError) {
      return (this as DioError).handleError();
    }

    if (this is ApiException) {
      return this as ApiException;
    }

    return this;
  }
}

extension FutureExtension<T> on Future<ApiResponse<T>> {
  Future<ApiResponse<T>> handleResponse() async {
    final response = await this;
    if (response.error) {
      throw ApiException(response.message ?? "Something wrong!");
    }
    return response;
  }
}

extension GoogleMapsExtension on GoogleMapController {
  void updateCameraPosition({required LatLng latLng}) {
    animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
      target: latLng,
      zoom: 14.4746,
    )));
  }
}

import 'shared/shared_stub.dart'
    if (dart.library.io) 'shared/shared_io.dart'
    if (dart.library.html) 'shared/shared_html.dart';

final Shared shared = Shared();

abstract class Shared {
  factory Shared() => getShared();

  Future<String> get brand;

  Future<String> get model;

  String get os;

  Future<String> get osVersion;

  Future<String> get buildVersion;

  Future<String> get buildNumber;
}

import 'package:flutter/material.dart';
import '../config/theme_colors.dart';

class CustomTile extends StatelessWidget {
  const CustomTile({
    Key? key,
    required this.icon,
    required this.title,
    this.subtitle,
    required this.onTap,
  }) : super(key: key);

  final dynamic icon;
  final String title;
  final String? subtitle;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: const BorderRadius.all(Radius.circular(20)),
      color: Colors.white,
      child: InkWell(
        onTap: onTap,
        borderRadius: const BorderRadius.all(Radius.circular(20)),
        child: Container(
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(20)),
          ),
          child: ListTile(
            leading: icon is String
                ? Image.asset(icon, width: 28, height: 28)
                : icon as Widget,
            minLeadingWidth: 0,
            title: Text(
              title,
              style: const TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
            ),
            subtitle: subtitle != null
                ? Text(
                    subtitle!,
                    style: const TextStyle(
                      fontSize: 10,
                      color: ThemeColors.greyPrimary,
                    ),
                  )
                : null,
            visualDensity: VisualDensity.compact,
          ),
        ),
      ),
    );
  }
}

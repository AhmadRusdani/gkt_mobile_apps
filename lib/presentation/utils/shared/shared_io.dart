import 'dart:async';
import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:package_info_plus/package_info_plus.dart';

import '../shared.dart';

Shared getShared() => SharedIo();

class SharedIo implements Shared {
  SharedIo() {
    final deviceInfo = DeviceInfoPlugin();
    if (Platform.isAndroid) {
      deviceInfo.androidInfo.then((info) {
        androidDeviceInfoPlugin.complete(info);
      });
    } else if (Platform.isIOS) {
      deviceInfo.iosInfo.then((info) {
        iosDeviceInfoPlugin.complete(info);
      });
    }

    PackageInfo.fromPlatform().then((info) => packageInfo.complete(info));
  }

  final androidDeviceInfoPlugin = Completer<AndroidDeviceInfo>();
  final iosDeviceInfoPlugin = Completer<IosDeviceInfo>();
  final packageInfo = Completer<PackageInfo>();

  @override
  Future<String> get brand async {
    if (Platform.isAndroid) {
      return (await androidDeviceInfoPlugin.future)
              .manufacturer
              ?.toUpperCase() ??
          'GOOGLE';
    }

    if (Platform.isIOS) {
      return 'APPLE';
    }

    throw UnsupportedError('unknown platform');
  }

  @override
  Future<String> get model async {
    if (Platform.isAndroid) {
      return (await androidDeviceInfoPlugin.future).model!;
    }

    if (Platform.isIOS) {
      return (await iosDeviceInfoPlugin.future).utsname.machine!;
    }

    throw UnsupportedError('unknown platform');
  }

  @override
  String get os => Platform.operatingSystem;

  @override
  Future<String> get osVersion async {
    if (Platform.isAndroid) {
      return (await androidDeviceInfoPlugin.future).version.release!;
    }

    if (Platform.isIOS) {
      return (await iosDeviceInfoPlugin.future).systemVersion!;
    }

    throw UnsupportedError('unknown platform');
  }

  @override
  Future<String> get buildNumber async =>
      (await packageInfo.future).buildNumber;

  @override
  Future<String> get buildVersion async => (await packageInfo.future).version;
}

import '../shared.dart';

Shared getShared() => SharedHtml();

class SharedHtml implements Shared {
  @override
  Future<String> get brand => throw UnimplementedError();

  @override
  Future<String> get model => throw UnimplementedError();

  @override
  String get os => throw UnimplementedError();

  @override
  Future<String> get osVersion => throw UnimplementedError();

  @override
  Future<String> get buildNumber => throw UnimplementedError();

  @override
  Future<String> get buildVersion => throw UnimplementedError();
}

import 'package:flutter/material.dart';

const bool kDevelopment =
    bool.fromEnvironment('DEVELOPMENT', defaultValue: false);

const int kPinLength = 6;
const int kOtpLength = 6;
const String kBaseUrlProd = 'https://';
const String kBaseUrlDev = 'http://149.28.156.71:8887/';

const double kHomeThresholdAppBar = 30.0;
const double kHomeThresholdAppBarTransition = 120.0;
const int kHomeBannerWidth = 375;
const int kHomeBannerHeight = 220;
const int kOnBoardingBannerWidth = 385;
const int kOnBoardingBannerHeight = 230;
const Duration kAutoPlayInterval = Duration(seconds: 10);
const Duration kAutoPlayAnimationDuration = Duration(milliseconds: 800);
const String kChannelId = 'channel_1';
const String kChannelName = 'gkt_channel_1';
const kModalBottomShape = RoundedRectangleBorder(
  borderRadius: BorderRadius.only(
    topLeft: Radius.circular(20),
    topRight: Radius.circular(20),
  ),
);

const String kLoremIpsum =
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ';
const String kAuth = 'Authorization';

const String registerSiteVisitMessage =
    'Halo Tabernakel Family Admin, saya ingin mendaftar untuk site visit GKT PIK2';
const String gktPhone = '628179115400';

part of 'change_password_cubit.dart';

@freezed
class ChangePasswordState with _$ChangePasswordState {
  const factory ChangePasswordState({
    @Default(FormzStatus.pure) FormzStatus status,
    @Default(Password.pure()) Password oldPassword,
    @Default(Password.pure()) Password newPassword,
    @Default(PasswordConfirmation.pure()) PasswordConfirmation confirmPassword,
    @Default(null) ErrorState? err,
  }) = _ChangePasswordState;
}
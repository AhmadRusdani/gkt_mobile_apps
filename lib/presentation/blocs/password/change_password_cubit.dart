import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:gkt/presentation/utils/extension.dart';
import 'package:gkt/presentation/validation/password_confirmation.dart';

import '../../../domain/repository/user_repository.dart';
import '../../validation/password.dart';
import '../base/error_state.dart';

part 'change_password_state.dart';

part 'change_password_cubit.freezed.dart';

class ChangePasswordCubit extends Cubit<ChangePasswordState> {
  ChangePasswordCubit({
    required UserRepository userRepository,
  })  : _userRepository = userRepository,
        super(const ChangePasswordState());

  final UserRepository _userRepository;

  void setOldPassword(String password) {
    final passwordValue = Password.dirty(password);
    emit(state.copyWith(
      oldPassword: passwordValue,
      status: Formz.validate([
        passwordValue,
        state.newPassword,
        state.confirmPassword,
      ]),
      err: const ErrorState(),
    ));
  }

  void setNewPassword(String password) {
    final passwordValue = Password.dirty(password);
    final confirmPassword = PasswordConfirmation.dirty(
        value: state.confirmPassword.value, password: password);
    emit(state.copyWith(
      newPassword: passwordValue,
      confirmPassword: state.confirmPassword.value.isNotEmpty
          ? confirmPassword
          : state.confirmPassword,
      status: Formz.validate([
        state.oldPassword,
        passwordValue,
        state.confirmPassword.value.isNotEmpty
            ? confirmPassword
            : state.confirmPassword,
      ]),
      err: const ErrorState(),
    ));
  }

  void setConfirmPassword(String password) {
    final passwordValue = PasswordConfirmation.dirty(
      value: password,
      password: state.newPassword.value,
    );

    emit(state.copyWith(
      confirmPassword: passwordValue,
      status: Formz.validate([
        state.oldPassword,
        state.newPassword,
        passwordValue,
      ]),
      err: const ErrorState(),
    ));
  }

  void changePassword() async {
    emit(state.copyWith(
      status: FormzStatus.submissionInProgress,
      err: null,
    ));
    try {
      final oldPassword = state.oldPassword.value;
      final newPassword = state.newPassword.value;
      final response = await _userRepository.changePassword(
        oldPassword: oldPassword,
        newPassword: newPassword,
      );
      emit(state.copyWith(
        status: response != null
            ? FormzStatus.submissionSuccess
            : FormzStatus.submissionFailure,
      ));
    } on Exception catch (e) {
      emit(state.copyWith(
          status: FormzStatus.submissionFailure,
          err: ErrorState(
            err: e.handle(),
            errMsg: e.toString(),
          )));
    } catch (e) {
      emit(
        state.copyWith(
            status: FormzStatus.submissionFailure,
            err: ErrorState(errMsg: e.toString())),
      );
    }
  }
}

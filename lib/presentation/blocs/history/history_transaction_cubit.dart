import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:gkt/domain/model/history/history_transaction_model.dart';
import 'package:gkt/domain/repository/main_repository.dart';
import 'package:gkt/presentation/blocs/base/base_state.dart';
import 'package:gkt/presentation/utils/extension.dart';

import '../base/error_state.dart';

part 'history_transaction_cubit.freezed.dart';
part 'history_transaction_state.dart';

class HistoryTransactionCubit extends Cubit<HistoryTransactionState> {
  HistoryTransactionCubit({
    required MainRepository mainRepository,
  })  : _mainRepository = mainRepository,
        super(const HistoryTransactionState());

  final MainRepository _mainRepository;

  void getMonthly() async {
    emit(state.copyWith(
      stateDataMonthly: state.stateDataMonthly.copyWith(
        status: FormzStatus.submissionInProgress,
        err: null,
      ),
    ));
    try {
      final response = await _mainRepository.getHistoryTransaction(
        type: HistoryTransactionType.monthly,
      );
      emit(
        state.copyWith(
          stateDataMonthly: state.stateDataMonthly.copyWith(
            status: FormzStatus.submissionSuccess,
            data: response.data,
          ),
        ),
      );
    } on Exception catch (e) {
      emit(state.copyWith(
        stateDataMonthly: state.stateDataMonthly.copyWith(
          status: FormzStatus.submissionFailure,
          err: ErrorState(
            err: e.handle(),
            errMsg: e.toString(),
          ),
        ),
      ));
    } catch (e) {
      emit(state.copyWith(
        stateDataMonthly: state.stateDataMonthly.copyWith(
          status: FormzStatus.submissionFailure,
          err: ErrorState(
            errMsg: e.toString(),
          ),
        ),
      ));
    }
  }

  void getAll() async {
    emit(state.copyWith(
      stateDataAll: state.stateDataAll.copyWith(
        status: FormzStatus.submissionInProgress,
        err: null,
      ),
    ));
    try {
      final response = await _mainRepository.getHistoryTransaction(
        type: HistoryTransactionType.monthly,
      );
      emit(
        state.copyWith(
          stateDataAll: state.stateDataAll.copyWith(
            status: FormzStatus.submissionSuccess,
            data: response.data,
          ),
        ),
      );
    } on Exception catch (e) {
      emit(state.copyWith(
        stateDataAll: state.stateDataAll.copyWith(
          status: FormzStatus.submissionFailure,
          err: ErrorState(
            err: e.handle(),
            errMsg: e.toString(),
          ),
        ),
      ));
    } catch (e) {
      emit(state.copyWith(
        stateDataAll: state.stateDataAll.copyWith(
          status: FormzStatus.submissionFailure,
          err: ErrorState(
            errMsg: e.toString(),
          ),
        ),
      ));
    }
  }

  void changeIndex(int index) {
    emit(state.copyWith(
      pageIndex: index,
    ));
  }
}

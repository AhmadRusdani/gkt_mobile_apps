part of 'history_transaction_cubit.dart';

@freezed
class HistoryTransactionState with _$HistoryTransactionState {
  const factory HistoryTransactionState({
    @Default(0) int pageIndex,
    @Default(BaseState<List<HistoryTransactionModel>>())
        BaseState<List<HistoryTransactionModel>> stateDataMonthly,
    @Default(BaseState<List<HistoryTransactionModel>>())
        BaseState<List<HistoryTransactionModel>> stateDataAll,
  }) = _HistoryTransactionState;
}

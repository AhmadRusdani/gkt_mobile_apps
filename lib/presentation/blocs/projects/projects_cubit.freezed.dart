// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'projects_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ProjectsState {
  FormzStatus get status => throw _privateConstructorUsedError;
  BaseState<BuildingProcessModel> get buildingState =>
      throw _privateConstructorUsedError;
  NominalNumber get amount => throw _privateConstructorUsedError;
  StringField get note => throw _privateConstructorUsedError;
  BaseState<OfferingModel> get offeringState =>
      throw _privateConstructorUsedError;
  BaseState<SiteVisitModel> get siteVisitState =>
      throw _privateConstructorUsedError;
  BaseState<List<ProcessSummaryModel>> get processSummaryState =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ProjectsStateCopyWith<ProjectsState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProjectsStateCopyWith<$Res> {
  factory $ProjectsStateCopyWith(
          ProjectsState value, $Res Function(ProjectsState) then) =
      _$ProjectsStateCopyWithImpl<$Res, ProjectsState>;
  @useResult
  $Res call(
      {FormzStatus status,
      BaseState<BuildingProcessModel> buildingState,
      NominalNumber amount,
      StringField note,
      BaseState<OfferingModel> offeringState,
      BaseState<SiteVisitModel> siteVisitState,
      BaseState<List<ProcessSummaryModel>> processSummaryState});

  $BaseStateCopyWith<BuildingProcessModel, $Res> get buildingState;
  $BaseStateCopyWith<OfferingModel, $Res> get offeringState;
  $BaseStateCopyWith<SiteVisitModel, $Res> get siteVisitState;
  $BaseStateCopyWith<List<ProcessSummaryModel>, $Res> get processSummaryState;
}

/// @nodoc
class _$ProjectsStateCopyWithImpl<$Res, $Val extends ProjectsState>
    implements $ProjectsStateCopyWith<$Res> {
  _$ProjectsStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? buildingState = null,
    Object? amount = null,
    Object? note = null,
    Object? offeringState = null,
    Object? siteVisitState = null,
    Object? processSummaryState = null,
  }) {
    return _then(_value.copyWith(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as FormzStatus,
      buildingState: null == buildingState
          ? _value.buildingState
          : buildingState // ignore: cast_nullable_to_non_nullable
              as BaseState<BuildingProcessModel>,
      amount: null == amount
          ? _value.amount
          : amount // ignore: cast_nullable_to_non_nullable
              as NominalNumber,
      note: null == note
          ? _value.note
          : note // ignore: cast_nullable_to_non_nullable
              as StringField,
      offeringState: null == offeringState
          ? _value.offeringState
          : offeringState // ignore: cast_nullable_to_non_nullable
              as BaseState<OfferingModel>,
      siteVisitState: null == siteVisitState
          ? _value.siteVisitState
          : siteVisitState // ignore: cast_nullable_to_non_nullable
              as BaseState<SiteVisitModel>,
      processSummaryState: null == processSummaryState
          ? _value.processSummaryState
          : processSummaryState // ignore: cast_nullable_to_non_nullable
              as BaseState<List<ProcessSummaryModel>>,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $BaseStateCopyWith<BuildingProcessModel, $Res> get buildingState {
    return $BaseStateCopyWith<BuildingProcessModel, $Res>(_value.buildingState,
        (value) {
      return _then(_value.copyWith(buildingState: value) as $Val);
    });
  }

  @override
  @pragma('vm:prefer-inline')
  $BaseStateCopyWith<OfferingModel, $Res> get offeringState {
    return $BaseStateCopyWith<OfferingModel, $Res>(_value.offeringState,
        (value) {
      return _then(_value.copyWith(offeringState: value) as $Val);
    });
  }

  @override
  @pragma('vm:prefer-inline')
  $BaseStateCopyWith<SiteVisitModel, $Res> get siteVisitState {
    return $BaseStateCopyWith<SiteVisitModel, $Res>(_value.siteVisitState,
        (value) {
      return _then(_value.copyWith(siteVisitState: value) as $Val);
    });
  }

  @override
  @pragma('vm:prefer-inline')
  $BaseStateCopyWith<List<ProcessSummaryModel>, $Res> get processSummaryState {
    return $BaseStateCopyWith<List<ProcessSummaryModel>, $Res>(
        _value.processSummaryState, (value) {
      return _then(_value.copyWith(processSummaryState: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_ProjectsStateCopyWith<$Res>
    implements $ProjectsStateCopyWith<$Res> {
  factory _$$_ProjectsStateCopyWith(
          _$_ProjectsState value, $Res Function(_$_ProjectsState) then) =
      __$$_ProjectsStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {FormzStatus status,
      BaseState<BuildingProcessModel> buildingState,
      NominalNumber amount,
      StringField note,
      BaseState<OfferingModel> offeringState,
      BaseState<SiteVisitModel> siteVisitState,
      BaseState<List<ProcessSummaryModel>> processSummaryState});

  @override
  $BaseStateCopyWith<BuildingProcessModel, $Res> get buildingState;
  @override
  $BaseStateCopyWith<OfferingModel, $Res> get offeringState;
  @override
  $BaseStateCopyWith<SiteVisitModel, $Res> get siteVisitState;
  @override
  $BaseStateCopyWith<List<ProcessSummaryModel>, $Res> get processSummaryState;
}

/// @nodoc
class __$$_ProjectsStateCopyWithImpl<$Res>
    extends _$ProjectsStateCopyWithImpl<$Res, _$_ProjectsState>
    implements _$$_ProjectsStateCopyWith<$Res> {
  __$$_ProjectsStateCopyWithImpl(
      _$_ProjectsState _value, $Res Function(_$_ProjectsState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? buildingState = null,
    Object? amount = null,
    Object? note = null,
    Object? offeringState = null,
    Object? siteVisitState = null,
    Object? processSummaryState = null,
  }) {
    return _then(_$_ProjectsState(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as FormzStatus,
      buildingState: null == buildingState
          ? _value.buildingState
          : buildingState // ignore: cast_nullable_to_non_nullable
              as BaseState<BuildingProcessModel>,
      amount: null == amount
          ? _value.amount
          : amount // ignore: cast_nullable_to_non_nullable
              as NominalNumber,
      note: null == note
          ? _value.note
          : note // ignore: cast_nullable_to_non_nullable
              as StringField,
      offeringState: null == offeringState
          ? _value.offeringState
          : offeringState // ignore: cast_nullable_to_non_nullable
              as BaseState<OfferingModel>,
      siteVisitState: null == siteVisitState
          ? _value.siteVisitState
          : siteVisitState // ignore: cast_nullable_to_non_nullable
              as BaseState<SiteVisitModel>,
      processSummaryState: null == processSummaryState
          ? _value.processSummaryState
          : processSummaryState // ignore: cast_nullable_to_non_nullable
              as BaseState<List<ProcessSummaryModel>>,
    ));
  }
}

/// @nodoc

class _$_ProjectsState implements _ProjectsState {
  const _$_ProjectsState(
      {this.status = FormzStatus.pure,
      this.buildingState = const BaseState<BuildingProcessModel>(),
      this.amount = const NominalNumber.pure(),
      this.note = const StringField.pure(),
      this.offeringState = const BaseState<OfferingModel>(),
      this.siteVisitState = const BaseState<SiteVisitModel>(),
      this.processSummaryState = const BaseState<List<ProcessSummaryModel>>()});

  @override
  @JsonKey()
  final FormzStatus status;
  @override
  @JsonKey()
  final BaseState<BuildingProcessModel> buildingState;
  @override
  @JsonKey()
  final NominalNumber amount;
  @override
  @JsonKey()
  final StringField note;
  @override
  @JsonKey()
  final BaseState<OfferingModel> offeringState;
  @override
  @JsonKey()
  final BaseState<SiteVisitModel> siteVisitState;
  @override
  @JsonKey()
  final BaseState<List<ProcessSummaryModel>> processSummaryState;

  @override
  String toString() {
    return 'ProjectsState(status: $status, buildingState: $buildingState, amount: $amount, note: $note, offeringState: $offeringState, siteVisitState: $siteVisitState, processSummaryState: $processSummaryState)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ProjectsState &&
            (identical(other.status, status) || other.status == status) &&
            (identical(other.buildingState, buildingState) ||
                other.buildingState == buildingState) &&
            (identical(other.amount, amount) || other.amount == amount) &&
            (identical(other.note, note) || other.note == note) &&
            (identical(other.offeringState, offeringState) ||
                other.offeringState == offeringState) &&
            (identical(other.siteVisitState, siteVisitState) ||
                other.siteVisitState == siteVisitState) &&
            (identical(other.processSummaryState, processSummaryState) ||
                other.processSummaryState == processSummaryState));
  }

  @override
  int get hashCode => Object.hash(runtimeType, status, buildingState, amount,
      note, offeringState, siteVisitState, processSummaryState);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ProjectsStateCopyWith<_$_ProjectsState> get copyWith =>
      __$$_ProjectsStateCopyWithImpl<_$_ProjectsState>(this, _$identity);
}

abstract class _ProjectsState implements ProjectsState {
  const factory _ProjectsState(
          {final FormzStatus status,
          final BaseState<BuildingProcessModel> buildingState,
          final NominalNumber amount,
          final StringField note,
          final BaseState<OfferingModel> offeringState,
          final BaseState<SiteVisitModel> siteVisitState,
          final BaseState<List<ProcessSummaryModel>> processSummaryState}) =
      _$_ProjectsState;

  @override
  FormzStatus get status;
  @override
  BaseState<BuildingProcessModel> get buildingState;
  @override
  NominalNumber get amount;
  @override
  StringField get note;
  @override
  BaseState<OfferingModel> get offeringState;
  @override
  BaseState<SiteVisitModel> get siteVisitState;
  @override
  BaseState<List<ProcessSummaryModel>> get processSummaryState;
  @override
  @JsonKey(ignore: true)
  _$$_ProjectsStateCopyWith<_$_ProjectsState> get copyWith =>
      throw _privateConstructorUsedError;
}

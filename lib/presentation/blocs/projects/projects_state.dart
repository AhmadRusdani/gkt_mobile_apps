part of 'projects_cubit.dart';

@freezed
class ProjectsState with _$ProjectsState {
  const factory ProjectsState({
    @Default(FormzStatus.pure) FormzStatus status,
    @Default(BaseState<BuildingProcessModel>())
        BaseState<BuildingProcessModel> buildingState,
    @Default(NominalNumber.pure()) NominalNumber amount,
    @Default(StringField.pure()) StringField note,
    @Default(BaseState<OfferingModel>()) BaseState<OfferingModel> offeringState,
    @Default(BaseState<SiteVisitModel>())
        BaseState<SiteVisitModel> siteVisitState,
    @Default(BaseState<List<ProcessSummaryModel>>())
        BaseState<List<ProcessSummaryModel>> processSummaryState,
  }) = _ProjectsState;
}

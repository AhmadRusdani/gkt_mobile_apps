import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:gkt/domain/model/building_process/building_process_model.dart';
import 'package:gkt/domain/model/offering/offering_model.dart';
import 'package:gkt/domain/model/process_summary/process_summary_model.dart';
import 'package:gkt/domain/model/site_visit/site_visit_model.dart';
import 'package:gkt/domain/repository/main_repository.dart';
import 'package:gkt/presentation/blocs/base/base_state.dart';
import 'package:gkt/presentation/blocs/base/error_state.dart';
import 'package:gkt/presentation/utils/extension.dart';
import 'package:gkt/presentation/validation/nominal_number.dart';
import 'package:gkt/presentation/validation/string_field.dart';

part 'projects_cubit.freezed.dart';

part 'projects_state.dart';

class ProjectsCubit extends Cubit<ProjectsState> {
  ProjectsCubit({required MainRepository mainRepository})
      : _mainRepository = mainRepository,
        super(const ProjectsState()) {
    initData();
  }

  final MainRepository _mainRepository;

  void initData() {
    getSiteVisit();
    getSummaryProcess();
    getBuildingProcess();
  }

  void nominalInput(
    String amount,
  ) async {
    final nominal = NominalNumber.dirty(
      value: amount,
      minValue: null,
      maxValue: null,
    );
    emit(state.copyWith(
      status: Formz.validate([state.note, nominal]),
      amount: nominal,
    ));
  }

  void noteInput(
    String notes,
  ) async {
    final note = StringField.dirty(notes);
    emit(state.copyWith(
      status: Formz.validate([note, state.amount]),
      note: note,
    ));
  }

  void sendOffering() async {
    emit(state.copyWith(
      offeringState: state.offeringState.copyWith(
        status: FormzStatus.submissionInProgress,
        err: null,
      ),
    ));
    try {
      final controller = state.amount.textEditingController;
      final response = await _mainRepository.sendOffering(
        amount: int.parse(
          controller.text.sanitizeAccountBank(),
        ),
        notes: state.note.value,
      );
      emit(state.copyWith(
        amount: const NominalNumber.pure(),
        note: const StringField.pure(),
        offeringState: state.offeringState.copyWith(
          status: FormzStatus.submissionSuccess,
          data: response.data,
        ),
      ));
    } on Exception catch (e) {
      emit(state.copyWith(
        offeringState: state.offeringState.copyWith(
            status: FormzStatus.submissionFailure,
            err: ErrorState(
              err: e.handle(),
              errMsg: e.toString(),
            )),
      ));
    } catch (e) {
      emit(state.copyWith(
        offeringState: state.offeringState.copyWith(
            status: FormzStatus.submissionFailure,
            err: ErrorState(errMsg: e.toString())),
      ));
    }
  }

  void refreshOfferingData() {
    const nominal = NominalNumber.pure();
    emit(state.copyWith(
      amount: nominal,
      offeringState: state.offeringState.copyWith(
        status: FormzStatus.pure,
        err: null,
        data: null,
      ),
    ));
  }

  void getSiteVisit() async {
    emit(state.copyWith(
      siteVisitState: state.siteVisitState.copyWith(
        status: FormzStatus.submissionInProgress,
        err: null,
      ),
    ));
    try {
      final response = await _mainRepository.getSiteVisit();
      emit(state.copyWith(
        siteVisitState: state.siteVisitState.copyWith(
          status: FormzStatus.submissionSuccess,
          data: response.data,
        ),
      ));
    } on Exception catch (e) {
      emit(state.copyWith(
        siteVisitState: state.siteVisitState.copyWith(
            status: FormzStatus.submissionFailure,
            err: ErrorState(
              err: e.handle(),
              errMsg: e.toString(),
            )),
      ));
    } catch (e) {
      emit(state.copyWith(
        siteVisitState: state.siteVisitState.copyWith(
            status: FormzStatus.submissionFailure,
            err: ErrorState(errMsg: e.toString())),
      ));
    }
  }

  void getSummaryProcess() async {
    emit(state.copyWith(
      processSummaryState: state.processSummaryState.copyWith(
        status: FormzStatus.submissionInProgress,
        err: null,
      ),
    ));
    try {
      final response = await _mainRepository.getProcessSummary();
      emit(state.copyWith(
        processSummaryState: state.processSummaryState.copyWith(
          status: FormzStatus.submissionSuccess,
          data: response.data,
        ),
      ));
    } on Exception catch (e) {
      emit(state.copyWith(
        processSummaryState: state.processSummaryState.copyWith(
            status: FormzStatus.submissionFailure,
            err: ErrorState(
              err: e.handle(),
              errMsg: e.toString(),
            )),
      ));
    } catch (e) {
      emit(state.copyWith(
        processSummaryState: state.processSummaryState.copyWith(
            status: FormzStatus.submissionFailure,
            err: ErrorState(errMsg: e.toString())),
      ));
    }
  }

  void getBuildingProcess() async {
    emit(state.copyWith(
      buildingState: state.buildingState.copyWith(
        status: FormzStatus.submissionInProgress,
        err: null,
      ),
    ));
    try {
      final response = await _mainRepository.getBuildingProcess();
      emit(state.copyWith(
        buildingState: state.buildingState.copyWith(
          status: FormzStatus.submissionSuccess,
          data: response.data,
        ),
      ));
    } on Exception catch (e) {
      emit(state.copyWith(
        buildingState: state.buildingState.copyWith(
            status: FormzStatus.submissionFailure,
            err: ErrorState(
              err: e.handle(),
              errMsg: e.toString(),
            )),
      ));
    } catch (e) {
      emit(state.copyWith(
        buildingState: state.buildingState.copyWith(
            status: FormzStatus.submissionFailure,
            err: ErrorState(errMsg: e.toString())),
      ));
    }
  }

  @override
  Future<void> close() {
    state.amount.textEditingController.dispose();
    return super.close();
  }
}

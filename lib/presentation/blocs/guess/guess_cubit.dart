import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:gkt/domain/model/offering/offering_model.dart';
import 'package:gkt/domain/repository/main_repository.dart';
import 'package:gkt/presentation/blocs/base/base_state.dart';
import 'package:gkt/presentation/blocs/base/error_state.dart';
import 'package:gkt/presentation/utils/extension.dart';
import 'package:gkt/presentation/validation/nominal_number.dart';

part 'guess_state.dart';
part 'guess_cubit.freezed.dart';

class GuessCubit extends Cubit<GuessState> {
  GuessCubit({
    required MainRepository mainRepository,
  })  : _mainRepository = mainRepository,
        super(const GuessState());

  final MainRepository _mainRepository;

  void nominalInput(
    String amount,
  ) async {
    final nominal = NominalNumber.dirty(
      value: amount,
      minValue: null,
      maxValue: null,
    );
    emit(state.copyWith(
      status: Formz.validate([nominal]),
      amount: nominal,
    ));
  }

  void sendOffering() async {
    emit(state.copyWith(
      offeringState: state.offeringState.copyWith(
        status: FormzStatus.submissionInProgress,
        err: null,
      ),
    ));
    try {
      final controller = state.amount.textEditingController;
      final response = await _mainRepository.sendOfferingGuess(
        amount: int.parse(
          controller.text.sanitizeAccountBank(),
        ),
      );
      emit(state.copyWith(
        amount: const NominalNumber.pure(),
        offeringState: state.offeringState.copyWith(
          status: FormzStatus.submissionSuccess,
          data: response.data,
        ),
      ));
    } on Exception catch (e) {
      emit(state.copyWith(
        offeringState: state.offeringState.copyWith(
            status: FormzStatus.submissionFailure,
            err: ErrorState(
              err: e.handle(),
              errMsg: e.toString(),
            )),
      ));
    } catch (e) {
      emit(state.copyWith(
        offeringState: state.offeringState.copyWith(
            status: FormzStatus.submissionFailure,
            err: ErrorState(errMsg: e.toString())),
      ));
    }
  }
}

// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'guess_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$GuessState {
  FormzStatus get status => throw _privateConstructorUsedError;
  int get projectsPercentage => throw _privateConstructorUsedError;
  NominalNumber get amount => throw _privateConstructorUsedError;
  BaseState<OfferingModel> get offeringState =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $GuessStateCopyWith<GuessState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GuessStateCopyWith<$Res> {
  factory $GuessStateCopyWith(
          GuessState value, $Res Function(GuessState) then) =
      _$GuessStateCopyWithImpl<$Res, GuessState>;
  @useResult
  $Res call(
      {FormzStatus status,
      int projectsPercentage,
      NominalNumber amount,
      BaseState<OfferingModel> offeringState});

  $BaseStateCopyWith<OfferingModel, $Res> get offeringState;
}

/// @nodoc
class _$GuessStateCopyWithImpl<$Res, $Val extends GuessState>
    implements $GuessStateCopyWith<$Res> {
  _$GuessStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? projectsPercentage = null,
    Object? amount = null,
    Object? offeringState = null,
  }) {
    return _then(_value.copyWith(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as FormzStatus,
      projectsPercentage: null == projectsPercentage
          ? _value.projectsPercentage
          : projectsPercentage // ignore: cast_nullable_to_non_nullable
              as int,
      amount: null == amount
          ? _value.amount
          : amount // ignore: cast_nullable_to_non_nullable
              as NominalNumber,
      offeringState: null == offeringState
          ? _value.offeringState
          : offeringState // ignore: cast_nullable_to_non_nullable
              as BaseState<OfferingModel>,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $BaseStateCopyWith<OfferingModel, $Res> get offeringState {
    return $BaseStateCopyWith<OfferingModel, $Res>(_value.offeringState,
        (value) {
      return _then(_value.copyWith(offeringState: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_GuessStateCopyWith<$Res>
    implements $GuessStateCopyWith<$Res> {
  factory _$$_GuessStateCopyWith(
          _$_GuessState value, $Res Function(_$_GuessState) then) =
      __$$_GuessStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {FormzStatus status,
      int projectsPercentage,
      NominalNumber amount,
      BaseState<OfferingModel> offeringState});

  @override
  $BaseStateCopyWith<OfferingModel, $Res> get offeringState;
}

/// @nodoc
class __$$_GuessStateCopyWithImpl<$Res>
    extends _$GuessStateCopyWithImpl<$Res, _$_GuessState>
    implements _$$_GuessStateCopyWith<$Res> {
  __$$_GuessStateCopyWithImpl(
      _$_GuessState _value, $Res Function(_$_GuessState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? projectsPercentage = null,
    Object? amount = null,
    Object? offeringState = null,
  }) {
    return _then(_$_GuessState(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as FormzStatus,
      projectsPercentage: null == projectsPercentage
          ? _value.projectsPercentage
          : projectsPercentage // ignore: cast_nullable_to_non_nullable
              as int,
      amount: null == amount
          ? _value.amount
          : amount // ignore: cast_nullable_to_non_nullable
              as NominalNumber,
      offeringState: null == offeringState
          ? _value.offeringState
          : offeringState // ignore: cast_nullable_to_non_nullable
              as BaseState<OfferingModel>,
    ));
  }
}

/// @nodoc

class _$_GuessState implements _GuessState {
  const _$_GuessState(
      {this.status = FormzStatus.pure,
      this.projectsPercentage = 0,
      this.amount = const NominalNumber.pure(),
      this.offeringState = const BaseState<OfferingModel>()});

  @override
  @JsonKey()
  final FormzStatus status;
  @override
  @JsonKey()
  final int projectsPercentage;
  @override
  @JsonKey()
  final NominalNumber amount;
  @override
  @JsonKey()
  final BaseState<OfferingModel> offeringState;

  @override
  String toString() {
    return 'GuessState(status: $status, projectsPercentage: $projectsPercentage, amount: $amount, offeringState: $offeringState)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_GuessState &&
            (identical(other.status, status) || other.status == status) &&
            (identical(other.projectsPercentage, projectsPercentage) ||
                other.projectsPercentage == projectsPercentage) &&
            (identical(other.amount, amount) || other.amount == amount) &&
            (identical(other.offeringState, offeringState) ||
                other.offeringState == offeringState));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, status, projectsPercentage, amount, offeringState);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_GuessStateCopyWith<_$_GuessState> get copyWith =>
      __$$_GuessStateCopyWithImpl<_$_GuessState>(this, _$identity);
}

abstract class _GuessState implements GuessState {
  const factory _GuessState(
      {final FormzStatus status,
      final int projectsPercentage,
      final NominalNumber amount,
      final BaseState<OfferingModel> offeringState}) = _$_GuessState;

  @override
  FormzStatus get status;
  @override
  int get projectsPercentage;
  @override
  NominalNumber get amount;
  @override
  BaseState<OfferingModel> get offeringState;
  @override
  @JsonKey(ignore: true)
  _$$_GuessStateCopyWith<_$_GuessState> get copyWith =>
      throw _privateConstructorUsedError;
}

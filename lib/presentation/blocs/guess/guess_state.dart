part of 'guess_cubit.dart';

@freezed
class GuessState with _$GuessState {
  const factory GuessState({
    @Default(FormzStatus.pure) FormzStatus status,
    @Default(0) int projectsPercentage,
    @Default(NominalNumber.pure()) NominalNumber amount,
    @Default(BaseState<OfferingModel>()) BaseState<OfferingModel> offeringState,
  }) = _GuessState;
}

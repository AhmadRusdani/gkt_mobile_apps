import 'dart:ui' as ui;
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:geocoding/geocoding.dart';
import 'package:gkt/domain/repository/maps_repository.dart';
import 'package:gkt/presentation/utils/extension.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../base/error_state.dart';

part 'maps_state.dart';
part 'maps_cubit.freezed.dart';

class MapsCubit extends Cubit<MapsState> {
  MapsCubit({
    required MapsRepository mapsRepository,
  })  : _mapsRepository = mapsRepository,
        super(const MapsState()) {
    _getBytesFromAsset('assets/images/gps_icon.png', 100);
  }

  final MapsRepository _mapsRepository;

  void setAddress(LatLng latLng) async {
    try {
      final address = await placemarkFromCoordinates(
        latLng.latitude,
        latLng.longitude,
      );
      print(address[0]);
      final addressName = address.isNotEmpty
          ? [
              address[0].street,
              address[0].name,
              address[0].subLocality,
              address[0].locality,
              address[0].administrativeArea,
            ].assamble()
          : null;
      print(addressName);
      emit(state.copyWith(
        latLng: latLng,
        address: addressName,
      ));
    } catch (e) {
      print(e.toString());
      if (e is Exception) {
        emit(state.copyWith(
          err: ErrorState(err: e),
        ));
      }
    }
  }

  void updateLocations(LatLng latLng) {
    emit(state.copyWith(
      latLng: latLng,
    ));
  }

  void updateMarker(LatLng latLng) async {
    final marker = <Marker>{};
    marker.add(_createMarker(latLng));
    try {
      emit(state.copyWith(
        marker: marker,
      ));
    } catch (e) {
      print(e.toString());
      if (e is Exception) {
        emit(state.copyWith(
          err: ErrorState(err: e),
        ));
      }
    }
  }

  Marker _createMarker(LatLng latLng) {
    final markerIcon = state.markerIcon;
    if (markerIcon != null) {
      return Marker(
        markerId: const MarkerId('marker_1'),
        position: latLng,
        icon: markerIcon,
      );
    } else {
      return const Marker(
        markerId: MarkerId('marker_1'),
        position: LatLng(-6.2973927, 106.640998),
      );
    }
  }

  void _getBytesFromAsset(String path, int width) async {
    try {
      ByteData data = await rootBundle.load(path);
      ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
          targetWidth: width);
      ui.FrameInfo fi = await codec.getNextFrame();
      final newPath =
          (await fi.image.toByteData(format: ui.ImageByteFormat.png))
              ?.buffer
              .asUint8List();
      emit(state.copyWith(
        markerIcon:
            newPath != null ? BitmapDescriptor.fromBytes(newPath) : null,
        err: null,
      ));
    } catch (e) {
      print(e.toString());
      if (e is Exception) {
        emit(state.copyWith(
          err: ErrorState(err: e),
        ));
      }
    }
  }
}

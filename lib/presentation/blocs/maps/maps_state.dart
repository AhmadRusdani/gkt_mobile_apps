part of 'maps_cubit.dart';

@freezed
class MapsState with _$MapsState {
  const factory MapsState({
    @Default(LatLng(-6.2973927, 106.640998)) LatLng latLng,
    @Default(null) BitmapDescriptor? markerIcon,
    @Default({}) Set<Marker> marker,
    @Default(null) String? address,
    @Default(false) bool isPermissionAcc,
    @Default(null) ErrorState? err,
  }) = _MapsState;
}

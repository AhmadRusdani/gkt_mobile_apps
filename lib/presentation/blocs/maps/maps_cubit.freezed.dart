// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'maps_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$MapsState {
  LatLng get latLng => throw _privateConstructorUsedError;
  BitmapDescriptor? get markerIcon => throw _privateConstructorUsedError;
  Set<Marker> get marker => throw _privateConstructorUsedError;
  String? get address => throw _privateConstructorUsedError;
  bool get isPermissionAcc => throw _privateConstructorUsedError;
  ErrorState? get err => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $MapsStateCopyWith<MapsState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MapsStateCopyWith<$Res> {
  factory $MapsStateCopyWith(MapsState value, $Res Function(MapsState) then) =
      _$MapsStateCopyWithImpl<$Res, MapsState>;
  @useResult
  $Res call(
      {LatLng latLng,
      BitmapDescriptor? markerIcon,
      Set<Marker> marker,
      String? address,
      bool isPermissionAcc,
      ErrorState? err});

  $ErrorStateCopyWith<$Res>? get err;
}

/// @nodoc
class _$MapsStateCopyWithImpl<$Res, $Val extends MapsState>
    implements $MapsStateCopyWith<$Res> {
  _$MapsStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? latLng = null,
    Object? markerIcon = freezed,
    Object? marker = null,
    Object? address = freezed,
    Object? isPermissionAcc = null,
    Object? err = freezed,
  }) {
    return _then(_value.copyWith(
      latLng: null == latLng
          ? _value.latLng
          : latLng // ignore: cast_nullable_to_non_nullable
              as LatLng,
      markerIcon: freezed == markerIcon
          ? _value.markerIcon
          : markerIcon // ignore: cast_nullable_to_non_nullable
              as BitmapDescriptor?,
      marker: null == marker
          ? _value.marker
          : marker // ignore: cast_nullable_to_non_nullable
              as Set<Marker>,
      address: freezed == address
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as String?,
      isPermissionAcc: null == isPermissionAcc
          ? _value.isPermissionAcc
          : isPermissionAcc // ignore: cast_nullable_to_non_nullable
              as bool,
      err: freezed == err
          ? _value.err
          : err // ignore: cast_nullable_to_non_nullable
              as ErrorState?,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $ErrorStateCopyWith<$Res>? get err {
    if (_value.err == null) {
      return null;
    }

    return $ErrorStateCopyWith<$Res>(_value.err!, (value) {
      return _then(_value.copyWith(err: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_MapsStateCopyWith<$Res> implements $MapsStateCopyWith<$Res> {
  factory _$$_MapsStateCopyWith(
          _$_MapsState value, $Res Function(_$_MapsState) then) =
      __$$_MapsStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {LatLng latLng,
      BitmapDescriptor? markerIcon,
      Set<Marker> marker,
      String? address,
      bool isPermissionAcc,
      ErrorState? err});

  @override
  $ErrorStateCopyWith<$Res>? get err;
}

/// @nodoc
class __$$_MapsStateCopyWithImpl<$Res>
    extends _$MapsStateCopyWithImpl<$Res, _$_MapsState>
    implements _$$_MapsStateCopyWith<$Res> {
  __$$_MapsStateCopyWithImpl(
      _$_MapsState _value, $Res Function(_$_MapsState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? latLng = null,
    Object? markerIcon = freezed,
    Object? marker = null,
    Object? address = freezed,
    Object? isPermissionAcc = null,
    Object? err = freezed,
  }) {
    return _then(_$_MapsState(
      latLng: null == latLng
          ? _value.latLng
          : latLng // ignore: cast_nullable_to_non_nullable
              as LatLng,
      markerIcon: freezed == markerIcon
          ? _value.markerIcon
          : markerIcon // ignore: cast_nullable_to_non_nullable
              as BitmapDescriptor?,
      marker: null == marker
          ? _value._marker
          : marker // ignore: cast_nullable_to_non_nullable
              as Set<Marker>,
      address: freezed == address
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as String?,
      isPermissionAcc: null == isPermissionAcc
          ? _value.isPermissionAcc
          : isPermissionAcc // ignore: cast_nullable_to_non_nullable
              as bool,
      err: freezed == err
          ? _value.err
          : err // ignore: cast_nullable_to_non_nullable
              as ErrorState?,
    ));
  }
}

/// @nodoc

class _$_MapsState implements _MapsState {
  const _$_MapsState(
      {this.latLng = const LatLng(-6.2973927, 106.640998),
      this.markerIcon = null,
      final Set<Marker> marker = const {},
      this.address = null,
      this.isPermissionAcc = false,
      this.err = null})
      : _marker = marker;

  @override
  @JsonKey()
  final LatLng latLng;
  @override
  @JsonKey()
  final BitmapDescriptor? markerIcon;
  final Set<Marker> _marker;
  @override
  @JsonKey()
  Set<Marker> get marker {
    if (_marker is EqualUnmodifiableSetView) return _marker;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableSetView(_marker);
  }

  @override
  @JsonKey()
  final String? address;
  @override
  @JsonKey()
  final bool isPermissionAcc;
  @override
  @JsonKey()
  final ErrorState? err;

  @override
  String toString() {
    return 'MapsState(latLng: $latLng, markerIcon: $markerIcon, marker: $marker, address: $address, isPermissionAcc: $isPermissionAcc, err: $err)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_MapsState &&
            (identical(other.latLng, latLng) || other.latLng == latLng) &&
            (identical(other.markerIcon, markerIcon) ||
                other.markerIcon == markerIcon) &&
            const DeepCollectionEquality().equals(other._marker, _marker) &&
            (identical(other.address, address) || other.address == address) &&
            (identical(other.isPermissionAcc, isPermissionAcc) ||
                other.isPermissionAcc == isPermissionAcc) &&
            (identical(other.err, err) || other.err == err));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      latLng,
      markerIcon,
      const DeepCollectionEquality().hash(_marker),
      address,
      isPermissionAcc,
      err);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_MapsStateCopyWith<_$_MapsState> get copyWith =>
      __$$_MapsStateCopyWithImpl<_$_MapsState>(this, _$identity);
}

abstract class _MapsState implements MapsState {
  const factory _MapsState(
      {final LatLng latLng,
      final BitmapDescriptor? markerIcon,
      final Set<Marker> marker,
      final String? address,
      final bool isPermissionAcc,
      final ErrorState? err}) = _$_MapsState;

  @override
  LatLng get latLng;
  @override
  BitmapDescriptor? get markerIcon;
  @override
  Set<Marker> get marker;
  @override
  String? get address;
  @override
  bool get isPermissionAcc;
  @override
  ErrorState? get err;
  @override
  @JsonKey(ignore: true)
  _$$_MapsStateCopyWith<_$_MapsState> get copyWith =>
      throw _privateConstructorUsedError;
}

// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'base_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$BaseState<T> {
  FormzStatus get status => throw _privateConstructorUsedError;
  ErrorState? get err => throw _privateConstructorUsedError;
  T? get data => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $BaseStateCopyWith<T, BaseState<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BaseStateCopyWith<T, $Res> {
  factory $BaseStateCopyWith(
          BaseState<T> value, $Res Function(BaseState<T>) then) =
      _$BaseStateCopyWithImpl<T, $Res, BaseState<T>>;
  @useResult
  $Res call({FormzStatus status, ErrorState? err, T? data});

  $ErrorStateCopyWith<$Res>? get err;
}

/// @nodoc
class _$BaseStateCopyWithImpl<T, $Res, $Val extends BaseState<T>>
    implements $BaseStateCopyWith<T, $Res> {
  _$BaseStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? err = freezed,
    Object? data = freezed,
  }) {
    return _then(_value.copyWith(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as FormzStatus,
      err: freezed == err
          ? _value.err
          : err // ignore: cast_nullable_to_non_nullable
              as ErrorState?,
      data: freezed == data
          ? _value.data
          : data // ignore: cast_nullable_to_non_nullable
              as T?,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $ErrorStateCopyWith<$Res>? get err {
    if (_value.err == null) {
      return null;
    }

    return $ErrorStateCopyWith<$Res>(_value.err!, (value) {
      return _then(_value.copyWith(err: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_BaseStateCopyWith<T, $Res>
    implements $BaseStateCopyWith<T, $Res> {
  factory _$$_BaseStateCopyWith(
          _$_BaseState<T> value, $Res Function(_$_BaseState<T>) then) =
      __$$_BaseStateCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call({FormzStatus status, ErrorState? err, T? data});

  @override
  $ErrorStateCopyWith<$Res>? get err;
}

/// @nodoc
class __$$_BaseStateCopyWithImpl<T, $Res>
    extends _$BaseStateCopyWithImpl<T, $Res, _$_BaseState<T>>
    implements _$$_BaseStateCopyWith<T, $Res> {
  __$$_BaseStateCopyWithImpl(
      _$_BaseState<T> _value, $Res Function(_$_BaseState<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? err = freezed,
    Object? data = freezed,
  }) {
    return _then(_$_BaseState<T>(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as FormzStatus,
      err: freezed == err
          ? _value.err
          : err // ignore: cast_nullable_to_non_nullable
              as ErrorState?,
      data: freezed == data
          ? _value.data
          : data // ignore: cast_nullable_to_non_nullable
              as T?,
    ));
  }
}

/// @nodoc

class _$_BaseState<T> implements _BaseState<T> {
  const _$_BaseState(
      {this.status = FormzStatus.pure, this.err = null, this.data = null});

  @override
  @JsonKey()
  final FormzStatus status;
  @override
  @JsonKey()
  final ErrorState? err;
  @override
  @JsonKey()
  final T? data;

  @override
  String toString() {
    return 'BaseState<$T>(status: $status, err: $err, data: $data)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_BaseState<T> &&
            (identical(other.status, status) || other.status == status) &&
            (identical(other.err, err) || other.err == err) &&
            const DeepCollectionEquality().equals(other.data, data));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, status, err, const DeepCollectionEquality().hash(data));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_BaseStateCopyWith<T, _$_BaseState<T>> get copyWith =>
      __$$_BaseStateCopyWithImpl<T, _$_BaseState<T>>(this, _$identity);
}

abstract class _BaseState<T> implements BaseState<T> {
  const factory _BaseState(
      {final FormzStatus status,
      final ErrorState? err,
      final T? data}) = _$_BaseState<T>;

  @override
  FormzStatus get status;
  @override
  ErrorState? get err;
  @override
  T? get data;
  @override
  @JsonKey(ignore: true)
  _$$_BaseStateCopyWith<T, _$_BaseState<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

import 'package:freezed_annotation/freezed_annotation.dart';

part 'error_state.freezed.dart';

@freezed
class ErrorState with _$ErrorState {
  const factory ErrorState({
    String? errMsg,
    Exception? err,
  }) = _ErrorState;
}

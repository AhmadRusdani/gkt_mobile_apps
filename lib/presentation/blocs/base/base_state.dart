import 'package:formz/formz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import 'error_state.dart';

part 'base_state.freezed.dart';
// part 'base_state.g.dart';

@Freezed(genericArgumentFactories: true)
class BaseState<T> with _$BaseState<T> {
  const factory BaseState({
    @Default(FormzStatus.pure) FormzStatus status,
    @Default(null) ErrorState? err,
    @Default(null) T? data,
  }) = _BaseState<T>;
}

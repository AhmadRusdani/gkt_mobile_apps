import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:gkt/domain/exception/api_exception.dart';
import 'package:gkt/domain/model/user/user_model.dart';
import 'package:gkt/domain/repository/auth_repository.dart';
import 'package:gkt/domain/repository/user_repository.dart';
import 'package:gkt/presentation/blocs/base/base_state.dart';

class ProfileCubit extends Cubit<BaseState<ChangeUserDetailModel>> {
  ProfileCubit({
    required this.authRepository,
    required this.userRepository,
  }) : super(const BaseState<ChangeUserDetailModel>());

  final AuthRepository authRepository;
  final UserRepository userRepository;

  changeData({
    String? address,
    String? phone,
  }) async {
    try {
      emit(state.copyWith(
        status: FormzStatus.submissionInProgress,
        data: null,
      ));
      if (phone?.startsWith('0') == true) phone = phone?.substring(1);
      final response = await (address != null
          ? userRepository.changeAddress(address: address ?? '')
          : userRepository.changePhone(phone: phone ?? ''));
      emit(state.copyWith(
        status: response.data != null
            ? FormzStatus.submissionSuccess
            : FormzStatus.submissionFailure,
        data: response.data,
      ));
    } on ApiException catch (_) {
      emit(state.copyWith(
        status: FormzStatus.submissionFailure,
        data: null,
      ));
    } catch (e) {
      emit(state.copyWith(
        status: FormzStatus.submissionFailure,
        data: null,
      ));
    }
  }
}

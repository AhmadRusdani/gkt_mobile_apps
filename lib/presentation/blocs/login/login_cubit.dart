import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:gkt/domain/exception/api_exception.dart';
import 'package:gkt/domain/repository/auth_repository.dart';
import 'package:gkt/domain/repository/user_repository.dart';
import 'package:gkt/presentation/blocs/base/error_state.dart';
import 'package:gkt/presentation/utils/extension.dart';
import 'package:gkt/presentation/validation/name.dart';
import 'package:gkt/presentation/validation/password.dart';

part 'login_state.dart';
part 'login_cubit.freezed.dart';

class LoginCubit extends Cubit<LoginState> {
  LoginCubit({
    required AuthRepository authRepository,
    required UserRepository userRepository,
  })  : _authRepository = authRepository,
        _userRepository = userRepository,
        super(const LoginState());

  final AuthRepository _authRepository;
  final UserRepository _userRepository;

  void login() async {
    emit(state.copyWith(status: FormzStatus.submissionInProgress));
    try {
      final response = await _authRepository.loginUser(
        noUser: state.number.value.sanitizeUserNumber(),
        password: state.password.value,
      );
      await _userRepository.setSessionToken(response.data?.accessToken);
      emit(state.copyWith(
        status: FormzStatus.submissionSuccess,
        name: response.data?.namaAnggota ?? '',
        err: const ErrorState(
          errMsg: null,
        ),
      ));
    } on ApiException catch (e) {
      emit(state.copyWith(
        status: FormzStatus.submissionFailure,
        err: ErrorState(err: e, errMsg: e.message),
      ));
    } catch (e) {
      if (e is Exception) {
        emit(state.copyWith(
          status: FormzStatus.submissionFailure,
          err: ErrorState(
            err: e.handle(),
            errMsg: e.toString(),
          ),
        ));
        return;
      }
      emit(state.copyWith(
        status: FormzStatus.submissionFailure,
        err: ErrorState(errMsg: e.toString()),
      ));
    }
  }

  void setNumber(String number) {
    final numberValue = NameField.dirty(number);
    emit(state.copyWith(
      number: numberValue,
      status: Formz.validate([
        numberValue,
        state.password,
      ]),
      name: '',
      err: const ErrorState(),
    ));
  }

  void setPassword(String password) {
    final passwordValue = Password.dirty(password);
    emit(state.copyWith(
      password: passwordValue,
      status: Formz.validate([
        state.number,
        passwordValue,
      ]),
      name: '',
      err: const ErrorState(),
    ));
  }
}

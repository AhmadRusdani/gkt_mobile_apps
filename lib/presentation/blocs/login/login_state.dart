part of 'login_cubit.dart';

@freezed
class LoginState with _$LoginState {
  const factory LoginState({
    @Default(FormzStatus.pure) FormzStatus status,
    @Default(NameField.pure()) NameField number,
    @Default(Password.pure()) Password password,
    @Default('') String name,
    @Default(null) ErrorState? err,
  }) = _LoginState;
}

part of 'building_process_cubit.dart';

@freezed
class BuildingProcessState with _$BuildingProcessState {
  const factory BuildingProcessState({
    @Default(FormzStatus.pure) FormzStatus status,
    @Default(null) ErrorState? err,
    @Default(null) List<DetailProcessSummaryItemModel>? data,
  }) = _BuildingProcessState;
}

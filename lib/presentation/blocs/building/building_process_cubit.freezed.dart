// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'building_process_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$BuildingProcessState {
  FormzStatus get status => throw _privateConstructorUsedError;
  ErrorState? get err => throw _privateConstructorUsedError;
  List<DetailProcessSummaryItemModel>? get data =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $BuildingProcessStateCopyWith<BuildingProcessState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BuildingProcessStateCopyWith<$Res> {
  factory $BuildingProcessStateCopyWith(BuildingProcessState value,
          $Res Function(BuildingProcessState) then) =
      _$BuildingProcessStateCopyWithImpl<$Res, BuildingProcessState>;
  @useResult
  $Res call(
      {FormzStatus status,
      ErrorState? err,
      List<DetailProcessSummaryItemModel>? data});

  $ErrorStateCopyWith<$Res>? get err;
}

/// @nodoc
class _$BuildingProcessStateCopyWithImpl<$Res,
        $Val extends BuildingProcessState>
    implements $BuildingProcessStateCopyWith<$Res> {
  _$BuildingProcessStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? err = freezed,
    Object? data = freezed,
  }) {
    return _then(_value.copyWith(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as FormzStatus,
      err: freezed == err
          ? _value.err
          : err // ignore: cast_nullable_to_non_nullable
              as ErrorState?,
      data: freezed == data
          ? _value.data
          : data // ignore: cast_nullable_to_non_nullable
              as List<DetailProcessSummaryItemModel>?,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $ErrorStateCopyWith<$Res>? get err {
    if (_value.err == null) {
      return null;
    }

    return $ErrorStateCopyWith<$Res>(_value.err!, (value) {
      return _then(_value.copyWith(err: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_BuildingProcessStateCopyWith<$Res>
    implements $BuildingProcessStateCopyWith<$Res> {
  factory _$$_BuildingProcessStateCopyWith(_$_BuildingProcessState value,
          $Res Function(_$_BuildingProcessState) then) =
      __$$_BuildingProcessStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {FormzStatus status,
      ErrorState? err,
      List<DetailProcessSummaryItemModel>? data});

  @override
  $ErrorStateCopyWith<$Res>? get err;
}

/// @nodoc
class __$$_BuildingProcessStateCopyWithImpl<$Res>
    extends _$BuildingProcessStateCopyWithImpl<$Res, _$_BuildingProcessState>
    implements _$$_BuildingProcessStateCopyWith<$Res> {
  __$$_BuildingProcessStateCopyWithImpl(_$_BuildingProcessState _value,
      $Res Function(_$_BuildingProcessState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? err = freezed,
    Object? data = freezed,
  }) {
    return _then(_$_BuildingProcessState(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as FormzStatus,
      err: freezed == err
          ? _value.err
          : err // ignore: cast_nullable_to_non_nullable
              as ErrorState?,
      data: freezed == data
          ? _value._data
          : data // ignore: cast_nullable_to_non_nullable
              as List<DetailProcessSummaryItemModel>?,
    ));
  }
}

/// @nodoc

class _$_BuildingProcessState implements _BuildingProcessState {
  const _$_BuildingProcessState(
      {this.status = FormzStatus.pure,
      this.err = null,
      final List<DetailProcessSummaryItemModel>? data = null})
      : _data = data;

  @override
  @JsonKey()
  final FormzStatus status;
  @override
  @JsonKey()
  final ErrorState? err;
  final List<DetailProcessSummaryItemModel>? _data;
  @override
  @JsonKey()
  List<DetailProcessSummaryItemModel>? get data {
    final value = _data;
    if (value == null) return null;
    if (_data is EqualUnmodifiableListView) return _data;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'BuildingProcessState(status: $status, err: $err, data: $data)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_BuildingProcessState &&
            (identical(other.status, status) || other.status == status) &&
            (identical(other.err, err) || other.err == err) &&
            const DeepCollectionEquality().equals(other._data, _data));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, status, err, const DeepCollectionEquality().hash(_data));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_BuildingProcessStateCopyWith<_$_BuildingProcessState> get copyWith =>
      __$$_BuildingProcessStateCopyWithImpl<_$_BuildingProcessState>(
          this, _$identity);
}

abstract class _BuildingProcessState implements BuildingProcessState {
  const factory _BuildingProcessState(
          {final FormzStatus status,
          final ErrorState? err,
          final List<DetailProcessSummaryItemModel>? data}) =
      _$_BuildingProcessState;

  @override
  FormzStatus get status;
  @override
  ErrorState? get err;
  @override
  List<DetailProcessSummaryItemModel>? get data;
  @override
  @JsonKey(ignore: true)
  _$$_BuildingProcessStateCopyWith<_$_BuildingProcessState> get copyWith =>
      throw _privateConstructorUsedError;
}

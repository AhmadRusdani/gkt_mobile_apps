import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:gkt/domain/model/detail_process_summary/detail_process_summary_model.dart';
import 'package:gkt/domain/repository/main_repository.dart';
import 'package:gkt/presentation/blocs/base/error_state.dart';
import 'package:gkt/presentation/utils/extension.dart';

part 'building_process_state.dart';
part 'building_process_cubit.freezed.dart';

class BuildingProcessCubit extends Cubit<BuildingProcessState> {
  BuildingProcessCubit({required this.mainRepository, required this.id})
      : super(const BuildingProcessState()) {
    getBuildingProcess();
  }

  final MainRepository mainRepository;
  final int id;

  void getBuildingProcess() async {
    emit(state.copyWith(
      status: FormzStatus.submissionInProgress,
      err: null,
    ));
    try {
      final response = await mainRepository.getDetailProcessSummary(id: id);
      emit(state.copyWith(
        status: FormzStatus.submissionSuccess,
        data: response.data?.detail,
      ));
    } on Exception catch (e) {
      emit(state.copyWith(
            status: FormzStatus.submissionFailure,
            err: ErrorState(
              err: e.handle(),
              errMsg: e.toString(),
            )));
    } catch (e) {
      emit(
        state.copyWith(
            status: FormzStatus.submissionFailure,
            err: ErrorState(errMsg: e.toString())),
      );
    }
  }
}

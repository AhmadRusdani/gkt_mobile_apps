// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'webview_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$WebViewState {
  String get title => throw _privateConstructorUsedError;
  String get url => throw _privateConstructorUsedError;
  bool get isLoading => throw _privateConstructorUsedError;
  double get progressValue => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $WebViewStateCopyWith<WebViewState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $WebViewStateCopyWith<$Res> {
  factory $WebViewStateCopyWith(
          WebViewState value, $Res Function(WebViewState) then) =
      _$WebViewStateCopyWithImpl<$Res, WebViewState>;
  @useResult
  $Res call({String title, String url, bool isLoading, double progressValue});
}

/// @nodoc
class _$WebViewStateCopyWithImpl<$Res, $Val extends WebViewState>
    implements $WebViewStateCopyWith<$Res> {
  _$WebViewStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? title = null,
    Object? url = null,
    Object? isLoading = null,
    Object? progressValue = null,
  }) {
    return _then(_value.copyWith(
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      url: null == url
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String,
      isLoading: null == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      progressValue: null == progressValue
          ? _value.progressValue
          : progressValue // ignore: cast_nullable_to_non_nullable
              as double,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_WebViewStateCopyWith<$Res>
    implements $WebViewStateCopyWith<$Res> {
  factory _$$_WebViewStateCopyWith(
          _$_WebViewState value, $Res Function(_$_WebViewState) then) =
      __$$_WebViewStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String title, String url, bool isLoading, double progressValue});
}

/// @nodoc
class __$$_WebViewStateCopyWithImpl<$Res>
    extends _$WebViewStateCopyWithImpl<$Res, _$_WebViewState>
    implements _$$_WebViewStateCopyWith<$Res> {
  __$$_WebViewStateCopyWithImpl(
      _$_WebViewState _value, $Res Function(_$_WebViewState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? title = null,
    Object? url = null,
    Object? isLoading = null,
    Object? progressValue = null,
  }) {
    return _then(_$_WebViewState(
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      url: null == url
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String,
      isLoading: null == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      progressValue: null == progressValue
          ? _value.progressValue
          : progressValue // ignore: cast_nullable_to_non_nullable
              as double,
    ));
  }
}

/// @nodoc

class _$_WebViewState implements _WebViewState {
  const _$_WebViewState(
      {this.title = 'Persembahan',
      this.url = '',
      this.isLoading = false,
      this.progressValue = 0.0});

  @override
  @JsonKey()
  final String title;
  @override
  @JsonKey()
  final String url;
  @override
  @JsonKey()
  final bool isLoading;
  @override
  @JsonKey()
  final double progressValue;

  @override
  String toString() {
    return 'WebViewState(title: $title, url: $url, isLoading: $isLoading, progressValue: $progressValue)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_WebViewState &&
            (identical(other.title, title) || other.title == title) &&
            (identical(other.url, url) || other.url == url) &&
            (identical(other.isLoading, isLoading) ||
                other.isLoading == isLoading) &&
            (identical(other.progressValue, progressValue) ||
                other.progressValue == progressValue));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, title, url, isLoading, progressValue);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_WebViewStateCopyWith<_$_WebViewState> get copyWith =>
      __$$_WebViewStateCopyWithImpl<_$_WebViewState>(this, _$identity);
}

abstract class _WebViewState implements WebViewState {
  const factory _WebViewState(
      {final String title,
      final String url,
      final bool isLoading,
      final double progressValue}) = _$_WebViewState;

  @override
  String get title;
  @override
  String get url;
  @override
  bool get isLoading;
  @override
  double get progressValue;
  @override
  @JsonKey(ignore: true)
  _$$_WebViewStateCopyWith<_$_WebViewState> get copyWith =>
      throw _privateConstructorUsedError;
}

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'webview_state.dart';
part 'webview_cubit.freezed.dart';

class WebViewCubit extends Cubit<WebViewState> {
  WebViewCubit({
    String? url,
    String? title,
  }) : super(WebViewState(
          url: url ?? '',
          title: title ?? 'GEREJA KEMAH TABERNACLE',
        ));

  void updateLoadingState(bool param) {
    emit(state.copyWith(
      isLoading: param,
    ));
  }

  void updateProgress(int? param) {
    final progress = double.parse((param ?? 0).toString());
    emit(state.copyWith(
      progressValue: progress > 0 ? progress / 100 : progress,
    ));
  }
}

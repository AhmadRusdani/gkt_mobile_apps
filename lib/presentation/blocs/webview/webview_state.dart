part of 'webview_cubit.dart';

@freezed
class WebViewState with _$WebViewState {
  const factory WebViewState({
    @Default('Persembahan') String title,
    @Default('') String url,
    @Default(false) bool isLoading,
    @Default(0.0) double progressValue,
  }) = _WebViewState;
}
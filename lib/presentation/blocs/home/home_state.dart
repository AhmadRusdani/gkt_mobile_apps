part of 'home_cubit.dart';

@freezed
class HomeState with _$HomeState {
  const factory HomeState({
    @Default(BaseState<List<NewsModel>>()) BaseState<List<NewsModel>> newsState,
    @Default(PrayScheduleState()) PrayScheduleState prayScheduleState,
    @Default(SeremonState()) SeremonState seremonState,
  }) = _HomeState;
}

@freezed
class PrayScheduleState with _$PrayScheduleState {
  const factory PrayScheduleState({
    @Default(FormzStatus.pure) FormzStatus status,
    @Default(null) ErrorState? err,
    @Default(null) List<PrayScheduleModel>? data,
    @Default(0) int pageIndex,
  }) = _PrayScheduleState;
}

@freezed
class SeremonState with _$SeremonState {
  const factory SeremonState({
    @Default(FormzStatus.pure) FormzStatus status,
    @Default(0) int pageIndex,
    @Default(null) ErrorState? err,
    @Default(null) List<SeremonModel>? dataMinggu,
    @Default(null) List<SeremonModel>? dataPa,
    @Default(null) List<SeremonModel>? dataMandarin,
  }) = _SeremonState;
}

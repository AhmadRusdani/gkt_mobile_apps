// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'home_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$HomeState {
  BaseState<List<NewsModel>> get newsState =>
      throw _privateConstructorUsedError;
  PrayScheduleState get prayScheduleState => throw _privateConstructorUsedError;
  SeremonState get seremonState => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $HomeStateCopyWith<HomeState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HomeStateCopyWith<$Res> {
  factory $HomeStateCopyWith(HomeState value, $Res Function(HomeState) then) =
      _$HomeStateCopyWithImpl<$Res, HomeState>;
  @useResult
  $Res call(
      {BaseState<List<NewsModel>> newsState,
      PrayScheduleState prayScheduleState,
      SeremonState seremonState});

  $BaseStateCopyWith<List<NewsModel>, $Res> get newsState;
  $PrayScheduleStateCopyWith<$Res> get prayScheduleState;
  $SeremonStateCopyWith<$Res> get seremonState;
}

/// @nodoc
class _$HomeStateCopyWithImpl<$Res, $Val extends HomeState>
    implements $HomeStateCopyWith<$Res> {
  _$HomeStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? newsState = null,
    Object? prayScheduleState = null,
    Object? seremonState = null,
  }) {
    return _then(_value.copyWith(
      newsState: null == newsState
          ? _value.newsState
          : newsState // ignore: cast_nullable_to_non_nullable
              as BaseState<List<NewsModel>>,
      prayScheduleState: null == prayScheduleState
          ? _value.prayScheduleState
          : prayScheduleState // ignore: cast_nullable_to_non_nullable
              as PrayScheduleState,
      seremonState: null == seremonState
          ? _value.seremonState
          : seremonState // ignore: cast_nullable_to_non_nullable
              as SeremonState,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $BaseStateCopyWith<List<NewsModel>, $Res> get newsState {
    return $BaseStateCopyWith<List<NewsModel>, $Res>(_value.newsState, (value) {
      return _then(_value.copyWith(newsState: value) as $Val);
    });
  }

  @override
  @pragma('vm:prefer-inline')
  $PrayScheduleStateCopyWith<$Res> get prayScheduleState {
    return $PrayScheduleStateCopyWith<$Res>(_value.prayScheduleState, (value) {
      return _then(_value.copyWith(prayScheduleState: value) as $Val);
    });
  }

  @override
  @pragma('vm:prefer-inline')
  $SeremonStateCopyWith<$Res> get seremonState {
    return $SeremonStateCopyWith<$Res>(_value.seremonState, (value) {
      return _then(_value.copyWith(seremonState: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_HomeStateCopyWith<$Res> implements $HomeStateCopyWith<$Res> {
  factory _$$_HomeStateCopyWith(
          _$_HomeState value, $Res Function(_$_HomeState) then) =
      __$$_HomeStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {BaseState<List<NewsModel>> newsState,
      PrayScheduleState prayScheduleState,
      SeremonState seremonState});

  @override
  $BaseStateCopyWith<List<NewsModel>, $Res> get newsState;
  @override
  $PrayScheduleStateCopyWith<$Res> get prayScheduleState;
  @override
  $SeremonStateCopyWith<$Res> get seremonState;
}

/// @nodoc
class __$$_HomeStateCopyWithImpl<$Res>
    extends _$HomeStateCopyWithImpl<$Res, _$_HomeState>
    implements _$$_HomeStateCopyWith<$Res> {
  __$$_HomeStateCopyWithImpl(
      _$_HomeState _value, $Res Function(_$_HomeState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? newsState = null,
    Object? prayScheduleState = null,
    Object? seremonState = null,
  }) {
    return _then(_$_HomeState(
      newsState: null == newsState
          ? _value.newsState
          : newsState // ignore: cast_nullable_to_non_nullable
              as BaseState<List<NewsModel>>,
      prayScheduleState: null == prayScheduleState
          ? _value.prayScheduleState
          : prayScheduleState // ignore: cast_nullable_to_non_nullable
              as PrayScheduleState,
      seremonState: null == seremonState
          ? _value.seremonState
          : seremonState // ignore: cast_nullable_to_non_nullable
              as SeremonState,
    ));
  }
}

/// @nodoc

class _$_HomeState implements _HomeState {
  const _$_HomeState(
      {this.newsState = const BaseState<List<NewsModel>>(),
      this.prayScheduleState = const PrayScheduleState(),
      this.seremonState = const SeremonState()});

  @override
  @JsonKey()
  final BaseState<List<NewsModel>> newsState;
  @override
  @JsonKey()
  final PrayScheduleState prayScheduleState;
  @override
  @JsonKey()
  final SeremonState seremonState;

  @override
  String toString() {
    return 'HomeState(newsState: $newsState, prayScheduleState: $prayScheduleState, seremonState: $seremonState)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_HomeState &&
            (identical(other.newsState, newsState) ||
                other.newsState == newsState) &&
            (identical(other.prayScheduleState, prayScheduleState) ||
                other.prayScheduleState == prayScheduleState) &&
            (identical(other.seremonState, seremonState) ||
                other.seremonState == seremonState));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, newsState, prayScheduleState, seremonState);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_HomeStateCopyWith<_$_HomeState> get copyWith =>
      __$$_HomeStateCopyWithImpl<_$_HomeState>(this, _$identity);
}

abstract class _HomeState implements HomeState {
  const factory _HomeState(
      {final BaseState<List<NewsModel>> newsState,
      final PrayScheduleState prayScheduleState,
      final SeremonState seremonState}) = _$_HomeState;

  @override
  BaseState<List<NewsModel>> get newsState;
  @override
  PrayScheduleState get prayScheduleState;
  @override
  SeremonState get seremonState;
  @override
  @JsonKey(ignore: true)
  _$$_HomeStateCopyWith<_$_HomeState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$PrayScheduleState {
  FormzStatus get status => throw _privateConstructorUsedError;
  ErrorState? get err => throw _privateConstructorUsedError;
  List<PrayScheduleModel>? get data => throw _privateConstructorUsedError;
  int get pageIndex => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $PrayScheduleStateCopyWith<PrayScheduleState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PrayScheduleStateCopyWith<$Res> {
  factory $PrayScheduleStateCopyWith(
          PrayScheduleState value, $Res Function(PrayScheduleState) then) =
      _$PrayScheduleStateCopyWithImpl<$Res, PrayScheduleState>;
  @useResult
  $Res call(
      {FormzStatus status,
      ErrorState? err,
      List<PrayScheduleModel>? data,
      int pageIndex});

  $ErrorStateCopyWith<$Res>? get err;
}

/// @nodoc
class _$PrayScheduleStateCopyWithImpl<$Res, $Val extends PrayScheduleState>
    implements $PrayScheduleStateCopyWith<$Res> {
  _$PrayScheduleStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? err = freezed,
    Object? data = freezed,
    Object? pageIndex = null,
  }) {
    return _then(_value.copyWith(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as FormzStatus,
      err: freezed == err
          ? _value.err
          : err // ignore: cast_nullable_to_non_nullable
              as ErrorState?,
      data: freezed == data
          ? _value.data
          : data // ignore: cast_nullable_to_non_nullable
              as List<PrayScheduleModel>?,
      pageIndex: null == pageIndex
          ? _value.pageIndex
          : pageIndex // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $ErrorStateCopyWith<$Res>? get err {
    if (_value.err == null) {
      return null;
    }

    return $ErrorStateCopyWith<$Res>(_value.err!, (value) {
      return _then(_value.copyWith(err: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_PrayScheduleStateCopyWith<$Res>
    implements $PrayScheduleStateCopyWith<$Res> {
  factory _$$_PrayScheduleStateCopyWith(_$_PrayScheduleState value,
          $Res Function(_$_PrayScheduleState) then) =
      __$$_PrayScheduleStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {FormzStatus status,
      ErrorState? err,
      List<PrayScheduleModel>? data,
      int pageIndex});

  @override
  $ErrorStateCopyWith<$Res>? get err;
}

/// @nodoc
class __$$_PrayScheduleStateCopyWithImpl<$Res>
    extends _$PrayScheduleStateCopyWithImpl<$Res, _$_PrayScheduleState>
    implements _$$_PrayScheduleStateCopyWith<$Res> {
  __$$_PrayScheduleStateCopyWithImpl(
      _$_PrayScheduleState _value, $Res Function(_$_PrayScheduleState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? err = freezed,
    Object? data = freezed,
    Object? pageIndex = null,
  }) {
    return _then(_$_PrayScheduleState(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as FormzStatus,
      err: freezed == err
          ? _value.err
          : err // ignore: cast_nullable_to_non_nullable
              as ErrorState?,
      data: freezed == data
          ? _value._data
          : data // ignore: cast_nullable_to_non_nullable
              as List<PrayScheduleModel>?,
      pageIndex: null == pageIndex
          ? _value.pageIndex
          : pageIndex // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_PrayScheduleState implements _PrayScheduleState {
  const _$_PrayScheduleState(
      {this.status = FormzStatus.pure,
      this.err = null,
      final List<PrayScheduleModel>? data = null,
      this.pageIndex = 0})
      : _data = data;

  @override
  @JsonKey()
  final FormzStatus status;
  @override
  @JsonKey()
  final ErrorState? err;
  final List<PrayScheduleModel>? _data;
  @override
  @JsonKey()
  List<PrayScheduleModel>? get data {
    final value = _data;
    if (value == null) return null;
    if (_data is EqualUnmodifiableListView) return _data;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  @JsonKey()
  final int pageIndex;

  @override
  String toString() {
    return 'PrayScheduleState(status: $status, err: $err, data: $data, pageIndex: $pageIndex)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PrayScheduleState &&
            (identical(other.status, status) || other.status == status) &&
            (identical(other.err, err) || other.err == err) &&
            const DeepCollectionEquality().equals(other._data, _data) &&
            (identical(other.pageIndex, pageIndex) ||
                other.pageIndex == pageIndex));
  }

  @override
  int get hashCode => Object.hash(runtimeType, status, err,
      const DeepCollectionEquality().hash(_data), pageIndex);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_PrayScheduleStateCopyWith<_$_PrayScheduleState> get copyWith =>
      __$$_PrayScheduleStateCopyWithImpl<_$_PrayScheduleState>(
          this, _$identity);
}

abstract class _PrayScheduleState implements PrayScheduleState {
  const factory _PrayScheduleState(
      {final FormzStatus status,
      final ErrorState? err,
      final List<PrayScheduleModel>? data,
      final int pageIndex}) = _$_PrayScheduleState;

  @override
  FormzStatus get status;
  @override
  ErrorState? get err;
  @override
  List<PrayScheduleModel>? get data;
  @override
  int get pageIndex;
  @override
  @JsonKey(ignore: true)
  _$$_PrayScheduleStateCopyWith<_$_PrayScheduleState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$SeremonState {
  FormzStatus get status => throw _privateConstructorUsedError;
  int get pageIndex => throw _privateConstructorUsedError;
  ErrorState? get err => throw _privateConstructorUsedError;
  List<SeremonModel>? get dataMinggu => throw _privateConstructorUsedError;
  List<SeremonModel>? get dataPa => throw _privateConstructorUsedError;
  List<SeremonModel>? get dataMandarin => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $SeremonStateCopyWith<SeremonState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SeremonStateCopyWith<$Res> {
  factory $SeremonStateCopyWith(
          SeremonState value, $Res Function(SeremonState) then) =
      _$SeremonStateCopyWithImpl<$Res, SeremonState>;
  @useResult
  $Res call(
      {FormzStatus status,
      int pageIndex,
      ErrorState? err,
      List<SeremonModel>? dataMinggu,
      List<SeremonModel>? dataPa,
      List<SeremonModel>? dataMandarin});

  $ErrorStateCopyWith<$Res>? get err;
}

/// @nodoc
class _$SeremonStateCopyWithImpl<$Res, $Val extends SeremonState>
    implements $SeremonStateCopyWith<$Res> {
  _$SeremonStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? pageIndex = null,
    Object? err = freezed,
    Object? dataMinggu = freezed,
    Object? dataPa = freezed,
    Object? dataMandarin = freezed,
  }) {
    return _then(_value.copyWith(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as FormzStatus,
      pageIndex: null == pageIndex
          ? _value.pageIndex
          : pageIndex // ignore: cast_nullable_to_non_nullable
              as int,
      err: freezed == err
          ? _value.err
          : err // ignore: cast_nullable_to_non_nullable
              as ErrorState?,
      dataMinggu: freezed == dataMinggu
          ? _value.dataMinggu
          : dataMinggu // ignore: cast_nullable_to_non_nullable
              as List<SeremonModel>?,
      dataPa: freezed == dataPa
          ? _value.dataPa
          : dataPa // ignore: cast_nullable_to_non_nullable
              as List<SeremonModel>?,
      dataMandarin: freezed == dataMandarin
          ? _value.dataMandarin
          : dataMandarin // ignore: cast_nullable_to_non_nullable
              as List<SeremonModel>?,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $ErrorStateCopyWith<$Res>? get err {
    if (_value.err == null) {
      return null;
    }

    return $ErrorStateCopyWith<$Res>(_value.err!, (value) {
      return _then(_value.copyWith(err: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_SeremonStateCopyWith<$Res>
    implements $SeremonStateCopyWith<$Res> {
  factory _$$_SeremonStateCopyWith(
          _$_SeremonState value, $Res Function(_$_SeremonState) then) =
      __$$_SeremonStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {FormzStatus status,
      int pageIndex,
      ErrorState? err,
      List<SeremonModel>? dataMinggu,
      List<SeremonModel>? dataPa,
      List<SeremonModel>? dataMandarin});

  @override
  $ErrorStateCopyWith<$Res>? get err;
}

/// @nodoc
class __$$_SeremonStateCopyWithImpl<$Res>
    extends _$SeremonStateCopyWithImpl<$Res, _$_SeremonState>
    implements _$$_SeremonStateCopyWith<$Res> {
  __$$_SeremonStateCopyWithImpl(
      _$_SeremonState _value, $Res Function(_$_SeremonState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? pageIndex = null,
    Object? err = freezed,
    Object? dataMinggu = freezed,
    Object? dataPa = freezed,
    Object? dataMandarin = freezed,
  }) {
    return _then(_$_SeremonState(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as FormzStatus,
      pageIndex: null == pageIndex
          ? _value.pageIndex
          : pageIndex // ignore: cast_nullable_to_non_nullable
              as int,
      err: freezed == err
          ? _value.err
          : err // ignore: cast_nullable_to_non_nullable
              as ErrorState?,
      dataMinggu: freezed == dataMinggu
          ? _value._dataMinggu
          : dataMinggu // ignore: cast_nullable_to_non_nullable
              as List<SeremonModel>?,
      dataPa: freezed == dataPa
          ? _value._dataPa
          : dataPa // ignore: cast_nullable_to_non_nullable
              as List<SeremonModel>?,
      dataMandarin: freezed == dataMandarin
          ? _value._dataMandarin
          : dataMandarin // ignore: cast_nullable_to_non_nullable
              as List<SeremonModel>?,
    ));
  }
}

/// @nodoc

class _$_SeremonState implements _SeremonState {
  const _$_SeremonState(
      {this.status = FormzStatus.pure,
      this.pageIndex = 0,
      this.err = null,
      final List<SeremonModel>? dataMinggu = null,
      final List<SeremonModel>? dataPa = null,
      final List<SeremonModel>? dataMandarin = null})
      : _dataMinggu = dataMinggu,
        _dataPa = dataPa,
        _dataMandarin = dataMandarin;

  @override
  @JsonKey()
  final FormzStatus status;
  @override
  @JsonKey()
  final int pageIndex;
  @override
  @JsonKey()
  final ErrorState? err;
  final List<SeremonModel>? _dataMinggu;
  @override
  @JsonKey()
  List<SeremonModel>? get dataMinggu {
    final value = _dataMinggu;
    if (value == null) return null;
    if (_dataMinggu is EqualUnmodifiableListView) return _dataMinggu;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  final List<SeremonModel>? _dataPa;
  @override
  @JsonKey()
  List<SeremonModel>? get dataPa {
    final value = _dataPa;
    if (value == null) return null;
    if (_dataPa is EqualUnmodifiableListView) return _dataPa;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  final List<SeremonModel>? _dataMandarin;
  @override
  @JsonKey()
  List<SeremonModel>? get dataMandarin {
    final value = _dataMandarin;
    if (value == null) return null;
    if (_dataMandarin is EqualUnmodifiableListView) return _dataMandarin;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'SeremonState(status: $status, pageIndex: $pageIndex, err: $err, dataMinggu: $dataMinggu, dataPa: $dataPa, dataMandarin: $dataMandarin)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SeremonState &&
            (identical(other.status, status) || other.status == status) &&
            (identical(other.pageIndex, pageIndex) ||
                other.pageIndex == pageIndex) &&
            (identical(other.err, err) || other.err == err) &&
            const DeepCollectionEquality()
                .equals(other._dataMinggu, _dataMinggu) &&
            const DeepCollectionEquality().equals(other._dataPa, _dataPa) &&
            const DeepCollectionEquality()
                .equals(other._dataMandarin, _dataMandarin));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      status,
      pageIndex,
      err,
      const DeepCollectionEquality().hash(_dataMinggu),
      const DeepCollectionEquality().hash(_dataPa),
      const DeepCollectionEquality().hash(_dataMandarin));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SeremonStateCopyWith<_$_SeremonState> get copyWith =>
      __$$_SeremonStateCopyWithImpl<_$_SeremonState>(this, _$identity);
}

abstract class _SeremonState implements SeremonState {
  const factory _SeremonState(
      {final FormzStatus status,
      final int pageIndex,
      final ErrorState? err,
      final List<SeremonModel>? dataMinggu,
      final List<SeremonModel>? dataPa,
      final List<SeremonModel>? dataMandarin}) = _$_SeremonState;

  @override
  FormzStatus get status;
  @override
  int get pageIndex;
  @override
  ErrorState? get err;
  @override
  List<SeremonModel>? get dataMinggu;
  @override
  List<SeremonModel>? get dataPa;
  @override
  List<SeremonModel>? get dataMandarin;
  @override
  @JsonKey(ignore: true)
  _$$_SeremonStateCopyWith<_$_SeremonState> get copyWith =>
      throw _privateConstructorUsedError;
}

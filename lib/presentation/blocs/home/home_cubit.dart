import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:gkt/domain/model/news/news_model.dart';
import 'package:gkt/domain/model/pray/pray_schedule_model.dart';
import 'package:gkt/domain/repository/main_repository.dart';
import 'package:gkt/presentation/blocs/base/error_state.dart';
import 'package:gkt/presentation/utils/extension.dart';

import '../../../domain/model/seremon/seremon_model.dart';
import '../../../domain/repository/user_repository.dart';
import '../base/base_state.dart';

part 'home_cubit.freezed.dart';
part 'home_state.dart';

class HomeCubit extends Cubit<HomeState> {
  HomeCubit({
    required MainRepository mainRepository,
    required UserRepository userRepository,
  })  : _mainRepository = mainRepository,
        _userRepository = userRepository,
        super(const HomeState()) {
    initialData();
  }

  final MainRepository _mainRepository;
  final UserRepository _userRepository;

  void initialData() async {
    getNews();
    getPraySchedule();
    getSeremonSchedule();
  }

  void getNews() async {
    emit(state.copyWith(
      newsState: state.newsState.copyWith(
        status: FormzStatus.submissionInProgress,
        err: null,
      ),
    ));
    try {
      final response = await _mainRepository.getNews();
      emit(state.copyWith(
        newsState: state.newsState.copyWith(
            status: FormzStatus.submissionSuccess, data: response.data ?? []),
      ));
    } on Exception catch (e) {
      emit(state.copyWith(
          newsState: state.newsState.copyWith(
              status: FormzStatus.submissionFailure,
              err: ErrorState(
                err: e.handle(),
                errMsg: e.toString(),
              )),
        ));
    } catch (e) {
      emit(state.copyWith(
        newsState: state.newsState.copyWith(
            status: FormzStatus.submissionFailure,
            err: ErrorState(errMsg: e.toString())),
      ));
    }
  }

  void getPraySchedule() async {
    emit(state.copyWith(
      prayScheduleState: state.prayScheduleState.copyWith(
        status: FormzStatus.submissionInProgress,
        err: null,
      ),
    ));
    try {
      final response = await _mainRepository.getPraySchedule();
      emit(state.copyWith(
        prayScheduleState: state.prayScheduleState.copyWith(
            status: FormzStatus.submissionSuccess, data: response.data ?? []),
      ));
    } on Exception catch (e) {
      emit(state.copyWith(
          prayScheduleState: state.prayScheduleState.copyWith(
              status: FormzStatus.submissionFailure,
              err: ErrorState(
                err: e.handle(),
                errMsg: e.toString(),
              )),
        ));
    } catch (e) {
      emit(state.copyWith(
        prayScheduleState: state.prayScheduleState.copyWith(
            status: FormzStatus.submissionFailure,
            err: ErrorState(errMsg: e.toString())),
      ));
    }
  }

  void getSeremonSchedule() async {
    emit(state.copyWith(
      seremonState: state.seremonState.copyWith(
        status: FormzStatus.submissionInProgress,
        err: null,
      ),
    ));
    try {
      final responseQueue = await Future.wait([
        _mainRepository.getSeremon(type: SeremonType.minggu),
        _mainRepository.getSeremon(type: SeremonType.pa),
        _mainRepository.getSeremon(type: SeremonType.mandarin),
      ]);
      emit(state.copyWith(
        seremonState: state.seremonState.copyWith(
          status: FormzStatus.submissionSuccess,
          dataMinggu: responseQueue[0].data,
          dataPa: responseQueue[1].data,
          dataMandarin: responseQueue[2].data,
        ),
      ));
    } on Exception catch (e) {
      emit(state.copyWith(
          seremonState: state.seremonState.copyWith(
              status: FormzStatus.submissionFailure,
              err: ErrorState(
                err: e.handle(),
                errMsg: e.toString(),
              )),
        ));
    } catch (e) {
      emit(state.copyWith(
        seremonState: state.seremonState.copyWith(
            status: FormzStatus.submissionFailure,
            err: ErrorState(errMsg: e.toString())),
      ));
    }
  }

  void changeIndexPraySchedule(int index) {
    emit(state.copyWith(
      prayScheduleState: state.prayScheduleState.copyWith(
        pageIndex: index,
      ),
    ));
  }

  void changeIndexSeremon(int index) {
    emit(state.copyWith(
      seremonState: state.seremonState.copyWith(
        pageIndex: index,
      ),
    ));
  }
}

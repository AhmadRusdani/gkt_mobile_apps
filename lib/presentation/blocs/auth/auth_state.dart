part of 'auth_cubit.dart';

@freezed
class AuthState with _$AuthState {
  const factory AuthState({
    @Default(AuthenticationStatus.unknown) AuthenticationStatus status,
    @Default(null) UserModel? user,
  }) = _AuthState;
}

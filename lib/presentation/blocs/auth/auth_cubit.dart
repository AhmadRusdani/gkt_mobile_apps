import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:gkt/domain/exception/api_exception.dart';
import 'package:gkt/domain/model/user/user_model.dart';
import 'package:gkt/domain/repository/auth_repository.dart';

import '../../../domain/repository/user_repository.dart';

part 'auth_cubit.freezed.dart';

part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  AuthCubit({
    required AuthRepository authRepository,
    required UserRepository userRepository,
  })  : _authRepository = authRepository,
        _userRepository = userRepository,
        super(const AuthState()) {
    _authenticationStatusSubscription = _authRepository.status.listen(
      onAuthStatusChange,
    );
  }

  final AuthRepository _authRepository;
  final UserRepository _userRepository;
  late StreamSubscription<AuthenticationStatus>
      _authenticationStatusSubscription;

  void onAuthStatusChange(AuthenticationStatus status) async {
    await Future.delayed(const Duration(milliseconds: 1200));
    switch (status) {
      case AuthenticationStatus.authenticated:
        validateLogin();
        break;
      case AuthenticationStatus.update:
        emit(state.copyWith(
          status: status,
        ));
        break;
      default:
        emit(state.copyWith(
          status: status,
          user: null,
        ));
        break;
    }
  }

  void updateUser() async {
    final user = await _userRepository.currentUser;
    emit(state.copyWith(
      user: user,
      status: AuthenticationStatus.update,
    ));
  }

  void validateLogin() async {
    final result = await Future.wait([
      _userRepository.currentUser,
      _userRepository.sessionToken,
    ]);
    final user = result[0] as UserModel?;
    final token = result[1] as String?;
    if (token != null) {
      if (user == null) {
        fetchUser();
        return;
      }
      emit(state.copyWith(
        status: AuthenticationStatus.authenticated,
        user: user,
      ));
    } else {
      emit(state.copyWith(
        status: AuthenticationStatus.unauthenticated,
        user: null,
      ));
    }
  }

  void fetchUser() async {
    try {
      final response = await _userRepository.fetchUser();
      emit(state.copyWith(
        status: response != null
            ? AuthenticationStatus.authenticated
            : AuthenticationStatus.unauthenticated,
        user: response,
      ));
    } on ApiException catch (_) {
      emit(state.copyWith(
        status: AuthenticationStatus.unauthenticated,
        user: null,
      ));
    } catch (e) {
      emit(state.copyWith(
        status: AuthenticationStatus.unauthenticated,
        user: null,
      ));
    }
  }

  FutureOr<void> onLogout() async {
    try {
      await Future.wait([
        _userRepository.setSessionToken(null),
        _userRepository.setCurrentUser(null),
      ]);
      onAuthStatusChange(AuthenticationStatus.unauthenticated);
    } catch (_) {}
  }

  @override
  Future<void> close() {
    _authenticationStatusSubscription.cancel();
    _authRepository.dispose();
    return super.close();
  }
}

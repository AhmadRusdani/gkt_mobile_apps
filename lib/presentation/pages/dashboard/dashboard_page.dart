import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:gkt/presentation/blocs/dashboard/dashboard_cubit.dart';
import 'package:gkt/presentation/blocs/home/home_cubit.dart';
import 'package:gkt/presentation/blocs/profile/profile_cubit.dart';
import 'package:gkt/presentation/blocs/projects/projects_cubit.dart';
import 'package:gkt/presentation/config/theme_texts.dart';
import 'package:gkt/presentation/pages/home/home_page.dart';
import 'package:gkt/presentation/utils/extension.dart';
import 'package:gkt/presentation/widgets/base/circle_tab_indicator.dart';

import '../../config/theme_colors.dart';
import '../profile/profile_page.dart';
import '../projects/projects_page.dart';

class DashboardPage extends StatefulWidget {
  static const routeName = '/dashboard';

  const DashboardPage({super.key});

  @override
  State<DashboardPage> createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage>
    with TickerProviderStateMixin {
  TabController? tabController;
  PageController pageController = PageController();

  @override
  void initState() {
    tabController = TabController(
      length: 3,
      vsync: this,
    );
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      tabController?.addListener(() {
        if (tabController?.indexIsChanging == true) {
          context.read<DashboardCubit>().changeIndex(tabController?.index ?? 0);
          pageController.jumpToPage(tabController?.index ?? 0);
        }
      });
    });
  }

  @override
  void dispose() {
    tabController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      body: _body(),
      bottomNavigationBar: Container(
        padding:
            EdgeInsets.only(top: 12, bottom: context.responsivePaddingBottom()),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: const BorderRadius.vertical(top: Radius.circular(42)),
            boxShadow: [
              BoxShadow(
                  color: Colors.grey.shade200,
                  offset: const Offset(0.0, -1),
                  blurRadius: 1)
            ]),
        child: BlocBuilder<DashboardCubit, DashboardState>(
            builder: (context, state) {
          return PreferredSize(
            preferredSize: tabBar(state.tabIndex).preferredSize,
            child: tabBar(state.tabIndex),
          );
        }),
      ),
    );
  }

  Widget _body() {
    return PageView(
      controller: pageController,
      physics: const NeverScrollableScrollPhysics(),
      children: [
        _homePage(),
        _projectsPage(),
        _profilePage(),
      ],
    );
  }

  Widget _homePage() => BlocProvider(
        create: (context) => HomeCubit(
          mainRepository: context.read(),
          userRepository: context.read(),
        ),
        child: const HomePage(),
      );

  Widget _projectsPage() => BlocProvider(
        create: (context) => ProjectsCubit(mainRepository: context.read()),
        child: const ProjectsPage(),
      );

  Widget _profilePage() => BlocProvider(
        create: (context) => ProfileCubit(
          authRepository: context.read(),
          userRepository: context.read(),
        ),
        child: const ProfilePage(),
      );

  TabBar tabBar(int index) {
    return TabBar(
        indicatorSize: TabBarIndicatorSize.tab,
        labelColor: ThemeColors.gold,
        unselectedLabelColor: ThemeColors.blackPrimary,
        controller: tabController,
        labelStyle: kTextStyleDescription.copyWith(fontSize: 12),
        unselectedLabelStyle: kTextStyleDescription.copyWith(fontSize: 12),
        indicator: CircleTabIndicator(
          color: ThemeColors.gold,
          radius: 3,
        ),
        tabs: [
          Tab(
            icon: SvgPicture.asset(
              'assets/icons/ic_house.svg',
              color: index == 0 ? ThemeColors.gold : ThemeColors.blackPrimary,
            ),
            text: 'Home',
          ),
          Tab(
            icon: SvgPicture.asset(
              'assets/icons/ic_project.svg',
              color: index == 1 ? ThemeColors.gold : ThemeColors.blackPrimary,
            ),
            text: 'Projects',
          ),
          Tab(
            icon: SvgPicture.asset('assets/icons/ic_profile.svg',
                color:
                    index == 2 ? ThemeColors.gold : ThemeColors.blackPrimary),
            text: 'Profile',
          ),
        ]);
  }
}

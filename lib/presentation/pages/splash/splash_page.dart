import 'package:flutter/material.dart';

class SplashPage extends StatelessWidget {
  static const routeName = '/';
  const SplashPage({super.key});

  @override
  Widget build(BuildContext context) {
    // Future.delayed(const Duration(seconds: 2), () {
    //   Navigator.pushNamedAndRemoveUntil(
    //       context, LoginPage.routeName, (route) => false);
    // });
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Image.asset(
          'assets/images/logo_tabernacle.png',
          height: 100,
          width: 100,
          fit: BoxFit.fitHeight,
        ),
      ),
    );
  }
}

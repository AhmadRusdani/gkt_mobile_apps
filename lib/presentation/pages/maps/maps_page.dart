import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gkt/presentation/blocs/maps/maps_cubit.dart';
import 'package:gkt/presentation/config/theme_texts.dart';
import 'package:gkt/presentation/utils/extension.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../widgets/base/appbar_widget.dart';

part 'maps_widget.dart';

class MapsPage extends StatefulWidget {
  static const routeName = '/dashboard/maps';
  const MapsPage({super.key, this.latLng});

  final LatLng? latLng;

  @override
  State<MapsPage> createState() => _MapsPageState();
}

class _MapsPageState extends State<MapsPage> {
  final Completer<GoogleMapController> _controller = Completer();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: appBarPrimaryBack(),
      body: Stack(
        children: [
          MapsWidget(
            mapsController: _controller,
            latLng: widget.latLng,
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: BlocBuilder<MapsCubit, MapsState>(
              builder: (context, state) {
                if (state.address == null) {
                  return const SizedBox.shrink();
                }
                return Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Container(
                      padding: const EdgeInsets.all(14),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(18),
                      ),
                      margin: const EdgeInsets.symmetric(
                        horizontal: 24,
                        vertical: 12,
                      ),
                      child: ListTile(
                        contentPadding: EdgeInsets.zero,
                        leading: Image.asset(
                          'assets/images/gps_point.png',
                          width: 45,
                          height: 45,
                          fit: BoxFit.scaleDown,
                        ),
                        title: Text(
                          state.address ?? '',
                          style: kTextStyleDescriptionBold16,
                        ),
                        onTap: () => updateCameraPosition(state.latLng),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.all(14),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(18),
                      ),
                      margin: EdgeInsets.fromLTRB(
                        24,
                        0,
                        24,
                        context.responsivePaddingBottom(),
                      ),
                      child: ListTile(
                        contentPadding: EdgeInsets.zero,
                        leading: Image.asset(
                          'assets/images/google_maps_logo.png',
                          width: 32,
                          height: 32,
                          fit: BoxFit.scaleDown,
                        ),
                        title: const Text(
                          'Buka di Google Maps',
                          style: kTextStyleTitleBold18,
                        ),
                        trailing: const Icon(
                          Icons.chevron_right_rounded,
                          size: 42,
                        ),
                        onTap: () {
                          try {
                            context.openMap(latLng: state.latLng);
                          } catch (e) {
                            context.showErrorToast(e.toString());
                          }
                        },
                      ),
                    ),
                  ],
                );
              },
            ),
          )
        ],
      ),
    );
  }

  void updateCameraPosition(LatLng latLng) async {
    final controller = await _controller.future;
    controller.updateCameraPosition(latLng: latLng);
  }
}

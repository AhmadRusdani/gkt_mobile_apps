part of 'maps_page.dart';

class MapsWidget extends StatelessWidget {
  const MapsWidget({
    super.key,
    required this.mapsController,
    this.latLng,
  });

  final Completer<GoogleMapController> mapsController;
  final LatLng? latLng;

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<MapsCubit>();
    return MultiBlocListener(
      listeners: [
        BlocListener<MapsCubit, MapsState>(
          listenWhen: (previous, current) =>
              previous.markerIcon != current.markerIcon,
          listener: (context, state) {
            if (state.markerIcon != null) {
              bloc.setAddress(latLng ?? const LatLng(-6.0468212, 106.7039274));
            }
          },
        ),
        BlocListener<MapsCubit, MapsState>(
          listenWhen: (previous, current) => previous.err != current.err,
          listener: (context, state) {
            if (state.err != null) {
              context.handleError(state.err?.err);
            }
          },
        ),
        BlocListener<MapsCubit, MapsState>(
          listenWhen: (previous, current) => previous.latLng != current.latLng,
          listener: (context, state) {
            bloc.updateMarker(state.latLng);
            _updateCameraPosition(state.latLng);
          },
        ),
      ],
      child: BlocBuilder<MapsCubit, MapsState>(
        builder: (context, state) => GoogleMap(
          mapType: MapType.normal,
          minMaxZoomPreference: const MinMaxZoomPreference(0, 16),
          zoomControlsEnabled: false,
          buildingsEnabled: false,
          initialCameraPosition: CameraPosition(
            target: state.latLng,
            zoom: 14.4746,
          ),
          myLocationButtonEnabled: false,
          onMapCreated: (controller) {
            mapsController.complete(controller);
          },
          // onTap: (latLng) {
          //   bloc.updateLocations(latLng);
          // },
          markers: state.marker,
        ),
      ),
    );
  }

  void _updateCameraPosition(LatLng latLng) async {
    final controller = await mapsController.future;
    controller.updateCameraPosition(latLng: latLng);
  }
}

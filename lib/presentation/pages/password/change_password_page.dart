import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hud/flutter_hud.dart';
import 'package:formz/formz.dart';
import 'package:gkt/presentation/blocs/password/change_password_cubit.dart';
import 'package:gkt/presentation/pages/dashboard/dashboard_page.dart';
import 'package:gkt/presentation/utils/extension.dart';

import '../../config/theme_texts.dart';
import '../../widgets/base/appbar_widget.dart';
import '../../widgets/base/background_widget.dart';
import '../../widgets/base/button_widget.dart';
import '../../widgets/base/text_field_widget.dart';
import '../../widgets/base/text_widget.dart';

class ChangePasswordPage extends StatefulWidget {
  static const routeName = '${DashboardPage.routeName}/profile/change_password';

  const ChangePasswordPage({super.key});

  @override
  State<ChangePasswordPage> createState() => _ChangePasswordPageState();
}

class _ChangePasswordPageState extends State<ChangePasswordPage> {
  @override
  Widget build(BuildContext context) {
    final bloc = context.read<ChangePasswordCubit>();
    return MultiBlocListener(
      listeners: [
        BlocListener<ChangePasswordCubit, ChangePasswordState>(
            listenWhen: (previous, current) =>
                previous.status != current.status,
            listener: (_, state) {
              final hud = PopupHUD(context);
              state.status.handleLoading(hud: hud);

              if (state.status == FormzStatus.submissionSuccess) {
                context.showOkToast('Password berhasil diganti!');
                Navigator.of(context).pop();
              }
            }),
        BlocListener<ChangePasswordCubit, ChangePasswordState>(
            listenWhen: (previous, current) =>
            previous.err != current.err,
            listener: (_, state) {
              if (state.err?.errMsg != null) {
                context.showErrorToast(state.err?.errMsg ?? '');
              }
            }),
      ],
      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: appBarPrimaryBack(),
        body: BackgroundGradient(
          minHeight: context.screenHeight(),
          assetsAlign: Alignment.centerRight,
          padding: const EdgeInsets.all(24),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              SizedBox(height: context.screenHeight() * 0.12),
              sectionTitle(
                padding:
                    const EdgeInsets.symmetric(vertical: 14, horizontal: 12),
                title: 'Ganti Password',
                fontSize: 24,
              ),
              const SizedBox(height: 32),
              BlocBuilder<ChangePasswordCubit, ChangePasswordState>(
                  builder: (context, state) {
                return Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 12),
                          child: Text(
                            'Kata Sandi Lama',
                            style: kTextStyleDescriptionBold16.copyWith(
                              color: Colors.black,
                            ),
                          ),
                        ),
                        const SizedBox(height: 6),
                        FormzTextField(
                          fillColor: Colors.grey.shade100,
                          onChanged: (value) {
                            bloc.setOldPassword(value);
                          },
                          leading: Padding(
                            padding: const EdgeInsets.fromLTRB(14, 14, 14, 14),
                            child: Image.asset(
                              'assets/icons/lock_icon.png',
                              height: 14,
                              width: 14,
                              fit: BoxFit.scaleDown,
                            ),
                          ),
                          textInputAction: TextInputAction.next,
                          hintText: 'Masukan Kata Sandi Lama',
                          obscureText: true,
                          errorText: state.oldPassword.errorPasswordText(),
                        ),
                        const SizedBox(height: 24),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 12),
                          child: Text(
                            'Kata Sandi Baru',
                            style: kTextStyleDescriptionBold16.copyWith(
                              color: Colors.black,
                            ),
                          ),
                        ),
                        const SizedBox(height: 6),
                        FormzTextField(
                          fillColor: Colors.grey.shade100,
                          onChanged: (value) {
                            bloc.setNewPassword(value);
                          },
                          leading: Padding(
                            padding: const EdgeInsets.fromLTRB(14, 14, 14, 14),
                            child: Image.asset(
                              'assets/icons/lock_icon.png',
                              height: 14,
                              width: 14,
                              fit: BoxFit.scaleDown,
                            ),
                          ),
                          textInputAction: TextInputAction.next,
                          hintText: 'Masukan Kata Sandi Baru',
                          obscureText: true,
                          errorText: state.newPassword.errorPasswordText(),
                        ),
                        const SizedBox(height: 24),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 12),
                          child: Text(
                            'Konfirmasi Kata Sandi',
                            style: kTextStyleDescriptionBold16.copyWith(
                              color: Colors.black,
                            ),
                          ),
                        ),
                        const SizedBox(height: 6),
                        FormzTextField(
                          fillColor: Colors.grey.shade100,
                          onChanged: (value) {
                            bloc.setConfirmPassword(value);
                          },
                          leading: Padding(
                            padding: const EdgeInsets.fromLTRB(14, 14, 14, 14),
                            child: Image.asset(
                              'assets/icons/lock_icon.png',
                              height: 14,
                              width: 14,
                              fit: BoxFit.scaleDown,
                            ),
                          ),
                          textInputAction: TextInputAction.done,
                          hintText: 'Konfirmasi Kata Sandi',
                          obscureText: true,
                          errorText: state.confirmPassword.errorPasswordText(),
                        ),
                        const SizedBox(height: 42),
                        GradientButton(
                          radius: const BorderRadius.all(Radius.circular(28)),
                          onPressed: state.status.isValid
                              ? () => bloc.changePassword()
                              : null,
                          size: const Size(double.infinity, 58),
                          fontSize: 16,
                          child: const Text('Ganti Password'),
                        ),
                      ],
                    ),
                  ),
                );
              }),
            ],
          ),
        ),
      ),
    );
  }
}

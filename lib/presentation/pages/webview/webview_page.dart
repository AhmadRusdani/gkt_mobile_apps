import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gkt/presentation/blocs/webview/webview_cubit.dart';
import 'package:gkt/presentation/config/theme_texts.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../../config/theme_colors.dart';

class WebViewPage extends StatefulWidget {
  static const routeName = 'dashboard/webview';
  const WebViewPage({super.key});

  @override
  State<WebViewPage> createState() => _WebViewPageState();
}

class _WebViewPageState extends State<WebViewPage> {
  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) WebView.platform = AndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<WebViewCubit>();
    return Scaffold(
      appBar: AppBar(
        title: Text(
          bloc.state.title,
          style: kTextStyleDescriptionBold16.copyWith(
            color: ThemeColors.gold,
          ),
        ),
        backgroundColor: Colors.white,
      ),
      body: BlocBuilder<WebViewCubit, WebViewState>(
        builder: (context, state) {
          return Stack(
            children: [
              WebView(
                initialUrl: state.url,
                javascriptMode: JavascriptMode.unrestricted,
                zoomEnabled: false,
                onProgress: (progress) {
                  bloc.updateProgress(progress);
                },
                onPageFinished: (url) {
                  bloc.updateLoadingState(false);
                },
                onPageStarted: (url) {
                  bloc.updateLoadingState(true);
                },
              ),
              state.isLoading
                  ? Positioned(
                      top: 0,
                      left: 0,
                      right: 0,
                      child: Container(
                        alignment: Alignment.topCenter,
                        child: LinearProgressIndicator(
                          backgroundColor: Colors.white,
                          value: state.progressValue,
                          minHeight: 4,
                        ),
                      ))
                  : Stack(),
            ],
          );
        },
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gkt/presentation/config/theme_colors.dart';
import 'package:gkt/presentation/config/theme_texts.dart';
import 'package:gkt/presentation/pages/home/home_news_widget.dart';
import 'package:gkt/presentation/pages/home/home_registration_widget.dart';
import 'package:gkt/presentation/pages/home/home_tutorial_widget.dart';
import 'package:gkt/presentation/utils/extension.dart';
import 'package:gkt/presentation/widgets/base/appbar_widget.dart';
import 'package:gkt/presentation/widgets/base/background_widget.dart';

import '../../blocs/home/home_cubit.dart';
import 'home_pray_schedule_widget.dart';
import 'home_seremon_widget.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    final bloc = context.read<HomeCubit>();
    return Scaffold(
      backgroundColor: ThemeColors.background,
      body: RefreshIndicator(
        onRefresh: () async => bloc.initialData(),
        child: SingleChildScrollView(
          child: BackgroundGradient(
            minHeight: context.screenHeight(),
            assetsAlign: Alignment.centerRight,
            padding:
                EdgeInsets.fromLTRB(0, 14, 0, context.screenHeight() * 0.2),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                appbarPrimary(),
                const SizedBox(height: 24),
                titleSection('Berita Terkini'),
                const SizedBox(height: 14),
                const HomeNewsWidget(),
                const SizedBox(height: 32),
                titleSection('Jadwal Ibadah'),
                const HomePrayScheduleWidget(),
                titleSection('Pendaftaran'),
                const HomeRegistrationWidget(),
                titleSection('Khotbah'),
                const HomeSeremonWidget(),
                titleSection('Tutorial'),
                const HomeTutorialWidget(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget titleSection(String title) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 42),
      child: Text(
        title,
        style: kTextStyleDescriptionBold16.copyWith(
          fontSize: 18,
          fontWeight: FontWeight.w600,
        ),
      ),
    );
  }

  Widget dottedVerticalLine() => SizedBox(
        height: 160,
        width: 20,
        child: Stack(
          children: [
            Positioned.fill(
              child: Align(
                alignment: Alignment.center,
                child: Container(
                  width: 3,
                  decoration: const BoxDecoration(
                    color: ThemeColors.gold,
                  ),
                ),
              ),
            ),
            Positioned.fill(
              child: Align(
                alignment: Alignment.topCenter,
                child: Container(
                  height: 20,
                  width: 20,
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    color: ThemeColors.gold,
                  ),
                ),
              ),
            ),
            Positioned.fill(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  height: 20,
                  width: 20,
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    color: ThemeColors.gold,
                  ),
                ),
              ),
            ),
          ],
        ),
      );

  Widget tutorialItem({
    required String assets,
    required String title,
  }) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            height: 120,
            margin: const EdgeInsets.symmetric(vertical: 12),
            decoration: BoxDecoration(
              color: ThemeColors.greyPrimary,
              borderRadius: BorderRadius.circular(12),
              image: DecorationImage(
                  image: AssetImage(
                    assets,
                  ),
                  fit: BoxFit.cover),
            ),
          ),
          Text(
            title,
            style: kTextStyleDescription.copyWith(
              fontWeight: FontWeight.w500,
              fontSize: 10,
            ),
            textAlign: TextAlign.center,
          )
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:gkt/presentation/config/theme_colors.dart';
import 'package:gkt/presentation/utils/extension.dart';
import 'package:gkt/presentation/widgets/base/base_data_widget.dart';
import 'package:gkt/presentation/widgets/base/empty_widget.dart';
import 'package:gkt/presentation/widgets/base/shimmer.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import '../../../domain/model/pray/pray_schedule_model.dart';
import '../../blocs/home/home_cubit.dart';
import '../../config/theme_texts.dart';
import '../../widgets/base/button_widget.dart';
import '../../widgets/base/notched_widget.dart';
import '../../widgets/base/text_widget.dart';

part 'home_pray_schedule_item_widget.dart';

class HomePrayScheduleWidget extends StatefulWidget {
  const HomePrayScheduleWidget({
    super.key,
  });

  @override
  State<HomePrayScheduleWidget> createState() => _HomePrayScheduleWidgetState();
}

class _HomePrayScheduleWidgetState extends State<HomePrayScheduleWidget> {
  final pageController = PageController();

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<HomeCubit>();
    return Container(
      margin: const EdgeInsets.fromLTRB(0, 14, 0, 32),
      height: 450,
      child: BlocConsumer<HomeCubit, HomeState>(
        listenWhen: (previous, current) =>
            previous.prayScheduleState.err != current.prayScheduleState.err,
        listener: (context, state) {
          if (state.prayScheduleState.err != null) {
            context.handleError(
              state.prayScheduleState.err?.err,
              retry: () => bloc.initialData(),
            );
          }
        },
        builder: (context, state) {
          final status = state.prayScheduleState.status;
          final data = state.prayScheduleState.data;
          if (status.isSubmissionInProgress && data.isNullOrEmpty()) {
            return const Shimmer(child: HomePrayScheduleItemWidget.shimmer());
          }

          return Stack(
            children: [
              Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: AnimatedSmoothIndicator(
                    count: data?.length ?? 0,
                    activeIndex: state.prayScheduleState.pageIndex,
                    effect: const ScrollingDotsEffect(
                      activeDotColor: ThemeColors.gold,
                      dotColor: ThemeColors.greyStyle5,
                      dotHeight: 8,
                      dotWidth: 8,
                      spacing: 4,
                    ),
                  ),
                ),
              ),
              if ((status.isSubmissionSuccess || status.isSubmissionFailure) &&
                  data.isNullOrEmpty()) ...[
                NotchedBottomWidget(
                  child: EmptyWidget.withImage(
                    icons: Icons.church_outlined,
                    title: 'Tidak ada jadwal ibadah',
                    msg: 'Tidak ada jadwal ibadah untuk saat ini',
                    onTap: status.isSubmissionFailure
                        ? () => bloc.getPraySchedule()
                        : null,
                  ),
                )
              ] else
                PageView(
                  controller: pageController,
                  onPageChanged: (value) {
                    bloc.changeIndexPraySchedule(value);
                  },
                  children: !data.isNullOrEmpty()
                      ? data!
                          .map((item) => HomePrayScheduleItemWidget.data(
                                data: item,
                              ))
                          .toList()
                      : [],
                )
            ],
          );
        },
      ),
    );
  }
}

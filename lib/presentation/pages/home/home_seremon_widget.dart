import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:gkt/presentation/config/theme_colors.dart';
import 'package:gkt/presentation/config/theme_texts.dart';
import 'package:gkt/presentation/utils/extension.dart';
import 'package:gkt/presentation/widgets/base/empty_widget.dart';
import 'package:gkt/presentation/widgets/base/segmented_control_widget.dart';

import '../../../domain/model/seremon/seremon_model.dart';
import '../../blocs/home/home_cubit.dart';
import '../../widgets/base/base_data_widget.dart';
import '../../widgets/base/shimmer.dart';

part 'home_seremon_item_widget.dart';

class HomeSeremonWidget extends StatefulWidget {
  const HomeSeremonWidget({super.key});

  @override
  State<HomeSeremonWidget> createState() => _HomeSeremonWidgetState();
}

class _HomeSeremonWidgetState extends State<HomeSeremonWidget> {
  int seremonIndex = 0;
  final pageController = PageController();

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<HomeCubit>();
    return BlocConsumer<HomeCubit, HomeState>(
      listenWhen: (previous, current) =>
          previous.seremonState.err != current.seremonState.err,
      listener: (context, state) {
        if (state.seremonState.err != null) {
          context.handleError(
            state.seremonState.err?.err,
            retry: () => bloc.initialData(),
          );
        }
      },
      builder: (context, state) {
        final status = state.seremonState.status;
        final dataMandarin = state.seremonState.dataMandarin;
        final dataPa = state.seremonState.dataPa;
        final dataMinggu = state.seremonState.dataMinggu;

        return Container(
          margin: const EdgeInsets.fromLTRB(24, 14, 24, 32),
          padding: const EdgeInsets.symmetric(
            vertical: 24,
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.white,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                  padding: const EdgeInsets.symmetric(
                    vertical: 6,
                    horizontal: 14,
                  ),
                  color: ThemeColors.greyStyle2,
                  child: CustomSegmentedControl(
                      children: [
                        SegmentedItem(
                            title: SeremonType.minggu.toString(),
                            isSelected: state.seremonState.pageIndex == 0),
                        SegmentedItem(
                            title: SeremonType.pa.toString(),
                            isSelected: state.seremonState.pageIndex == 1),
                        SegmentedItem(
                          title: SeremonType.mandarin.toString(),
                          isSelected: state.seremonState.pageIndex == 2,
                        ),
                      ],
                      onValueChanged: (value) {
                        bloc.changeIndexSeremon(value);
                        seremonIndex = value;
                        pageController.jumpToPage(value);
                      })),
              status.isSubmissionInProgress &&
                      (dataMinggu.isNullOrEmpty() ||
                          dataMandarin.isNullOrEmpty() ||
                          dataPa.isNullOrEmpty())
                  ? const Shimmer(
                      child: HomeSeremonItemWidget.shimmer(),
                    )
                  : SizedBox(
                      height: context.screenHeight() * 0.4,
                      child: PageView(
                        controller: pageController,
                        physics: const NeverScrollableScrollPhysics(),
                        children: [
                          seremonItem(
                            type: SeremonType.minggu,
                            status: status.isSubmissionFailure,
                            data: dataMinggu,
                          ),
                          seremonItem(
                            type: SeremonType.pa,
                            status: status.isSubmissionFailure,
                            data: dataPa,
                          ),
                          seremonItem(
                            type: SeremonType.mandarin,
                            status: status.isSubmissionFailure,
                            data: dataMandarin,
                          ),
                        ],
                      ),
                    ),
            ],
          ),
        );
      },
    );
  }

  Widget seremonItem({
    required SeremonType type,
    required bool status,
    List<SeremonModel>? data,
  }) {
    final bloc = context.read<HomeCubit>();
    return data.isNullOrEmpty()
        ? EmptyWidget.withImage(
            icons: Icons.list_alt_outlined,
            title: 'Tidak ada khotbah ${type.toString().toLowerCase()}',
            msg: 'Tidak ada khotbah ${type.toString()} untuk saat ini',
            onTap: status ? () => bloc.getSeremonSchedule() : null,
          )
        : HomeSeremonItemWidget.data(type: type, data: data ?? []);
  }
}

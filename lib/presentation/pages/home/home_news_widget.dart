import 'package:carousel_slider/carousel_slider.dart';
import 'package:fluentui_system_icons/fluentui_system_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:gkt/domain/model/news/news_model.dart';
import 'package:gkt/presentation/utils/extension.dart';
import 'package:gkt/presentation/widgets/base/base_data_widget.dart';
import 'package:gkt/presentation/widgets/base/empty_widget.dart';

import '../../blocs/home/home_cubit.dart';
import '../../config/theme_colors.dart';
import '../../utils/constant.dart';
import '../../widgets/base/shimmer.dart';

part 'home_news_item_widget.dart';

class HomeNewsWidget extends StatelessWidget {
  const HomeNewsWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<HomeCubit>();
    return BlocConsumer<HomeCubit, HomeState>(
      listenWhen: (previous, current) =>
          previous.newsState.err != current.newsState.err,
      listener: (context, state) {
        if (state.newsState.err != null) {
          context.handleError(
            state.newsState.err?.err,
            retry: () => bloc.initialData(),
          );
        }
      },
      builder: ((context, state) {
        final news = state.newsState.data;
        final status = state.newsState.status;

        if (status.isSubmissionInProgress && news.isNullOrEmpty()) {
          return Shimmer(
              child: HomeNewsItemWidget.shimmer(width: context.screenWidth()));
        }

        if (news.isNullOrEmpty()) {
          return EmptyWidget.withImage(
            icons: FluentIcons.news_28_regular,
            title: 'Tidak ada Berita',
            msg: 'Tidak ada data yang ditampilkan',
            onTap: status.isSubmissionFailure ? () => bloc.getNews() : null,
          );
        }

        final bannerList =
            news!.map((item) => HomeNewsItemWidget.data(data: item)).toList();
        return Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: [
            const SizedBox(
              height: 14,
            ),
            CarouselSlider(
                items: bannerList,
                options: CarouselOptions(
                  aspectRatio: kOnBoardingBannerWidth / kOnBoardingBannerHeight,
                  viewportFraction: 1,
                  height: 180,
                  autoPlay: true,
                  autoPlayInterval: kAutoPlayInterval,
                  autoPlayAnimationDuration: kAutoPlayAnimationDuration,
                  onPageChanged: (index, reason) {
                    //on page change
                  },
                )),
            const SizedBox(
              height: 14,
            ),
          ],
        );
      }),
    );
  }
}

part of 'home_news_widget.dart';

class HomeNewsItemWidget extends DataWidget<NewsModel> {
  const HomeNewsItemWidget.data({
    super.key,
    required super.data,
    this.width,
  }) : super.data();

  const HomeNewsItemWidget.shimmer({super.key, this.width}) : super.shimmer();

  final double? width;

  @override
  Widget buildData(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 24),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(14),
        child: Container(
          color: Colors.white,
          padding: const EdgeInsets.all(12),
          child: Image.network(
            data?.imageUri ?? '',
            fit: BoxFit.cover,
            loadingBuilder: (context, child, loadingProgress) {
              if (loadingProgress == null) {
                return child;
              }
              return buildShimmer(context);
            },
            errorBuilder: (context, error, stackTrace) {
              return Container(
                height: 180,
                color: ThemeColors.greyStyle3,
              );
            },
          ),
        ),
      ),
    );
  }

  @override
  ShimmerLoading buildShimmer(BuildContext context) {
    return ShimmerLoading(
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 24),
        width: width,
        height: 180,
        decoration: const BoxDecoration(
          color: ThemeColors.greyStyle3,
          borderRadius: BorderRadius.all(Radius.circular(20)),
        ),
        child: const SizedBox(height: 180),
      ),
    );
  }
}

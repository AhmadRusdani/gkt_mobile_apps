part of 'home_pray_schedule_widget.dart';

class HomePrayScheduleItemWidget extends DataWidget<PrayScheduleModel> {
  const HomePrayScheduleItemWidget.data({
    super.key,
    required super.data,
  }) : super.data();

  const HomePrayScheduleItemWidget.shimmer({super.key}) : super.shimmer();

  @override
  Widget buildData(BuildContext context) {
    return notchContainer(
      context,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Text(
            data?.details?.first.namaIbadah ?? '',
            style: kTextStyleTitleBold18,
            textAlign: TextAlign.center,
          ),
          TextWithSideLines(
            child: Container(
              margin: const EdgeInsets.all(14),
              child: Text(data?.hari ?? '',
                  style: kTextStyleTitleBold18.copyWith(
                    fontSize: 14,
                    fontFamily: 'Inter',
                  )),
            ),
          ),
          SizedBox(
            height: 250,
            child: ListView.builder(
              shrinkWrap: true,
              padding: EdgeInsets.zero,
              itemBuilder: (context, index) =>
                  TextScheduleData(model: data!.details![index]),
              itemCount: data!.details!.length,
            ),
          ),
          const SizedBox(height: 14),
          Container(
            width: double.infinity,
            height: 4,
            decoration: const BoxDecoration(
                color: ThemeColors.gold,
                borderRadius:
                    BorderRadius.horizontal(left: Radius.circular(6))),
          ),
          // Row(
          //   mainAxisSize: MainAxisSize.max,
          //   children: [
          //     Expanded(
          //       child: Container(
          //         margin: const EdgeInsets.only(left: 6),
          //         height: 4,
          //         decoration: const BoxDecoration(
          //             color: ThemeColors.gold,
          //             borderRadius:
          //                 BorderRadius.horizontal(left: Radius.circular(6))),
          //       ),
          //     ),
          //     Container(
          //       margin: const EdgeInsets.symmetric(horizontal: 12),
          //       child: PrimaryButton(
          //           visualDensity: const VisualDensity(
          //             horizontal: 0,
          //             vertical: 0,
          //           ),
          //           onPressed: () {},
          //           child: Padding(
          //             padding: EdgeInsets.symmetric(
          //               horizontal: context.screenWidth() * 0.12,
          //             ),
          //             child: Text(
          //               'DAFTAR',
          //               style: kTextStyleDescription.copyWith(
          //                   fontWeight: FontWeight.w600,
          //                   fontSize: 14,
          //                   color: Colors.white,
          //                   fontFamily: 'Inter'),
          //             ),
          //           )),
          //     ),
          //     Expanded(
          //       child: Container(
          //         margin: const EdgeInsets.only(right: 6),
          //         height: 4,
          //         decoration: const BoxDecoration(
          //             color: ThemeColors.gold,
          //             borderRadius:
          //                 BorderRadius.horizontal(right: Radius.circular(6))),
          //       ),
          //     ),
          //   ],
          // ),
        ],
      ),
    );
  }

  @override
  ShimmerLoading buildShimmer(BuildContext context) {
    return ShimmerLoading(
      child: notchContainer(context, child: const SizedBox.shrink()),
    );
  }

  Widget notchContainer(BuildContext context, {required Widget child}) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 24),
      child: NotchedWidget(
        child: Container(
          decoration: const BoxDecoration(
            color: Colors.white,
          ),
          height: 450,
          width: context.screenWidth(),
          padding: const EdgeInsets.fromLTRB(18, 14, 18, 42),
          child: child,
        ),
      ),
    );
  }
}

part of 'home_seremon_widget.dart';

class HomeSeremonItemWidget extends DataWidget<List<SeremonModel>> {
  const HomeSeremonItemWidget.data({
    super.key,
    required super.data,
    required this.type,
  }) : super.data();

  const HomeSeremonItemWidget.shimmer({
    super.key,
  })  : type = null,
        super.shimmer();

  final SeremonType? type;

  @override
  Widget buildData(BuildContext context) {
    return ListView.separated(
      padding: const EdgeInsets.symmetric(
        vertical: 0,
        horizontal: 14,
      ),
      primary: false,
      shrinkWrap: true,
      itemBuilder: (context, index) {
        final model = data![index];
        return ListTile(
          title: Text(type.toString(),
              style: kTextStyleDescription.copyWith(
                fontWeight: FontWeight.w500,
                fontSize: 14,
              )),
          subtitle: Text(
              model.jadwalIbadah?.waktuIbadah
                      ?.formatDate(format: 'dd MMMM yyyy') ??
                  '',
              style: kTextStyleDescription.copyWith(
                fontSize: 14,
              )),
          trailing: const Icon(
            Icons.play_circle_fill_rounded,
            size: 32,
            color: ThemeColors.greyPrimary,
          ),
        );
      },
      separatorBuilder: (context, index) {
        return const Divider(
          color: ThemeColors.greyPrimary,
          height: 2,
        );
      },
      itemCount: data!.length,
    );
  }

  @override
  ShimmerLoading buildShimmer(BuildContext context) {
    return ShimmerLoading(
      child: Container(
        padding: const EdgeInsets.symmetric(
          vertical: 24,
          horizontal: 14,
        ),
        height: 350,
        decoration: const BoxDecoration(
          color: Colors.white,
        ),
      ),
    );
  }
}

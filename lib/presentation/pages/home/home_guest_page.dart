import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hud/flutter_hud.dart';
import 'package:gkt/presentation/blocs/guess/guess_cubit.dart';
import 'package:gkt/presentation/config/theme_texts.dart';
import 'package:gkt/presentation/pages/webview/webview_page.dart';
import 'package:gkt/presentation/utils/extension.dart';
import 'package:gkt/presentation/widgets/base/background_widget.dart';
import 'package:pattern_formatter/numeric_formatter.dart';

import '../../config/theme_colors.dart';
import '../../widgets/base/base_material_page.dart';
import '../../widgets/base/text_field_widget.dart';

class HomeGuestPage extends StatelessWidget {
  static const routeName = '/dashboard_guest';
  const HomeGuestPage({super.key});

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<GuessCubit>();
    return BaseMaterialPage(
      child: MultiBlocListener(
        listeners: _listeners(context),
        child: Scaffold(
          extendBodyBehindAppBar: true,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            centerTitle: true,
            title: Text.rich(
                textAlign: TextAlign.center,
                TextSpan(
                    text: 'GEREJA KEMAH TABERNACLE\n',
                    style: kTextStyleDescription12.copyWith(
                      color: ThemeColors.goldStyle7,
                      fontSize: 14,
                    ),
                    children: [
                      TextSpan(
                        text: 'PEMBANGUNAN',
                        style: kTextStyleDescription12.copyWith(
                          color: ThemeColors.gold,
                          fontSize: 14,
                        ),
                      )
                    ])),
          ),
          body: BlocBuilder<GuessCubit, GuessState>(builder: (context, state) {
            return BackgroundGradient(
              height: context.screenHeight(),
              child: SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                padding: const EdgeInsets.fromLTRB(24, 100, 24, 32),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 24, vertical: 24),
                      child: Text(
                        'BERIKAN PERSEMBAHAN',
                        style: kTextStyleDescriptionBold16.copyWith(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    ClipRRect(
                      borderRadius: const BorderRadius.all(Radius.circular(24)),
                      child: Container(
                        color: Colors.white,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Container(
                              margin: const EdgeInsets.fromLTRB(24, 24, 24, 8),
                              child: Text('Nominal Persembahan',
                                  style: kTextStyleTitleBold18.copyWith(
                                    fontSize: 16,
                                    color: Colors.black,
                                    fontFamily: 'Inter',
                                    fontWeight: FontWeight.w600,
                                  )),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 24),
                              child: FormzTextField(
                                textEditingController:
                                    state.amount.textEditingController,
                                fillColor: Colors.grey.shade100,
                                style: kTextStyleTitleBold18.copyWith(
                                    fontSize: 14),
                                hintText: '0',
                                keyboardType: TextInputType.number,
                                leading: Container(
                                  padding:
                                      const EdgeInsets.fromLTRB(14, 14, 8, 14),
                                  child: Text('Rp',
                                      style: kTextStyleTitleBold18.copyWith(
                                          fontSize: 14)),
                                ),
                                onChanged: (amount) {
                                  bloc.nominalInput(amount);
                                },
                                format: [ThousandsFormatter()],
                                textInputAction: TextInputAction.next,
                              ),
                            ),
                            const SizedBox(height: 24),
                            InkWell(
                              onTap: state.amount.valid
                                  ? () {
                                      _showAlertDialog(context);
                                    }
                                  : null,
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                  vertical: 14,
                                  horizontal: context.screenWidth() * 0.15,
                                ),
                                color: state.amount.valid
                                    ? ThemeColors.gold
                                    : ThemeColors.greyStyle3,
                                alignment: Alignment.center,
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      child: Text(
                                        'LANJUT KE PEMBAYARAN',
                                        style: kTextStyleTitle16.copyWith(
                                          color: state.amount.valid
                                              ? Colors.white
                                              : ThemeColors.greyPrimary,
                                        ),
                                      ),
                                    ),
                                    Icon(
                                      Icons.chevron_right_rounded,
                                      color: state.amount.valid
                                          ? Colors.white
                                          : ThemeColors.greyPrimary,
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    // Expanded(child: Container()),
                  ],
                ),
              ),
            );
          }),
        ),
      ),
    );
  }

  List<BlocListener> _listeners(BuildContext context) {
    return [
      BlocListener<GuessCubit, GuessState>(
          listenWhen: (previous, current) =>
              previous.offeringState.status != current.offeringState.status,
          listener: (context, state) {
            final hud = PopupHUD(context);
            state.offeringState.status.handleLoading(hud: hud);
          }),
      BlocListener<GuessCubit, GuessState>(
        listenWhen: (previous, current) =>
            previous.offeringState.err != current.offeringState.err,
        listener: (context, state) {
          final offering = state.offeringState;
          if (offering.err != null) {
            context.handleError(offering.err?.err);
          }
        },
      ),
      BlocListener<GuessCubit, GuessState>(
        listenWhen: (previous, current) =>
            previous.offeringState.data != current.offeringState.data,
        listener: (context, state) {
          final offering = state.offeringState;
          if (offering.data != null) {
            Navigator.of(context).pushNamed(WebViewPage.routeName, arguments: {
              'url': offering.data?.paymentUrl,
              'title': 'Persembahan',
            });
          }
        },
      ),
    ];
  }

  Future<void> _showAlertDialog(BuildContext context) async {
    final bloc = context.read<GuessCubit>();
    return await context
        .showConfirmationBottomSheet(
      title: 'Konfirmasi',
      message: 'Anda yakin ingin memberikan persembahan '
          'senilai Rp.${bloc.state.amount.value}',
      negativeButton: 'Tidak',
      positiveButton: 'Ya',
    )
        .then((value) {
      if (value == true) {
        bloc.sendOffering();
      }
    });
  }
}

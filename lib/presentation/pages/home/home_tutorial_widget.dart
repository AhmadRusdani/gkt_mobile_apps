import 'package:flutter/material.dart';
import 'package:gkt/presentation/config/theme_colors.dart';
import 'package:gkt/presentation/config/theme_texts.dart';
import 'package:gkt/presentation/utils/extension.dart';
import 'package:gkt/presentation/widgets/base/notched_widget.dart';

class HomeTutorialWidget extends StatefulWidget {
  const HomeTutorialWidget({super.key});

  @override
  State<HomeTutorialWidget> createState() => _HomeTutorialWidgetState();
}

class _HomeTutorialWidgetState extends State<HomeTutorialWidget> {
  final pageController = PageController();
  int activeIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 14, bottom: 32),
      height: 210,
      child: Stack(
        children: [
          PageView(
            controller: pageController,
            onPageChanged: (value) {
              setState(() {
                activeIndex = value;
              });
            },
            children: tutorials(),
          ),
          Align(
            alignment: Alignment.center,
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                IconButton(
                    onPressed: activeIndex > 0
                        ? () {
                            print(activeIndex);
                            animateSlider(activeIndex - 1);
                          }
                        : null,
                    icon: const Icon(
                      Icons.chevron_left_rounded,
                      color: Colors.white,
                      size: 34,
                    )),
                IconButton(
                    onPressed: activeIndex < 2
                        ? () {
                            print(activeIndex);
                            animateSlider(activeIndex + 1);
                          }
                        : null,
                    icon: const Icon(
                      Icons.chevron_right_rounded,
                      color: Colors.white,
                      size: 34,
                    ))
              ],
            ),
          ),
        ],
      ),
    );
  }

  List<Widget> tutorials() {
    return [
      tutorialsItem(),
      tutorialsItem(),
      tutorialsItem(),
    ];
  }

  Widget tutorialsItem() {
    return Container(
      margin: const EdgeInsets.only(right: 24, left: 24),
      width: context.screenWidth(),
      child: NotchedWidgetSide(
        notchedRadius: 28,
        child: Container(
          height: 210,
          decoration: const BoxDecoration(
            color: Colors.white,
          ),
          padding: const EdgeInsets.fromLTRB(24, 14, 24, 14),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                  child: tutorialItem(
                assets: 'assets/images/img_tutorial_1.png',
                title: 'Protokol Saat Perjamuan Kudus',
              )),
              dottedVerticalLine(),
              Expanded(
                  child: tutorialItem(
                assets: 'assets/images/img_tutorial_2.png',
                title: 'Cara menggunakan fitur persembahan',
              )),
            ],
          ),
        ),
      ),
    );
  }

  Widget dottedVerticalLine() => SizedBox(
        height: 160,
        width: 20,
        child: Stack(
          children: [
            Positioned.fill(
              child: Align(
                alignment: Alignment.center,
                child: Container(
                  width: 3,
                  decoration: const BoxDecoration(
                    color: ThemeColors.gold,
                  ),
                ),
              ),
            ),
            Positioned.fill(
              child: Align(
                alignment: Alignment.topCenter,
                child: Container(
                  height: 20,
                  width: 20,
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    color: ThemeColors.gold,
                  ),
                ),
              ),
            ),
            Positioned.fill(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  height: 20,
                  width: 20,
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    color: ThemeColors.gold,
                  ),
                ),
              ),
            ),
          ],
        ),
      );

  Widget tutorialItem({
    required String assets,
    required String title,
  }) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            height: 120,
            margin: const EdgeInsets.symmetric(vertical: 12),
            decoration: BoxDecoration(
              color: ThemeColors.greyPrimary,
              borderRadius: BorderRadius.circular(12),
              image: DecorationImage(
                  image: AssetImage(
                    assets,
                  ),
                  fit: BoxFit.cover),
            ),
          ),
          Text(
            title,
            style: kTextStyleDescription.copyWith(
              fontWeight: FontWeight.w500,
              fontSize: 10,
            ),
            textAlign: TextAlign.center,
          )
        ],
      ),
    );
  }

  void animateSlider(int page) {
    pageController.animateToPage(
      page,
      duration: const Duration(milliseconds: 300),
      curve: Curves.linear,
    );
  }
}

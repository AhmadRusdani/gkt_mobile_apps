import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gkt/presentation/config/theme_colors.dart';
import 'package:gkt/presentation/config/theme_texts.dart';

class HomeRegistrationWidget extends StatelessWidget {
  const HomeRegistrationWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(24, 14, 24, 32),
      padding: const EdgeInsets.symmetric(
        horizontal: 8,
        vertical: 14,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Colors.white,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          registerTypeItem(
            assets: 'assets/icons/ic_church.svg',
            title: 'Baptis',
          ),
          Container(
            height: 44,
            width: 2,
            color: ThemeColors.greyStyle2,
          ),
          registerTypeItem(
            assets: 'assets/icons/ic_family.svg',
            title: 'Pemberkatan Anak',
          ),
          Container(
            height: 44,
            width: 2,
            color: ThemeColors.greyStyle2,
          ),
          registerTypeItem(
            assets: 'assets/icons/ic_ring_married.svg',
            title: 'Pemberkatan Nikah',
          ),
          Container(
            height: 44,
            width: 2,
            color: ThemeColors.greyStyle2,
          ),
          registerTypeItem(
            assets: 'assets/icons/ic_kebab_menu.svg',
            title: 'Lainnya',
          ),
        ],
      ),
    );
  }

  Widget registerTypeItem({
    required String assets,
    required String title,
    VoidCallback? onTap,
  }) {
    return Expanded(
      child: GestureDetector(
        onTap: onTap,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 4),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              SvgPicture.asset(assets),
              const SizedBox(height: 12),
              Text(
                title,
                style: kTextStyleDescription.copyWith(fontSize: 10),
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

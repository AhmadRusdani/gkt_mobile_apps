import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hud/flutter_hud.dart';
import 'package:formz/formz.dart';
import 'package:gkt/domain/model/user/user_model.dart';
import 'package:gkt/presentation/blocs/base/base_state.dart';
import 'package:gkt/presentation/blocs/profile/profile_cubit.dart';
import 'package:gkt/presentation/config/theme_colors.dart';
import 'package:gkt/presentation/config/theme_texts.dart';
import 'package:gkt/presentation/pages/password/change_password_page.dart';
import 'package:gkt/presentation/utils/extension.dart';
import 'package:gkt/presentation/widgets/base/appbar_widget.dart';
import 'package:gkt/presentation/widgets/base/avatar_widget.dart';
import 'package:gkt/presentation/widgets/base/background_widget.dart';
import 'package:gkt/presentation/widgets/base/notched_widget.dart';
import 'package:gkt/presentation/widgets/base/text_widget.dart';
import 'package:gkt/presentation/widgets/dialog/gkt_dialog.dart';
import 'package:qr_flutter/qr_flutter.dart';

import '../../blocs/auth/auth_cubit.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({super.key});

  @override
  Widget build(BuildContext context) {
    final authBloc = context.read<AuthCubit>();
    final profileUser = context.read<ProfileCubit>();
    return MultiBlocListener(
      listeners: [
        BlocListener<ProfileCubit, BaseState<ChangeUserDetailModel>>(
            listenWhen: (previous, current) =>
                previous.status != current.status,
            listener: (context, state) {
              final hud = PopupHUD(context);
              state.status.handleLoading(hud: hud);

              if (state.status == FormzStatus.submissionSuccess) {
                context.showOkToast('Data berhasil diganti!');
                authBloc.updateUser();
              }
            }),
        BlocListener<ProfileCubit, BaseState<ChangeUserDetailModel>>(
            listenWhen: (previous, current) => previous.err != current.err,
            listener: (context, state) {
              if (state.err != null) {
                context.handleError(state.err?.err);
              }
            }),
      ],
      child: Scaffold(
          backgroundColor: ThemeColors.background,
          body: SingleChildScrollView(
            child: BackgroundGradient(
              minHeight: context.screenHeight(),
              padding:
                  EdgeInsets.fromLTRB(24, 14, 24, context.screenHeight() * 0.2),
              child:
                  BlocBuilder<AuthCubit, AuthState>(builder: (context, state) {
                return Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    appbarPrimary(title: 'PROFILE'),
                    const SizedBox(height: 24),
                    const CircleAvatars(
                      radius: 56,
                      image: 'assets/icons/profile_icon.png',
                      color: Colors.white,
                      useElevation: true,
                    ),
                    const SizedBox(height: 24),
                    NotchedWidget(
                      child: Container(
                        decoration: const BoxDecoration(
                          color: Colors.white,
                        ),
                        padding: const EdgeInsets.fromLTRB(18, 14, 18, 42),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Text(
                              state.user?.namaAnggota?.toUpperCase() ?? '',
                              style: kTextStyleDescriptionBold16.copyWith(
                                color: Colors.black,
                                fontWeight: FontWeight.w600,
                                fontSize: 18,
                              ),
                              textAlign: TextAlign.center,
                            ),
                            const SizedBox(height: 6),
                            TextWithSideLines(
                              child: Container(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 8),
                                child: Text(state.user?.noAnggota ?? '',
                                    style: kTextStyleDescription.copyWith(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w600,
                                      fontSize: 14,
                                    )),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(
                                horizontal: context.screenWidth() * 0.1,
                              ),
                              child: state.user?.noAnggota != null
                                  ? QrImageView(
                                      data: state.user!.noAnggota!,
                                      padding: const EdgeInsets.all(24),
                                    )
                                  : const SizedBox.shrink(),
                            )
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(height: 32),
                    Container(
                      padding: const EdgeInsets.symmetric(
                        vertical: 14,
                        horizontal: 18,
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.white,
                      ),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Text(
                            'DATA DIRI',
                            style: kTextStyleDescriptionBold16.copyWith(
                              color: Colors.black,
                              fontWeight: FontWeight.w600,
                              fontSize: 18,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          const TextWithSideLines(),
                          const SizedBox(height: 12),
                          userDataItem(
                              type: 'No. Telp',
                              title: state.user?.detail?.phoneNumber
                                          .isNullOrEmpty() ==
                                      false
                                  ? state.user!.detail!.phoneNumber.toString()
                                  : 'Belum diisi',
                              onTap: () async {
                                await showChangeUserDialog(context,
                                        isPhone: true)
                                    .then((value) {
                                  if (!value.isNullOrEmpty()) {
                                    profileUser.changeData(phone: value);
                                  }
                                });
                              }),
                          userDataItem(
                              type: 'Alamat',
                              title:
                              state.user?.detail?.alamat
                                  .isNullOrEmpty() ==
                                  false
                                  ? state.user!.detail!.alamat.toString()
                                  : 'Belum diisi',
                              onTap: () async {
                                await showChangeUserDialog(context,
                                        isPhone: false)
                                    .then((value) {
                                  if (!value.isNullOrEmpty()) {
                                    profileUser.changeData(address: value);
                                  }
                                });
                              }),
                        ],
                      ),
                    ),
                    const SizedBox(height: 18),
                    itemActionWidget(
                        title: 'Ganti Kata Sandi',
                        icons: Icons.lock_rounded,
                        onTap: () {
                          Navigator.of(context)
                              .pushNamed(ChangePasswordPage.routeName);
                        }),
                    const SizedBox(height: 18),
                    itemActionWidget(
                      title: 'Help Center',
                      icons: Icons.help,
                    ),
                    const SizedBox(height: 18),
                    itemActionWidget(
                      title: 'Keluar',
                      icons: Icons.exit_to_app,
                      onTap: () {
                        context
                            .showConfirmationBottomSheet(
                              title: 'Keluar Aplikasi',
                              message: 'Anda yakin ingin keluar dari aplikasi?',
                            )
                            .then((value) =>
                                value == true ? authBloc.onLogout() : null);
                      },
                    ),
                  ],
                );
              }),
            ),
          )),
    );
  }

  Widget itemActionWidget({
    required String title,
    required IconData icons,
    VoidCallback? onTap,
  }) =>
      Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Colors.white,
        ),
        child: ListTile(
          onTap: onTap,
          leading: Icon(
            icons,
            color: ThemeColors.gold,
            size: 32,
          ),
          title: Text(
            title,
            style: kTextStyleDescriptionBold16,
          ),
        ),
      );

  Widget userDataItem({
    required String type,
    required String title,
    VoidCallback? onTap,
  }) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 14, vertical: 6),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 1,
            child: TextFit(
              type,
              style: kTextStyleDescription,
              align: TextAlign.center,
            ),
          ),
          Container(
            alignment: Alignment.center,
            margin: const EdgeInsets.symmetric(horizontal: 14),
            width: 2,
            height: 34,
            color: ThemeColors.greyPrimary,
          ),
          Expanded(
            flex: 4,
            child: Text(
              title,
              style: kTextStyleDescription,
            ),
          ),
          InkWell(
            onTap: onTap,
            child: Container(
              margin: const EdgeInsets.only(left: 6),
              child: const Icon(
                Icons.edit_outlined,
                color: ThemeColors.goldStyle8,
                size: 24,
              ),
            ),
          )
        ],
      ),
    );
  }
}

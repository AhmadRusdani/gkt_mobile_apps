import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hud/flutter_hud.dart';
import 'package:formz/formz.dart';
import 'package:gkt/presentation/blocs/login/login_cubit.dart';
import 'package:gkt/presentation/config/theme_texts.dart';
import 'package:gkt/presentation/pages/greeting/greeting_page.dart';
import 'package:gkt/presentation/pages/home/home_guest_page.dart';
import 'package:gkt/presentation/utils/extension.dart';
import 'package:gkt/presentation/utils/number_stripped_formatter.dart';
import 'package:gkt/presentation/widgets/base/background_widget.dart';
import 'package:gkt/presentation/widgets/base/button_widget.dart';
import 'package:gkt/presentation/widgets/base/text_field_widget.dart';

import '../../config/theme_colors.dart';
import '../../widgets/base/base_material_page.dart';

class LoginPage extends StatefulWidget {
  static const routeName = '/login';
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  PopupHUD? popUpHud;

  @override
  void initState() {
    super.initState();
    // WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
    //   popUpHud = PopupHUD(context);
    // });
  }

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<LoginCubit>();
    return BaseMaterialPage(
      child: MultiBlocListener(
        listeners: [
          BlocListener<LoginCubit, LoginState>(
            listenWhen: (previous, current) =>
                previous.status != current.status,
            listener: (context, state) {
              state.status.handleLoading(hud: PopupHUD(context));
              if (state.status.isSubmissionSuccess) {
                Navigator.of(context).pushNamedAndRemoveUntil(
                  GreetingPage.routeName,
                  (route) => false,
                  arguments: state.name,
                );
              }
            },
          ),
          BlocListener<LoginCubit, LoginState>(
            listenWhen: (previous, current) => previous.err != current.err,
            listener: (context, state) {
              if (state.err != null) {
                context.handleError(state.err?.err);
              }
            },
          )
        ],
        child: Scaffold(
          body: BlocBuilder<LoginCubit, LoginState>(builder: (context, state) {
            return Stack(
              children: [
                BackgroundPrimary(
                    child: Container(
                  height: context.screenHeight(),
                )),
                SingleChildScrollView(
                  padding: const EdgeInsets.all(24),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      SizedBox(height: context.screenHeight() * 0.08),
                      Container(
                        alignment: Alignment.center,
                        padding: const EdgeInsets.all(24),
                        height: 120,
                        width: 120,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.shade200,
                              offset: const Offset(5, -5),
                              blurRadius: 5,
                            ),
                            BoxShadow(
                              color: Colors.grey.shade200,
                              offset: const Offset(-5, 5),
                              blurRadius: 5,
                            ),
                          ],
                        ),
                        child: Image.asset(
                          'assets/images/logo_tabernacle.png',
                          height: 90,
                          width: 90,
                          fit: BoxFit.fitHeight,
                        ),
                      ),
                      const SizedBox(height: 28),
                      Text(
                        'GEREJA KEMAH TABERNACLE',
                        style: kTextStyleTitleBold18.copyWith(
                          color: ThemeColors.greyPrimary,
                          fontFamily: 'Inter',
                        ),
                        textAlign: TextAlign.center,
                      ),
                      const SizedBox(height: 28),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 12),
                        child: Text(
                          'Nomor Anggota',
                          style: kTextStyleDescriptionBold16.copyWith(
                            color: Colors.black,
                          ),
                        ),
                      ),
                      const SizedBox(height: 6),
                      FormzTextField(
                        fillColor: Colors.grey.shade100,
                        format: [
                          NumberStrippedFormatter(separator: '-'),
                        ],
                        onChanged: (value) {
                          bloc.setNumber(value);
                        },
                        leading: Padding(
                          padding: const EdgeInsets.fromLTRB(14, 14, 14, 14),
                          child: Image.asset(
                            'assets/icons/profile_icon.png',
                            height: 14,
                            width: 14,
                            fit: BoxFit.scaleDown,
                          ),
                        ),
                        textInputAction: TextInputAction.next,
                        keyboardType: TextInputType.number,
                        hintText: 'Masukan Nomor Anggota Anda',
                      ),
                      const SizedBox(height: 24),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 12),
                        child: Text(
                          'Kata Sandi',
                          style: kTextStyleDescriptionBold16.copyWith(
                            color: Colors.black,
                          ),
                        ),
                      ),
                      const SizedBox(height: 6),
                      FormzTextField(
                        fillColor: Colors.grey.shade100,
                        onChanged: (value) {
                          bloc.setPassword(value);
                        },
                        leading: Padding(
                          padding: const EdgeInsets.fromLTRB(14, 14, 14, 14),
                          child: Image.asset(
                            'assets/icons/lock_icon.png',
                            height: 14,
                            width: 14,
                            fit: BoxFit.scaleDown,
                          ),
                        ),
                        textInputAction: TextInputAction.done,
                        hintText: 'Masukan Kata Sandi Anda',
                        obscureText: true,
                      ),
                      SizedBox(height: context.screenHeight() * 0.08),
                      GradientButton(
                        radius: const BorderRadius.all(Radius.circular(28)),
                        onPressed: state.status.isValid
                            ? () {
                                bloc.login();
                              }
                            : null,
                        size: const Size(double.infinity, 58),
                        fontSize: 16,
                        child: const Text('Log in'),
                      ),
                      const SizedBox(height: 12),
                      WhiteButton(
                        shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(28),
                          ),
                        ),
                        minSize: const Size(double.infinity, 58),
                        visualDensity:
                            const VisualDensity(horizontal: 0, vertical: 0),
                        onPressed: () {
                          Navigator.of(context).pushNamedAndRemoveUntil(
                              HomeGuestPage.routeName, (route) => false);
                        },
                        fontSize: 16,
                        child: const Text('Log in as Guest'),
                      ),
                    ],
                  ),
                ),
              ],
            );
          }),
        ),
      ),
    );
  }
}

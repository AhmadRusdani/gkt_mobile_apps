import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:gkt/domain/model/detail_process_summary/detail_process_summary_model.dart';
import 'package:gkt/presentation/pages/building/building_process_page.dart';
import 'package:gkt/presentation/pages/gallery/gallery_detail_page.dart';
import 'package:gkt/presentation/utils/extension.dart';
import 'package:gkt/presentation/widgets/base/appbar_widget.dart';
import 'package:gkt/presentation/widgets/base/background_widget.dart';
import 'package:gkt/presentation/widgets/base/text_widget.dart';

class GalleryPage extends StatelessWidget {
  static const routeName = '${BuildingProcessPage.routeName}/gallery';
  const GalleryPage({
    super.key,
    required this.photos,
    this.phase,
  });

  final List<PhotoProcessSummaryModel> photos;
  final String? phase;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BackgroundGradient(
        minHeight: context.screenHeight(),
        assetsAlign: Alignment.centerRight,
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            appBarPrimaryBack(),
            SizedBox(height: context.screenHeight() * 0.02),
            sectionTitle(
              title: 'Gallery',
              fontSize: 24,
            ),
            sectionTitle(
                title: phase ?? '',
                fontSize: 14,
                padding: const EdgeInsets.fromLTRB(24, 0, 24, 18)),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(
                  bottom: context.responsivePaddingBottom(),
                ),
                padding: const EdgeInsets.all(14),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(24),
                ),
                child: GridView.builder(
                  padding: EdgeInsets.zero,
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    crossAxisSpacing: 14,
                    mainAxisSpacing: 14,
                  ),
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    if (photos[index].imageUrl == null) {
                      return Container(
                        height: 46,
                        decoration: BoxDecoration(
                          color: Colors.grey[400],
                          borderRadius: BorderRadius.circular(14),
                        ),
                      );
                    }

                    return CachedNetworkImage(
                      imageUrl: photos[index].imageUrl!,
                      placeholder: (context, url) =>
                          const Center(child: CircularProgressIndicator()),
                      errorWidget: (context, url, error) =>
                          const Icon(Icons.error_outline_rounded),
                      imageBuilder: (context, imageProvider) => InkWell(
                        onTap: () => Navigator.of(context).pushNamed(
                          GalleryDetailPage.routeName,
                          arguments: photos[index].imageUrl!,
                        ),
                        child: Container(
                          height: 46,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(14),
                            image: DecorationImage(
                              image: imageProvider,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                  itemCount: photos.length,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:gkt/presentation/pages/gallery/gallery_page.dart';
import 'package:gkt/presentation/utils/extension.dart';
import 'package:gkt/presentation/widgets/base/appbar_widget.dart';

class GalleryDetailPage extends StatelessWidget {
  static const routeName = '${GalleryPage.routeName}/details';
  const GalleryDetailPage({super.key, required this.photo});

  final String photo;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarPrimaryBlack(),
      body: InteractiveViewer(
        maxScale: 2.0,
        minScale: 0.1,
        child: Container(
          color: Colors.black87,
          width: context.screenWidth(),
          height: context.screenHeight(),
          child: CachedNetworkImage(
            imageUrl: photo,
            placeholder: (context, url) =>
                const Center(child: CircularProgressIndicator()),
            errorWidget: (context, url, error) =>
                const Icon(Icons.error_outline_rounded),
            imageBuilder: (context, imageProvider) => Container(
              height: 46,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(14),
                image: DecorationImage(
                  image: imageProvider,
                  fit: BoxFit.scaleDown,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

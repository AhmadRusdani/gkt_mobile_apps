import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hud/flutter_hud.dart';
import 'package:gkt/presentation/blocs/projects/projects_cubit.dart';
import 'package:gkt/presentation/config/theme_colors.dart';
import 'package:gkt/presentation/pages/webview/webview_page.dart';
import 'package:gkt/presentation/utils/extension.dart';
import 'package:gkt/presentation/widgets/base/text_field_widget.dart';
import 'package:pattern_formatter/numeric_formatter.dart';

import '../../config/theme_texts.dart';

class GiveOfferingWidget extends StatelessWidget {
  const GiveOfferingWidget({
    super.key,
    this.isValid = false,
  });

  final bool isValid;

  @override
  Widget build(BuildContext context) {
    return MultiBlocListener(
      listeners: _listeners(context),
      child: ClipRRect(
        borderRadius: const BorderRadius.all(Radius.circular(24)),
        child: Container(
          color: Colors.white,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                margin: const EdgeInsets.fromLTRB(24, 24, 24, 8),
                child: Text('Nominal Persembahan',
                    style: kTextStyleTitleBold18.copyWith(
                      fontSize: 16,
                      color: Colors.black,
                      fontFamily: 'Inter',
                      fontWeight: FontWeight.w600,
                    )),
              ),
              _formInput(),
              Container(
                margin: const EdgeInsets.fromLTRB(24, 24, 24, 8),
                child: Text('Catatan',
                    style: kTextStyleTitleBold18.copyWith(
                      fontSize: 16,
                      color: Colors.black,
                      fontFamily: 'Inter',
                      fontWeight: FontWeight.w600,
                    )),
              ),
              _formInputNote(),
              const SizedBox(height: 24),
              InkWell(
                onTap: isValid
                    ? () {
                        _showAlertDialog(context);
                      }
                    : null,
                child: Container(
                  padding: EdgeInsets.symmetric(
                    vertical: 14,
                    horizontal: context.screenWidth() * 0.15,
                  ),
                  color: isValid ? ThemeColors.gold : ThemeColors.greyStyle3,
                  alignment: Alignment.center,
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Text(
                          'LANJUT KE PEMBAYARAN',
                          style: kTextStyleTitle16.copyWith(
                            color: isValid
                                ? Colors.white
                                : ThemeColors.greyPrimary,
                          ),
                        ),
                      ),
                      Icon(
                        Icons.chevron_right_rounded,
                        color: isValid ? Colors.white : ThemeColors.greyPrimary,
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _formInput() {
    return BlocBuilder<ProjectsCubit, ProjectsState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: FormzTextField(
            fillColor: Colors.grey.shade100,
            style: kTextStyleTitleBold18.copyWith(fontSize: 14),
            hintText: '0',
            textEditingController: state.amount.textEditingController,
            keyboardType: TextInputType.number,
            leading: Container(
              padding: const EdgeInsets.fromLTRB(14, 14, 8, 14),
              child: Text('Rp',
                  style: kTextStyleTitleBold18.copyWith(fontSize: 14)),
            ),
            onChanged: (number) {
              context.read<ProjectsCubit>().nominalInput(number);
            },
            format: [
              ThousandsFormatter(),
            ],
            textInputAction: TextInputAction.next,
          ),
        );
      },
    );
  }

  Widget _formInputNote() {
    return BlocBuilder<ProjectsCubit, ProjectsState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: FormzTextField(
            textEditingController: state.note.textEditingController,
            fillColor: Colors.grey.shade100,
            style: kTextStyleTitleBold18.copyWith(fontSize: 12),
            hintText: 'Tambahkan catatan disini',
            minLines: 4,
            maxLines: null,
            keyboardType: TextInputType.multiline,
            textInputAction: TextInputAction.next,
            onChanged: (notes) {
              context.read<ProjectsCubit>().noteInput(notes);
            },
          ),
        );
      },
    );
  }

  List<BlocListener> _listeners(BuildContext context) {
    return [
      BlocListener<ProjectsCubit, ProjectsState>(
          listenWhen: (previous, current) =>
              previous.offeringState.status != current.offeringState.status,
          listener: (context, state) {
            final hud = PopupHUD(context);
            state.offeringState.status.handleLoading(hud: hud);
          }),
      BlocListener<ProjectsCubit, ProjectsState>(
        listenWhen: (previous, current) =>
            previous.offeringState.err != current.offeringState.err,
        listener: (context, state) {
          final offering = state.offeringState;
          if (offering.err != null) {
            context.handleError(offering.err?.err);
          }
        },
      ),
      BlocListener<ProjectsCubit, ProjectsState>(
        listenWhen: (previous, current) =>
            previous.offeringState.data != current.offeringState.data,
        listener: (context, state) {
          final offering = state.offeringState;
          if (offering.data != null) {
            Navigator.of(context).pushNamed(WebViewPage.routeName, arguments: {
              'url': offering.data?.paymentUrl,
              'title': 'Persembahan',
            });
          }
        },
      ),
    ];
  }

  Future<void> _showAlertDialog(BuildContext context) async {
    final bloc = context.read<ProjectsCubit>();
    return await context
        .showConfirmationBottomSheet(
      title: 'Konfirmasi',
      message: 'Anda yakin ingin memberikan persembahan '
          'senilai Rp.${bloc.state.amount.value}',
      negativeButton: 'Tidak',
      positiveButton: 'Ya',
    )
        .then((value) {
      if (value == true) {
        bloc.sendOffering();
      }
    });
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gkt/presentation/blocs/projects/projects_cubit.dart';
import 'package:gkt/presentation/config/theme_texts.dart';
import 'package:gkt/presentation/pages/history/history_page.dart';
import 'package:gkt/presentation/pages/projects/building_progress_widget.dart';
import 'package:gkt/presentation/pages/projects/give_offering_widget.dart';
import 'package:gkt/presentation/pages/projects/process_summary_widget.dart';
import 'package:gkt/presentation/pages/projects/site_visit_widget.dart';
import 'package:gkt/presentation/utils/extension.dart';
import 'package:gkt/presentation/widgets/base/appbar_widget.dart';
import 'package:gkt/presentation/widgets/base/background_widget.dart';
import 'package:gkt/presentation/widgets/base/base_material_page.dart';

import '../../config/theme_colors.dart';

class ProjectsPage extends StatelessWidget {
  const ProjectsPage({super.key});

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<ProjectsCubit>();
    return BaseMaterialPage(
      child: Scaffold(
        backgroundColor: ThemeColors.background,
        body: BlocBuilder<ProjectsCubit, ProjectsState>(
            builder: (context, state) {
          return RefreshIndicator(
            onRefresh: () async => bloc.initData(),
            child: SingleChildScrollView(
              child: BackgroundGradient(
                minHeight: context.screenHeight(),
                assets: 'assets/images/logo_background_2_big.png',
                assetsAlign: Alignment.center,
                padding: EdgeInsets.fromLTRB(
                    24, 14, 24, context.screenHeight() * 0.2),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    appbarPrimary(
                      title: 'PEMBANGUNAN',
                      actions: [
                        InkWell(
                          onTap: () {
                            Navigator.of(context)
                                .pushNamed(HistoryPage.routeName);
                          },
                          child: const Icon(
                            Icons.history_rounded,
                            size: 34,
                            color: ThemeColors.gold,
                          ),
                        )
                      ],
                    ),
                    const SizedBox(height: 42),
                    const BuildingProgressWidget(),
                    sectionTitle(title: 'Berikan Persembahan'),
                    GiveOfferingWidget(
                      isValid: state.amount.valid && state.note.valid,
                    ),
                    const SizedBox(height: 14),
                    sectionTitle(title: 'Site Visit'),
                    const SiteVisitWidget(),
                    const SizedBox(height: 14),
                    sectionTitle(title: 'Rangkuman Proses'),
                    const ProcessSummaryWidget(),
                  ],
                ),
              ),
            ),
          );
        }),
      ),
    );
  }

  Widget sectionTitle({required String title}) => Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 24,
          vertical: 14,
        ),
        child: Text(
          title,
          style: kTextStyleDescriptionBold16.copyWith(
            fontSize: 18,
            fontWeight: FontWeight.w600,
          ),
        ),
      );
}

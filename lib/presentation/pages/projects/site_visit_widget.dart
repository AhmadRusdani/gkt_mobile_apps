import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:gkt/presentation/blocs/projects/projects_cubit.dart';
import 'package:gkt/presentation/config/theme_colors.dart';
import 'package:gkt/presentation/config/theme_texts.dart';
import 'package:gkt/presentation/pages/maps/maps_page.dart';
import 'package:gkt/presentation/utils/constant.dart';
import 'package:gkt/presentation/utils/extension.dart';
import 'package:gkt/presentation/widgets/base/empty_widget.dart';
import 'package:gkt/presentation/widgets/base/shimmer.dart';
import 'package:gkt/presentation/widgets/base/text_widget.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

class SiteVisitWidget extends StatelessWidget {
  const SiteVisitWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<ProjectsCubit>();
    return BlocConsumer<ProjectsCubit, ProjectsState>(
      listenWhen: (previous, current) =>
          previous.siteVisitState.err != current.siteVisitState.err,
      listener: (context, state) {
        if (state.siteVisitState.err != null) {
          context.handleError(state.siteVisitState.err?.err);
        }
      },
      builder: (context, state) {
        if (state.siteVisitState.status.isSubmissionInProgress &&
            state.siteVisitState.data == null) {
          return _siteVisitShimmer();
        }

        return Container(
          padding: const EdgeInsets.symmetric(
            horizontal: 20,
            vertical: 20,
          ),
          width: double.infinity,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(24),
          ),
          child: state.siteVisitState.data == null
              ? EmptyWidget.withImage(
                  icons: Icons.location_off_sharp,
                  title: 'Tidak ada Site Visit',
                  msg: 'Tidak ada data yang ditampilkan',
                  onTap: state.siteVisitState.status.isSubmissionFailure
                      ? () => bloc.getSiteVisit()
                      : null,
                )
              : Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text('Site Visit akan diadakan tanggal',
                        style: kTextStyleDescriptionBold16.copyWith(
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                          fontFamily: 'Inter',
                        )),
                    const SizedBox(height: 6),
                    Text(
                        siteVisitDateFormat(
                            state.siteVisitState.data?.tanggalBerlangsung),
                        style: kTextStyleDescriptionBold16.copyWith(
                          fontSize: 20,
                          fontFamily: 'Inter',
                        )),
                    const SizedBox(height: 6),
                    const TextWithSideLines(),
                    const SizedBox(height: 12),
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: 14),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                            child: siteVisitItem(
                                icon: Icons.edit_calendar_rounded,
                                title: 'DAFTAR',
                                onTap: () => _launchWhatsApp(
                                    siteVisitDateFormat(state.siteVisitState
                                        .data?.tanggalBerlangsung))),
                          ),
                          const SizedBox(width: 12),
                          Expanded(
                            child: siteVisitItem(
                              icon: Icons.location_on_outlined,
                              title: 'LIHAT LOKASI',
                              onTap: () => Navigator.of(context).pushNamed(
                                  MapsPage.routeName,
                                  arguments: state
                                              .siteVisitState.data?.latitude !=
                                          null
                                      ? LatLng(
                                          state.siteVisitState.data?.latitude ??
                                              0.0,
                                          state.siteVisitState.data
                                                  ?.longitude ??
                                              0.0,
                                        )
                                      : null),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
        );
      },
    );
  }

  void _launchWhatsApp(String date) async {
    final url =
        'https://wa.me/$gktPhone?text=${Uri.parse('$registerSiteVisitMessage $date')}';
    await launchUrl(Uri.parse(url), mode: LaunchMode.externalApplication);
  }

  Widget siteVisitItem({
    required IconData icon,
    required String title,
    VoidCallback? onTap,
  }) =>
      GestureDetector(
        onTap: onTap,
        child: Container(
          padding: const EdgeInsets.symmetric(
            horizontal: 14,
            vertical: 6,
          ),
          decoration: BoxDecoration(
              color: ThemeColors.goldStyle10,
              borderRadius: BorderRadius.circular(14),
              boxShadow: const [
                BoxShadow(
                  color: ThemeColors.gold,
                  offset: Offset(0.0, 1),
                  blurRadius: 1,
                )
              ]),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(
                icon,
                size: 28,
                color: ThemeColors.gold,
              ),
              const SizedBox(height: 4),
              Text(
                title,
                style: kTextStyleDescription.copyWith(
                  fontSize: 10,
                  color: Colors.black,
                ),
              )
            ],
          ),
        ),
      );

  Widget _siteVisitShimmer() => Shimmer(
        child: ShimmerLoading(
          child: Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 20,
            ),
            width: double.infinity,
            height: 200,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(24),
            ),
          ),
        ),
      );

  String siteVisitDateFormat(String? date) => date.formatDate(
        format: 'dd MMMM yyyy',
        oldFormat: 'yyyy-MM-dd',
      );
}

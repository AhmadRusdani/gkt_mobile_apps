import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:gkt/presentation/blocs/projects/projects_cubit.dart';
import 'package:gkt/presentation/config/theme_colors.dart';
import 'package:gkt/presentation/config/theme_texts.dart';
import 'package:gkt/presentation/utils/notched_paint.dart';
import 'package:gkt/presentation/widgets/base/button_widget.dart';

class BuildingProgressWidget extends StatefulWidget {
  const BuildingProgressWidget({super.key});

  @override
  State<BuildingProgressWidget> createState() => _BuildingProgressWidgetState();
}

class _BuildingProgressWidgetState extends State<BuildingProgressWidget>
    with TickerProviderStateMixin {
  AnimationController? _controller;
  Animation<double>? _anim;

  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 1),
    );
    animatedProgress(0.0);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _controller?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<ProjectsCubit>();
    return BlocConsumer<ProjectsCubit, ProjectsState>(
      listenWhen: (previous, current) =>
          (previous.buildingState.data?.totalPersentase ?? 0) !=
          (current.buildingState.data?.totalPersentase ?? 0),
      listener: (context, state) {
        final percentage = double.parse(
            (state.buildingState.data?.totalPersentase ?? 0).toString());

        animatedProgress(percentage);
      },
      builder: (context, state) {
        return Container(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          height: 140,
          child: Stack(
            children: [
              Positioned.fill(
                child: Align(
                  alignment: Alignment.center,
                  child: Image.asset(
                    'assets/images/img_building.png',
                    fit: BoxFit.fitWidth,
                    width: double.infinity,
                    height: 120,
                  ),
                ),
              ),
              Positioned.fill(
                child: Align(
                  alignment: Alignment.center,
                  child: AnimatedBuilder(
                      animation: _anim!,
                      builder: (context, child) {
                        return ClipPath(
                          clipper: CropClipperPath(
                            percentageCrop: _anim!.value,
                          ),
                          child: Image.asset('assets/images/img_building.png',
                              fit: BoxFit.fitWidth,
                              width: double.infinity,
                              height: 120,
                              color: Colors.white.withAlpha(200)),
                        );
                      }),
                ),
              ),
              AnimatedBuilder(
                  animation: _anim!,
                  builder: (context, child) {
                    return Positioned(
                      right: 0,
                      bottom: calculateHeight(
                        maxHeight: 140,
                        percentage: _anim!.value,
                        height: 42,
                      ),
                      child: Container(
                        height: 42,
                        width: 42,
                        decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.white,
                        ),
                        alignment: Alignment.center,
                        child: Text(
                          '${_anim!.value.round()}%',
                          style: kTextStyleTitleBold18.copyWith(
                            fontSize: 14,
                            color: ThemeColors.gold,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    );
                  }),
              if (state.buildingState.status.isSubmissionInProgress) ...[
                const Center(
                  child: CircularProgressIndicator(),
                )
              ],
              if (state.buildingState.data == null &&
                  state.buildingState.status.isSubmissionFailure) ...[
                Center(
                  child: PrimaryButton(
                    onPressed: () => bloc.getBuildingProcess(),
                    child: const Text('Coba Lagi'),
                  ),
                ),
              ],
            ],
          ),
        );
      },
    );
  }

  void animatedProgress(double percent) {
    setState(() {
      _anim = Tween<double>(
        begin: 0.0,
        end: percent,
      ).animate(_controller!);
      _controller?.forward(from: 0.0);
    });
  }

  double calculateHeight({
    required double maxHeight,
    required double percentage,
    required double height,
  }) {
    final actualPercentage = percentage > 100
        ? 100
        : percentage < 0
            ? 0
            : percentage;
    return (maxHeight - height) * (actualPercentage / 100);
  }

  double dynamicPercentageHeight({
    required double maxHeight,
    required double percentage,
  }) {
    final actualPercentage = percentage > 100
        ? 100
        : percentage < 0
            ? 0
            : percentage;
    return maxHeight * (actualPercentage / 100);
  }
}

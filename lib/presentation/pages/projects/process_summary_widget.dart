import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:gkt/domain/model/process_summary/process_summary_model.dart';
import 'package:gkt/presentation/blocs/projects/projects_cubit.dart';
import 'package:gkt/presentation/config/theme_colors.dart';
import 'package:gkt/presentation/config/theme_texts.dart';
import 'package:gkt/presentation/pages/building/building_process_page.dart';
import 'package:gkt/presentation/utils/extension.dart';
import 'package:gkt/presentation/widgets/base/base_data_widget.dart';
import 'package:gkt/presentation/widgets/base/empty_widget.dart';
import 'package:gkt/presentation/widgets/base/shimmer.dart';

import '../../widgets/base/notched_widget.dart';

part 'process_summary_item_widget.dart';

class ProcessSummaryWidget extends StatelessWidget {
  const ProcessSummaryWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<ProjectsCubit>();
    return BlocConsumer<ProjectsCubit, ProjectsState>(
      listenWhen: (previous, current) =>
          previous.processSummaryState.err != current.processSummaryState.err,
      listener: (context, state) {
        if (state.siteVisitState.err != null) {
          context.handleError(state.siteVisitState.err?.err);
        }
      },
      builder: (context, state) {
        if (state.processSummaryState.status.isSubmissionInProgress &&
            state.processSummaryState.data.isNullOrEmpty()) {
          return Shimmer(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: const [
                ProcessSummaryItemWidget.shimmer(),
                SizedBox(height: 14),
                ProcessSummaryItemWidget.shimmer(),
                SizedBox(height: 14),
                ProcessSummaryItemWidget.shimmer(),
              ],
            ),
          );
        }

        if (state.processSummaryState.data.isNullOrEmpty()) {
          return EmptyWidget.withImage(
            icons: Icons.home_work_outlined,
            title: 'Tidak ada Rangkuman Proses',
            msg: 'Tidak ada data yang ditampilkan',
            onTap: state.processSummaryState.status.isSubmissionFailure
                ? () => bloc.getSummaryProcess()
                : null,
          );
        }

        return ListView.builder(
          padding: EdgeInsets.zero,
          primary: false,
          shrinkWrap: true,
          itemCount: state.processSummaryState.data?.length ?? 0,
          itemBuilder: (context, index) {
            return ProcessSummaryItemWidget.data(
              data: state.processSummaryState.data![index],
              index: index,
              size: state.processSummaryState.data?.length ?? 0,
              onTap: () {
                Navigator.of(context).pushNamed(
                  BuildingProcessPage.routeName,
                  arguments: {
                    'title': state.processSummaryState.data![index].namaProses,
                    'id': state.processSummaryState.data![index].id,
                  },
                );
              },
            );
          },
        );
      },
    );
  }
}

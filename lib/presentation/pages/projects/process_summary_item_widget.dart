part of 'process_summary_widget.dart';

class ProcessSummaryItemWidget extends DataWidget<ProcessSummaryModel> {
  const ProcessSummaryItemWidget.shimmer()
      : index = 0,
        size = 0,
        onTap = null,
        super.shimmer();

  const ProcessSummaryItemWidget.data({
    required super.data,
    required this.index,
    required this.size,
    this.onTap,
  }) : super.data();

  final int index;
  final int size;
  final VoidCallback? onTap;

  int get percent => data?.persentase ?? 0;

  @override
  Widget buildData(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        index != 0
            ? Align(
                alignment: Alignment.centerLeft,
                child: SizedBox(
                  height: 24,
                  child: Container(
                    margin: const EdgeInsets.only(left: 49),
                    width: 4,
                    height: 50,
                    color: _stepColor(),
                  ),
                ),
              )
            : const SizedBox.shrink(),
        NotchedWidgetSide(
          notchedRadius: 18,
          sideRadius: 14,
          child: InkWell(
            onTap: onTap,
            child: Container(
              padding: const EdgeInsets.symmetric(
                horizontal: 34,
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(24),
                color: _colorBackground(),
              ),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Stack(
                    alignment: Alignment.center,
                    children: [
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Container(
                            width: 4,
                            height: 50,
                            color:
                                index == 0 ? Colors.transparent : _stepColor(),
                          ),
                          Container(
                            width: 4,
                            height: 50,
                            color: index == size - 1
                                ? Colors.transparent
                                : _stepColor(),
                          ),
                        ],
                      ),
                      _pointWidget()
                    ],
                  ),
                  const SizedBox(width: 12),
                  Expanded(
                    flex: 3,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Text(data?.namaProses ?? '',
                            style: kTextStyleDescriptionBold16.copyWith(
                              fontFamily: 'Inter',
                            )),
                        const SizedBox(height: 6),
                        Text(
                          _titleStatus(),
                          style: kTextStyleDescription12.copyWith(
                            fontFamily: 'Inter',
                            color: Colors.black,
                            fontSize: 12,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: 2,
                    height: 60,
                    color: percent > 0 && percent < 100
                        ? ThemeColors.greyPrimary
                        : Colors.white,
                  ),
                  Expanded(
                    flex: 1,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 14),
                      child: Text(
                        '$percent%',
                        style: kTextStyleDescriptionBold16.copyWith(
                          fontFamily: 'Inter',
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  @override
  ShimmerLoading buildShimmer(BuildContext context) {
    return ShimmerLoading(
      child: NotchedWidgetSide(
        notchedRadius: 18,
        sideRadius: 14,
        child: Container(
          padding: const EdgeInsets.symmetric(
            horizontal: 34,
          ),
          height: 120,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(24),
            color: Colors.black,
          ),
        ),
      ),
    );
  }

  Color _colorBackground() {
    if (percent <= 0) {
      return ThemeColors.greyPrimary;
    } else if (percent > 0 && percent < 100) {
      return Colors.white;
    } else {
      return ThemeColors.goldStyle8;
    }
  }

  Color _stepColor() {
    if (percent <= 0) {
      return Colors.white;
    } else if (percent > 0 && percent < 100) {
      return ThemeColors.goldStyle8;
    } else {
      return Colors.white;
    }
  }

  Widget _pointWidget() {
    if (percent <= 0) {
      return Container(
        margin: const EdgeInsets.all(7),
        width: 20,
        height: 20,
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.shade400,
                offset: const Offset(0.0, 1),
                blurRadius: 1,
              )
            ]),
      );
    } else if (percent > 0 && percent < 100) {
      return Container(
        margin: const EdgeInsets.all(7),
        width: 20,
        height: 20,
        decoration: const BoxDecoration(
            shape: BoxShape.circle,
            color: ThemeColors.goldStyle8,
            boxShadow: [
              BoxShadow(
                color: ThemeColors.goldStyle8,
                offset: Offset(0.0, 1),
                blurRadius: 1,
              )
            ]),
      );
    } else {
      return Container(
        width: 34,
        height: 34,
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.shade400,
                offset: const Offset(0.0, 1),
                blurRadius: 1,
              )
            ]),
        child: const Icon(
          Icons.check_rounded,
          color: ThemeColors.goldStyle7,
          size: 24,
        ),
      );
    }
  }

  String _titleStatus() {
    if (percent <= 0) {
      return 'Fase belum dimulai';
    } else if (percent > 0 && percent < 100) {
      return 'sedang berlangsung';
    } else {
      return 'Fase sudah selesai';
    }
  }
}

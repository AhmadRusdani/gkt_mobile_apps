import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:gkt/presentation/blocs/building/building_process_cubit.dart';
import 'package:gkt/presentation/pages/building/building_process_item_widget.dart';
import 'package:gkt/presentation/pages/dashboard/dashboard_page.dart';
import 'package:gkt/presentation/pages/gallery/gallery_page.dart';
import 'package:gkt/presentation/utils/extension.dart';
import 'package:gkt/presentation/widgets/base/appbar_widget.dart';
import 'package:gkt/presentation/widgets/base/background_widget.dart';
import 'package:gkt/presentation/widgets/base/empty_widget.dart';
import 'package:gkt/presentation/widgets/base/text_widget.dart';

import '../../widgets/base/shimmer.dart';

class BuildingProcessPage extends StatelessWidget {
  static const routeName = '${DashboardPage.routeName}/projects/step';
  const BuildingProcessPage({
    super.key,
    required this.floorTitle,
  });

  final String floorTitle;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BackgroundGradient(
        minHeight: context.screenHeight(),
        assetsAlign: Alignment.centerRight,
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: BlocConsumer<BuildingProcessCubit, BuildingProcessState>(
            listenWhen: (previous, current) => previous.err != current.err,
            listener: (context, state) {
              if (state.err != null) {
                context.handleError(state.err?.err);
              }
            },
            builder: (context, state) {
              return Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  appBarPrimaryBack(),
                  SizedBox(height: context.screenHeight() * 0.02),
                  sectionTitle(
                    title: floorTitle,
                    fontSize: 24,
                  ),
                  Expanded(
                    child: contentWidget(
                      context: context,
                      state: state,
                    ),
                  ),
                ],
              );
            }),
      ),
    );
  }

  Widget contentWidget({
    required BuildContext context,
    required BuildingProcessState state,
  }) {
    final bloc = context.read<BuildingProcessCubit>();
    if (state.status.isSubmissionInProgress && state.data.isNullOrEmpty()) {
      return Shimmer(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: const [
            BuildingProcessItemWidget.shimmer(),
            SizedBox(height: 14),
            BuildingProcessItemWidget.shimmer(),
            SizedBox(height: 14),
            BuildingProcessItemWidget.shimmer(),
          ],
        ),
      );
    }

    if (state.data.isNullOrEmpty()) {
      return EmptyWidget.withImage(
        icons: Icons.home_work_outlined,
        title: 'Tidak ada Proses Pembangunan',
        msg: 'Tidak ada data yang ditampilkan',
        onTap: state.status.isSubmissionFailure
            ? () => bloc.getBuildingProcess()
            : null,
      );
    }

    return ListView.separated(
      padding: const EdgeInsets.only(top: 14),
      shrinkWrap: true,
      itemBuilder: (context, index) {
        final item = state.data?[index];
        return BuildingProcessItemWidget.data(
          data: item!,
          onTap: () => Navigator.of(context).pushNamed(
            GalleryPage.routeName,
            arguments: {
              'title': '$floorTitle: ${item.nama}',
              'photos': item.photos,
            },
          ),
        );
      },
      separatorBuilder: (context, index) {
        return const SizedBox(
          height: 14,
        );
      },
      itemCount: state.data?.length ?? 0,
    );
  }
}

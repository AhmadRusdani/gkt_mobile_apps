import 'package:flutter/material.dart';
import 'package:gkt/domain/model/detail_process_summary/detail_process_summary_model.dart';
import 'package:gkt/presentation/config/theme_colors.dart';
import 'package:gkt/presentation/config/theme_texts.dart';
import 'package:gkt/presentation/widgets/base/base_data_widget.dart';
import 'package:gkt/presentation/widgets/base/shimmer.dart';

class BuildingProcessItemWidget
    extends DataWidget<DetailProcessSummaryItemModel> {
  const BuildingProcessItemWidget.data({required super.data, this.onTap})
      : super.data();
  const BuildingProcessItemWidget.shimmer()
      : onTap = null,
        super.shimmer();

  final VoidCallback? onTap;

  Widget progress({required int progress}) => SizedBox(
        width: double.infinity,
        height: 14,
        child: ClipRRect(
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          child: LinearProgressIndicator(
            value: progress / 100,
            valueColor: const AlwaysStoppedAnimation<Color>(ThemeColors.gold),
            backgroundColor: ThemeColors.greyStyle3,
          ),
        ),
      );

  @override
  Widget buildData(BuildContext context) {
    return ClipRRect(
      borderRadius: const BorderRadius.all(Radius.circular(18)),
      child: Container(
        color: Colors.white,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              margin: const EdgeInsets.fromLTRB(24, 24, 24, 8),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(data?.nama ?? '',
                      style: kTextStyleDescriptionBold16.copyWith(
                        fontFamily: 'Inter',
                      )),
                  const SizedBox(height: 6),
                  Text(
                    data?.fase ?? '',
                    style: kTextStyleDescription12.copyWith(
                      fontFamily: 'Inter',
                      color: Colors.black,
                      fontWeight: FontWeight.w600,
                      fontSize: 12,
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 14),
                    child: progress(progress: data?.persentase ?? 0),
                  ),
                ],
              ),
            ),
            InkWell(
              onTap: onTap,
              child: Container(
                padding: const EdgeInsets.symmetric(
                  vertical: 14,
                  horizontal: 34,
                ),
                color: ThemeColors.goldStyle10,
                alignment: Alignment.center,
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Text(
                        'GALLERY',
                        style: kTextStyleTitle16.copyWith(
                          color: ThemeColors.gold,
                          fontWeight: FontWeight.w600,
                        ),
                        textAlign: TextAlign.start,
                      ),
                    ),
                    const Icon(
                      Icons.chevron_right_rounded,
                      color: ThemeColors.gold,
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  ShimmerLoading buildShimmer(BuildContext context) {
    return ShimmerLoading(
      child: Container(
        height: 160,
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(18)),
          color: Colors.black,
        ),
      ),
    );
  }
}

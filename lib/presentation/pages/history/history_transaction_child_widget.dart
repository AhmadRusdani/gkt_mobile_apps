part of 'history_transaction_widget.dart';

enum HistoryChildType {
  monthly,
  all;
}

class HistoryTransactionChildWidget extends StatefulWidget {
  const HistoryTransactionChildWidget({super.key, required this.type});

  final HistoryChildType type;

  @override
  State<HistoryTransactionChildWidget> createState() =>
      _HistoryTransactionChildWidgetState();
}

class _HistoryTransactionChildWidgetState
    extends State<HistoryTransactionChildWidget> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      final bloc = context.read<HistoryTransactionCubit>();
      if (widget.type == HistoryChildType.monthly) {
        bloc.getMonthly();
      } else {
        bloc.getAll();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<HistoryTransactionCubit>();
    return BlocConsumer<HistoryTransactionCubit, HistoryTransactionState>(
      listenWhen: (previous, current) => widget.type == HistoryChildType.monthly
          ? previous.stateDataMonthly.err != current.stateDataMonthly.err
          : previous.stateDataAll.err != current.stateDataAll.err,
      listener: (context, state) {
        final err = widget.type == HistoryChildType.monthly
            ? state.stateDataMonthly.err
            : state.stateDataAll.err;
        if (err != null) {
          context.handleError(err.err);
        }
      },
      builder: (context, state) {
        final status = widget.type == HistoryChildType.monthly
            ? state.stateDataMonthly.status
            : state.stateDataAll.status;

        final data = widget.type == HistoryChildType.monthly
            ? state.stateDataMonthly.data
            : state.stateDataAll.data;

        if (status.isSubmissionInProgress && data.isNullOrEmpty()) {
          return Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: const [
              Shimmer(child: HistoryTransactionItemWidget.shimmer()),
            ],
          );
        }

        if (data.isNullOrEmpty()) {
          return EmptyWidget.withImage(
            icons: FluentIcons.money_dismiss_24_regular,
            title: 'History kosong',
            msg: 'Tidak ada data yang ditampilkan',
            onTap: status.isSubmissionFailure
                ? () {
                    widget.type == HistoryChildType.monthly
                        ? bloc.getMonthly()
                        : bloc.getAll();
                  }
                : null,
          );
        }

        return ListView.separated(
          padding: const EdgeInsets.symmetric(
            vertical: 0,
            horizontal: 14,
          ),
          shrinkWrap: true,
          itemBuilder: (context, index) {
            final item = data![index];
            return HistoryTransactionItemWidget.data(
                data: item,
                onTap: item.paymentLink != null && item.isPaid == false
                    ? () => Navigator.of(context).pushNamed(
                          WebViewPage.routeName,
                          arguments: {
                            'url': item.paymentLink,
                            'title': 'Persembahan',
                          },
                        )
                    : null);
          },
          separatorBuilder: (context, index) {
            return const Divider(
              color: ThemeColors.greyPrimary,
              height: 2,
            );
          },
          itemCount: data!.length,
        );
      },
    );
  }
}

import 'package:fluentui_system_icons/fluentui_system_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:formz/formz.dart';
import 'package:gkt/presentation/blocs/history/history_transaction_cubit.dart';
import 'package:gkt/presentation/config/theme_colors.dart';
import 'package:gkt/presentation/config/theme_texts.dart';
import 'package:gkt/presentation/pages/webview/webview_page.dart';
import 'package:gkt/presentation/utils/extension.dart';
import 'package:gkt/presentation/widgets/base/base_data_widget.dart';
import 'package:gkt/presentation/widgets/base/empty_widget.dart';
import 'package:gkt/presentation/widgets/base/segmented_control_widget.dart';
import 'package:gkt/presentation/widgets/base/shimmer.dart';

import '../../../domain/model/history/history_transaction_model.dart';

part 'history_transaction_item_widget.dart';
part 'history_transaction_child_widget.dart';

class HistoryTransactionWidget extends StatelessWidget {
  HistoryTransactionWidget({super.key});

  final pageController = PageController();

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<HistoryTransactionCubit>();
    return BlocBuilder<HistoryTransactionCubit, HistoryTransactionState>(
        builder: (context, state) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            padding: const EdgeInsets.symmetric(
              vertical: 6,
              horizontal: 14,
            ),
            color: ThemeColors.greyStyle2,
            child: CustomSegmentedControl(
                children: [
                  SegmentedItem(
                    title: 'Transaksi Bulan Ini',
                    isSelected: state.pageIndex == 0,
                  ),
                  SegmentedItem(
                    title: 'Seluruhnya',
                    isSelected: state.pageIndex == 1,
                  ),
                ],
                onValueChanged: (value) {
                  bloc.changeIndex(value);
                  pageController.jumpToPage(value);
                }),
          ),
          Expanded(
            child: PageView(
              controller: pageController,
              physics: const NeverScrollableScrollPhysics(),
              children: const [
                HistoryTransactionChildWidget(type: HistoryChildType.monthly),
                HistoryTransactionChildWidget(type: HistoryChildType.all),
              ],
            ),
          )
        ],
      );
    });
  }
}

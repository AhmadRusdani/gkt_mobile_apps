part of 'history_transaction_widget.dart';

class HistoryTransactionItemWidget
    extends DataWidget<HistoryTransactionModel> {
  const HistoryTransactionItemWidget.data({
    super.key,
    required super.data,
    required this.onTap,
  }) : super.data();

  const HistoryTransactionItemWidget.shimmer({super.key})
      : onTap = null,
        super.shimmer();

  final VoidCallback? onTap;

  @override
  Widget buildData(BuildContext context) {
    return ListTile(
      onTap: onTap,
      title: Text(data?.tanggal ?? '',
          style: kTextStyleDescription.copyWith(
            fontWeight: FontWeight.w500,
            fontSize: 14,
          )),
      subtitle: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Text((data?.jumlah ?? 0).toRp(),
              style: kTextStyleDescription.copyWith(
                fontSize: 14,
              )),
          const SizedBox(height: 8),
          Text(
              data?.paymentLink != null || data?.isPaid == false
                  ? 'Belum dibayar'
                  : 'Terbayar',
              style: kTextStyleDescription.copyWith(
                fontSize: 12,
                color: data?.paymentLink != null || data?.isPaid == false
                    ? Colors.red
                    : ThemeColors.greenStyle2,
              )),
        ],
      ),
      trailing: SvgPicture.asset('assets/icons/ic_receipt.svg'),
    );
  }

  @override
  ShimmerLoading buildShimmer(BuildContext context) {
    return ShimmerLoading(
      child: Column(
        children: List.generate(
          3,
          (index) => ListTile(
            title: const PlaceholderText(),
            subtitle: const PlaceholderText(width: 100),
            trailing: SvgPicture.asset('assets/icons/ic_receipt.svg'),
          ),
        ),
      ),
    );
  }
}

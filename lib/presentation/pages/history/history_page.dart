import 'package:flutter/material.dart';
import 'package:gkt/presentation/pages/dashboard/dashboard_page.dart';
import 'package:gkt/presentation/pages/history/history_transaction_widget.dart';
import 'package:gkt/presentation/utils/extension.dart';
import 'package:gkt/presentation/widgets/base/appbar_widget.dart';
import 'package:gkt/presentation/widgets/base/background_widget.dart';
import 'package:gkt/presentation/widgets/base/text_widget.dart';

class HistoryPage extends StatelessWidget {
  static const routeName = '${DashboardPage.routeName}/history';
  const HistoryPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: appBarPrimaryBack(),
      body: BackgroundGradient(
        minHeight: context.screenHeight(),
        assetsAlign: Alignment.centerRight,
        padding: const EdgeInsets.all(24),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            SizedBox(height: context.screenHeight() * 0.12),
            sectionTitle(
              title: 'History',
              fontSize: 24,
            ),
            const SizedBox(height: 32),
            Expanded(
              child: Container(
                padding: const EdgeInsets.symmetric(
                  vertical: 24,
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.white,
                ),
                child: HistoryTransactionWidget(),
              ),
            ),
            SizedBox(height: context.responsivePaddingBottom()),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gkt/presentation/blocs/auth/auth_cubit.dart';
import 'package:gkt/presentation/config/theme_texts.dart';

import '../../../domain/repository/auth_repository.dart';

class GreetingPage extends StatelessWidget {
  static const routeName = '/login/greeting';
  const GreetingPage({super.key, required this.name});

  final String name;

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<AuthCubit>();
    Future.delayed(
      const Duration(milliseconds: 1300),
      () {
        bloc.onAuthStatusChange(AuthenticationStatus.authenticated);
      },
    );
    return Scaffold(
      body: Container(
        margin: const EdgeInsets.symmetric(horizontal: 32),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text(
              'Selamat Datang,',
              style: kTextStyleTitleBold18,
            ),
            const SizedBox(height: 6),
            Text(
              name,
              style: kTextStyleTitleBold18,
            ),
          ],
        ),
      ),
      // floatingActionButton: Container(
      //     margin: const EdgeInsets.symmetric(horizontal: 44),
      //     alignment: Alignment.bottomLeft,
      //     child: FloatingActionButton.large(
      //       elevation: 2,
      //       backgroundColor: Colors.white,
      //       materialTapTargetSize: MaterialTapTargetSize.padded,
      //       onPressed: () {},
      //       child: Container(
      //         padding: const EdgeInsets.all(14),
      //         child: Image.asset(
      //           'assets/images/logo_tabernacle.png',
      //           fit: BoxFit.fitHeight,
      //         ),
      //       ),
      //     )),
    );
  }
}

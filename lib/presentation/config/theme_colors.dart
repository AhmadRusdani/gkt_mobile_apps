import 'package:flutter/material.dart';

class ThemeColors {
  ThemeColors._();

  static const MaterialColor gold = MaterialColor(
    _goldPrimaryValue,
    <int, Color>{
      50: Color(0xFFFEF7E2),
      100: Color(0xFFFCEBB6),
      200: Color(0xFFFADD86),
      300: Color(0xFFF8CF55),
      400: Color(0xFFF7C530),
      500: Color(_goldPrimaryValue),
      600: Color(0xFFF4B50A),
      700: Color(0xFFF2AC08),
      800: Color(0xFFF0A406),
      900: Color(0xFFEE9603),
    },
  );

  static const int _goldPrimaryValue = 0xFFF5BB0C;

  static const Color goldStyle10 = Color(0xFFFEF7E2);
  static const Color goldStyle9 = Color(0xFFFCEBB6);
  static const Color goldStyle8 = Color(0xFFFADD86);
  static const Color goldStyle7 = Color(0xFFF8CF55);
  static const Color goldStyle6 = Color(0xFFF7C530);
  static const Color goldStyle5 = Color(_goldPrimaryValue);
  static const Color goldStyle4 = Color(0xFFF4B50A);
  static const Color goldStyle3 = Color(0xFFF2AC08);
  static const Color goldStyle2 = Color(0xFFF0A406);
  static const Color goldStyle1 = Color(0xFFEE9603);

  static const Color goldStyle11 = Color(0xFFDCC46E);
  static const Color goldStyle12 = Color(0xFFF8D982);

  static const Color redPrimary = Color(0xFFF23939);
  static const Color redStyle1 = Color(0xFFEF2E2E);

  static const Color orangePrimary = Color(0xFFFD783F);
  static const Color orangeStyle1 = Color(0xFFF28739);

  static const Color background = Color(0xFFF9F9F9);

  static const Color blackPrimary = Color(0xFF323232);
  static const Color blackStyle1 = Color(0xFF130F26);

  static const Color orangeStyle2 = Color(0xFFFFFAF2);
  static const Color orangeStyle3 = Color(0xFFFFF4CC);
  static const Color orangeStyle4 = Color(0xFFFFE7DD);
  static const Color orangeStyle5 = Color(0xFFFFFCF8);
  static const Color yellowPrimary = Color(0xFFFFC120);

  static const Color greyPrimary = Color(0xFFBDBDBD);
  static const Color greyStyle1 = Color(0xFFD8CFCD);
  static const Color greyStyle2 = Color(0xFFEBE7E6);
  static const Color greyStyle3 = Color(0xFFF7F5F5);
  static const Color greyStyle4 = Color(0xFFF8F8F8);
  static const Color greyStyle5 = Color(0xFFD9D9D9);
  static const Color greyStyle6 = Color(0xFF979797);

  static const Color greenPrimary = Color(0xFF2CB853);
  static const Color greenStyle1 = Color(0xFFD3FFCC);
  static const Color greenStyle2 = Color(0xFF00C947);
  static const Color greenStyle3 = Color(0xFFEAFFF1);

  static const Color purpleStyle1 = Color(0xFF7FACF8);

  static const Color pinkPrimary = Color(0xFFFF51D9);

  static const Color darkBlue = Color(0xFF0F2552);

  static const Color textColor = Color(0xFF4F4F4F);

  static const Color backgroundModal = Color(0x99000000);

  MaterialColor createMaterialColor(Color color) {
    List strengths = <double>[.05];
    Map<int, Color> swatch = {};
    final int r = color.red, g = color.green, b = color.blue;

    for (int i = 1; i < 10; i++) {
      strengths.add(0.1 * i);
    }
    for (var strength in strengths) {
      final double ds = 0.5 - strength;
      swatch[(strength * 1000).round()] = Color.fromRGBO(
        r + ((ds < 0 ? r : (255 - r)) * ds).round(),
        g + ((ds < 0 ? g : (255 - g)) * ds).round(),
        b + ((ds < 0 ? b : (255 - b)) * ds).round(),
        1,
      );
    }
    return MaterialColor(color.value, swatch);
  }
}

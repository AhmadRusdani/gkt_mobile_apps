import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gkt/domain/model/detail_process_summary/detail_process_summary_model.dart';
import 'package:gkt/presentation/blocs/building/building_process_cubit.dart';
import 'package:gkt/presentation/blocs/dashboard/dashboard_cubit.dart';
import 'package:gkt/presentation/blocs/guess/guess_cubit.dart';
import 'package:gkt/presentation/blocs/history/history_transaction_cubit.dart';
import 'package:gkt/presentation/blocs/login/login_cubit.dart';
import 'package:gkt/presentation/blocs/maps/maps_cubit.dart';
import 'package:gkt/presentation/blocs/webview/webview_cubit.dart';
import 'package:gkt/presentation/pages/building/building_process_page.dart';
import 'package:gkt/presentation/pages/gallery/gallery_detail_page.dart';
import 'package:gkt/presentation/pages/gallery/gallery_page.dart';
import 'package:gkt/presentation/pages/greeting/greeting_page.dart';
import 'package:gkt/presentation/pages/history/history_page.dart';
import 'package:gkt/presentation/pages/home/home_guest_page.dart';
import 'package:gkt/presentation/pages/maps/maps_page.dart';
import 'package:gkt/presentation/pages/splash/splash_page.dart';
import 'package:gkt/presentation/pages/webview/webview_page.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../blocs/password/change_password_cubit.dart';
import '../pages/dashboard/dashboard_page.dart';
import '../pages/login/login_page.dart';
import '../pages/password/change_password_page.dart';

class AppRoute {
  AppRoute._();

  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    WidgetBuilder builder = (_) => const SplashPage();
    bool fullscreenDialog = false;

    switch (settings.name) {
      case SplashPage.routeName:
        builder = (_) => const SplashPage();
        break;
      case LoginPage.routeName:
        builder = (context) => BlocProvider(
              create: (context) => LoginCubit(
                authRepository: context.read(),
                userRepository: context.read(),
              ),
              child: const LoginPage(),
            );
        break;
      case GreetingPage.routeName:
        builder = (_) => GreetingPage(name: settings.arguments as String);
        break;
      case HomeGuestPage.routeName:
        builder = (_) => BlocProvider(
              create: (context) => GuessCubit(
                mainRepository: context.read(),
              ),
              child: const HomeGuestPage(),
            );
        break;
      case HistoryPage.routeName:
        builder = (_) => BlocProvider(
              create: (context) =>
                  HistoryTransactionCubit(mainRepository: context.read()),
              child: const HistoryPage(),
            );
        break;
      case BuildingProcessPage.routeName:
        final args = settings.arguments as Map<String, dynamic>;
        final title = args['title'] as String;
        final id = args['id'] as int;
        builder = (_) => BlocProvider(
              create: (context) => BuildingProcessCubit(
                mainRepository: context.read(),
                id: id,
              ),
              child: BuildingProcessPage(
                floorTitle: title,
              ),
            );
        break;
      case GalleryPage.routeName:
        final args = settings.arguments as Map<String, dynamic>;
        final photos = args['photos'] as List<PhotoProcessSummaryModel>?;
        final title = args['title'] as String?;
        builder = (_) => GalleryPage(
              photos: photos ?? [],
              phase: title,
            );
        break;
      case GalleryDetailPage.routeName:
        final args = settings.arguments as String;
        builder = (_) => GalleryDetailPage(
              photo: args,
            );
        break;
      case DashboardPage.routeName:
        builder = (_) => BlocProvider(
              create: (context) => DashboardCubit(),
              child: const DashboardPage(),
            );
        break;
      case WebViewPage.routeName:
        final args = settings.arguments as Map<String, String?>;
        final url = args['url'];
        final title = args['title'];
        builder = (_) => BlocProvider(
              create: (context) => WebViewCubit(
                title: title,
                url: url,
              ),
              child: const WebViewPage(),
            );
        break;
      case MapsPage.routeName:
        final args = settings.arguments as LatLng?;
        builder = (_) => BlocProvider(
              create: (context) => MapsCubit(mapsRepository: context.read()),
              child: MapsPage(latLng: args),
            );
        break;
      case ChangePasswordPage.routeName:
        builder = (_) => BlocProvider(
          create: (context) => ChangePasswordCubit(userRepository: context.read()),
          child: const ChangePasswordPage(),
        );
        break;
      default:
    }
    return MaterialPageRoute(
      builder: builder,
      fullscreenDialog: fullscreenDialog,
      maintainState: true,
      settings: settings,
    );
  }
}

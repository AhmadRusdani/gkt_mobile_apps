import 'package:flutter/material.dart';

const kTextStyleTitleBold20 = TextStyle(
  fontWeight: FontWeight.bold,
  fontSize: 20,
);
const kTextStyleTitleBold18 = TextStyle(
  fontWeight: FontWeight.bold,
  fontSize: 18,
);

const kTextStyleTitleSemiBold18 = TextStyle(
  fontWeight: FontWeight.w600,
  fontSize: 18,
);

const kTextStyleDescriptionBold16 = TextStyle(
  fontWeight: FontWeight.bold,
  fontSize: 16,
);
const kTextStyleTitle16 = TextStyle(
  fontWeight: FontWeight.normal,
  fontSize: 16,
);

const kTextStyleDescriptionBold = TextStyle(
  fontWeight: FontWeight.w700,
  fontSize: 14,
);
const kTextStyleDescription = TextStyle(
  fontWeight: FontWeight.normal,
  fontSize: 14,
);
const kTextStyleDescriptionLight = TextStyle(
  fontWeight: FontWeight.w300,
  fontSize: 14,
);

const kTextStyleDescriptionBold12 = TextStyle(
  fontWeight: FontWeight.bold,
  fontSize: 12,
);
const kTextStyleDescription12 = TextStyle(
  fontWeight: FontWeight.normal,
  fontSize: 12,
);
const kTextStyleDescription10 = TextStyle(
  fontWeight: FontWeight.normal,
  fontSize: 10,
);

const kTextStyleDescriptionItalicBold = TextStyle(
  fontWeight: FontWeight.w700,
  fontStyle: FontStyle.italic,
  fontSize: 14,
);
const kTextStyleDescriptionItalic = TextStyle(
  fontWeight: FontWeight.normal,
  fontStyle: FontStyle.italic,
  fontSize: 14,
);

Widget flightShuttleBuilder(
  BuildContext flightContext,
  Animation<double> animation,
  HeroFlightDirection flightDirection,
  BuildContext fromHeroContext,
  BuildContext toHeroContext,
) {
  return DefaultTextStyle(
    style: DefaultTextStyle.of(toHeroContext).style,
    child: toHeroContext.widget,
  );
}

import 'package:flutter/material.dart';
import 'package:gkt/presentation/widgets/base/shimmer.dart';
import '../../config/theme_colors.dart';

const _kShape = RoundedRectangleBorder(
  borderRadius: BorderRadius.all(
    Radius.circular(20),
  ),
);
const _kVisualDensity = VisualDensity(vertical: 2.0);

class WhiteButton extends ElevatedButton {
  WhiteButton({
    super.key,
    required super.onPressed,
    required super.child,
    OutlinedBorder? shape,
    VisualDensity? visualDensity,
    MaterialTapTargetSize? tapSize,
    Size? minSize,
    double? fontSize,
  }) : super(
          style: ElevatedButton.styleFrom(
            foregroundColor: ThemeColors.gold,
            tapTargetSize: tapSize,
            backgroundColor: Colors.white,
            shape: shape ?? _kShape,
            minimumSize: minSize,
            visualDensity: visualDensity ?? _kVisualDensity,
            textStyle: TextStyle(
              fontWeight: FontWeight.w600,
              fontFamily: 'Poppins',
              color: ThemeColors.goldStyle11,
              fontSize: fontSize,
            ),
          ),
        );
}

class RedOutlineButton extends OutlinedButton {
  RedOutlineButton({
    super.key,
    required super.onPressed,
    required super.child,
    VisualDensity visualDensity = _kVisualDensity,
    double? fontSize,
    OutlinedBorder? shape,
    Size? size,
    MaterialTapTargetSize? tapSize,
  }) : super(
          style: OutlinedButton.styleFrom(
            shape: shape ?? _kShape,
            minimumSize: size,
            side: BorderSide(
              color:
                  onPressed != null ? ThemeColors.gold : ThemeColors.greyStyle2,
            ),
            tapTargetSize: tapSize,
            visualDensity: visualDensity,
            textStyle: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: fontSize,
                fontFamily: 'averta'),
          ),
        );
}

class CustomOutlineButton extends OutlinedButton {
  CustomOutlineButton({
    super.key,
    required super.onPressed,
    required super.child,
    VisualDensity visualDensity = _kVisualDensity,
    double? fontSize,
    OutlinedBorder? shape,
    Size? size,
    MaterialTapTargetSize? tapSize,
    Color? color,
  }) : super(
          style: OutlinedButton.styleFrom(
            shape: shape ?? _kShape,
            minimumSize: size,
            side: BorderSide(
              color: color ?? ThemeColors.gold,
            ),
            tapTargetSize: tapSize,
            visualDensity: visualDensity,
            textStyle: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: fontSize,
                fontFamily: 'averta'),
          ),
        );
}

class PrimaryButton extends StatelessWidget {
  const PrimaryButton({
    Key? key,
    required this.onPressed,
    required this.child,
    this.busy = false,
    this.fontSize,
    this.visualDensity = _kVisualDensity,
    this.tapSize,
  }) : super(key: key);

  final VoidCallback? onPressed;
  final Widget child;
  final bool busy;
  final double? fontSize;
  final VisualDensity visualDensity;
  final MaterialTapTargetSize? tapSize;

  @override
  Widget build(BuildContext context) {
    final style = ElevatedButton.styleFrom(
      shape: _kShape,
      visualDensity: visualDensity,
      tapTargetSize: tapSize,
      textStyle: TextStyle(
        fontWeight: FontWeight.w600,
        fontSize: fontSize,
        color: Colors.white,
        fontFamily: 'Poppins',
      ),
    );

    if (busy) {
      return ElevatedButton.icon(
        style: style,
        onPressed: null,
        icon: const CircularProgressIndicator.adaptive(
          backgroundColor: Colors.white,
        ),
        label: child,
      );
    }

    return ElevatedButton(
      style: style,
      onPressed: onPressed,
      child: child,
    );
  }
}

class PrimaryButtonIcon extends PrimaryButton {
  PrimaryButtonIcon({
    super.key,
    required super.onPressed,
    required String label,
    required IconData icon,
  }) : super(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(label),
              const SizedBox(width: 4),
              Icon(icon),
            ],
          ),
        );
}

class CircleButton extends StatelessWidget {
  CircleButton.icon({
    Key? key,
    required IconData icon,
    this.iconColor,
    this.onTap,
  })  : child = Icon(icon, size: 28),
        super(key: key);

  CircleButton.asset({
    Key? key,
    required String image,
    this.iconColor,
    this.onTap,
  })  : child = Image.asset(image, height: 28),
        super(key: key);

  final Widget child;
  final Color? iconColor;
  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) {
    final button = CircleAvatar(
      backgroundColor: ThemeColors.greyStyle6.withOpacity(0.1),
      foregroundColor: iconColor,
      radius: 24,
      child: child,
    );

    if (onTap != null) {
      return GestureDetector(onTap: onTap, child: button);
    }

    return button;
  }

  static ShimmerLoading shimmer() {
    return const ShimmerLoading(
      child: CircleAvatar(
        radius: 24,
        backgroundColor: ThemeColors.greyPrimary,
      ),
    );
  }
}

class GradientButton extends StatelessWidget {
  const GradientButton({
    Key? key,
    required this.onPressed,
    required this.child,
    this.busy = false,
    this.fontSize,
    this.visualDensity = _kVisualDensity,
    this.size,
    this.hasElevation = true,
    this.radius,
  }) : super(key: key);

  final VoidCallback? onPressed;
  final Widget child;
  final bool busy;
  final double? fontSize;
  final VisualDensity visualDensity;
  final Size? size;
  final bool hasElevation;
  final BorderRadius? radius;

  @override
  Widget build(BuildContext context) {
    final styleButton = const ButtonStyle().copyWith(
      backgroundColor: MaterialStateProperty.all(Colors.transparent),
      foregroundColor: MaterialStateProperty.all(Colors.white),
      shadowColor: MaterialStateProperty.all(Colors.transparent),
      textStyle: MaterialStateProperty.all(
        TextStyle(
          fontFamily: 'Poppins',
          fontWeight: FontWeight.w600,
          fontSize: fontSize,
        ),
      ),
      visualDensity: _kVisualDensity,
      shape: MaterialStateProperty.all(_kShape),
      tapTargetSize: MaterialTapTargetSize.shrinkWrap,
    );

    return Container(
        width: size?.width,
        height: size?.height,
        decoration: BoxDecoration(
          boxShadow: hasElevation
              ? [
                  BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      offset: const Offset(0, 1),
                      blurRadius: 1,
                      spreadRadius: 1),
                ]
              : null,
          borderRadius: radius ??
              const BorderRadius.all(
                Radius.circular(20),
              ),
          color: onPressed == null ? ThemeColors.greyPrimary : null,
          gradient: onPressed != null
              ? const LinearGradient(
                  colors: [ThemeColors.goldStyle12, ThemeColors.goldStyle11],
                )
              : null,
        ),
        child: busy
            ? ElevatedButton.icon(
                style: styleButton,
                onPressed: null,
                icon: const CircularProgressIndicator.adaptive(
                  backgroundColor: Colors.white,
                ),
                label: child,
              )
            : ElevatedButton(
                style: styleButton,
                onPressed: onPressed,
                child: child,
              ));
  }
}

class GradientButtonIcon extends GradientButton {
  GradientButtonIcon({
    super.key,
    required super.onPressed,
    required String label,
    required IconData icon,
  }) : super(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(label),
              const SizedBox(width: 4),
              Icon(icon),
            ],
          ),
        );
}

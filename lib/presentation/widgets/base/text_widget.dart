import 'package:flutter/material.dart';
import 'package:gkt/domain/model/pray/pray_schedule_model.dart';
import 'package:gkt/presentation/utils/extension.dart';

import '../../config/theme_colors.dart';
import '../../config/theme_texts.dart';

class TextWithSideLines extends StatelessWidget {
  const TextWithSideLines({
    super.key,
    this.child,
  });

  final Widget? child;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(
          child: Stack(
            children: [
              Container(
                height: 20,
                width: 20,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: ThemeColors.gold,
                ),
              ),
              Positioned.fill(
                child: Align(
                  alignment: Alignment.center,
                  child: Container(
                    margin: const EdgeInsets.only(left: 14),
                    height: 3,
                    decoration: const BoxDecoration(
                      color: ThemeColors.gold,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        if (child != null) ...[child!],
        Expanded(
          child: Stack(
            children: [
              Positioned.fill(
                child: Align(
                  alignment: Alignment.center,
                  child: Container(
                    margin: const EdgeInsets.only(right: 14),
                    height: 3,
                    decoration: const BoxDecoration(
                      color: ThemeColors.gold,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.centerRight,
                child: Container(
                  height: 20,
                  width: 20,
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    color: ThemeColors.gold,
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class TextSchedule extends StatelessWidget {
  const TextSchedule({
    super.key,
    this.schedule,
    this.speaker,
  });

  final String? schedule;
  final String? speaker;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 2),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: context.screenWidth() * 0.32,
            padding: const EdgeInsets.all(12),
            child: Text(
              schedule ?? '21 Jun 2022 19:00',
              style: kTextStyleDescription,
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            color: ThemeColors.greyStyle2,
            width: 3,
            height: 54,
          ),
          Expanded(
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 12),
              child: Text(
                speaker ?? 'Pdt. Handoyo Santoso, D.D',
                style: kTextStyleDescription,
                textAlign: TextAlign.center,
              ),
            ),
          )
        ],
      ),
    );
  }
}

class TextScheduleData extends StatelessWidget {
  const TextScheduleData({
    super.key,
    required this.model,
  });

  final PrayDetail model;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 2),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: context.screenWidth() * 0.32,
            padding: const EdgeInsets.all(12),
            child: Text.rich(
              TextSpan(
                  text:
                      '${model.waktuIbadah?.formatDate(format: 'dd MMM yyyy') ?? ''}\n',
                  children: [
                    if (model.sesiIbadah != null) ...[
                      TextSpan(
                        text: '${model.sesiIbadah} ',
                        style: kTextStyleDescriptionBold,
                      ),
                      const TextSpan(
                        text: '| ',
                        style: kTextStyleDescription,
                      )
                    ],
                    TextSpan(
                      text:
                          model.waktuIbadah?.formatDate(format: 'HH:mm') ?? '',
                      style: kTextStyleDescription,
                    ),
                  ]),
              style: kTextStyleDescription,
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            color: ThemeColors.greyStyle2,
            width: 3,
            height: 54,
          ),
          Expanded(
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 12),
              child: Text(
                model.namaPendeta ?? '',
                style: kTextStyleDescription,
                textAlign: TextAlign.center,
              ),
            ),
          )
        ],
      ),
    );
  }
}

class TextFit extends StatelessWidget {
  const TextFit(
    this.text, {
    Key? key,
    this.style,
    this.align,
    this.gravity = Alignment.centerLeft,
    this.maxLines,
  }) : super(key: key);
  final String text;
  final TextStyle? style;
  final TextAlign? align;
  final Alignment gravity;
  final int? maxLines;

  @override
  Widget build(BuildContext context) {
    if (text.isEmpty) {
      return Text(
        text,
        style: style,
        textAlign: align,
      );
    }

    return FittedBox(
      alignment: gravity,
      fit: BoxFit.scaleDown,
      child: Text(
        text,
        style: style,
        textAlign: align,
        maxLines: maxLines,
      ),
    );
  }
}

Widget sectionTitle({
  required String title,
  double? fontSize,
  EdgeInsetsGeometry? padding,
}) =>
    Padding(
      padding: padding ??
          const EdgeInsets.symmetric(
            horizontal: 24,
            vertical: 14,
          ),
      child: Text(
        title,
        style: kTextStyleDescriptionBold16.copyWith(
          fontSize: fontSize ?? 18,
          fontWeight: FontWeight.w600,
        ),
      ),
    );

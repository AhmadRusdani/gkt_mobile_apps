import 'package:flutter/material.dart';
import 'package:gkt/presentation/config/theme_colors.dart';
import 'package:gkt/presentation/config/theme_texts.dart';

AppBar appbarPrimary({
  String? title,
  List<Widget>? actions,
}) =>
    AppBar(
      backgroundColor: Colors.transparent,
      centerTitle: true,
      title: Container(
        padding: const EdgeInsets.symmetric(vertical: 8),
        child: Text.rich(
          textAlign: TextAlign.center,
          TextSpan(
              text: 'GEREJA KEMAH TABERNACLE\n',
              style: kTextStyleDescription12.copyWith(
                color: ThemeColors.goldStyle8,
                fontSize: 14,
              ),
              children: [
                if (title != null) ...[
                  TextSpan(
                    text: title.toString(),
                    style: kTextStyleDescription12.copyWith(
                      color: ThemeColors.gold,
                      fontSize: 14,
                    ),
                  )
                ]
              ]),
        ),
      ),
      actions: actions,
    );

AppBar appBarPrimaryBack() => AppBar(
      iconTheme: const IconThemeData(
        color: ThemeColors.gold,
      ),
      backgroundColor: Colors.transparent,
    );

AppBar appBarPrimaryBlack() => AppBar(
      iconTheme: const IconThemeData(
        color: Colors.white,
      ),
      backgroundColor: const Color.fromARGB(255, 21, 20, 20),
    );

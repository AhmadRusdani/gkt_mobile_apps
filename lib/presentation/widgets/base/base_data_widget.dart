import 'package:flutter/material.dart';
import 'package:gkt/presentation/widgets/base/shimmer.dart';

abstract class DataWidget<T> extends StatelessWidget {
  const DataWidget.shimmer({Key? key})
      : _data = null,
        super(key: key);

  const DataWidget.data({
    Key? key,
    required T data,
  })  : _data = data,
        super(key: key);

  final T? _data;

  T? get data => _data;

  @override
  Widget build(BuildContext context) {
    final contact = _data;

    if (contact == null) {
      return buildShimmer(context);
    }

    return buildData(context);
  }

  ShimmerLoading buildShimmer(BuildContext context);

  Widget buildData(BuildContext context);
}

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../config/theme_colors.dart';
import '../../config/theme_texts.dart';

class FormTextLabel extends StatelessWidget {
  const FormTextLabel({
    Key? key,
    required this.label,
    this.mandatory = false,
    this.optional = false,
    this.fontColor = Colors.black,
  }) : super(key: key);

  final String label;
  final bool mandatory;
  final bool optional;
  final Color fontColor;

  @override
  Widget build(BuildContext context) {
    if (mandatory) {
      return Text.rich(
        style: kTextStyleDescription12,
        TextSpan(
          text: label,
          children: const [
            TextSpan(text: '*', style: TextStyle(color: Colors.red)),
          ],
        ),
      );
    } else if (optional) {
      return Text.rich(
        TextSpan(
          text: label,
          children: const [
            TextSpan(
              text: ' (Optional)',
              style: TextStyle(
                color: ThemeColors.greyPrimary,
              ),
            ),
          ],
        ),
      );
    }

    return Text(label,
        style: kTextStyleDescription12.copyWith(
          color: fontColor,
        ));
  }
}

class FormzTextField extends StatelessWidget {
  const FormzTextField({
    Key? key,
    this.hintText,
    this.leading,
    this.trailing,
    this.obscureText = false,
    this.readOnly = false,
    this.textEditingController,
    this.keyboardType,
    this.textInputAction,
    this.onChanged,
    this.onSubmitted,
    this.errorText,
    this.style,
    this.maxLines = 1,
    this.minLines,
    this.onTap,
    this.format,
    this.maxLength,
    this.focusNode,
    this.fillColor,
  }) : super(key: key);

  final String? hintText;
  final String? errorText;
  final Widget? leading;
  final Widget? trailing;
  final bool obscureText;
  final bool readOnly;
  final TextEditingController? textEditingController;
  final TextInputType? keyboardType;
  final TextInputAction? textInputAction;
  final ValueChanged<String>? onChanged;
  final ValueChanged<String>? onSubmitted;
  final TextStyle? style;
  final int? maxLines;
  final int? minLines;
  final VoidCallback? onTap;
  final List<TextInputFormatter>? format;
  final int? maxLength;
  final FocusNode? focusNode;
  final Color? fillColor;

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: textEditingController,
      obscureText: obscureText,
      readOnly: readOnly,
      decoration: InputDecoration(
        filled: fillColor != null,
        fillColor: fillColor,
        border: const OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.all(Radius.circular(12)),
        ),
        isDense: true,
        prefixIcon: leading,
        suffixIcon: trailing,
        hintText: hintText,
        hintStyle:
            kTextStyleDescription.copyWith(color: ThemeColors.greyPrimary),
        errorText: errorText,
      ),
      inputFormatters: format,
      keyboardType: keyboardType,
      textInputAction: textInputAction,
      onChanged: onChanged,
      onSubmitted: onSubmitted,
      style: style ?? kTextStyleDescription,
      maxLines: maxLines,
      minLines: minLines,
      maxLength: maxLength,
      focusNode: focusNode,
      onTap: onTap,
    );
  }
}

class FormTextField extends StatefulWidget {
  const FormTextField({
    Key? key,
    this.hint,
    this.leading,
    this.trailing,
    this.obscureText = false,
    this.clearText = false,
    this.textEditingController,
    this.keyboardType,
    this.textInputAction,
    this.onChanged,
    this.validator,
    this.onTap,
  }) : super(key: key);

  final String? hint;
  final Widget? leading;
  final Widget? trailing;
  final bool obscureText;
  final bool clearText;
  final TextEditingController? textEditingController;
  final TextInputType? keyboardType;
  final TextInputAction? textInputAction;
  final ValueChanged<String>? onChanged;
  final FormFieldValidator<String>? validator;
  final VoidCallback? onTap;

  @override
  State<FormTextField> createState() => _FormTextFieldState();
}

class _FormTextFieldState extends State<FormTextField> {
  late TextEditingController textEditingController;
  late bool obscureText;

  bool showClearText = false;

  @override
  void initState() {
    textEditingController =
        widget.textEditingController ?? TextEditingController();
    showClearText = textEditingController.text.isNotEmpty;
    obscureText = widget.obscureText;

    super.initState();
  }

  @override
  void didUpdateWidget(covariant FormTextField oldWidget) {
    super.didUpdateWidget(oldWidget);

    if (oldWidget.hint != widget.hint ||
        oldWidget.leading != widget.leading ||
        oldWidget.trailing != widget.trailing ||
        oldWidget.obscureText != widget.obscureText ||
        oldWidget.textEditingController != widget.textEditingController ||
        oldWidget.keyboardType != widget.keyboardType ||
        oldWidget.textInputAction != widget.textInputAction) {
      textEditingController =
          widget.textEditingController ?? TextEditingController();
      obscureText = widget.obscureText;
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget? trailing = widget.trailing;

    if (showClearText && widget.clearText && trailing == null) {
      trailing = GestureDetector(
        onTap: () {
          setState(() {
            showClearText = false;
            textEditingController.text = '';

            if (widget.onChanged != null) {
              widget.onChanged!('');
            }
          });
        },
        child: const Icon(Icons.cancel),
      );
    }

    return TextFormField(
      controller: textEditingController,
      obscureText: obscureText,
      decoration: InputDecoration(
        border: const OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(12)),
        ),
        isDense: true,
        hintText: widget.hint,
        prefixIcon: widget.leading,
        suffixIcon: trailing,
      ),
      keyboardType: widget.keyboardType,
      textInputAction: widget.textInputAction,
      onChanged: (val) {
        if (widget.onChanged != null) {
          widget.onChanged!(val);
        }

        setState(() {
          showClearText = val.isNotEmpty;
        });
      },
      validator: widget.validator,
      style: kTextStyleDescription,
      onTap: widget.onTap,
    );
  }
}

class SearchFieldBar extends StatelessWidget {
  const SearchFieldBar({
    Key? key,
    required this.hintText,
    this.controller,
    this.onSubmitted,
    this.trailing,
    this.margin = EdgeInsets.zero,
    this.onChanged,
  }) : super(key: key);

  final String hintText;
  final TextEditingController? controller;
  final ValueChanged<String>? onSubmitted;
  final ValueChanged<String>? onChanged;
  final Widget? trailing;
  final EdgeInsetsGeometry margin;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      child: TextField(
        onChanged: onChanged,
        controller: controller,
        decoration: InputDecoration(
          border: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(12)),
            borderSide: BorderSide.none,
          ),
          prefixIcon: const Icon(
            Icons.search_rounded,
            size: 18,
            color: Colors.black,
          ),
          prefixIconConstraints: const BoxConstraints(minWidth: 48),
          hintText: hintText,
          hintStyle: const TextStyle(height: 1.5),
          suffixIcon: trailing,
          suffixIconConstraints: const BoxConstraints(minWidth: 48),
          filled: true,
          fillColor: ThemeColors.greyStyle3,
          isCollapsed: true,
          contentPadding: const EdgeInsets.fromLTRB(12, 8, 12, 8),
        ),
        textInputAction: TextInputAction.search,
        onSubmitted: onSubmitted,
      ),
    );
  }
}

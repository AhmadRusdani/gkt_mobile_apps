import 'package:flutter/material.dart';
import 'package:flutter_progress_hud/flutter_progress_hud.dart';

import '../../config/theme_colors.dart';
import '../../config/theme_texts.dart';

class HudWidget extends StatelessWidget {
  const HudWidget({super.key, required this.child});
  final Widget child;
  @override
  Widget build(BuildContext context) {
    return ProgressHUD(
      indicatorColor: ThemeColors.goldStyle1,
      padding: const EdgeInsets.all(24),
      barrierEnabled: true,
      textStyle: kTextStyleDescription12.copyWith(
        color: Colors.white,
        fontSize: 14,
      ),
      child: child,
    );
  }
}

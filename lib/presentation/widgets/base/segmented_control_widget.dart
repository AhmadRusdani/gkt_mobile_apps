import 'package:custom_sliding_segmented_control/custom_sliding_segmented_control.dart';
import 'package:flutter/material.dart';
import 'package:gkt/presentation/config/theme_colors.dart';

import '../../config/theme_texts.dart';

class SegmentedItem {
  final String title;
  final bool isSelected;
  const SegmentedItem({
    required this.title,
    this.isSelected = false,
  });
}

class CustomSegmentedControl extends CustomSlidingSegmentedControl<int> {
  CustomSegmentedControl({
    super.key,
    required List<SegmentedItem> children,
    required super.onValueChanged,
    super.initialValue,
    super.fixedWidth,
  }) : super(
          children: <Widget>[
            for (int i = 0; i < children.length; i++) ...{
              Builder(builder: (context) {
                final title = children[i].title;
                final isSelected = children[i].isSelected;
                final color =
                    isSelected ? Colors.white : ThemeColors.greyPrimary;

                final label = Text(
                  title,
                  style: kTextStyleDescriptionBold16.copyWith(
                    fontWeight: FontWeight.w600,
                    color: color,
                    fontSize: 14,
                  ),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.center,
                );

                return label;
              })
            },
          ].asMap(),
          thumbDecoration: BoxDecoration(
            color: ThemeColors.goldStyle6,
            borderRadius: BorderRadius.circular(36),
            boxShadow: [
              BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  offset: const Offset(0, 1),
                  blurRadius: 1,
                  spreadRadius: 1),
            ],
          ),
          curve: Curves.easeInToLinear,
          innerPadding: const EdgeInsets.all(6),
          isStretch: true,
        );
}

import 'package:flutter/material.dart';

import '../../config/theme_colors.dart';

class BackgroundPrimary extends Container {
  BackgroundPrimary({super.key, required Widget child})
      : super(
          decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/images/logo_background.png'),
                fit: BoxFit.fitWidth,
                alignment: Alignment.bottomLeft,
                opacity: 0.7),
          ),
          child: child,
        );
}

class BackgroundGradient extends Container {
  BackgroundGradient({
    super.key,
    AlignmentGeometry begin = Alignment.topCenter,
    AlignmentGeometry end = Alignment.bottomCenter,
    List<Color> colors = const [
      ThemeColors.background,
      Color.fromARGB(255, 220, 201, 150),
    ],
    required Widget child,
    super.margin,
    super.padding,
    super.height,
    BoxShape shape = BoxShape.rectangle,
    BorderRadiusGeometry? borderRadius,
    String? assets,
    Alignment? assetsAlign,
    double? minHeight,
  }) : super(
          constraints: BoxConstraints(minHeight: minHeight ?? 0),
          decoration: BoxDecoration(
            shape: shape,
            gradient: LinearGradient(
              colors: colors,
              begin: begin,
              end: end,
            ),
            image: DecorationImage(
                image: AssetImage(
                    assets ?? 'assets/images/logo_background_reverse.png'),
                fit: BoxFit.fitWidth,
                alignment: assetsAlign ?? Alignment.bottomRight,
                opacity: 0.7),
            borderRadius: borderRadius,
          ),
          child: child,
        );
}

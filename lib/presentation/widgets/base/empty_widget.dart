import 'package:flutter/material.dart';
import 'package:gkt/presentation/config/theme_texts.dart';
import 'package:gkt/presentation/utils/extension.dart';
import 'package:gkt/presentation/widgets/base/button_widget.dart';

class EmptyWidget extends StatelessWidget {
  const EmptyWidget.withText({
    Key? key,
    required String msg,
    EdgeInsetsGeometry? margin,
  })  : _onTap = null,
        _msg = msg,
        _title = null,
        _margin = margin,
        _icons = null,
        super(key: key);

  const EmptyWidget.withImage({
    Key? key,
    required IconData icons,
    required String title,
    required String msg,
    EdgeInsetsGeometry? margin,
    VoidCallback? onTap,
  })  : _onTap = onTap,
        _msg = msg,
        _title = title,
        _margin = margin,
        _icons = icons,
        super(key: key);

  final String? _title;
  final String _msg;
  final IconData? _icons;
  final VoidCallback? _onTap;
  final EdgeInsetsGeometry? _margin;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        margin: _margin ?? const EdgeInsets.all(28),
        child: _title == null
            ? _buildSubtitle()
            : Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Icon(_icons, size: 60),
                  const SizedBox(height: 24),
                  _buildTitle(),
                  const SizedBox(height: 8),
                  _buildSubtitle(),
                  if (_onTap != null) ...[
                    Container(
                      margin: const EdgeInsets.only(top: 24),
                      width: context.screenWidth() * 0.3,
                      child: PrimaryButton(
                        onPressed: _onTap,
                        child: const Text('Coba Lagi'),
                      ),
                    )
                  ],
                ],
              ),
      ),
    );
  }

  Widget _buildTitle() {
    return Text(
      _title ?? '',
      style: kTextStyleDescriptionBold16,
      textAlign: TextAlign.center,
    );
  }

  Widget _buildSubtitle() {
    return Text(
      _msg,
      style: kTextStyleDescriptionLight,
      textAlign: TextAlign.center,
    );
  }
}

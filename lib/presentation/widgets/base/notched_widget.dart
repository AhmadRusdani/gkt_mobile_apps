import 'package:flutter/material.dart';
import 'package:gkt/presentation/utils/extension.dart';

import '../../utils/notched_paint.dart';

class NotchedWidget extends StatelessWidget {
  const NotchedWidget({
    super.key,
    required this.child,
    this.sideRadius,
    this.notchedRadius,
  });

  final Widget child;
  final double? sideRadius;
  final double? notchedRadius;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(sideRadius ?? 20),
      child: ClipPath(
        clipper: DolDurmaClipper(
          holeRadius: notchedRadius ?? 38,
          right: 0,
        ),
        child: child,
      ),
    );
  }
}

class NotchedWidgetSide extends StatelessWidget {
  const NotchedWidgetSide({
    super.key,
    required this.child,
    this.sideRadius,
    this.notchedRadius,
  });

  final Widget child;
  final double? sideRadius;
  final double? notchedRadius;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(sideRadius ?? 20),
      child: ClipPath(
        clipper: TicketClipper(radius: notchedRadius),
        child: child,
      ),
    );
  }
}

class NotchedBottomWidget extends StatelessWidget {
  const NotchedBottomWidget({super.key, required this.child});

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 24),
      child: NotchedWidget(
        child: Container(
          decoration: const BoxDecoration(
            color: Colors.white,
          ),
          height: 450,
          width: context.screenWidth(),
          padding: const EdgeInsets.fromLTRB(18, 14, 18, 42),
          child: child,
        ),
      ),
    );
  }
}

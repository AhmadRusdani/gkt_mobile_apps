import 'dart:io';

import 'package:flutter/material.dart';

class CircleAvatars extends StatelessWidget {
  const CircleAvatars({
    Key? key,
    this.image,
    this.radius = 28,
    this.fit,
    this.heroTag,
    this.color,
    this.useElevation = false,
  }) : super(key: key);

  final String? image;
  final double radius;
  final BoxFit? fit;
  final Object? heroTag;
  final Color? color;
  final bool useElevation;

  @override
  Widget build(BuildContext context) {
    final container = Container(
      width: radius * 2,
      height: radius * 2,
      decoration: BoxDecoration(
        color: color,
        shape: BoxShape.circle,
        image: image != null
            ? DecorationImage(
                image: _buildImage(context),
                fit: fit,
              )
            : null,
        boxShadow: useElevation
            ? [
                BoxShadow(
                  color: Colors.grey.shade200,
                  offset: const Offset(0.0, 5),
                  blurRadius: 5,
                )
              ]
            : [],
      ),
    );

    if (heroTag != null) {
      return Hero(tag: heroTag!, child: container);
    }

    return container;
  }

  ImageProvider _buildImage(BuildContext context) {
    final isNetwork = image?.startsWith('http');
    final isAsset = image?.startsWith('assets/');

    if (isNetwork!) {
      return NetworkImage(image!);
    }

    if (!isAsset!) {
      return FileImage(File(image!));
    }

    return AssetImage(image!);
  }
}

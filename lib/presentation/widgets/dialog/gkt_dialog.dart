import 'package:flutter/material.dart';
import 'package:gkt/presentation/config/theme_colors.dart';
import 'package:gkt/presentation/config/theme_texts.dart';
import 'package:gkt/presentation/utils/extension.dart';
import 'package:gkt/presentation/widgets/base/button_widget.dart';
import 'package:gkt/presentation/widgets/base/text_field_widget.dart';

const kModalBottomShape = RoundedRectangleBorder(
  borderRadius: BorderRadius.only(
    topLeft: Radius.circular(20),
    topRight: Radius.circular(20),
  ),
);

Future<T> showGktBottomSheet<T>({
  required BuildContext context,
  required LayoutWidgetBuilder builder,
  ShapeBorder? shape = kModalBottomShape,
  bool isScrollControlled = false,
  bool isDismissible = true,
  Color? backgroundColor,
  String? title,
  String? subtitle,
}) async {
  return await showModalBottomSheet(
    context: context,
    shape: shape,
    isScrollControlled: isScrollControlled,
    isDismissible: isDismissible,
    backgroundColor: backgroundColor ?? Colors.white,
    barrierColor: ThemeColors.backgroundModal,
    builder: (BuildContext bc) {
      if (title != null) {
        return LayoutBuilder(builder: (context, constraints) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(height: 8),
              ListTile(
                contentPadding: const EdgeInsets.symmetric(horizontal: 20),
                trailing: InkWell(
                  child: const Icon(Icons.close),
                  onTap: () => Navigator.pop(context),
                ),
                minLeadingWidth: 0,
                title: Text(title, style: kTextStyleTitle16),
              ),
              if (subtitle != null)
                Padding(
                  padding: const EdgeInsets.fromLTRB(20, 0, 20, 16),
                  child: Text(subtitle, style: kTextStyleDescription12),
                ),
              builder(context, constraints),
            ],
          );
        });
      }

      return LayoutBuilder(builder: builder);
    },
  );
}

Future<String?> showChangeUserDialog(
  BuildContext context, {
  bool isPhone = true,
}) async {
  return await showGktBottomSheet(
    context: context,
    builder: (context, constraints) {
      return _ChangeUserBodyDialog(
        isPhone: isPhone,
      );
    },
  );
}

class _ChangeUserBodyDialog extends StatefulWidget {
  const _ChangeUserBodyDialog({Key? key, this.isPhone = true})
      : super(key: key);
  final bool isPhone;

  @override
  State<_ChangeUserBodyDialog> createState() => _ChangeUserBodyDialogState();
}

class _ChangeUserBodyDialogState extends State<_ChangeUserBodyDialog> {
  String result = '';

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ListTile(
            contentPadding: EdgeInsets.zero,
            trailing: InkWell(
              child: const Icon(
                Icons.cancel,
                color: Colors.black,
              ),
              onTap: () {
                Navigator.pop(context, null);
              },
            ),
            minLeadingWidth: 0,
            title: Text(
              'Ubah ${widget.isPhone ? 'Nomor Telepon' : 'Alamat'}',
              style: kTextStyleTitleBold18,
            ),
          ),
          const SizedBox(height: 8),
          widget.isPhone
              ? FormzTextField(
                  hintText: 'Masukan Nomor Telepon Anda',
                  textInputAction: TextInputAction.done,
                  keyboardType: TextInputType.number,
                  maxLines: 1,
                  fillColor: Colors.grey.shade100,
                  errorText: result.isEmpty
                      ? '⚠️ Silakan isi Nomor Telepon'
                      : result.isNotEmpty && result.length < 7
                          ? '⚠️ Nomor Telepon terlalu pendek'
                          : null,
                  onChanged: (value) => setState(() {
                    result = value;
                  }),
                )
              : FormzTextField(
                  hintText: 'Masukan Alamat Lengkap Anda',
                  textInputAction: TextInputAction.done,
                  keyboardType: TextInputType.text,
                  maxLines: 4,
                  fillColor: Colors.grey.shade100,
                  onChanged: (value) => setState(() {
                    result = value;
                  }),
                ),
          const SizedBox(height: 44),
          PrimaryButton(
              onPressed: result.isNullOrEmpty()
                  ? null
                  : () {
                      Navigator.pop(context, result);
                    },
              child: const Text('Selesai'))
        ],
      ),
    );
  }
}

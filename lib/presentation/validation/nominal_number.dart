import 'package:flutter/material.dart';
import 'package:formz/formz.dart';
import 'package:gkt/presentation/utils/extension.dart';

enum NominalValidationError { empty, notValid, notReachMinimum }

class NominalNumber extends FormzInput<String, NominalValidationError>
    with FormzNumberMixin {
  const NominalNumber.pure()
      : minValue = null,
        maxValue = null,
        super.pure('');

  const NominalNumber.dirty({this.minValue, this.maxValue, String value = ''})
      : super.dirty(value);

  NominalNumber copyWith({
    String? value,
    int? minValue,
    int? maxValue,
  }) {
    return NominalNumber.dirty(
      value: value ?? this.value,
      minValue: minValue ?? this.minValue,
      maxValue: maxValue ?? this.maxValue,
    );
  }

  final int? minValue;
  final int? maxValue;

  @override
  NominalValidationError? validator(String? value) {
    if (value?.isNotEmpty == true) {
      if ((value ?? '').startsWith('0')) {
        return NominalValidationError.notValid;
      }

      try {
        if (minValue != null) {
          if (int.parse((sanitizedValue)) < minValue!) {
            return NominalValidationError.notReachMinimum;
          }
        }

        if (maxValue != null) {
          if (int.parse((sanitizedValue)) >= maxValue!) {
            return NominalValidationError.notReachMinimum;
          }
        }

        return null;
      } catch (e) {
        return NominalValidationError.notValid;
      }
    }

    return NominalValidationError.empty;
  }

  String get sanitizedValue => value.sanitizeAccountBank();
}

mixin FormzNumberMixin on FormzInput<String, NominalValidationError> {
  TextEditingController get textEditingController {
    final textEditingController = TextEditingController();
    textEditingController.text = value;
    textEditingController.selection =
        TextSelection.collapsed(offset: value.length);
    return textEditingController;
  }
}

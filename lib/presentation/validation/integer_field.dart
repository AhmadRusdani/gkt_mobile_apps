import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:formz/formz.dart';

enum IntegerFieldValidationError { empty, invalid }

class IntegerField extends FormzInput<String, IntegerFieldValidationError>
    with FormzIntegerInputMixin {
  const IntegerField.pure() : super.pure('');

  const IntegerField.dirty([String value = '']) : super.dirty(value);

  IntegerField toDirty() => IntegerField.dirty(value);

  @override
  IntegerFieldValidationError? validator(String? value) {
    if (value != null && value.isNotEmpty) {
      if (int.tryParse(value) == null) {
        return IntegerFieldValidationError.invalid;
      }
      return null;
    }
    return IntegerFieldValidationError.empty;
  }
}

class IntegerFieldOptional
    extends FormzInput<String, IntegerFieldValidationError>
    with FormzIntegerInputMixin {
  const IntegerFieldOptional.pure() : super.pure('');

  const IntegerFieldOptional.dirty([String value = '']) : super.dirty(value);

  IntegerFieldOptional toDirty() => IntegerFieldOptional.dirty(value);

  @override
  IntegerFieldValidationError? validator(String? value) {
    if (value != null && value.isNotEmpty) {
      if (int.tryParse(value) == null) {
        return IntegerFieldValidationError.invalid;
      }
      return null;
    }
    return null;
  }
}

mixin FormzIntegerInputMixin
    on FormzInput<String, IntegerFieldValidationError> {
  TextEditingController textEditingController([TextInputFormatter? formatter]) {
    final textEditingController = TextEditingController();
    if (formatter != null) {
      final editingValue = formatter.formatEditUpdate(
        const TextEditingValue(),
        TextEditingValue(
          text: value,
          selection: TextSelection.collapsed(offset: value.length),
        ),
      );
      textEditingController.text = editingValue.text;
      textEditingController.selection = editingValue.selection;
    } else {
      textEditingController.text = value;
      textEditingController.selection =
          TextSelection.collapsed(offset: value.length);
    }
    return textEditingController;
  }

  int? get number => int.tryParse(value);
}

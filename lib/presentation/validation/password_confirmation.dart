import 'package:formz/formz.dart';

enum PasswordConfirmationValidationError { empty, invalid, short }

class PasswordConfirmation
    extends FormzInput<String, PasswordConfirmationValidationError> {
  const PasswordConfirmation.pure()
      : password = '',
        super.pure('');

  const PasswordConfirmation.dirty({String value = '', this.password = ''})
      : super.dirty(value);

  PasswordConfirmation copyWith({String? value, String? password}) {
    return PasswordConfirmation.dirty(
      value: value ?? this.value,
      password: password ?? this.password,
    );
  }

  final String password;

  @override
  PasswordConfirmationValidationError? validator(String? value) {
    if (value == null || value.isEmpty) {
      return PasswordConfirmationValidationError.empty;
    }
    if (value != password) {
      return PasswordConfirmationValidationError.invalid;
    }
    return null;
  }

  String? errorPasswordText() {
    if (value.isEmpty) {
      return null;
    }
    final validator = this.validator(value);
    if (validator != null) {
      switch (validator) {
        case PasswordConfirmationValidationError.empty:
          return "Password konfirmasi tidak boleh kosong!";
        case PasswordConfirmationValidationError.short:
          return "Password konfirmasi terlalu pendek!";
        case PasswordConfirmationValidationError.invalid:
          return "Password konfirmasi tidak sama!";
        default:
          null;
      }
    }
    return null;
  }
}

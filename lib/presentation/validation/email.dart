import 'package:formz/formz.dart';

enum EmailValidationError { empty }

class EmailField extends FormzInput<String, EmailValidationError> {
  const EmailField.pure() : super.pure('');

  const EmailField.dirty([String value = '']) : super.dirty(value);

  @override
  EmailValidationError? validator(String? value) {
    return value?.isNotEmpty == true ? null : EmailValidationError.empty;
  }
}

class EmailOptional extends FormzInput<String, EmailValidationError> {
  const EmailOptional.pure() : super.pure('');

  const EmailOptional.dirty([String value = '']) : super.dirty(value);

  @override
  EmailValidationError? validator(String? value) {
    return null;
  }
}

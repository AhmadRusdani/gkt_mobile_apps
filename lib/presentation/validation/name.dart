import 'package:formz/formz.dart';

enum NameValidationError { empty }

class NameField extends FormzInput<String, NameValidationError> {
  const NameField.pure() : super.pure('');

  const NameField.dirty([String value = '']) : super.dirty(value);

  @override
  NameValidationError? validator(String? value) {
    return value?.isNotEmpty == true ? null : NameValidationError.empty;
  }
}

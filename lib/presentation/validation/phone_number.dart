import 'package:formz/formz.dart';
import 'package:gkt/presentation/utils/extension.dart';

enum PhoneValidationError { empty }

class PhoneNumber extends FormzInput<String, PhoneValidationError> {
  const PhoneNumber.pure() : super.pure('');

  const PhoneNumber.dirty([String value = '']) : super.dirty(value);

  @override
  PhoneValidationError? validator(String? value) {
    return value?.isNotEmpty == true ? null : PhoneValidationError.empty;
  }

  String get sanitizedValue => value.sanitizePhone();
}

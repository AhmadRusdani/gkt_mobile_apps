import 'package:flutter/widgets.dart';
import 'package:formz/formz.dart';

enum StringFieldValidationError { empty }

class StringField extends FormzInput<String, StringFieldValidationError>
    with FormzInputMixin {
  const StringField.pure() : super.pure('');

  const StringField.dirty([String value = '']) : super.dirty(value);

  StringField toDirty() => StringField.dirty(value);

  @override
  StringFieldValidationError? validator(String? value) {
    return value?.isNotEmpty == true ? null : StringFieldValidationError.empty;
  }
}

class StringFieldOptional extends FormzInput<String, StringFieldValidationError>
    with FormzInputMixin {
  const StringFieldOptional.pure() : super.pure('');

  const StringFieldOptional.dirty([String value = '']) : super.dirty(value);

  StringFieldOptional toDirty() => StringFieldOptional.dirty(value);

  @override
  StringFieldValidationError? validator(String? value) {
    return null;
  }
}

mixin FormzInputMixin on FormzInput<String, StringFieldValidationError> {
  TextEditingController get textEditingController {
    final textEditingController = TextEditingController();
    textEditingController.text = value;
    textEditingController.selection =
        TextSelection.collapsed(offset: value.length);
    return textEditingController;
  }
}

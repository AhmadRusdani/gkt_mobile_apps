import 'package:formz/formz.dart';

enum PasswordValidationError { empty, short }

class Password extends FormzInput<String, PasswordValidationError> {
  const Password.pure() : super.pure('');

  const Password.dirty([String value = '']) : super.dirty(value);

  @override
  PasswordValidationError? validator(String? value) {
    return value?.isNotEmpty == true
        ? (value?.length ?? 0) >= 6
            ? null
            : PasswordValidationError.short
        : PasswordValidationError.empty;
  }

  String? errorPasswordText() {
    if (value.isEmpty) {
      return null;
    }
    final validator = this.validator(value);
    if (validator != null) {
      return validator == PasswordValidationError.empty
          ? 'Password tidak boleh kosong!'
          : 'Password terlalu pendek';
    }
    return null;
  }
}

import 'package:dio/dio.dart';
import 'package:dio_flutter_transformer/dio_flutter_transformer.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:gkt/data/data_source/local/storage_service.dart';
import 'package:gkt/data/data_source/remote/api_service.dart';
import 'package:gkt/data/repository/auth_repository_impl.dart';
import 'package:gkt/data/repository/maps_repository_impl.dart';
import 'package:gkt/data/repository/user_repository_impl.dart';
import 'package:gkt/domain/data_source/local_data_source.dart';
import 'package:gkt/domain/repository/auth_repository.dart';
import 'package:gkt/domain/repository/maps_repository.dart';
import 'package:gkt/domain/repository/user_repository.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

import 'data/repository/main_repository_impl.dart';
import 'domain/repository/main_repository.dart';

final injector = GetIt.instance;

void initializeDependencies() {
  // initialize Navigator Key
  injector.registerLazySingleton(() => GlobalKey<NavigatorState>());

  // initialize API service
  final dio = Dio();
  if (kDebugMode) {
    final logger = PrettyDioLogger(
      requestHeader: true,
      requestBody: true,
      responseBody: true,
      responseHeader: false,
      error: true,
      compact: true,
      maxWidth: 180,
    );
    dio.interceptors.add(logger);
    dio.options.connectTimeout = 40000;
    dio.options.receiveTimeout = 70000;
  }
  injector.registerLazySingleton(() => dio
    ..transformer = FlutterTransformer()
    ..options.validateStatus = (int? status) {
      if (status == null) return false;
      if (status == 403) {
        return false;
      }
      return true; //(status >= 200 && status < 300) || (status >= 400 && status < 500);
    });

  injector
    ..registerLazySingleton<LocalDataSource>(() => const StorageService())
    ..registerLazySingleton<ApiService>(
      () => ApiService(injector()),
    );

  injector
    ..registerLazySingleton<UserRepository>(
      () => UserRepositoryImpl(injector(), injector()),
    )
    ..registerLazySingleton<AuthRepository>(
      () => AuthRepositoryImpl(injector(), injector()),
    )
    ..registerLazySingleton<MainRepository>(
      () => MainRepositoryImpl(injector(), injector()),
    )
    ..registerLazySingleton<MapsRepository>(
      () => MapsRepositoryImpl(),
    );
}

DATE_NOW:=`date +%Y%m%d.%H%M`
VERSION:=`cider version`
APP_NAME:=gkt
PATH_APK:=`PWD`/build/app/outputs/flutter-apk
PATH_APP_BUNDLE:=`PWD`/build/app/outputs/bundle/release

generate:
	flutter pub run build_runner build --delete-conflicting-outputs
get:
	flutter pub get
clean:
	flutter clean
apk:
	flutter build apk --split-per-abi --target-platform=android-arm --release
	mv $(PATH_APK)/app-armeabi-v7a-release.apk $(PATH_APK)/$(APP_NAME)-v$(VERSION)-$(DATE_NOW).apk